﻿using PCLS.DataAccess.Authentication;
using System;
using System.Data;

namespace PCLS.Bussiness.Authentication
{
    public class DomainBLL
    {
        public DataTable DomainSelectBLL(string Condition)
        {
            try
            {
                return DomainDAL.Instance.DomainSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static DomainBLL _instance;
        public static DomainBLL Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DomainBLL();
                }
                return _instance;
            }
        }
        #endregion
    }
}