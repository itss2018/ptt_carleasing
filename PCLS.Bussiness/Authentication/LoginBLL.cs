﻿using PCLS.DataAccess.Authentication;
using System;
using System.Data;

namespace PCLS.Bussiness.Authentication
{
    public class LoginBLL
    {
        public DataTable CountLoginBLL(string UserName)
        {
            try
            {
                return LoginDAL.Instance.CountLoginDAL(UserName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void ClearLoginBLL(string UserName)
        {
            try
            {
                LoginDAL.Instance.ClearLoginDAL(UserName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static LoginBLL _instance;
        public static LoginBLL Instance
        {
            get
            {
                return _instance = new LoginBLL();
            }
        }
        #endregion
    }
}