﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class BrandBLL
    {
        public DataTable BrandSelectBLL(string Condition)
        {
            try
            {
                return BrandDAL.Instance.BrandSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static BrandBLL _instance;
        public static BrandBLL Instance
        {
            get
            {
                _instance = new BrandBLL();
                return _instance;
            }
        }
        #endregion
    }
}