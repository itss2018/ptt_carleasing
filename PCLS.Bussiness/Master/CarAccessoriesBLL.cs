﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarAccessoriesBLL
    {
        public DataTable CarAccessoriesSelectBLL(string Condition)
        {
            try
            {
                return CarAccessoriesDAL.Instance.CarAccessoriesSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarAccessoriesAddBLL(int CarAccessoriesID, string CarAccessoriesName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarAccessoriesDAL.Instance.CarAccessoriesAddDAL(CarAccessoriesID, CarAccessoriesName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarAccessoriesBLL _instance;
        public static CarAccessoriesBLL Instance
        {
            get
            {
                _instance = new CarAccessoriesBLL();
                return _instance;
            }
        }
        #endregion
    }
}