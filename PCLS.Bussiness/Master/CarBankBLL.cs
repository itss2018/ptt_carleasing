﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarBankBLL
    {
        public DataTable CarBankSelectBLL(string Condition)
        {
            try
            {
                return CarBankDAL.Instance.CarBankSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarBankAddBLL(int CarBankID, string CarBankName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarBankDAL.Instance.CarBankAddDAL(CarBankID, CarBankName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarBankBLL _instance;
        public static CarBankBLL Instance
        {
            get
            {
                _instance = new CarBankBLL();
                return _instance;
            }
        }
        #endregion
    }
}