﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarBrandBLL
    {
        public DataTable CarBrandSelectBLL(string Condition)
        {
            try
            {
                return CarBrandDAL.Instance.CarBrandSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarBrandAddBLL(int CarBrandID, string CarBrandName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarBrandDAL.Instance.CarBrandAddDAL(CarBrandID, CarBrandName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarBrandBLL _instance;
        public static CarBrandBLL Instance
        {
            get
            {
                _instance = new CarBrandBLL();
                return _instance;
            }
        }
        #endregion
    }
}