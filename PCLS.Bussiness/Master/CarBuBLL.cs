﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarBuBLL
    {
        public DataTable CarBuSelectBLL(string Condition)
        {
            try
            {
                return CarBuDAL.Instance.CarBuSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarBuAddBLL(string CarBuType, string CarBuCode, string CarBuCodename, string CarBuCodeValue, int CarBuCodeLen, int CarBuNameLen, string CarBuValidStatus, string CarBuSetBy, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarBuDAL.Instance.CarBuAddDAL(CarBuType, CarBuCode, CarBuCodename, CarBuCodeValue, CarBuCodeLen, CarBuNameLen, CarBuValidStatus, CarBuSetBy, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarBuBLL _instance;
        public static CarBuBLL Instance
        {
            get
            {
                _instance = new CarBuBLL();
                return _instance;
            }
        }
        #endregion
    }
}