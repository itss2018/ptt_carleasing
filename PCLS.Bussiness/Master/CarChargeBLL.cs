﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarChargeBLL
    {
        public DataTable CarChargeSelectBLL(string Condition)
        {
            try
            {
                return CarChargeDAL.Instance.CarChargeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarChargeAddBLL(int CarChargeID, string CarChargeName, int CarTypeTrafficTicketID, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarChargeDAL.Instance.CarChargeAddDAL(CarChargeID, CarChargeName, CarTypeTrafficTicketID, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarChargeBLL _instance;
        public static CarChargeBLL Instance
        {
            get
            {
                _instance = new CarChargeBLL();
                return _instance;
            }
        }
        #endregion
    }
}