﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarColorBLL
    {
        public DataTable CarColorSelectBLL(string Condition)
        {
            try
            {
                return CarColorDAL.Instance.CarColorSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarColorAddBLL(int CarColorID, string CarColorName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarColorDAL.Instance.CarColorAddDAL(CarColorID, CarColorName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarColorBLL _instance;
        public static CarColorBLL Instance
        {
            get
            {
                _instance = new CarColorBLL();
                return _instance;
            }
        }
        #endregion
    }
}