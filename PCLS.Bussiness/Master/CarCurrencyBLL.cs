﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarCurrencyBLL
    {
        public DataTable CarCurrencySelectBLL(string Condition)
        {
            try
            {
                return CarCurrencyDAL.Instance.CarCurrencySelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarCurrencyAddBLL(string CarCurrencyType, string CarCurrencyCode, string CarCurrencyCodename, string CarCurrencyCodeValue, int CarCurrencyCodeLen, int CarCurrencyNameLen, string CarCurrencyValidStatus, string CarCurrencySetBy, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarCurrencyDAL.Instance.CarCurrencyAddDAL(CarCurrencyType, CarCurrencyCode, CarCurrencyCodename, CarCurrencyCodeValue, CarCurrencyCodeLen, CarCurrencyNameLen, CarCurrencyValidStatus, CarCurrencySetBy, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarCurrencyBLL _instance;
        public static CarCurrencyBLL Instance
        {
            get
            {
                _instance = new CarCurrencyBLL();
                return _instance;
            }
        }
        #endregion
    }
}