﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarDepartmentBLL
    {
        public DataTable CarDepartmentSelectBLL(string Condition)
        {
            try
            {
                return CarDepartmentDAL.Instance.CarDepartmentSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarDepartmentAddBLL(string ORG_ID, string ORG_NAME, string ORG_ABBV, string PARENT_ID, DateTime VALID_FROM, DateTime VALID_TO, string ROOT_FLAG, int ORG_LEVEL, string BU, string CUST_CODE, string ORG_STRUCTURE, string COSTCENTER, string BA, string IO_ID, string REMARK, string ISVALID, string CCTR_NEW, string CREATEBY, int cAction)
        {
            try
            {
                CarDepartmentDAL.Instance.CarDepartmentAddDAL(ORG_ID, ORG_NAME, ORG_ABBV, PARENT_ID, VALID_FROM, VALID_TO, ROOT_FLAG, ORG_LEVEL, BU, CUST_CODE, ORG_STRUCTURE, COSTCENTER, BA, IO_ID, REMARK, ISVALID, CCTR_NEW, CREATEBY, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarDepartmentBLL _instance;
        public static CarDepartmentBLL Instance
        {
            get
            {
                _instance = new CarDepartmentBLL();
                return _instance;
            }
        }
        #endregion
    }
}