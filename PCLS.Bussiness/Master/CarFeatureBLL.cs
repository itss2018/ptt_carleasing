﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarFeatureBLL
    {
        public DataTable CarFeatureSelectBLL(string Condition)
        {
            try
            {
                return CarFeatureDAL.Instance.CarFeatureSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarFeatureAddBLL(int CarFeatureID, string CarFeatureCode, string CarFeatureName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarFeatureDAL.Instance.CarFeatureAddDAL(CarFeatureID, CarFeatureCode, CarFeatureName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarFeatureBLL _instance;
        public static CarFeatureBLL Instance
        {
            get
            {
                _instance = new CarFeatureBLL();
                return _instance;
            }
        }
        #endregion
    }
}