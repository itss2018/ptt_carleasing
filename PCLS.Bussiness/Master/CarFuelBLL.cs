﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarFuelBLL
    {
        public DataTable CarFuelSelectBLL(string Condition)
        {
            try
            {
                return CarFuelDAL.Instance.CarFuelSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarFuelAddBLL(int CarFuelID, string CarFuelName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarFuelDAL.Instance.CarFuelAddDAL(CarFuelID, CarFuelName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarFuelBLL _instance;
        public static CarFuelBLL Instance
        {
            get
            {
                _instance = new CarFuelBLL();
                return _instance;
            }
        }
        #endregion
    }
}