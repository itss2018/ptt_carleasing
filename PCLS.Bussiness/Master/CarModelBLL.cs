﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarModelBLL
    {
        public DataTable CarModelSelectBLL(string Condition)
        {
            try
            {
                return CarModelDAL.Instance.CarModelSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarModelAddBLL(int CarModelID, int CarBrandID, string CarModelName, string CarTypeName, string CarEngineSize, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarModelDAL.Instance.CarModelAddDAL(CarModelID, CarBrandID, CarModelName, CarTypeName, CarEngineSize, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarModelBLL _instance;
        public static CarModelBLL Instance
        {
            get
            {
                _instance = new CarModelBLL();
                return _instance;
            }
        }
        #endregion
    }
}