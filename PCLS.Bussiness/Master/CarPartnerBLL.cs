﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarPartnerBLL
    {
        public DataTable CarPartnerSelectBLL(string Condition)
        {
            try
            {
                return CarPartnerDAL.Instance.CarPartnerSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarPartnerAddBLL(string CarPartnerID, string CarPartnerName, string CarPartnerType, int VendorID, int CustomerCode, string Vendor, string TaxID, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarPartnerDAL.Instance.CarPartnerAddDAL(CarPartnerID, CarPartnerName, CarPartnerType, VendorID, CustomerCode, Vendor, TaxID, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarPartnerBLL _instance;
        public static CarPartnerBLL Instance
        {
            get
            {
                _instance = new CarPartnerBLL();
                return _instance;
            }
        }
        #endregion
    }
}