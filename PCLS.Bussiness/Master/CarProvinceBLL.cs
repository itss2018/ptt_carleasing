﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarProvinceBLL
    {
        public DataTable CarProvinceSelectBLL(string Condition)
        {
            try
            {
                return CarProvinceDAL.Instance.CarProvinceSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarProvinceAddBLL(string CarProvinceType, string CarProvinceCode, string CarProvinceCodename, string CarProvinceCodeValue, int CarProvinceCodeLen, int CarProvinceNameLen, string CarProvinceValidStatus, string CarProvinceSetBy, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarProvinceDAL.Instance.CarProvinceAddDAL(CarProvinceType, CarProvinceCode, CarProvinceCodename, CarProvinceCodeValue, CarProvinceCodeLen, CarProvinceNameLen, CarProvinceValidStatus, CarProvinceSetBy, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarProvinceBLL _instance;
        public static CarProvinceBLL Instance
        {
            get
            {
                _instance = new CarProvinceBLL();
                return _instance;
            }
        }
        #endregion
    }
}