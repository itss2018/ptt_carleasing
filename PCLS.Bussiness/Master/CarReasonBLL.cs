﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarReasonBLL
    {
        public DataTable CarReasonSelectBLL(string Condition)
        {
            try
            {
                return CarReasonDAL.Instance.CarReasonSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarReasonAddBLL(int CarReasonID, string CarReasonName, string CarReasonType, string CarReasonForm, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarReasonDAL.Instance.CarReasonAddDAL(CarReasonID, CarReasonName, CarReasonType, CarReasonForm, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarReasonBLL _instance;
        public static CarReasonBLL Instance
        {
            get
            {
                _instance = new CarReasonBLL();
                return _instance;
            }
        }
        #endregion
    }
}