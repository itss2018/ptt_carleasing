﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarStatusBLL
    {
        public DataTable CarStatusSelectBLL(string Condition)
        {
            //
            try
            {
                return CarStatusDAL.Instance.CarStatusSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarStatusAddBLL(string CarStatusType, string CarStatusCode, string CarStatusCodename, string CarStatusCodeValue, int CarStatusCodeLen, int CarStatusNameLen, string CarStatusValidStatus, string CarStatusSetBy, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarStatusDAL.Instance.CarStatusAddDAL(CarStatusType, CarStatusCode, CarStatusCodename, CarStatusCodeValue, CarStatusCodeLen, CarStatusNameLen, CarStatusValidStatus, CarStatusSetBy, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarStatusBLL _instance;
        public static CarStatusBLL Instance
        {
            get
            {
                _instance = new CarStatusBLL();
                return _instance;
            }
        }
        #endregion
    }
}