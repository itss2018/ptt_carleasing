﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeBLL
    {
        public DataTable CarTypeSelectBLL(string Condition)
        {
            try
            {
                return CarTypeDAL.Instance.CarTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeAddBLL(int CarTypeID, string CarTypeName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeDAL.Instance.CarTypeAddDAL(CarTypeID, CarTypeName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeBLL _instance;
        public static CarTypeBLL Instance
        {
            get
            {
                _instance = new CarTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}