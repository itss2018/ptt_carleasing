﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeCardBLL
    {
        public DataTable CarTypeCardSelectBLL(string Condition)
        {
            try
            {
                return CarTypeCardDAL.Instance.CarTypeCardSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeCardAddBLL(int CarTypeCardID, string CarTypeCardName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeCardDAL.Instance.CarTypeCardAddDAL(CarTypeCardID, CarTypeCardName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeCardBLL _instance;
        public static CarTypeCardBLL Instance
        {
            get
            {
                _instance = new CarTypeCardBLL();
                return _instance;
            }
        }
        #endregion
    }
}