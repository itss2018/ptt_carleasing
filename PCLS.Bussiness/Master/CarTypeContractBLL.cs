﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeContractBLL
    {
        public DataTable CarTypeContractSelectBLL(string Condition)
        {
            try
            {
                return CarTypeContractDAL.Instance.CarTypeContractSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeContractAddBLL(int CarTypeContractID, string CarTypeContractName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeContractDAL.Instance.CarTypeContractAddDAL(CarTypeContractID, CarTypeContractName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeContractBLL _instance;
        public static CarTypeContractBLL Instance
        {
            get
            {
                _instance = new CarTypeContractBLL();
                return _instance;
            }
        }
        #endregion
    }
}