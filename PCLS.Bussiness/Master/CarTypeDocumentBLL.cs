﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeDocumentBLL
    {
        public DataTable CarTypeDocumentSelectBLL(string Condition)
        {
            try
            {
                return CarTypeDocumentDAL.Instance.CarTypeDocumentSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeDocumentAddBLL(int CarTypeDocumentID, string CarTypeDocumentName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeDocumentDAL.Instance.CarTypeDocumentAddDAL(CarTypeDocumentID, CarTypeDocumentName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeDocumentBLL _instance;
        public static CarTypeDocumentBLL Instance
        {
            get
            {
                _instance = new CarTypeDocumentBLL();
                return _instance;
            }
        }
        #endregion
    }
}