﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarEngineBLL
    {
        public DataTable CarEngineSelectBLL(string Condition)
        {
            try
            {
                return CarEngineDAL.Instance.CarEngineSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarEngineAddBLL(int CarEngineID, string CarEngineName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarEngineDAL.Instance.CarEngineAddDAL(CarEngineID, CarEngineName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarEngineBLL _instance;
        public static CarEngineBLL Instance
        {
            get
            {
                _instance = new CarEngineBLL();
                return _instance;
            }
        }
        #endregion
    }
}