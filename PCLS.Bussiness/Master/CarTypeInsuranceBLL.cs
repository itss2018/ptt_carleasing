﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeInsuranceBLL
    {
        public DataTable CarTypeInsuranceSelectBLL(string Condition)
        {
            try
            {
                return CarTypeInsuranceDAL.Instance.CarTypeInsuranceSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeInsuranceAddBLL(int CarTypeInsuranceID, string CarTypeInsuranceName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeInsuranceDAL.Instance.CarTypeInsuranceAddDAL(CarTypeInsuranceID, CarTypeInsuranceName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeInsuranceBLL _instance;
        public static CarTypeInsuranceBLL Instance
        {
            get
            {
                _instance = new CarTypeInsuranceBLL();
                return _instance;
            }
        }
        #endregion
    }
}