﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeLeasingBLL
    {
        public DataTable CarTypeLeasingSelectBLL(string Condition)
        {
            try
            {
                return CarTypeLeasingDAL.Instance.CarTypeLeasingSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeLeasingAddBLL(string CarTypeLeasingType, string CarTypeLeasingCode, string CarTypeLeasingCodename, string CarTypeLeasingCodeValue, int CarTypeLeasingCodeLen, int CarTypeLeasingNameLen, string CarTypeLeasingValidStatus, string CarTypeLeasingSetBy, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeLeasingDAL.Instance.CarTypeLeasingAddDAL(CarTypeLeasingType, CarTypeLeasingCode, CarTypeLeasingCodename, CarTypeLeasingCodeValue, CarTypeLeasingCodeLen, CarTypeLeasingNameLen, CarTypeLeasingValidStatus, CarTypeLeasingSetBy, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeLeasingBLL _instance;
        public static CarTypeLeasingBLL Instance
        {
            get
            {
                _instance = new CarTypeLeasingBLL();
                return _instance;
            }
        }
        #endregion
    }
}