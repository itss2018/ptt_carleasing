﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypePayBLL
    {
        public DataTable CarTypePaySelectBLL(string Condition)
        {
            try
            {
                return CarTypePayDAL.Instance.CarTypePaySelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypePayAddBLL(string CarTypePayType, string CarTypePayCode, string CarTypePayCodename, string CarTypePayCodeValue, int CarTypePayCodeLen, int CarTypePayNameLen, string CarTypePayValidStatus, string CarTypePaySetBy, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypePayDAL.Instance.CarTypePayAddDAL(CarTypePayType, CarTypePayCode, CarTypePayCodename, CarTypePayCodeValue, CarTypePayCodeLen, CarTypePayNameLen, CarTypePayValidStatus, CarTypePaySetBy, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypePayBLL _instance;
        public static CarTypePayBLL Instance
        {
            get
            {
                _instance = new CarTypePayBLL();
                return _instance;
            }
        }
        #endregion
    }
}