﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypePurchaseBLL
    {
        public DataTable CarTypePurchaseSelectBLL(string Condition)
        {
            try
            {
                return CarTypePurchaseDAL.Instance.CarTypePurchaseSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypePurchaseAddBLL(int CarTypePurchaseID, string CarTypePurchaseName, int CarTypeSaleID, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypePurchaseDAL.Instance.CarTypePurchaseAddDAL(CarTypePurchaseID, CarTypePurchaseName, CarTypeSaleID, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypePurchaseBLL _instance;
        public static CarTypePurchaseBLL Instance
        {
            get
            {
                _instance = new CarTypePurchaseBLL();
                return _instance;
            }
        }
        #endregion
    }
}