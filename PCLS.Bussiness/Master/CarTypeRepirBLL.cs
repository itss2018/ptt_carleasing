﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeRepairBLL
    {
        public DataTable CarTypeRepairSelectBLL(string Condition)
        {
            try
            {
               //
                return CarTypeRepairDAL.Instance.CarTypeRepairSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeRepairAddBLL(string CarTypeRepairType, string CarTypeRepairCode, string CarTypeRepairCodename, string CarTypeRepairCodeValue, int CarTypeRepairCodeLen, int CarTypeRepairNameLen, string CarTypeRepairValidStatus, string CarTypeRepairSetBy, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeRepairDAL.Instance.CarTypeRepairAddDAL(CarTypeRepairType, CarTypeRepairCode, CarTypeRepairCodename, CarTypeRepairCodeValue, CarTypeRepairCodeLen, CarTypeRepairNameLen, CarTypeRepairValidStatus, CarTypeRepairSetBy, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeRepairBLL _instance;
        public static CarTypeRepairBLL Instance
        {
            get
            {
                _instance = new CarTypeRepairBLL();
                return _instance;
            }
        }
        #endregion
    }
}