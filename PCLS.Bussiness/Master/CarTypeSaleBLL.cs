﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeSaleBLL
    {
        public DataTable CarTypeSaleSelectBLL(string Condition)
        {
            try
            {
                return CarTypeSaleDAL.Instance.CarTypeSaleSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeSaleAddBLL(int CarTypeSaleID, string CarTypeSaleName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeSaleDAL.Instance.CarTypeSaleAddDAL(CarTypeSaleID, CarTypeSaleName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeSaleBLL _instance;
        public static CarTypeSaleBLL Instance
        {
            get
            {
                _instance = new CarTypeSaleBLL();
                return _instance;
            }
        }
        #endregion
    }
}