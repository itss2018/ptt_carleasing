﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeSupplyBLL
    {
        public DataTable CarTypeSupplySelectBLL(string Condition)
        {
            try
            {
                return CarTypeSupplyDAL.Instance.CarTypeSupplySelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeSupplyAddBLL(string CarTypeSupplyType, string CarTypeSupplyCode, string CarTypeSupplyCodename, string CarTypeSupplyCodeValue, int CarTypeSupplyCodeLen, int CarTypeSupplyNameLen, string CarTypeSupplyValidStatus, string CarTypeSupplySetBy, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeSupplyDAL.Instance.CarTypeSupplyAddDAL(CarTypeSupplyType, CarTypeSupplyCode, CarTypeSupplyCodename, CarTypeSupplyCodeValue, CarTypeSupplyCodeLen, CarTypeSupplyNameLen, CarTypeSupplyValidStatus, CarTypeSupplySetBy, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeSupplyBLL _instance;
        public static CarTypeSupplyBLL Instance
        {
            get
            {
                _instance = new CarTypeSupplyBLL();
                return _instance;
            }
        }
        #endregion
    }
}