﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CarTypeTrafficTicketBLL
    {
        public DataTable CarTypeTrafficTicketSelectBLL(string Condition)
        {
            try
            {
                return CarTypeTrafficTicketDAL.Instance.CarTypeTrafficTicketSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void CarTypeTrafficTicketAddBLL(int CarTypeTrafficTicketID, string CarTypeTrafficTicketName, string IsActive, string CreateBy, int cAction)
        {
            try
            {
                CarTypeTrafficTicketDAL.Instance.CarTypeTrafficTicketAddDAL(CarTypeTrafficTicketID, CarTypeTrafficTicketName, IsActive, CreateBy, cAction);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeTrafficTicketBLL _instance;
        public static CarTypeTrafficTicketBLL Instance
        {
            get
            {
                _instance = new CarTypeTrafficTicketBLL();
                return _instance;
            }
        }
        #endregion
    }
}