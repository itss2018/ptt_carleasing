﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class CheckListBLL
    {
        public DataTable CheckListSelectBLL(int CheckTypeID, int TruckSysID)
        {
            try
            {
                return CheckListDAL.Instance.CheckListSelectDAL(CheckTypeID, TruckSysID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckListSelectBLL2(int CheckTypeID, int TruckSysID, string Subject, string Status)
        {
            try
            {
                return CheckListDAL.Instance.CheckListSelectDAL2(CheckTypeID, TruckSysID, Subject, Status);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckListTypeSelectBLL()
        {
            try
            {
                return CheckListDAL.Instance.CheckListTypeSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckTruckSysBLL(int Level, int LevelID)
        {
            try
            {
                return CheckListDAL.Instance.CheckTruckSysDAL(Level, LevelID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetSubjectLevel1BLL(int ChkTypeID)
        {
            try
            {
                return CheckListDAL.Instance.GetSubjectLevel1DAL(ChkTypeID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetSubjectLevel2and3BLL(int Level, int LevelID)
        {
            try
            {
                return CheckListDAL.Instance.GetSubjectLevel2and3DAL(Level, LevelID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetSubjectByLevelIDBLL(int Level, int LevelID)
        {
            try
            {
                return CheckListDAL.Instance.GetSubjectByLevelIDDAL(Level, LevelID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTruckSystemByLevelIDBLL(int Level, int LevelID)
        {
            try
            {
                return CheckListDAL.Instance.GetTruckSystemByLevelIDDAL(Level, LevelID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CheckListBLL _instance;
        public static CheckListBLL Instance
        {
            get
            {
                _instance = new CheckListBLL();
                return _instance;
            }
        }
        #endregion
    }
}