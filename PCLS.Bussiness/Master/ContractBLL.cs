﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class ContractBLL
    {
        #region ContractNo
        #region ContractNoSave
        public DataTable ContractNoSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {

                return ContractDAL.Instance.ContractNoSave(I_ID,
                                          I_USERID,
                                          I_DATATABLE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractNoSelect
        public DataTable ContractNoSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                return ContractDAL.Instance.ContractNoSelect(I_SEARCH,
                                          I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractNoSelectByID
        public DataTable ContractNoSelectByID(string I_ID)
        {
            try
            {
                return ContractDAL.Instance.ContractNoSelectByID(I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractNoNotSelect
        public DataTable ContractNoNotSelect(string I_SEARCH, string I_IS_ACTIVE, string I_CONTRACT_NO_CODE)
        {
            try
            {
                return ContractDAL.Instance.ContractNoNotSelect(I_SEARCH, I_IS_ACTIVE, I_CONTRACT_NO_CODE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region Contract
        #region ContractSave
        public DataTable ContractSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataSet I_DATATABLE_T,
                                          DataSet I_DATATABLE_P,
                                          DataTable dtUpload
            )
        {
            try
            {

                return ContractDAL.Instance.ContractSave(I_ID,
                                          I_USERID,
                                          I_DATATABLE,
                                          I_DATATABLE_T,
                                          I_DATATABLE_P,
                                          dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSelect
        public DataTable ContractSelect(string I_VENDOR_CODE, string I_SEARCH,
                                          string I_CACTIVE,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {
                return ContractDAL.Instance.ContractSelect(I_VENDOR_CODE, I_SEARCH,
                                          I_CACTIVE,
                                          I_COLUMN_NAME,
                                          I_ASC);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSelectByID
        public DataSet ContractSelectByID(string I_ID)
        {
            try
            {
                return ContractDAL.Instance.ContractSelectByID(I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion

        #region ContractCategorySelect
        public DataTable ContractCategorySelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                return ContractDAL.Instance.ContractCategorySelect(I_SEARCH,
                                          I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractTypeSelect
        public DataTable ContractTypeSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                return ContractDAL.Instance.ContractTypeSelect(I_SEARCH,
                                          I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractEmploymentSelect
        public DataTable ContractEmploymentSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                return ContractDAL.Instance.ContractEmploymentSelect(I_SEARCH,
                                          I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSectorSelect
        public DataTable ContractSectorSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                return ContractDAL.Instance.ContractSectorSelect(I_SEARCH,
                                          I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static ContractBLL _instance;
        public static ContractBLL Instance
        {
            get
            {
                _instance = new ContractBLL();
                return _instance;
            }
        }
        #endregion
    }
}