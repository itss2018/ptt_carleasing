﻿using PCLS.DataAccess.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.Bussiness.Master
{
    public class ContractTruckBLL
    {
        #region + Instance +
        private static ContractTruckBLL _instance;
        public static ContractTruckBLL Instance
        {
            get
            {
                _instance = new ContractTruckBLL();
                return _instance;
            }
        }
        #endregion

        #region ContractTruckRequest
        #region ContractTruckRequestSave
        public DataTable ContractTruckRequestSave(int I_USERID,
                                          int I_SYSTEM_ID,
                                          DataSet I_DATATABLE,
                                          DataSet I_DATATABLE_TRUCK
            )
        {
            try
            {

                return ContractTruckDAL.Instance.ContractTruckRequestSave(
                                          I_USERID,
                                          I_SYSTEM_ID,
                                          I_DATATABLE,
                                          I_DATATABLE_TRUCK);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractTruckRequestSelect
        public DataTable ContractTruckRequestSelect(int I_SYSTEM_ID, string I_SEARCH,
                                          string I_VENDOR_CODE,
                                          string I_CONTRACT_ID,
                                          string I_REQUESTTYPE_CODE,
                                          string I_CACTIVE,
                                          string I_CONTRACT_DATE,
                                          string I_CONTRACT_DATE_TO,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractTruckRequestSelect(I_SYSTEM_ID, I_SEARCH,
                                          I_VENDOR_CODE,
                                          I_CONTRACT_ID,
                                          I_REQUESTTYPE_CODE,
                                          I_CACTIVE,
                                          I_CONTRACT_DATE,
                                          I_CONTRACT_DATE_TO,
                                          I_COLUMN_NAME,
                                          I_ASC);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractTruckRequestSelectByOrderID
        public DataTable ContractTruckRequestSelectByOrderID(
                                          int I_SYSTEM_ID, string I_ORDER_ID)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractTruckRequestSelectByOrderID(I_SYSTEM_ID, I_ORDER_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion

        #region ContractTypeTruckSelect
        public DataTable ContractTypeTruckSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractTypeTruckSelect(I_SEARCH, I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractRequestSelect
        public DataTable ContractRequestSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractRequestSelect(I_SEARCH, I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TruckSelectByContract
        public DataTable TruckSelectByContract(string I_CONTRACT_ID,
                                          int I_SYSTEM_ID, string I_VENDOR_CODE)
        {
            try
            {
                return ContractTruckDAL.Instance.TruckSelectByContract(I_CONTRACT_ID, I_SYSTEM_ID, I_VENDOR_CODE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractTruckCarCountCheck
        public DataTable ContractTruckCarCountCheck(string I_CONTRACT_ID,
                                          int I_SYSTEM_ID)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractTruckCarCountCheck(I_CONTRACT_ID, I_SYSTEM_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Interface SAP
        #region +TU+
        public DataSet ContractTruckAddRemoveTU(string I_CONTRACT_DATE, string I_ORDER_ID)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractTruckAddRemoveTU(I_CONTRACT_DATE, I_ORDER_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region +Veh+
        public DataTable ContractTruckAddRemoveVeh(string I_CONTRACT_DATE, string I_ORDER_ID)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractTruckAddRemoveVeh(I_CONTRACT_DATE, I_ORDER_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region +LPG Sotred+
        public DataTable ContractTruckAdd(string I_CONTRACT_DATE, string I_ORDER_ID, int? I_TRUCK_MAPPING_ID)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractTruckAdd(I_CONTRACT_DATE, I_ORDER_ID, I_TRUCK_MAPPING_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable ContractTruckRemove(string I_CONTRACT_DATE, string I_ORDER_ID, int? I_TRUCK_MAPPING_ID, int I_CONTRACT_ID_REMOVE)
        {
            try
            {
                return ContractTruckDAL.Instance.ContractTruckRemove(I_CONTRACT_DATE, I_ORDER_ID, I_TRUCK_MAPPING_ID, I_CONTRACT_ID_REMOVE);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion
    }
}
