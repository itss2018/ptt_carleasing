﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class DriverBLL
    {
        #region DriverRequest
        #region DriverRequestSelect
        public DataTable DriverRequestSelect(
                                          int I_SYSTEM_ID,
                                          string I_VENDOR_CODE,
                                          string I_SEARCH,
                                          string I_DATEFROM,
                                          string I_DATETO,
                                          string I_STATUS,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {

                return DriverDAL.Instance.DriverRequestSelect(I_SYSTEM_ID,
                                           I_VENDOR_CODE,
                                           I_SEARCH,
                                           I_DATEFROM,
                                           I_DATETO,
                                           I_STATUS,
                                           I_COLUMN_NAME,
                                           I_ASC);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverRequestSelectByID
        public DataTable DriverRequestSelectByID(int I_SYSTEM_ID, string I_ID)
        {
            try
            {

                return DriverDAL.Instance.DriverRequestSelectByID(I_SYSTEM_ID, I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverCheckCardNull
        public DataTable DriverCheckCardNull(string I_ID, string I_DATA)
        {
            try
            {
                return DriverDAL.Instance.DriverCheckCardNull(I_ID, I_DATA);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverUpdateCardByID
        public DataTable DriverUpdateCardByID(string I_ID)
        {
            try
            {

                return DriverDAL.Instance.DriverUpdateCardByID(I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverRequestSave
        public DataTable DriverRequestSave(string I_ID,
                                          int I_SYSTEM_ID,
                                          int I_USERID,
                                          string I_VENDOR_CODE,
                                          int I_STATUS_TYPE,
                                          int I_STATUS,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload,
                                          DataSet dsLog,
                                          DataSet dsLogDetail)
        {
            try
            {

                return DriverDAL.Instance.DriverRequestSave(I_ID,
                                           I_SYSTEM_ID,
                                           I_USERID,
                                           I_VENDOR_CODE,
                                           I_STATUS_TYPE,
                                           I_STATUS,
                                           I_DATATABLE,
                                           dtUpload,
                                          dsLog,
                                          dsLogDetail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverRequestStatusUpdate
        public DataTable DriverRequestStatusUpdate(string I_ID,
                                          int I_SYSTEM_ID,
                                          string I_REMARK,
                                          int I_STATUS_TYPE,
                                          int I_STATUS,
                                          int I_USERID,
                                          DataSet dsLog,
                                          DataSet dsLogDetail)
        {
            try
            {

                return DriverDAL.Instance.DriverRequestStatusUpdate(I_ID,
                                           I_SYSTEM_ID,
                                           I_REMARK,
                                           I_STATUS_TYPE,
                                           I_STATUS,
                                           I_USERID,
                                          dsLog,
                                          dsLogDetail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region Driver
        #region DriverSelect
        public DataTable DriverSelect(
                                          int I_SYSTEM_ID,
                                          string I_VENDOR_CODE,
                                          string I_SEARCH,
                                          string I_DATEFROM,
                                          string I_DATETO,
                                          string I_STATUS,
                                          string I_EMPLOY,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {

                return DriverDAL.Instance.DriverSelect(I_SYSTEM_ID,
                                           I_VENDOR_CODE,
                                           I_SEARCH,
                                           I_DATEFROM,
                                           I_DATETO,
                                           I_STATUS,
                                           I_EMPLOY,
                                           I_COLUMN_NAME,
                                           I_ASC);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public DataSet SelectLogInterfaceSapBLL(string Condition)
        {
            try
            {
                return DriverDAL.Instance.SelectLogInterfaceSapDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet SelectLogInterfaceSapTuBLL(string Condition)
        {
            try
            {
                return DriverDAL.Instance.SelectLogInterfaceSapTuDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet SelectLogInterfaceSapVehBLL(string Condition)
        {
            try
            {
                return DriverDAL.Instance.SelectLogInterfaceSapVehDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region DriverSelectExpire
        public DataTable DriverSelectExpire(
                                          int I_SYSTEM_ID,
                                          string I_VENDOR_CODE,
                                          string I_SEARCH,
                                          string I_DATEFROM,
                                          string I_DATETO,
                                          string I_EMPLOY,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {

                return DriverDAL.Instance.DriverSelectExpire(I_SYSTEM_ID,
                                           I_VENDOR_CODE,
                                           I_SEARCH,
                                           I_DATEFROM,
                                           I_DATETO,
                                           I_EMPLOY,
                                           I_COLUMN_NAME,
                                           I_ASC);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverSelectByID
        public DataTable DriverSelectByID(int I_SYSTEM_ID, string I_ID)
        {
            try
            {

                return DriverDAL.Instance.DriverSelectByID(I_SYSTEM_ID, I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverSelectByCardNo
        public DataTable DriverSelectByCardNo(
                                          int I_SYSTEM_ID,
                                          string I_CARD_NO
                                          , int I_VENDOR_ID
                                          )
        {
            try
            {

                return DriverDAL.Instance.DriverSelectByCardNo(I_SYSTEM_ID,
                                           I_CARD_NO, I_VENDOR_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverRequestSelectByCardNo
        public DataTable DriverRequestSelectByCardNo(
                                          int I_SYSTEM_ID,
                                          string I_CARD_NO
                                          )
        {
            try
            {

                return DriverDAL.Instance.DriverRequestSelectByCardNo(I_SYSTEM_ID,
                                           I_CARD_NO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverSave
        public DataTable DriverSave(string I_ID,
                                          string I_REQUEST_ID,
                                          int I_SYSTEM_ID,
                                          int I_USERID,
                                          string I_VENDOR_CODE,
                                          string I_DRIVER_CODE_SAP,
                                          string I_STATUS,
                                          int I_STATUS_REQUEST,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload,
                                          DataSet dsLog,
                                          DataSet dsLogDetail)
        {
            try
            {

                return DriverDAL.Instance.DriverSave(I_ID,
                                           I_REQUEST_ID,
                                           I_SYSTEM_ID,
                                           I_USERID,
                                           I_VENDOR_CODE,
                                           I_DRIVER_CODE_SAP,
                                           I_STATUS,
                                           I_STATUS_REQUEST,
                                           I_DATATABLE,
                                           dtUpload,
                                           dsLog,
                                           dsLogDetail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverEmployUpdate
        public DataTable DriverEmployUpdate(string I_ID,
                                          int I_SYSTEM_ID,
                                          int I_USERID,
                                          DataSet dsLog,
                                          DataSet dsLogDetail
                                          )
        {
            try
            {

                return DriverDAL.Instance.DriverEmployUpdate(I_ID,
                                          I_SYSTEM_ID,
                                          I_USERID,
                                          dsLog,
                                          dsLogDetail);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region DriverHistory
        #region DriverHistorySelect
        public DataSet DriverHistorySelect(
                                          string I_CARD_NO)
        {
            try
            {
                return DriverDAL.Instance.DriverHistorySelect(I_CARD_NO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region + Instance +
        private static DriverBLL _instance;
        public static DriverBLL Instance
        {
            get
            {
                _instance = new DriverBLL();
                return _instance;
            }
        }
        #endregion
    }
}
