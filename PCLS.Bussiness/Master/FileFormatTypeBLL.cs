﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class FileFormatTypeBLL
    {
        public DataTable FileFormatTypeSelectBLL(string Condition)
        {
            try
            {
                return FileFormatTypeDAL.Instance.FileFormatTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static FileFormatTypeBLL _instance;
        public static FileFormatTypeBLL Instance
        {
            get
            {
                _instance = new FileFormatTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}