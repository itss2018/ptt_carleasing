﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class FillTypeBLL
    {
        public DataTable FillTypeSelectBLL(string Condition)
        {
            try
            {
                return FillTypeDAL.Instance.FillTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static FillTypeBLL _instance;
        public static FillTypeBLL Instance
        {
            get
            {
                _instance = new FillTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}