﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class FormulaBLL
    {
        public DataTable FormulaSelectBLL(string Condition)
        {
            try
            {
                return FormulaDAL.Instance.FormulaSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void FormulaAddBLL(int FormulaID, string FormulaName, string Remark, string IsActive, int CreateBy)
        {
            try
            {
                FormulaDAL.Instance.FormulaAddDAL(FormulaID, FormulaName, Remark, IsActive, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static FormulaBLL _instance;
        public static FormulaBLL Instance
        {
            get
            {
                _instance = new FormulaBLL();
                return _instance;
            }
        }
        #endregion
    }
}