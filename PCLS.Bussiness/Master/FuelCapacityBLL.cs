﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class FuelCapacityBLL
    {
        public DataTable FuelCapacitySelectBLL(string Condition)
        {
            try
            {
                return FuelCapacityDAL.Instance.FuelCapacitySelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static FuelCapacityBLL _instance;
        public static FuelCapacityBLL Instance
        {
            get
            {
                _instance = new FuelCapacityBLL();
                return _instance;
            }
        }
        #endregion
    }
}