﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class FuelTypeBLL
    {
        public DataTable FuelTypeSelectBLL(string Condition)
        {
            try
            {
                return FuelTypeDAL.Instance.FuelTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static FuelTypeBLL _instance;
        public static FuelTypeBLL Instance
        {
            get
            {
                _instance = new FuelTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}