﻿using PCLS.DataAccess.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.Bussiness.Master
{
    public class GroupBLL
    {
        #region + Instance +
        private static GroupBLL _instance;
        public static GroupBLL Instance
        {
            get
            {
                _instance = new GroupBLL();
                return _instance;
            }
        }
        #endregion

        #region Group
        #region GroupSave
        public DataTable GroupSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {

                return GroupDAL.Instance.GroupSave(I_ID,
                                          I_USERID,
                                          I_DATATABLE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupSelect
        public DataTable GroupSelect(string I_SEARCH,
                                          string I_CACTIVE, string I_GROUP_WORK_CODE)
        {
            try
            {
                return GroupDAL.Instance.GroupSelect(I_SEARCH,
                                          I_CACTIVE, I_GROUP_WORK_CODE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupSelectByID
        public DataTable GroupSelectByID(string I_ID)
        {
            try
            {
                return GroupDAL.Instance.GroupSelectByID(I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region GroupWork
        #region GroupWorkSave
        public DataTable GroupWorkSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {

                return GroupDAL.Instance.GroupWorkSave(I_ID,
                                          I_USERID,
                                          I_DATATABLE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupWorkSelect
        public DataTable GroupWorkSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                return GroupDAL.Instance.GroupWorkSelect(I_SEARCH,
                                          I_CACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupWorkSelectByID
        public DataTable GroupWorkSelectByID(string I_ID)
        {
            try
            {
                return GroupDAL.Instance.GroupWorkSelectByID(I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region GroupBind
        #region GroupBindSave
        public DataTable GroupBindSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {

                return GroupDAL.Instance.GroupBindSave(I_ID,
                                          I_USERID,
                                          I_DATATABLE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupBindCancel
        public DataTable GroupBindCancel(
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {

                return GroupDAL.Instance.GroupBindCancel(
                                          I_USERID,
                                          I_DATATABLE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupBindSelect
        public DataTable GroupBindSelect(string I_GROUPWORK,
                                          string I_GROUP)
        {
            try
            {
                return GroupDAL.Instance.GroupBindSelect(I_GROUPWORK,
                                          I_GROUP);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupBindSelectByID
        public DataTable GroupBindSelectByID(string I_ID)
        {
            try
            {
                return GroupDAL.Instance.GroupBindSelectByID(I_ID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
    }
}
