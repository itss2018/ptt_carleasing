﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class InletBLL
    {
        public DataTable InletSelectBLL(string Condition)
        {
            try
            {
                return InletDAL.Instance.InletSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static InletBLL _instance;
        public static InletBLL Instance
        {
            get
            {
                _instance = new InletBLL();
                return _instance;
            }
        }
        #endregion
    }
}