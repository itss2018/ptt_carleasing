﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class InletLevelBLL
    {
        public DataTable InletLevelSelectBLL(string Condition)
        {
            try
            {
                return InletLevelDAL.Instance.InletLevelSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static InletLevelBLL _instance;
        public static InletLevelBLL Instance
        {
            get
            {
                _instance = new InletLevelBLL();
                return _instance;
            }
        }
        #endregion
    }
}