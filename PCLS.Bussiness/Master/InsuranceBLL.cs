﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class InsuranceBLL
    {
        public DataTable InsuranceSelectBLL(string Condition)
        {
            try
            {
                return InsuranceDAL.Instance.InsuranceSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static InsuranceBLL _instance;
        public static InsuranceBLL Instance
        {
            get
            {
                _instance = new InsuranceBLL();
                return _instance;
            }
        }
        #endregion
    }
}