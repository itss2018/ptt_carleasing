﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class InsuranceTypeBLL
    {
        public DataTable InsuranceTypeSelectBLL(string Condition)
        {
            try
            {
                return InsuranceTypeDAL.Instance.InsuranceTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static InsuranceTypeBLL _instance;
        public static InsuranceTypeBLL Instance
        {
            get
            {
                _instance = new InsuranceTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}