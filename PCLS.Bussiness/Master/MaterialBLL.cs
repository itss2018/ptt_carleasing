﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class MaterialBLL
    {
        public DataTable MaterialSelectBLL(string Condition)
        {
            try
            {
                return MaterialDAL.Instance.MaterialSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void MaterialAddBLL(int MaterialID, int CustomerID, string MaterialName, string ShortName, int MaterialTypeID, string IsActive, int CreateBy, DataTable dtFormula)
        {
            try
            {
                MaterialDAL.Instance.MaterialAddDAL(MaterialID, CustomerID, MaterialName, ShortName, MaterialTypeID, IsActive, CreateBy, dtFormula);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static MaterialBLL _instance;
        public static MaterialBLL Instance
        {
            get
            {
                _instance = new MaterialBLL();
                return _instance;
            }
        }
        #endregion
    }
}