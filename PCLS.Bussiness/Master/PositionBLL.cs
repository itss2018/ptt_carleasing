﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class PositionBLL
    {
        public DataTable PositionSelectBLL(string Condition)
        {
            try
            {
                return PositionDAL.Instance.PositionSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void PositionAddBLL(int PositionID, string PositionName, string IsActive, int CreateBy)
        {
            try
            {
                PositionDAL.Instance.PositionAddDAL(PositionID, PositionName, IsActive, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static PositionBLL _instance;
        public static PositionBLL Instance
        {
            get
            {
                _instance = new PositionBLL();
                return _instance;
            }
        }
        #endregion
    }
}