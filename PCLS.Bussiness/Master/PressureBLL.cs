﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class PressureBLL
    {
        public DataTable PressureSelectBLL(string Condition)
        {
            try
            {
                return PressureDAL.Instance.PressureSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static PressureBLL _instance;
        public static PressureBLL Instance
        {
            get
            {
                _instance = new PressureBLL();
                return _instance;
            }
        }
        #endregion
    }
}