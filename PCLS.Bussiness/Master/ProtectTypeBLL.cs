﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class ProtectTypeBLL
    {
        public DataTable ProtectTypeSelectBLL(string Condition)
        {
            try
            {
                return ProtectTypeDAL.Instance.ProtectTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static ProtectTypeBLL _instance;
        public static ProtectTypeBLL Instance
        {
            get
            {
                _instance = new ProtectTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}