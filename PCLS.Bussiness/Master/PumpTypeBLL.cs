﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class PumpTypeBLL
    {
        public DataTable PumpTypeSelectBLL(string Condition)
        {
            try
            {
                return PumpTypeDAL.Instance.PumpTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static PumpTypeBLL _instance;
        public static PumpTypeBLL Instance
        {
            get
            {
                _instance = new PumpTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}