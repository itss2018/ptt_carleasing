﻿using PCLS.DataAccess.Master;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.Bussiness.Master
{
    public class RequireBLL
    {
        #region FieldSelect
        public DataTable FieldSelect(string I_FIELD_TYPE)
        {
            try
            {
                return RequireDAL.Instance.FieldSelect(I_FIELD_TYPE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireFieldSelect
        public DataTable RequireFieldSelect(string I_FIELD_TYPE, string I_FIELD_REFERENCE)
        {
            try
            {
                return RequireDAL.Instance.RequireFieldSelect(I_FIELD_TYPE, I_FIELD_REFERENCE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireSave
        public DataTable RequireSave(int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {
                return RequireDAL.Instance.RequireSave(I_USERID, I_DATATABLE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static RequireBLL _instance;
        public static RequireBLL Instance
        {
            get
            {
                _instance = new RequireBLL();
                return _instance;
            }
        }
        #endregion
    }
}
