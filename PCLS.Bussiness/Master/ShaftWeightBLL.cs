﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class ShaftWeightBLL
    {
        public DataTable ShaftWeightSelectBLL(string Condition)
        {
            try
            {
                return ShaftWeightDAL.Instance.ShaftWeightSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static ShaftWeightBLL _instance;
        public static ShaftWeightBLL Instance
        {
            get
            {
                _instance = new ShaftWeightBLL();
                return _instance;
            }
        }
        #endregion
    }
}