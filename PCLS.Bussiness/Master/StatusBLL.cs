﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class StatusBLL
    {
        public DataTable StatusSelectBLL(string Condition)
        {
            try
            {
                return StatusDAL.Instance.StatusSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static StatusBLL _instance;
        public static StatusBLL Instance
        {
            get
            {
                _instance = new StatusBLL();
                return _instance;
            }
        }
        #endregion
    }
}