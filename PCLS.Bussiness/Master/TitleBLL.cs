﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class TitleBLL
    {
        public DataTable TitleSelectBLL(string Condition)
        {
            try
            {
                return TitleDAL.Instance.TitleSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void TitleAddBLL(int TitleID, string TitleName, string IsActive, int CreateBy)
        {
            try
            {
                TitleDAL.Instance.TitleAddDAL(TitleID, TitleName, IsActive, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static TitleBLL _instance;
        public static TitleBLL Instance
        {
            get
            {
                _instance = new TitleBLL();
                return _instance;
            }
        }
        #endregion
    }
}