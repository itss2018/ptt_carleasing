﻿using PCLS.DataAccess.Master;
using PCLS.Entity.Transaction.Truck;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class TruckBLL
    {
        public DataTable TruckAddBLL(TruckEntity truckEntity, DataTable dtUpload, DataTable dtInlet, string TruckTable)
        {
            try
            {
                return TruckDAL.Instance.TruckAddDAL(truckEntity, dtUpload, dtInlet, TruckTable);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet TruckTempSelectBLL(string Condition, string ConditionInlet)
        {
            try
            {
                return TruckDAL.Instance.TruckTempSelectDAL(Condition, ConditionInlet);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable TruckUpdateStatusBLL(string I_ID, string I_TYPE, string I_TRUCK_MAPPING)
        {
            try
            {

                return TruckDAL.Instance.TruckUpdateStatusDAL(I_ID, I_TYPE, I_TRUCK_MAPPING);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable TruckTempMappingSelectBLL(string Condition)
        {
            try
            {
                return TruckDAL.Instance.TruckTempMappingSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void TruckTempUpdateStatusBLL(int TruckTempID, string TypeUpdate, int UpdateBy, string Remark)
        {
            try
            {
                TruckDAL.Instance.TruckTempUpdateStatusDAL(TruckTempID, TypeUpdate, UpdateBy, Remark);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void TruckSatusUpdateBll(int TruckID, int UpdateBy, int ISACTIVE)
        {
            try
            {
                TruckDAL.Instance.TruckSatusUpdateDAL(TruckID, UpdateBy, ISACTIVE);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void TruckTempMappingUpdateStatusBLL(int TruckTempMappingID, string TypeUpdate, int UpdateBy)
        {
            try
            {
                TruckDAL.Instance.TruckTempMappingUpdateStatusDAL(TruckTempMappingID, TypeUpdate, UpdateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet TruckSelectBLL(string Condition, string ConditionInlet, int? TruckID)
        {
            try
            {
                return TruckDAL.Instance.TruckSelectDAL(Condition, ConditionInlet, TruckID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckMappingSelectBLL(string Condition)
        {
            try
            {
                return TruckDAL.Instance.TruckMappingSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckHeadSelectBLL(int TruckTypeID, string VendorID, bool IncludeInContract)
        {
            try
            {
                return TruckDAL.Instance.TruckHeadSelectDAL(TruckTypeID, VendorID, IncludeInContract);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckDetailSelectBLL(int TruckTypeID, string VendorID, bool IncludeInContract)
        {
            try
            {
                return TruckDAL.Instance.TruckDetailSelectDAL(TruckTypeID, VendorID, IncludeInContract);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckHeadSelectContractBLL(int TruckTypeID, string VendorID, bool IncludeInContract)
        {
            try
            {
                return TruckDAL.Instance.TruckHeadSelectContractDAL(TruckTypeID, VendorID, IncludeInContract);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckDetailSelectContractBLL(int TruckTypeID, string VendorID, bool IncludeInContract)
        {
            try
            {
                return TruckDAL.Instance.TruckDetailSelectContractDAL(TruckTypeID, VendorID, IncludeInContract);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable TruckMappingAddBLL(int TruckTempMappingID, int TruckIDHead, int TruckIDDetail, int CreateBy, int TruckTypeMappingID, int VendorID, int IsActive)
        {
            try
            {
                return TruckDAL.Instance.TruckMappingAddDAL(TruckTempMappingID, TruckIDHead, TruckIDDetail, CreateBy, TruckTypeMappingID, VendorID, IsActive);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckGraphicSelectBLL(int Step, int TruckTypeID, int TruckIDHead, string TruckTable, int SystemID)
        {
            try
            {
                return TruckDAL.Instance.TruckGraphicSelectDAL(Step, TruckTypeID, TruckIDHead, TruckTable, SystemID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet TruckSelectMobile_BLL()
        {
            try
            {
                return TruckDAL.Instance.TruckSelectMobile_DAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataSet TruckInletSelectMobile_BLL(int TruckID)
        {
            try
            {
                return TruckDAL.Instance.TruckInletSelectMobile_DAL(TruckID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static TruckBLL _instance;
        public static TruckBLL Instance
        {
            get
            {
                _instance = new TruckBLL();
                return _instance;
            }
        }
        #endregion
    }
}
