﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class TruckSystemBLL
    {
        public DataTable TruckSystemSelectBLL()
        {
            try
            {
                return TruckSystemDAL.Instance.TruckSystemSelectDAL();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static TruckSystemBLL _instance;
        public static TruckSystemBLL Instance
        {
            get
            {
                _instance = new TruckSystemBLL();
                return _instance;
            }
        }
        #endregion
    }
}