﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class TruckTypeBLL
    {
        public DataTable TruckTypeSelectBLL(string Condition)
        {
            try
            {
                return TruckTypeDAL.Instance.TruckTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static TruckTypeBLL _instance;
        public static TruckTypeBLL Instance
        {
            get
            {
                _instance = new TruckTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}
