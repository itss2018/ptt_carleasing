﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class TruckTypeMappingBLL
    {
        public DataTable TruckTypeMappingSelectBLL(string Condition)
        {
            try
            {
                return TruckTypeMappingDAL.Instance.TruckTypeMappingSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static TruckTypeMappingBLL _instance;
        public static TruckTypeMappingBLL Instance
        {
            get
            {
                _instance = new TruckTypeMappingBLL();
                return _instance;
            }
        }
        #endregion
    }
}