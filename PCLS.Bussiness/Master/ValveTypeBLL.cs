﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class ValveTypeBLL
    {
        public DataTable ValveTypeSelectBLL(string Condition)
        {
            try
            {
                return ValveTypeDAL.Instance.ValveTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static ValveTypeBLL _instance;
        public static ValveTypeBLL Instance
        {
            get
            {
                _instance = new ValveTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}