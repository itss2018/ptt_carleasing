﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class VendorBLL
    {
        public DataTable VendorSelectBLL(string VendorID, string VendorCode, string VendorName, string IsActive)
        {
            try
            {
                return VendorDAL.Instance.VendorSelectDAL(VendorID, VendorCode, VendorName, IsActive);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void VendorAddBLL(int VendorID, string VendorCode, string VendorName, string Telephone, string Fax, string Email, string IsActive, int CreateBy)
        {
            try
            {
                VendorDAL.Instance.VendorAddDAL(VendorID, VendorCode, VendorName, Telephone, Fax, Email, IsActive, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static VendorBLL _instance;
        public static VendorBLL Instance
        {
            get
            {
                _instance = new VendorBLL();
                return _instance;
            }
        }
        #endregion
    }
}