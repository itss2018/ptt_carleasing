﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class VendorTankBLL
    {
        public DataTable VendorTankSelectBLL(string Condition)
        {
            try
            {
                return VendorTankDAL.Instance.VendorTankSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static VendorTankBLL _instance;
        public static VendorTankBLL Instance
        {
            get
            {
                _instance = new VendorTankBLL();
                return _instance;
            }
        }
        #endregion
    }
}