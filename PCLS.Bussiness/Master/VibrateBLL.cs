﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class VibrateBLL
    {
        public DataTable VibrateSelectBLL(string Condition)
        {
            try
            {
                return VibrateDAL.Instance.VibrateSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static VibrateBLL _instance;
        public static VibrateBLL Instance
        {
            get
            {
                _instance = new VibrateBLL();
                return _instance;
            }
        }
        #endregion
    }
}