﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.Master
{
    public class WarehouseBLL
    {
        public DataTable WarehouseSelectBLL(string Condition)
        {
            try
            {
                return WarehouseDAL.Instance.WarehouseSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void WarehouseAddBLL(int WarehouseID, string WarehouseName, string ShortName, string IsActive, int CreateBy, string PrefixSale, string PrefixContract, string PrefixOrder)
        {
            try
            {
                WarehouseDAL.Instance.WarehouseAddDAL(WarehouseID, WarehouseName, ShortName, IsActive, CreateBy, PrefixSale, PrefixContract, PrefixOrder);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static WarehouseBLL _instance;
        public static WarehouseBLL Instance
        {
            get
            {
                _instance = new WarehouseBLL();
                return _instance;
            }
        }
        #endregion
    }
}