﻿using PCLS.DataAccess.PIS;
using System;
using System.Data;

namespace PCLS.Bussiness.PIS
{
    public class PISBLL
    {
        public DataTable PisSelectBLL(string Condition)
        {
            try
            {
                return PISDAL.Instance.PisSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static PISBLL _instance;
        public static PISBLL Instance
        {
            get
            {
                _instance = new PISBLL();
                return _instance;
            }
        }
        #endregion
    }
}