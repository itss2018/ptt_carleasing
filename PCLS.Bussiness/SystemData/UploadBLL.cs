﻿using PCLS.DataAccess.SystemData;
using System;
using System.Data;

namespace PCLS.Bussiness.SystemData
{
    public class UploadBLL
    {
        public DataTable UploadRequestFileBLL(string Condition)
        {
            try
            {
                return UploadDAL.Instance.UploadRequestFileDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadSelectBLL(string RefStr, string UploadType, string Condition)
        {
            try
            {
                return UploadDAL.Instance.UploadSelect(RefStr, UploadType, Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UploadBLL _instance;
        public static UploadBLL Instance
        {
            get
            {
                _instance = new UploadBLL();
                return _instance;
            }
        }
        #endregion
    }
}