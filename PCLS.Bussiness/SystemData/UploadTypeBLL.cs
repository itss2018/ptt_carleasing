﻿using PCLS.DataAccess.SystemData;
using System;
using System.Data;

namespace PCLS.Bussiness.SystemData
{
    public class UploadTypeBLL
    {
        public DataTable UploadTypeSelectBLL(string Condition)
        {
            try
            {
                return UploadTypeDAL.Instance.UploadTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadTypeHeaderSelectBLL(string Condition)
        {
            try
            {
                return UploadTypeDAL.Instance.UploadTypeHeaderSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UploadTypeAddBLL(string UploadType, string UploadName, string Extention, int MaxFileSize, string IsActive, int CreateBy, string UploadTypeTH, string Require)
        {
            try
            {
                UploadTypeDAL.Instance.UploadTypeAddDAL(UploadType, UploadName, Extention, MaxFileSize, IsActive, CreateBy, UploadTypeTH, Require);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UploadTypeBLL _instance;
        public static UploadTypeBLL Instance
        {
            get
            {
                _instance = new UploadTypeBLL();
                return _instance;
            }
        }
        #endregion
    }
}