﻿using PCLS.DataAccess.SystemData;
using System;
using System.Data;

namespace PCLS.Bussiness.SystemData
{
    public class UserBLL
    {
        public DataTable UserSelectBLL(string Condition)
        {
            try
            {
                return UserDAL.Instance.UserSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UserContractSelectBLL(string Condition)
        {
            try
            {
                return UserDAL.Instance.UserContractSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UserSelect2BLL(string Username, string Password)
        {
            try
            {
                return UserDAL.Instance.UserSelect2DAL(Username, Password);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UserPlantSelectBLL(string Condition)
        {
            try
            {
                return UserDAL.Instance.UserPlantSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UserTypeSelectBLL(string Condition)
        {
            try
            {
                return UserDAL.Instance.UserTypeSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UserAddBLL(int UserID, string UserName, string Password, string DateOfBirth, int TitleID, string FirstName, string LastName
                                  , string Address, string SubDistrict, string District, string Province, string ZipCode, string Country, string Telephone
                                  , string Email, int UserGroupID, int IsActive, int CreateBy, string ImagePath, int UserTypeID, DataTable dtUpload, int VendorID)
        {
            try
            {
                UserDAL.Instance.UserAddDAL(UserID, UserName, Password, DateOfBirth, TitleID, FirstName, LastName
                                          , Address, SubDistrict, District, Province, ZipCode
                                          , Country, Telephone, Email, UserGroupID, IsActive, CreateBy
                                          , ImagePath, UserTypeID, dtUpload, VendorID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UserBLL _instance;
        public static UserBLL Instance
        {
            get
            {
                _instance = new UserBLL();
                return _instance;
            }
        }
        #endregion
    }
}