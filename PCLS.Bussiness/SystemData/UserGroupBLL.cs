﻿using PCLS.DataAccess.Master;
using System;
using System.Data;

namespace PCLS.Bussiness.System
{
    public class UserGroupBLL
    {
        public DataTable UserGroupSelectBLL(string Condition)
        {
            try
            {
                return UserGroupDAL.Instance.UserGroupSelectDAL(Condition);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UserGroupAddBLL(int UserGroupID, string UserGroupName, string IsAdmin, string IsActive, int CreateBy)
        {
            try
            {
                UserGroupDAL.Instance.UserGroupAddDAL(UserGroupID, UserGroupName, IsAdmin, IsActive, CreateBy);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UserGroupBLL _instance;
        public static UserGroupBLL Instance
        {
            get
            {
                _instance = new UserGroupBLL();
                return _instance;
            }
        }
        #endregion
    }
}