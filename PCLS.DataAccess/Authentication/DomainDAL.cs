﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Authentication
{
    public partial class DomainDAL : OracleConnectionDAL
    {
        public DomainDAL()
        {
            InitializeComponent();
        }

        public DomainDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable DomainSelectDAL(string Condition)
        {
            try
            {
                DataTable dt = new DataTable();

                dt = dbManager.ExecuteDataTable(cmdDomainSelect, "cmdDomainSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static DomainDAL _instance;
        public static DomainDAL Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DomainDAL();
                }
                return _instance;
            }
        }
        #endregion
    }
}