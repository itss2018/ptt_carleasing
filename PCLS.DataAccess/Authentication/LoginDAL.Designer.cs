﻿namespace PCLS.DataAccess.Authentication
{
    partial class LoginDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdClearLogin = new System.Data.OracleClient.OracleCommand();
            this.cmdCountLogin = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdClearLogin
            // 
            this.cmdClearLogin.CommandText = "UPDATE M_USER SET COUNT_INVALID = \'0\' WHERE USERNAME=:USERNAME";
            this.cmdClearLogin.Connection = this.OracleConn;
            this.cmdClearLogin.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter(":USERNAME", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdCountLogin
            // 
            this.cmdCountLogin.Connection = this.OracleConn;
            this.cmdCountLogin.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("LogInName", System.Data.OracleClient.OracleType.VarChar),
            new System.Data.OracleClient.OracleParameter("OutputValue", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdClearLogin;
        private System.Data.OracleClient.OracleCommand cmdCountLogin;
    }
}
