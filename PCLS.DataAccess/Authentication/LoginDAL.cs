﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Authentication
{
    public partial class LoginDAL : OracleConnectionDAL
    {
        public LoginDAL()
        {
            InitializeComponent();
        }

        public LoginDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CountLoginDAL(string UserName)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCountLogin.CommandText = "USP_M_LogIn";
                cmdCountLogin.Parameters["LogInName"].Value = UserName;

                DataTable dt = dbManager.ExecuteDataTable(cmdCountLogin, "cmdCountLogin");
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void ClearLoginDAL(string UserName)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdClearLogin.Parameters[":USERNAME"].Value = UserName;

                dbManager.ExecuteNonQuery(cmdClearLogin);
                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static LoginDAL _instance;
        public static LoginDAL Instance
        {
            get
            {
                return new LoginDAL();
            }
        }
        #endregion
    }
}