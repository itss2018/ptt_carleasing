﻿using dbManager;
using System.ComponentModel;

namespace PCLS.DataAccess.ConnectionDAL
{
    public partial class OracleConnectionDAL_PIS : Component
    {
        public OracleConnectionDAL_PIS()
        {
            InitializeComponent();
        }

        public OracleConnectionDAL_PIS(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DBManager dbManager = new DBManager(DataProvider.Oracle, Properties.Settings.Default.OracleConnectionString_PIS);
    }
}