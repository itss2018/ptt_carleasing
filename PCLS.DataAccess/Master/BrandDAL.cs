﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class BrandDAL : OracleConnectionDAL
    {
        public BrandDAL()
        {
            InitializeComponent();
        }

        public BrandDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable BrandSelectDAL(string Condition)
        {
            string Query = cmdBrandSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdBrandSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdBrandSelect, "cmdBrandSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdBrandSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static BrandDAL _instance;
        public static BrandDAL Instance
        {
            get
            {
                _instance = new BrandDAL();
                return _instance;
            }
        }
        #endregion
    }
}