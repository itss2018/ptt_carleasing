﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarAccessoriesDAL : OracleConnectionDAL
    {
        public CarAccessoriesDAL()
        {
            InitializeComponent();
        }

        public CarAccessoriesDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarAccessoriesSelectDAL(string Condition)
        {
            string Query = cmdCarAccessoriesSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarAccessoriesSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarAccessoriesSelect, "cmdCarAccessoriesSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarAccessoriesSelect.CommandText = Query;
            }
        }

        public void CarAccessoriesAddDAL(int ACCESSORIESID, string ACCESSORIESNAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarAccessoriesAdd.CommandType = CommandType.StoredProcedure;
                cmdCarAccessoriesAdd.CommandText = "USP_M_ACCESSORIES";

                cmdCarAccessoriesAdd.Parameters["I_ACCESSORIESID"].Value = ACCESSORIESID;
                cmdCarAccessoriesAdd.Parameters["I_ACCESSORIESNAME"].Value = ACCESSORIESNAME;
                cmdCarAccessoriesAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarAccessoriesAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarAccessoriesAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarAccessoriesAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarAccessoriesDAL _instance;
        public static CarAccessoriesDAL Instance
        {
            get
            {
                _instance = new CarAccessoriesDAL();
                return _instance;
            }
        }
        #endregion
    }
}