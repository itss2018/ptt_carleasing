﻿namespace PCLS.DataAccess.Master
{
    partial class CarAccessoriesDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarAccessoriesSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarAccessoriesAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdAccessoriesSelect
            // 
            this.cmdCarAccessoriesSelect.CommandText = "SELECT * FROM VW_M_ACCESSORIES_SELECT WHERE 1=1";
            this.cmdCarAccessoriesSelect.Connection = this.OracleConn;
            // 
            // cmdAccessoriesAdd
            // 
            this.cmdCarAccessoriesAdd.Connection = this.OracleConn;
            this.cmdCarAccessoriesAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_ACCESSORIESID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_ACCESSORIESNAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarAccessoriesSelect;
        private System.Data.OracleClient.OracleCommand cmdCarAccessoriesAdd;
    }
}
