﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarBankDAL : OracleConnectionDAL
    {
        public CarBankDAL()
        {
            InitializeComponent();
        }

        public CarBankDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarBankSelectDAL(string Condition)
        {
            string Query = cmdCarBankSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarBankSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarBankSelect, "cmdCarBankSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarBankSelect.CommandText = Query;
            }
        }

        public void CarBankAddDAL(int BANKID, string BANKNAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarBankAdd.CommandType = CommandType.StoredProcedure;
                cmdCarBankAdd.CommandText = "USP_M_BANK";

                cmdCarBankAdd.Parameters["I_BANKID"].Value = BANKID;
                cmdCarBankAdd.Parameters["I_BANKNAME"].Value = BANKNAME;
                cmdCarBankAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarBankAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarBankAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarBankAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarBankDAL _instance;
        public static CarBankDAL Instance
        {
            get
            {
                _instance = new CarBankDAL();
                return _instance;
            }
        }
        #endregion
    }
}