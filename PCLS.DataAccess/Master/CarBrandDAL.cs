﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarBrandDAL : OracleConnectionDAL
    {
        public CarBrandDAL()
        {
            InitializeComponent();
        }

        public CarBrandDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarBrandSelectDAL(string Condition)
        {
            string Query = cmdCarBrandSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarBrandSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarBrandSelect, "cmdCarBrandSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarBrandSelect.CommandText = Query;
            }
        }

        public void CarBrandAddDAL(int BRANDID, string BRANDNAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarBrandAdd.CommandType = CommandType.StoredProcedure;
                cmdCarBrandAdd.CommandText = "USP_M_CARBRAND";

                cmdCarBrandAdd.Parameters["I_BRANDID"].Value = BRANDID;
                cmdCarBrandAdd.Parameters["I_BRANDNAME"].Value = BRANDNAME;
                cmdCarBrandAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarBrandAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarBrandAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarBrandAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarBrandDAL _instance;
        public static CarBrandDAL Instance
        {
            get
            {
                _instance = new CarBrandDAL();
                return _instance;
            }
        }
        #endregion
    }
}