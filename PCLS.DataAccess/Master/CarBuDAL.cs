﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarBuDAL : OracleConnectionDAL
    {
        public CarBuDAL()
        {
            InitializeComponent();
        }

        public CarBuDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarBuSelectDAL(string Condition)
        {
            string Query = cmdCarBuSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarBuSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarBuSelect, "cmdCarBuSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarBuSelect.CommandText = Query;
            }
        }

        public void CarBuAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarBuAdd.CommandType = CommandType.StoredProcedure;
                cmdCarBuAdd.CommandText = "USP_M_BU";

                cmdCarBuAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarBuAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarBuAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarBuAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarBuAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarBuAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarBuAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarBuAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarBuAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarBuAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarBuAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarBuAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarBuDAL _instance;
        public static CarBuDAL Instance
        {
            get
            {
                _instance = new CarBuDAL();
                return _instance;
            }
        }
        #endregion
    }
}