﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarChargeDAL : OracleConnectionDAL
    {
        public CarChargeDAL()
        {
            InitializeComponent();
        }

        public CarChargeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarChargeSelectDAL(string Condition)
        {
            string Query = cmdCarChargeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarChargeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarChargeSelect, "cmdCarChargeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarChargeSelect.CommandText = Query;
            }
        }

        public void CarChargeAddDAL(int CHARGEID, string CHARGENAME, int TYPETRAFFICTICKETID, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarChargeAdd.CommandType = CommandType.StoredProcedure;
                cmdCarChargeAdd.CommandText = "USP_M_CHARGE";

                cmdCarChargeAdd.Parameters["I_CHARGEID"].Value = CHARGEID;
                cmdCarChargeAdd.Parameters["I_CHARGENAME"].Value = CHARGENAME;
                cmdCarChargeAdd.Parameters["I_TYPETRAFFICTICKETID"].Value = TYPETRAFFICTICKETID;
                cmdCarChargeAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarChargeAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarChargeAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarChargeAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarChargeDAL _instance;
        public static CarChargeDAL Instance
        {
            get
            {
                _instance = new CarChargeDAL();
                return _instance;
            }
        }
        #endregion
    }
}