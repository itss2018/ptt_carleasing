﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarColorDAL : OracleConnectionDAL
    {
        public CarColorDAL()
        {
            InitializeComponent();
        }

        public CarColorDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarColorSelectDAL(string Condition)
        {
            string Query = cmdCarColorSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarColorSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarColorSelect, "cmdCarColorSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarColorSelect.CommandText = Query;
            }
        }

        public void CarColorAddDAL(int COLORID, string COLOR, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarColorAdd.CommandType = CommandType.StoredProcedure;
                cmdCarColorAdd.CommandText = "USP_M_CARCOLOR";

                cmdCarColorAdd.Parameters["I_COLORID"].Value = COLORID;
                cmdCarColorAdd.Parameters["I_COLOR"].Value = COLOR;
                cmdCarColorAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarColorAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarColorAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarColorAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarColorDAL _instance;
        public static CarColorDAL Instance
        {
            get
            {
                _instance = new CarColorDAL();
                return _instance;
            }
        }
        #endregion
    }
}