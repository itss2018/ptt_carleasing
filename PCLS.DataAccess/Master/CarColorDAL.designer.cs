﻿namespace PCLS.DataAccess.Master
{
    partial class CarColorDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarColorSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarColorAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdCarColorSelect
            // 
            this.cmdCarColorSelect.CommandText = "SELECT * FROM VW_M_CARCOLOR_SELECT WHERE 1=1";
            this.cmdCarColorSelect.Connection = this.OracleConn;
            // 
            // cmdCarColorAdd
            // 
            this.cmdCarColorAdd.Connection = this.OracleConn;
            this.cmdCarColorAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_COLORID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_COLOR", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarColorSelect;
        private System.Data.OracleClient.OracleCommand cmdCarColorAdd;
    }
}
