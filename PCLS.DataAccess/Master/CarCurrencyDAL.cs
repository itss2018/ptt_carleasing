﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarCurrencyDAL : OracleConnectionDAL
    {
        public CarCurrencyDAL()
        {
            InitializeComponent();
        }

        public CarCurrencyDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarCurrencySelectDAL(string Condition)
        {
            string Query = cmdCarCurrencySelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarCurrencySelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarCurrencySelect, "cmdCarCurrencySelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarCurrencySelect.CommandText = Query;
            }
        }

        public void CarCurrencyAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarCurrencyAdd.CommandType = CommandType.StoredProcedure;
                cmdCarCurrencyAdd.CommandText = "USP_M_CURRENCY";

                cmdCarCurrencyAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarCurrencyAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarCurrencyAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarCurrencyAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarCurrencyAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarCurrencyAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarCurrencyAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarCurrencyAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarCurrencyAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarCurrencyAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarCurrencyAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarCurrencyAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarCurrencyDAL _instance;
        public static CarCurrencyDAL Instance
        {
            get
            {
                _instance = new CarCurrencyDAL();
                return _instance;
            }
        }
        #endregion
    }
}