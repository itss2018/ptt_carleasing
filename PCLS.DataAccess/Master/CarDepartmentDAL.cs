﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarDepartmentDAL : OracleConnectionDAL
    {
        public CarDepartmentDAL()
        {
            InitializeComponent();
        }

        public CarDepartmentDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarDepartmentSelectDAL(string Condition)
        {
            string Query = cmdCarDepartmentSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarDepartmentSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarDepartmentSelect, "cmdCarDepartmentSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarDepartmentSelect.CommandText = Query;
            }
        }

        public void CarDepartmentAddDAL(string ORG_ID, string ORG_NAME, string ORG_ABBV, string PARENT_ID, DateTime VALID_FROM, DateTime VALID_TO, string ROOT_FLAG, int ORG_LEVEL, string BU, string CUST_CODE, string ORG_STRUCTURE, string COSTCENTER, string BA, string IO_ID, string REMARK, string ISVALID, string CCTR_NEW, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarDepartmentAdd.CommandType = CommandType.StoredProcedure;
                cmdCarDepartmentAdd.CommandText = "USP_M_ORGANIZE";

                cmdCarDepartmentAdd.Parameters["I_ORG_ID"].Value = ORG_ID;
                cmdCarDepartmentAdd.Parameters["I_ORG_NAME"].Value = ORG_NAME;
                cmdCarDepartmentAdd.Parameters["I_ORG_ABBV"].Value = ORG_ABBV;
                cmdCarDepartmentAdd.Parameters["I_PARENT_ID"].Value = PARENT_ID;
                cmdCarDepartmentAdd.Parameters["I_VALID_FROM"].Value = VALID_FROM;
                cmdCarDepartmentAdd.Parameters["I_VALID_TO"].Value = VALID_TO;
                cmdCarDepartmentAdd.Parameters["I_ROOT_FLAG"].Value = ROOT_FLAG;
                cmdCarDepartmentAdd.Parameters["I_ORG_LEVEL"].Value = ORG_LEVEL;
                cmdCarDepartmentAdd.Parameters["I_BU"].Value = BU;
                cmdCarDepartmentAdd.Parameters["I_CUST_CODE"].Value = CUST_CODE;
                cmdCarDepartmentAdd.Parameters["I_ORG_STRUCTURE"].Value = ORG_STRUCTURE;
                cmdCarDepartmentAdd.Parameters["I_COSTCENTER"].Value = COSTCENTER;
                cmdCarDepartmentAdd.Parameters["I_BA"].Value = BA;
                cmdCarDepartmentAdd.Parameters["I_IO_ID"].Value = IO_ID;
                cmdCarDepartmentAdd.Parameters["I_REMARK"].Value = REMARK;
                cmdCarDepartmentAdd.Parameters["I_ISVALID"].Value = ISVALID;
                cmdCarDepartmentAdd.Parameters["I_CCTR_NEW"].Value = CCTR_NEW;
                cmdCarDepartmentAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarDepartmentAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarDepartmentAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarDepartmentDAL _instance;
        public static CarDepartmentDAL Instance
        {
            get
            {
                _instance = new CarDepartmentDAL();
                return _instance;
            }
        }
        #endregion
    }
}