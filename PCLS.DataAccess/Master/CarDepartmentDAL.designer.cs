﻿namespace PCLS.DataAccess.Master
{
    partial class CarDepartmentDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarDepartmentSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarDepartmentAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTypeTrafficTicketSelect
            // 
            this.cmdCarDepartmentSelect.CommandText = "SELECT * FROM VW_M_ORGANIZE_SELECT WHERE 1=1";
            this.cmdCarDepartmentSelect.Connection = this.OracleConn;
            // 
            // cmdTypeTrafficTicketAdd
            // 
            this.cmdCarDepartmentAdd.Connection = this.OracleConn;
            this.cmdCarDepartmentAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_ORG_ID", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ORG_NAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ORG_ABBV", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_PARENT_ID", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_VALID_FROM", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_VALID_TO", System.Data.OracleClient.OracleType.DateTime),
            new System.Data.OracleClient.OracleParameter("I_ROOT_FLAG ", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ORG_LEVEL", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_BU", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_CUST_CODE", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ORG_STRUCTURE", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_COSTCENTER", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_BA", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_IO_ID", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_REMARK", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISVALID", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_CCTR_NEW", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarDepartmentSelect;
        private System.Data.OracleClient.OracleCommand cmdCarDepartmentAdd;
    }
}
