﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeAcquisitionDAL : OracleConnectionDAL
    {
        public CarTypeAcquisitionDAL()
        {
            InitializeComponent();
        }

        public CarTypeAcquisitionDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeAcquisitionSelectDAL(string Condition)
        {
            string Query = cmdCarTypeAcquisitionSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeAcquisitionSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeAcquisitionSelect, "cmdCarTypeAcquisitionSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeAcquisitionSelect.CommandText = Query;
            }
        }

        public void CarTypeAcquisitionAddDAL(int TAXSUBTYPEID, string TAXSUBTYPEDETAIL, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeAcquisitionAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeAcquisitionAdd.CommandText = "USP_M_TAXSUBTYPE";

                cmdCarTypeAcquisitionAdd.Parameters["I_TAXSUBTYPEID"].Value = TAXSUBTYPEID;
                cmdCarTypeAcquisitionAdd.Parameters["I_TAXSUBTYPEDETAIL"].Value = TAXSUBTYPEDETAIL;
                cmdCarTypeAcquisitionAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeAcquisitionAdd.Parameters["I_CREATEBY"].Value = CREATEBY;

                dbManager.ExecuteNonQuery(cmdCarTypeAcquisitionAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeAcquisitionDAL _instance;
        public static CarTypeAcquisitionDAL Instance
        {
            get
            {
                _instance = new CarTypeAcquisitionDAL();
                return _instance;
            }
        }
        #endregion
    }
}