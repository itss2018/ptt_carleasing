﻿namespace PCLS.DataAccess.Master
{
    partial class CarFeatureDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarFeatureSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarFeatureAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdCarFeatureSelect
            // 
            this.cmdCarFeatureSelect.CommandText = "SELECT * FROM VW_M_TAXTYPE_SELECT WHERE 1=1";
            this.cmdCarFeatureSelect.Connection = this.OracleConn;
            // 
            // cmdCarFeatureAdd
            // 
            this.cmdCarFeatureAdd.Connection = this.OracleConn;
            this.cmdCarFeatureAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TAXTYPEID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TAXTYPECODE", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_TAXTYPENAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarFeatureSelect;
        private System.Data.OracleClient.OracleCommand cmdCarFeatureAdd;
    }
}
