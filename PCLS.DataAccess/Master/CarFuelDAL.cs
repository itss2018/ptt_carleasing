﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarFuelDAL : OracleConnectionDAL
    {
        public CarFuelDAL()
        {
            InitializeComponent();
        }

        public CarFuelDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarFuelSelectDAL(string Condition)
        {
            string Query = cmdCarFuelSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarFuelSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarFuelSelect, "cmdCarFuelSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarFuelSelect.CommandText = Query;
            }
        }

        public void CarFuelAddDAL(int FUELID, string FUELNAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarFuelAdd.CommandType = CommandType.StoredProcedure;
                cmdCarFuelAdd.CommandText = "USP_M_FUEL";

                cmdCarFuelAdd.Parameters["I_FUELID"].Value = FUELID;
                cmdCarFuelAdd.Parameters["I_FUELNAME"].Value = FUELNAME;
                cmdCarFuelAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarFuelAdd.Parameters["I_CREATEBY"].Value = CREATEBY;

                dbManager.ExecuteNonQuery(cmdCarFuelAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarFuelDAL _instance;
        public static CarFuelDAL Instance
        {
            get
            {
                _instance = new CarFuelDAL();
                return _instance;
            }
        }
        #endregion
    }
}