﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarModelDAL : OracleConnectionDAL
    {
        public CarModelDAL()
        {
            InitializeComponent();
        }

        public CarModelDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarModelSelectDAL(string Condition)
        {
            string Query = cmdCarModelSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarModelSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarModelSelect, "cmdCarModelSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarModelSelect.CommandText = Query;
            }
        }

        public void CarModelAddDAL(int MODELID, int BRANDID, string MODELNAME, string CARTYPE, string ENGINESIZE, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarModelAdd.CommandType = CommandType.StoredProcedure;
                cmdCarModelAdd.CommandText = "USP_M_CARMODEL";

                cmdCarModelAdd.Parameters["I_MODELID"].Value = MODELID;
                cmdCarModelAdd.Parameters["I_BRANDID"].Value = BRANDID;
                cmdCarModelAdd.Parameters["I_MODELNAME"].Value = MODELNAME;
                cmdCarModelAdd.Parameters["I_CARTYPE"].Value = CARTYPE;
                cmdCarModelAdd.Parameters["I_ENGINESIZE"].Value = ENGINESIZE;
                cmdCarModelAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarModelAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarModelAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarModelAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarModelDAL _instance;
        public static CarModelDAL Instance
        {
            get
            {
                _instance = new CarModelDAL();
                return _instance;
            }
        }
        #endregion
    }
}