﻿namespace PCLS.DataAccess.Master
{
    partial class CarModelDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarModelSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarModelAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdCarModelSelect
            // 
            this.cmdCarModelSelect.CommandText = "SELECT * FROM VW_M_CARMODEL_SELECT WHERE 1=1";
            this.cmdCarModelSelect.Connection = this.OracleConn;
            // 
            // cmdCarModelAdd
            // 
            this.cmdCarModelAdd.Connection = this.OracleConn;
            this.cmdCarModelAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_MODELID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_BRANDID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MODELNAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_CARTYPE", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ENGINESIZE", System.Data.OracleClient.OracleType.VarChar, 5),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarModelSelect;
        private System.Data.OracleClient.OracleCommand cmdCarModelAdd;
    }
}
