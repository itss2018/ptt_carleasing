﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarPartnerDAL : OracleConnectionDAL
    {
        public CarPartnerDAL()
        {
            InitializeComponent();
        }

        public CarPartnerDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarPartnerSelectDAL(string Condition)
        {
            string Query = cmdCarPartnerSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarPartnerSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarPartnerSelect, "cmdCarPartnerSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarPartnerSelect.CommandText = Query;
            }
        }

        public void CarPartnerAddDAL(string COMPNO, string COMPNAME, string COMPTYPE, int VENDOR_ID, int CUSTOMER_CODE, string VENDOR, string TAX_ID, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarPartnerAdd.CommandType = CommandType.StoredProcedure;
                cmdCarPartnerAdd.CommandText = "USP_M_COMPANY";
                cmdCarPartnerAdd.Parameters["I_COMPNO"].Value = COMPNO;
                cmdCarPartnerAdd.Parameters["I_COMPNAME"].Value = COMPNAME;
                cmdCarPartnerAdd.Parameters["I_COMPTYPE"].Value = COMPTYPE;
                cmdCarPartnerAdd.Parameters["I_VENDOR_ID"].Value = VENDOR_ID;
                cmdCarPartnerAdd.Parameters["I_CUSTOMER_CODE"].Value = CUSTOMER_CODE;
                cmdCarPartnerAdd.Parameters["I_VENDOR"].Value = VENDOR;
                cmdCarPartnerAdd.Parameters["I_TAX_ID"].Value = TAX_ID;
                cmdCarPartnerAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarPartnerAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarPartnerAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarPartnerAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarPartnerDAL _instance;
        public static CarPartnerDAL Instance
        {
            get
            {
                _instance = new CarPartnerDAL();
                return _instance;
            }
        }
        #endregion
    }
}