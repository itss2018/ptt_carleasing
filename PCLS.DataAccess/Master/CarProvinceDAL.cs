﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarProvinceDAL : OracleConnectionDAL
    {
        public CarProvinceDAL()
        {
            InitializeComponent();
        }

        public CarProvinceDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarProvinceSelectDAL(string Condition)
        {
            string Query = cmdCarProvinceSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarProvinceSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarProvinceSelect, "cmdCarProvinceSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarProvinceSelect.CommandText = Query;
            }
        }

        public void CarProvinceAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarProvinceAdd.CommandType = CommandType.StoredProcedure;
                cmdCarProvinceAdd.CommandText = "USP_M_PROVINCE";

                cmdCarProvinceAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarProvinceAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarProvinceAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarProvinceAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarProvinceAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarProvinceAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarProvinceAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarProvinceAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarProvinceAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarProvinceAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarProvinceAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarProvinceAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarProvinceDAL _instance;
        public static CarProvinceDAL Instance
        {
            get
            {
                _instance = new CarProvinceDAL();
                return _instance;
            }
        }
        #endregion
    }
}