﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarReasonDAL : OracleConnectionDAL
    {
        public CarReasonDAL()
        {
            InitializeComponent();
        }

        public CarReasonDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarReasonSelectDAL(string Condition)
        {
            string Query = cmdCarReasonSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarReasonSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarReasonSelect, "cmdCarReasonSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarReasonSelect.CommandText = Query;
            }
        }

        public void CarReasonAddDAL(int REASONID, string REASONNAME, string REASONTYPE, string REASONFORM, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarReasonAdd.CommandType = CommandType.StoredProcedure;
                cmdCarReasonAdd.CommandText = "USP_M_TYPETRAFFICTICKET";

                cmdCarReasonAdd.Parameters["I_REASONID"].Value = REASONID;
                cmdCarReasonAdd.Parameters["I_REASONNAME"].Value = REASONNAME;
                cmdCarReasonAdd.Parameters["I_REASONTYPE"].Value = REASONTYPE;
                cmdCarReasonAdd.Parameters["I_REASONFORM"].Value = REASONFORM;
                cmdCarReasonAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarReasonAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarReasonAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarReasonAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarReasonDAL _instance;
        public static CarReasonDAL Instance
        {
            get
            {
                _instance = new CarReasonDAL();
                return _instance;
            }
        }
        #endregion
    }
}