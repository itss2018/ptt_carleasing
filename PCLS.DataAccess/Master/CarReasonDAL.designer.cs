﻿namespace PCLS.DataAccess.Master
{
    partial class CarReasonDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarReasonSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarReasonAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTypeTrafficTicketSelect
            // 
            this.cmdCarReasonSelect.CommandText = "SELECT * FROM VW_M_REASON_SELECT WHERE 1=1";
            this.cmdCarReasonSelect.Connection = this.OracleConn;
            // 
            // cmdTypeTrafficTicketAdd
            // 
            this.cmdCarReasonAdd.Connection = this.OracleConn;
            this.cmdCarReasonAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_REASONID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REASONNAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_REASONTYPE", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_REASONFORM", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarReasonSelect;
        private System.Data.OracleClient.OracleCommand cmdCarReasonAdd;
    }
}
