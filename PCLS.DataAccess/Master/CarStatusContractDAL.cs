﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarContractStatusDAL : OracleConnectionDAL
    {
        public CarContractStatusDAL()
        {
            InitializeComponent();
        }

        public CarContractStatusDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarContractStatusSelectDAL(string Condition)
        {
            string Query = cmdCarContractStatusSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarContractStatusSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarContractStatusSelect, "cmdCarContractStatusSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarContractStatusSelect.CommandText = Query;
            }
        }

        public void CarContractStatusAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarContractStatusAdd.CommandType = CommandType.StoredProcedure;
                cmdCarContractStatusAdd.CommandText = "USP_M_CONTRACTSTATUS";

                cmdCarContractStatusAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarContractStatusAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarContractStatusAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarContractStatusAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarContractStatusAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarContractStatusAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarContractStatusAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarContractStatusAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarContractStatusAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarContractStatusAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarContractStatusAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarContractStatusAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarContractStatusDAL _instance;
        public static CarContractStatusDAL Instance
        {
            get
            {
                _instance = new CarContractStatusDAL();
                return _instance;
            }
        }
        #endregion
    }
}