﻿namespace PCLS.DataAccess.Master
{
    partial class CarContractStatusDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarContractStatusSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarContractStatusAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdCarContractStatusSelect
            // 
            this.cmdCarContractStatusSelect.CommandText = "SELECT * FROM VW_M_CONTRACTSTATUS_SELECT WHERE 1=1";
            this.cmdCarContractStatusSelect.Connection = this.OracleConn;
            // 
            // cmdCarContractStatusAdd
            // 
            this.cmdCarContractStatusAdd.Connection = this.OracleConn;
            this.cmdCarContractStatusAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TYPE", System.Data.OracleClient.OracleType.VarChar,100),
            new System.Data.OracleClient.OracleParameter("I_CODE", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_CODENAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_CODEVALUE", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_CODELEN", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_NAMELEN", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VALID_STATUS", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_SETBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarContractStatusSelect;
        private System.Data.OracleClient.OracleCommand cmdCarContractStatusAdd;
    }
}
