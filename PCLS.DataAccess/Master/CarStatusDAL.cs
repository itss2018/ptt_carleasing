﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarStatusDAL : OracleConnectionDAL
    {
        public CarStatusDAL()
        {
            InitializeComponent();
        }

        public CarStatusDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarStatusSelectDAL(string Condition)
        {
            string Query = cmdCarStatusSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarStatusSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarStatusSelect, "cmdCarStatusSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarStatusSelect.CommandText = Query;
            }
        }

        public void CarStatusAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarStatusAdd.CommandType = CommandType.StoredProcedure;
                cmdCarStatusAdd.CommandText = "USP_M_CARSTATUS";

                cmdCarStatusAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarStatusAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarStatusAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarStatusAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarStatusAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarStatusAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarStatusAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarStatusAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarStatusAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarStatusAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarStatusAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarStatusAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarStatusDAL _instance;
        public static CarStatusDAL Instance
        {
            get
            {
                _instance = new CarStatusDAL();
                return _instance;
            }
        }
        #endregion
    }
}