﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarFeatureDAL : OracleConnectionDAL
    {
        public CarFeatureDAL()
        {
            InitializeComponent();
        }

        public CarFeatureDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarFeatureSelectDAL(string Condition)
        {
            string Query = cmdCarFeatureSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarFeatureSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarFeatureSelect, "cmdCarFeatureSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarFeatureSelect.CommandText = Query;
            }
        }

        public void CarFeatureAddDAL(int TAXTYPEID, string TAXTYPECODE, string TAXTYPENAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarFeatureAdd.CommandType = CommandType.StoredProcedure;
                cmdCarFeatureAdd.CommandText = "USP_M_TAXTYPE";

                cmdCarFeatureAdd.Parameters["I_TAXTYPEID"].Value = TAXTYPEID;
                cmdCarFeatureAdd.Parameters["I_TAXTYPECODE"].Value = TAXTYPECODE;
                cmdCarFeatureAdd.Parameters["I_TAXTYPENAME"].Value = TAXTYPENAME;
                cmdCarFeatureAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarFeatureAdd.Parameters["I_CREATEBY"].Value = CREATEBY;

                dbManager.ExecuteNonQuery(cmdCarFeatureAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarFeatureDAL _instance;
        public static CarFeatureDAL Instance
        {
            get
            {
                _instance = new CarFeatureDAL();
                return _instance;
            }
        }
        #endregion
    }
}