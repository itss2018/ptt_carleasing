﻿namespace PCLS.DataAccess.Master
{
    partial class CarTypeAcquisitionDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarTypeAcquisitionSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarTypeAcquisitionAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdCarTypeAcquisitionSelect
            // 
            this.cmdCarTypeAcquisitionSelect.CommandText = "SELECT * FROM USP_M_TAXSUBTYPE WHERE 1=1";
            this.cmdCarTypeAcquisitionSelect.Connection = this.OracleConn;
            // 
            // cmdCarTypeAcquisitionAdd
            // 
            this.cmdCarTypeAcquisitionAdd.Connection = this.OracleConn;
            this.cmdCarTypeAcquisitionAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TAXSUBTYPEID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TAXSUBTYPEDETAIL", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarTypeAcquisitionSelect;
        private System.Data.OracleClient.OracleCommand cmdCarTypeAcquisitionAdd;
    }
}
