﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeCardDAL : OracleConnectionDAL
    {
        public CarTypeCardDAL()
        {
            InitializeComponent();
        }

        public CarTypeCardDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeCardSelectDAL(string Condition)
        {
            string Query = cmdCarTypeCardSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeCardSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeCardSelect, "cmdCarTypeCardSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeCardSelect.CommandText = Query;
            }
        }

        public void CarTypeCardAddDAL(int TYPECARDID, string TYPECARDNAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeCardAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeCardAdd.CommandText = "USP_M_TYPECARD";

                cmdCarTypeCardAdd.Parameters["I_TYPECARDID"].Value = TYPECARDID;
                cmdCarTypeCardAdd.Parameters["I_TYPECARDNAME"].Value = TYPECARDNAME;
                cmdCarTypeCardAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeCardAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeCardAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeCardAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeCardDAL _instance;
        public static CarTypeCardDAL Instance
        {
            get
            {
                _instance = new CarTypeCardDAL();
                return _instance;
            }
        }
        #endregion
    }
}