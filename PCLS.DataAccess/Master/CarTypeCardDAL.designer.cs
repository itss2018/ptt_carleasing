﻿namespace PCLS.DataAccess.Master
{
    partial class CarTypeCardDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarTypeCardSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarTypeCardAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTypeCardSelect
            // 
            this.cmdCarTypeCardSelect.CommandText = "SELECT * FROM VW_M_TYPECARD_SELECT WHERE 1=1";
            this.cmdCarTypeCardSelect.Connection = this.OracleConn;
            // 
            // cmdTypeCardAdd
            // 
            this.cmdCarTypeCardAdd.Connection = this.OracleConn;
            this.cmdCarTypeCardAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TYPECARDID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TYPECARDNAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarTypeCardSelect;
        private System.Data.OracleClient.OracleCommand cmdCarTypeCardAdd;
    }
}
