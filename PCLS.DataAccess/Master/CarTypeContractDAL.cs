﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeContractDAL : OracleConnectionDAL
    {
        public CarTypeContractDAL()
        {
            InitializeComponent();
        }

        public CarTypeContractDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeContractSelectDAL(string Condition)
        {
            string Query = cmdCarTypeContractSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeContractSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeContractSelect, "cmdCarTypeContractSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeContractSelect.CommandText = Query;
            }
        }

        public void CarTypeContractAddDAL(int TYPECONTRACTID, string TYPECONTRACTNAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeContractAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeContractAdd.CommandText = "USP_M_TYPECONTRACT";

                cmdCarTypeContractAdd.Parameters["I_TYPECONTRACTID"].Value = TYPECONTRACTID;
                cmdCarTypeContractAdd.Parameters["I_TYPECONTRACTNAME"].Value = TYPECONTRACTNAME;
                cmdCarTypeContractAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeContractAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeContractAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeContractAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeContractDAL _instance;
        public static CarTypeContractDAL Instance
        {
            get
            {
                _instance = new CarTypeContractDAL();
                return _instance;
            }
        }
        #endregion
    }
}