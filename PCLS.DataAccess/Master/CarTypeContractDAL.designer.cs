﻿namespace PCLS.DataAccess.Master
{
    partial class CarTypeContractDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarTypeContractSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarTypeContractAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTypeTrafficTicketSelect
            // 
            this.cmdCarTypeContractSelect.CommandText = "SELECT * FROM VW_M_TYPECONTRACT_SELECT WHERE 1=1";
            this.cmdCarTypeContractSelect.Connection = this.OracleConn;
            // 
            // cmdTypeTrafficTicketAdd
            // 
            this.cmdCarTypeContractAdd.Connection = this.OracleConn;
            this.cmdCarTypeContractAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TYPECONTRACTID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TYPECONTRACTNAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarTypeContractSelect;
        private System.Data.OracleClient.OracleCommand cmdCarTypeContractAdd;
    }
}
