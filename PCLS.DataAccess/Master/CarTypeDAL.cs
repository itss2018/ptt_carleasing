﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeDAL : OracleConnectionDAL
    {
        public CarTypeDAL()
        {
            InitializeComponent();
        }

        public CarTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeSelectDAL(string Condition)
        {
            string Query = cmdCarTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeSelect, "cmdCarTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeSelect.CommandText = Query;
            }
        }

        public void CarTypeAddDAL(int CARTYPEID, string CARTYPE, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeAdd.CommandText = "USP_M_CARTYPE";

                cmdCarTypeAdd.Parameters["I_CARTYPEID"].Value = CARTYPEID;
                cmdCarTypeAdd.Parameters["I_CARTYPE"].Value = CARTYPE;
                cmdCarTypeAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeAdd.Parameters["I_CREATEBY"].Value = CREATEBY;

                dbManager.ExecuteNonQuery(cmdCarTypeAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeDAL _instance;
        public static CarTypeDAL Instance
        {
            get
            {
                _instance = new CarTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}