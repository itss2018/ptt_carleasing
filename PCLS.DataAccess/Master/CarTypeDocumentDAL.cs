﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeDocumentDAL : OracleConnectionDAL
    {
        public CarTypeDocumentDAL()
        {
            InitializeComponent();
        }

        public CarTypeDocumentDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeDocumentSelectDAL(string Condition)
        {
            string Query = cmdCarTypeDocumentSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeDocumentSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeDocumentSelect, "cmdCarTypeDocumentSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeDocumentSelect.CommandText = Query;
            }
        }

        public void CarTypeDocumentAddDAL(int TYPEDOCUMENTID, string TYPEDOCUMENTNAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeDocumentAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeDocumentAdd.CommandText = "USP_M_TYPEDOCUMENT";

                cmdCarTypeDocumentAdd.Parameters["I_TYPEDOCUMENTID"].Value = TYPEDOCUMENTID;
                cmdCarTypeDocumentAdd.Parameters["I_TYPEDOCUMENTNAME"].Value = TYPEDOCUMENTNAME;
                cmdCarTypeDocumentAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeDocumentAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeDocumentAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeDocumentAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeDocumentDAL _instance;
        public static CarTypeDocumentDAL Instance
        {
            get
            {
                _instance = new CarTypeDocumentDAL();
                return _instance;
            }
        }
        #endregion
    }
}