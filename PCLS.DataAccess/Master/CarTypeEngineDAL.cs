﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarEngineDAL : OracleConnectionDAL
    {
        public CarEngineDAL()
        {
            InitializeComponent();
        }

        public CarEngineDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarEngineSelectDAL(string Condition)
        {
            string Query = cmdCarEngineSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarEngineSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarEngineSelect, "cmdCarEngineSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarEngineSelect.CommandText = Query;
            }
        }

        public void CarEngineAddDAL(int TYPEENGINEID, string TYPEENGINENAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarEngineAdd.CommandType = CommandType.StoredProcedure;
                cmdCarEngineAdd.CommandText = "USP_M_TYPEENGINE";

                cmdCarEngineAdd.Parameters["I_TYPEENGINEID"].Value = TYPEENGINEID;
                cmdCarEngineAdd.Parameters["I_TYPEENGINENAME"].Value = TYPEENGINENAME;
                cmdCarEngineAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarEngineAdd.Parameters["I_CREATEBY"].Value = CREATEBY;

                dbManager.ExecuteNonQuery(cmdCarEngineAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarEngineDAL _instance;
        public static CarEngineDAL Instance
        {
            get
            {
                _instance = new CarEngineDAL();
                return _instance;
            }
        }
        #endregion
    }
}