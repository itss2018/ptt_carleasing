﻿namespace PCLS.DataAccess.Master
{
    partial class CarEngineDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarEngineSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarEngineAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdCarEngineSelect
            // 
            this.cmdCarEngineSelect.CommandText = "SELECT * FROM VW_M_TYPEENGINE_SELECT WHERE 1=1";
            this.cmdCarEngineSelect.Connection = this.OracleConn;
            // 
            // cmdCarEngineAdd
            // 
            this.cmdCarEngineAdd.Connection = this.OracleConn;
            this.cmdCarEngineAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TYPEENGINEID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TYPEENGINENAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarEngineSelect;
        private System.Data.OracleClient.OracleCommand cmdCarEngineAdd;
    }
}
