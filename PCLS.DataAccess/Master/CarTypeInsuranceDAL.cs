﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeInsuranceDAL : OracleConnectionDAL
    {
        public CarTypeInsuranceDAL()
        {
            InitializeComponent();
        }

        public CarTypeInsuranceDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeInsuranceSelectDAL(string Condition)
        {
            string Query = cmdCarTypeInsuranceSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeInsuranceSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeInsuranceSelect, "cmdCarTypeInsuranceSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeInsuranceSelect.CommandText = Query;
            }
        }

        public void CarTypeInsuranceAddDAL(int TYPEINSURANCEID, string TYPEINSURANCENAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeInsuranceAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeInsuranceAdd.CommandText = "USP_M_TYPEINSURANCE";

                cmdCarTypeInsuranceAdd.Parameters["I_TYPEINSURANCEID"].Value = TYPEINSURANCEID;
                cmdCarTypeInsuranceAdd.Parameters["I_TYPEINSURANCENAME"].Value = TYPEINSURANCENAME;
                cmdCarTypeInsuranceAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeInsuranceAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeInsuranceAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeInsuranceAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeInsuranceDAL _instance;
        public static CarTypeInsuranceDAL Instance
        {
            get
            {
                _instance = new CarTypeInsuranceDAL();
                return _instance;
            }
        }
        #endregion
    }
}