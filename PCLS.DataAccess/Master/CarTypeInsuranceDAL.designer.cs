﻿namespace PCLS.DataAccess.Master
{
    partial class CarTypeInsuranceDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarTypeInsuranceSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarTypeInsuranceAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTypeInsuranceSelect
            // 
            this.cmdCarTypeInsuranceSelect.CommandText = "SELECT * FROM VW_M_TYPEINSURANCE_SELECT WHERE 1=1";
            this.cmdCarTypeInsuranceSelect.Connection = this.OracleConn;
            // 
            // cmdTypeInsuranceAdd
            // 
            this.cmdCarTypeInsuranceAdd.Connection = this.OracleConn;
            this.cmdCarTypeInsuranceAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TYPEINSURANCEID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TYPEINSURANCENAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarTypeInsuranceSelect;
        private System.Data.OracleClient.OracleCommand cmdCarTypeInsuranceAdd;
    }
}
