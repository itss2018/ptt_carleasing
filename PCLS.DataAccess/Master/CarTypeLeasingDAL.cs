﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeLeasingDAL : OracleConnectionDAL
    {
        public CarTypeLeasingDAL()
        {
            InitializeComponent();
        }

        public CarTypeLeasingDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeLeasingSelectDAL(string Condition)
        {
            string Query = cmdCarTypeLeasingSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeLeasingSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeLeasingSelect, "cmdCarTypeLeasingSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeLeasingSelect.CommandText = Query;
            }
        }

        public void CarTypeLeasingAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeLeasingAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeLeasingAdd.CommandText = "USP_M_PAYTYPE";

                cmdCarTypeLeasingAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarTypeLeasingAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarTypeLeasingAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarTypeLeasingAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarTypeLeasingAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarTypeLeasingAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarTypeLeasingAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarTypeLeasingAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarTypeLeasingAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeLeasingAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeLeasingAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeLeasingAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeLeasingDAL _instance;
        public static CarTypeLeasingDAL Instance
        {
            get
            {
                _instance = new CarTypeLeasingDAL();
                return _instance;
            }
        }
        #endregion
    }
}