﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypePayDAL : OracleConnectionDAL
    {
        public CarTypePayDAL()
        {
            InitializeComponent();
        }

        public CarTypePayDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypePaySelectDAL(string Condition)
        {
            string Query = cmdCarTypePaySelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypePaySelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypePaySelect, "cmdCarTypePaySelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypePaySelect.CommandText = Query;
            }
        }

        public void CarTypePayAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypePayAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypePayAdd.CommandText = "USP_M_SUPPLYTYPE";

                cmdCarTypePayAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarTypePayAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarTypePayAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarTypePayAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarTypePayAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarTypePayAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarTypePayAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarTypePayAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarTypePayAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypePayAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypePayAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypePayAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypePayDAL _instance;
        public static CarTypePayDAL Instance
        {
            get
            {
                _instance = new CarTypePayDAL();
                return _instance;
            }
        }
        #endregion
    }
}