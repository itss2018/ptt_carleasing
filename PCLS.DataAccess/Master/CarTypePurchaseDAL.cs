﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypePurchaseDAL : OracleConnectionDAL
    {
        public CarTypePurchaseDAL()
        {
            InitializeComponent();
        }

        public CarTypePurchaseDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypePurchaseSelectDAL(string Condition)
        {
            string Query = cmdCarTypePurchaseSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypePurchaseSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypePurchaseSelect, "cmdCarTypePurchaseSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypePurchaseSelect.CommandText = Query;
            }
        }

        public void CarTypePurchaseAddDAL(int TYPEPURCHASEID, string TYPEPURCHASENAME, int TYPESALEID, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypePurchaseAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypePurchaseAdd.CommandText = "USP_M_TYPEPURCHASE";

                cmdCarTypePurchaseAdd.Parameters["I_TYPEPURCHASEID"].Value = TYPEPURCHASEID;
                cmdCarTypePurchaseAdd.Parameters["I_TYPEPURCHASENAME"].Value = TYPEPURCHASENAME;
                cmdCarTypePurchaseAdd.Parameters["I_TYPESALEID"].Value = TYPESALEID;
                cmdCarTypePurchaseAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypePurchaseAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypePurchaseAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypePurchaseAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypePurchaseDAL _instance;
        public static CarTypePurchaseDAL Instance
        {
            get
            {
                _instance = new CarTypePurchaseDAL();
                return _instance;
            }
        }
        #endregion
    }
}