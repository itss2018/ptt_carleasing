﻿namespace PCLS.DataAccess.Master
{
    partial class CarTypePurchaseDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarTypePurchaseSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarTypePurchaseAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTypePurchaseSelect
            // 
            this.cmdCarTypePurchaseSelect.CommandText = "SELECT * FROM VW_M_TYPEPURCHASE_SELECT WHERE 1=1";
            this.cmdCarTypePurchaseSelect.Connection = this.OracleConn;
            // 
            // cmdTypePurchaseAdd
            // 
            this.cmdCarTypePurchaseAdd.Connection = this.OracleConn;
            this.cmdCarTypePurchaseAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TYPEPURCHASEID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TYPEPURCHASENAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_TYPESALEID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TYPESALENAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarTypePurchaseSelect;
        private System.Data.OracleClient.OracleCommand cmdCarTypePurchaseAdd;
    }
}
