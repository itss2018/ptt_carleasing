﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeRepairDAL : OracleConnectionDAL
    {
        public CarTypeRepairDAL()
        {
            InitializeComponent();
        }

        public CarTypeRepairDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeRepairSelectDAL(string Condition)
        {
            string Query = cmdCarTypeRepairSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeRepairSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeRepairSelect, "cmdCarTypeRepairSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeRepairSelect.CommandText = Query;
            }
        }

        public void CarTypeRepairAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeRepairAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeRepairAdd.CommandText = "USP_M_REPAIRTYPE";

                cmdCarTypeRepairAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarTypeRepairAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarTypeRepairAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarTypeRepairAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarTypeRepairAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarTypeRepairAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarTypeRepairAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarTypeRepairAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarTypeRepairAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeRepairAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeRepairAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeRepairAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeRepairDAL _instance;
        public static CarTypeRepairDAL Instance
        {
            get
            {
                _instance = new CarTypeRepairDAL();
                return _instance;
            }
        }
        #endregion
    }
}