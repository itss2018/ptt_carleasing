﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeSaleDAL : OracleConnectionDAL
    {
        public CarTypeSaleDAL()
        {
            InitializeComponent();
        }

        public CarTypeSaleDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeSaleSelectDAL(string Condition)
        {
            string Query = cmdCarTypeSaleSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeSaleSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeSaleSelect, "cmdCarTypeSaleSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeSaleSelect.CommandText = Query;
            }
        }

        public void CarTypeSaleAddDAL(int TYPESALEID, string TYPESALENAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeSaleAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeSaleAdd.CommandText = "USP_M_TYPESALE";

                cmdCarTypeSaleAdd.Parameters["I_TYPESALEID"].Value = TYPESALEID;
                cmdCarTypeSaleAdd.Parameters["I_TYPESALENAME"].Value = TYPESALENAME;
                cmdCarTypeSaleAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeSaleAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeSaleAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeSaleAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeSaleDAL _instance;
        public static CarTypeSaleDAL Instance
        {
            get
            {
                _instance = new CarTypeSaleDAL();
                return _instance;
            }
        }
        #endregion
    }
}