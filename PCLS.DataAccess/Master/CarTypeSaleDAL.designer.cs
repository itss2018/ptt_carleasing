﻿namespace PCLS.DataAccess.Master
{
    partial class CarTypeSaleDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCarTypeSaleSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCarTypeSaleAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTypeSaleSelect
            // 
            this.cmdCarTypeSaleSelect.CommandText = "SELECT * FROM VW_M_TYPESALE_SELECT WHERE 1=1";
            this.cmdCarTypeSaleSelect.Connection = this.OracleConn;
            // 
            // cmdTypeSaleAdd
            // 
            this.cmdCarTypeSaleAdd.Connection = this.OracleConn;
            this.cmdCarTypeSaleAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TYPESALEID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TYPESALENAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ISACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATEBY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ACTION", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCarTypeSaleSelect;
        private System.Data.OracleClient.OracleCommand cmdCarTypeSaleAdd;
    }
}
