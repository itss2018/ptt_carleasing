﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeSupplyDAL : OracleConnectionDAL
    {
        public CarTypeSupplyDAL()
        {
            InitializeComponent();
        }

        public CarTypeSupplyDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeSupplySelectDAL(string Condition)
        {
            string Query = cmdCarTypeSupplySelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeSupplySelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeSupplySelect, "cmdCarTypeSupplySelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeSupplySelect.CommandText = Query;
            }
        }

        public void CarTypeSupplyAddDAL(string TYPE, string CODE, string CODENAME, string CODEVALUE,int CODELEN,int NAMELEN, string VALID_STATUS, string SETBY, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeSupplyAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeSupplyAdd.CommandText = "USP_M_CARSOURCE";

                cmdCarTypeSupplyAdd.Parameters["I_TYPE"].Value = TYPE;
                cmdCarTypeSupplyAdd.Parameters["I_CODE"].Value = CODE;
                cmdCarTypeSupplyAdd.Parameters["I_CODENAME"].Value = CODENAME;
                cmdCarTypeSupplyAdd.Parameters["I_CODEVALUE"].Value = CODEVALUE;
                cmdCarTypeSupplyAdd.Parameters["I_CODELEN"].Value = CODELEN;
                cmdCarTypeSupplyAdd.Parameters["I_NAMELEN"].Value = NAMELEN;
                cmdCarTypeSupplyAdd.Parameters["I_VALID_STATUS"].Value = VALID_STATUS;
                cmdCarTypeSupplyAdd.Parameters["I_SETBY"].Value = SETBY;
                cmdCarTypeSupplyAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeSupplyAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeSupplyAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeSupplyAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeSupplyDAL _instance;
        public static CarTypeSupplyDAL Instance
        {
            get
            {
                _instance = new CarTypeSupplyDAL();
                return _instance;
            }
        }
        #endregion
    }
}