﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CarTypeTrafficTicketDAL : OracleConnectionDAL
    {
        public CarTypeTrafficTicketDAL()
        {
            InitializeComponent();
        }

        public CarTypeTrafficTicketDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CarTypeTrafficTicketSelectDAL(string Condition)
        {
            string Query = cmdCarTypeTrafficTicketSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdCarTypeTrafficTicketSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdCarTypeTrafficTicketSelect, "cmdCarTypeTrafficTicketSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdCarTypeTrafficTicketSelect.CommandText = Query;
            }
        }

        public void CarTypeTrafficTicketAddDAL(int TYPETRAFFICTICKETID, string TYPETRAFFICTICKETNAME, string ISACTIVE, string CREATEBY, int iACTION)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdCarTypeTrafficTicketAdd.CommandType = CommandType.StoredProcedure;
                cmdCarTypeTrafficTicketAdd.CommandText = "USP_M_TYPETRAFFICTICKET";

                cmdCarTypeTrafficTicketAdd.Parameters["I_TYPETRAFFICTICKETID"].Value = TYPETRAFFICTICKETID;
                cmdCarTypeTrafficTicketAdd.Parameters["I_TYPETRAFFICTICKETNAME"].Value = TYPETRAFFICTICKETNAME;
                cmdCarTypeTrafficTicketAdd.Parameters["I_ISACTIVE"].Value = ISACTIVE;
                cmdCarTypeTrafficTicketAdd.Parameters["I_CREATEBY"].Value = CREATEBY;
                cmdCarTypeTrafficTicketAdd.Parameters["I_ACTION"].Value = iACTION;

                dbManager.ExecuteNonQuery(cmdCarTypeTrafficTicketAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CarTypeTrafficTicketDAL _instance;
        public static CarTypeTrafficTicketDAL Instance
        {
            get
            {
                _instance = new CarTypeTrafficTicketDAL();
                return _instance;
            }
        }
        #endregion
    }
}