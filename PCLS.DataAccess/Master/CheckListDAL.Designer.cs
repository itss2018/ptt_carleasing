﻿namespace PCLS.DataAccess.Master
{
    partial class CheckListDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCheckListSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckListTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckListSelect2 = new System.Data.OracleClient.OracleCommand();
            this.cmdCheckTruckSys = new System.Data.OracleClient.OracleCommand();
            this.cmdGetSubjectLevel1 = new System.Data.OracleClient.OracleCommand();
            this.cmdGetSubjectLevel2and3 = new System.Data.OracleClient.OracleCommand();
            this.cmdGetSubjectByLevelID = new System.Data.OracleClient.OracleCommand();
            this.cmdGetTruckTypeByLevelID = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdCheckListSelect
            // 
            this.cmdCheckListSelect.Connection = this.OracleConn;
            this.cmdCheckListSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CHK_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_SYS_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdCheckListTypeSelect
            // 
            this.cmdCheckListTypeSelect.Connection = this.OracleConn;
            // 
            // cmdCheckListSelect2
            // 
            this.cmdCheckListSelect2.Connection = this.OracleConn;
            this.cmdCheckListSelect2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CHK_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_SYS_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SUBJECT", System.Data.OracleClient.OracleType.VarChar, 0, System.Data.ParameterDirection.Input, "", System.Data.DataRowVersion.Current, true, null),
            new System.Data.OracleClient.OracleParameter("I_ACTIVE", System.Data.OracleClient.OracleType.VarChar, 0, System.Data.ParameterDirection.Input, "", System.Data.DataRowVersion.Current, true, null)});
            // 
            // cmdCheckTruckSys
            // 
            this.cmdCheckTruckSys.Connection = this.OracleConn;
            this.cmdCheckTruckSys.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_LVL_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetSubjectLevel1
            // 
            this.cmdGetSubjectLevel1.Connection = this.OracleConn;
            this.cmdGetSubjectLevel1.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CHK_TYPE_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetSubjectLevel2and3
            // 
            this.cmdGetSubjectLevel2and3.Connection = this.OracleConn;
            this.cmdGetSubjectLevel2and3.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_LVL_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetSubjectByLevelID
            // 
            this.cmdGetSubjectByLevelID.Connection = this.OracleConn;
            this.cmdGetSubjectByLevelID.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_LVL_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdGetTruckTypeByLevelID
            // 
            this.cmdGetTruckTypeByLevelID.Connection = this.OracleConn;
            this.cmdGetTruckTypeByLevelID.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_LVL_ID", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdCheckListSelect;
        private System.Data.OracleClient.OracleCommand cmdCheckListTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdCheckListSelect2;
        private System.Data.OracleClient.OracleCommand cmdCheckTruckSys;
        private System.Data.OracleClient.OracleCommand cmdGetSubjectLevel1;
        private System.Data.OracleClient.OracleCommand cmdGetSubjectLevel2and3;
        private System.Data.OracleClient.OracleCommand cmdGetSubjectByLevelID;
        private System.Data.OracleClient.OracleCommand cmdGetTruckTypeByLevelID;
    }
}
