﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class CheckListDAL : OracleConnectionDAL
    {
        public CheckListDAL()
        {
            InitializeComponent();
        }

        public CheckListDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable CheckListSelectDAL(int CheckTypeID, int TruckSysID)
        {
            try
            {
                DataTable dt = new DataTable();

                cmdCheckListSelect.CommandText = @"SELECT *
                                                   FROM VW_M_CHK_SELECT
                                                   WHERE CHK_TYPE_ID=:I_CHK_TYPE_ID
                                                   AND TRUCK_SYS_ID=:I_TRUCK_SYS_ID";

                cmdCheckListSelect.Parameters["I_CHK_TYPE_ID"].Value = CheckTypeID;
                cmdCheckListSelect.Parameters["I_TRUCK_SYS_ID"].Value = TruckSysID;

                dt = dbManager.ExecuteDataTable(cmdCheckListSelect, "cmdCheckListSelect");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckListSelectDAL2(int CheckTypeID, int TruckSysID, string Subject, string Status)
        {
            try
            {
                DataTable dt = new DataTable();

                cmdCheckListSelect2.CommandText = @"SELECT *
                                                    FROM VW_M_CHK_SELECT2
                                                    WHERE CHK_TYPE_ID=:I_CHK_TYPE_ID
                                                    AND TRUCK_SYS_ID=:I_TRUCK_SYS_ID
                                                    AND (SUBJECT_LVL1 LIKE :I_SUBJECT OR SUBJECT_LVL2 LIKE :I_SUBJECT OR SUBJECT_LVL3 LIKE :I_SUBJECT)
                                                    AND (ACTIVE_LVL1 LIKE :I_ACTIVE OR ACTIVE_LVL2 LIKE :I_ACTIVE OR ACTIVE_LVL3 LIKE :I_ACTIVE)";
                 
                cmdCheckListSelect2.Parameters["I_CHK_TYPE_ID"].Value = CheckTypeID;
                cmdCheckListSelect2.Parameters["I_TRUCK_SYS_ID"].Value = TruckSysID;
                cmdCheckListSelect2.Parameters["I_SUBJECT"].Value = "%" + Subject + "%";
                cmdCheckListSelect2.Parameters["I_ACTIVE"].Value = "%" + Status + "%";

                dt = dbManager.ExecuteDataTable(cmdCheckListSelect2, "cmdCheckListSelect2");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckListTypeSelectDAL()
        {
            try
            {
                DataTable dt = new DataTable();

                cmdCheckListTypeSelect.CommandText = @"SELECT CHK_TYPE_ID, CHK_TYPE_NAME
                                                       FROM M_CHK_TYPE";

                dt = dbManager.ExecuteDataTable(cmdCheckListTypeSelect, "cmdCheckListTypeSelect");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable CheckTruckSysDAL(int Level, int LevelID)
        {
            try
            {
                DataTable dt = new DataTable();

                if (Level == 1)
                {
                    cmdCheckTruckSys.CommandText = @"SELECT TRUCK_SYS_ID
                                                     FROM M_CHK_TRUCK_LVL1
                                                     WHERE LVL1_ID =:I_LVL_ID";
                }
                else if (Level == 2)
                {
                    cmdCheckTruckSys.CommandText = @"SELECT TRUCK_SYS_ID
                                                     FROM M_CHK_TRUCK_LVL2
                                                     WHERE LVL2_ID =:I_LVL_ID";
                }
                else if (Level == 3)
                {
                    cmdCheckTruckSys.CommandText = @"SELECT TRUCK_SYS_ID
                                                     FROM M_CHK_TRUCK_LVL3
                                                     WHERE LVL3_ID =:I_LVL_ID";
                }

                cmdCheckTruckSys.Parameters["I_LVL_ID"].Value = LevelID;
                dt = dbManager.ExecuteDataTable(cmdCheckTruckSys, "cmdCheckTruckSys");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetSubjectLevel1DAL(int ChkTypeID)
        {
            try
            {
                DataTable dt = new DataTable();

                cmdGetSubjectLevel1.CommandText = @"SELECT LVL1_ID, SUBJECT, IS_ACTIVE
                                                    FROM M_CHK_LVL1
                                                    WHERE CHK_TYPE_ID =:I_CHK_TYPE_ID";

                cmdGetSubjectLevel1.Parameters["I_CHK_TYPE_ID"].Value = ChkTypeID;
                dt = dbManager.ExecuteDataTable(cmdGetSubjectLevel1, "cmdGetSubjectLevel1");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetSubjectLevel2and3DAL(int Level, int LevelID)
        {
            try
            {
                DataTable dt = new DataTable();

                if (Level == 2)
                {
                    cmdGetSubjectLevel2and3.CommandText = @"SELECT LVL2_ID, SUBJECT, IS_ACTIVE
                                                            FROM M_CHK_LVL2
                                                            WHERE LVL1_ID =:I_LVL_ID";
                }
                else if (Level == 3)
                {
                    cmdGetSubjectLevel2and3.CommandText = @"SELECT LVL3_ID, SUBJECT
                                                            FROM M_CHK_LVL3
                                                            WHERE LVL2_ID =:I_LVL_ID";
                }

                cmdGetSubjectLevel2and3.Parameters["I_LVL_ID"].Value = LevelID;
                dt = dbManager.ExecuteDataTable(cmdGetSubjectLevel2and3, "cmdGetSubjectLevel2and3");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetSubjectByLevelIDDAL(int Level, int LevelID)
        {
            try
            {
                DataTable dt = new DataTable();

                if (Level == 1)
                {
                    cmdGetSubjectByLevelID.CommandText = @"SELECT LVL1_ID, SUBJECT, IS_ACTIVE
                                                           FROM M_CHK_LVL1
                                                           WHERE LVL1_ID =:I_LVL_ID";
                }
                else if (Level == 2)
                {
                    cmdGetSubjectByLevelID.CommandText = @"SELECT LVL2_ID, SUBJECT, IS_ACTIVE
                                                           FROM M_CHK_LVL2
                                                           WHERE LVL2_ID =:I_LVL_ID";
                }
                else if (Level == 3)
                {
                    cmdGetSubjectByLevelID.CommandText = @"SELECT LVL3_ID, SUBJECT, IS_ACTIVE
                                                           FROM M_CHK_LVL3
                                                           WHERE LVL3_ID =:I_LVL_ID";
                }

                cmdGetSubjectByLevelID.Parameters["I_LVL_ID"].Value = LevelID;
                dt = dbManager.ExecuteDataTable(cmdGetSubjectByLevelID, "cmdGetSubjectByLevelID");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable GetTruckSystemByLevelIDDAL(int Level, int LevelID)
        {
            try
            {
                DataTable dt = new DataTable();

                if (Level == 1)
                {
                    cmdGetTruckTypeByLevelID.CommandText = @"SELECT TRUCK_SYS_ID
                                                             FROM M_CHK_TRUCK_LVL1
                                                             WHERE LVL1_ID =:I_LVL_ID";
                }
                else if (Level == 2)
                {
                    cmdGetTruckTypeByLevelID.CommandText = @"SELECT TRUCK_SYS_ID
                                                             FROM M_CHK_TRUCK_LVL2
                                                             WHERE LVL2_ID =:I_LVL_ID";
                }
                else if (Level == 3)
                {
                    cmdGetTruckTypeByLevelID.CommandText = @"SELECT TRUCK_SYS_ID
                                                             FROM M_CHK_TRUCK_LVL3
                                                             WHERE LVL3_ID =:I_LVL_ID";
                }

                cmdGetTruckTypeByLevelID.Parameters["I_LVL_ID"].Value = LevelID;
                dt = dbManager.ExecuteDataTable(cmdGetTruckTypeByLevelID, "cmdGetTruckTypeByLevelID");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static CheckListDAL _instance;
        public static CheckListDAL Instance
        {
            get
            {
                _instance = new CheckListDAL();
                return _instance;
            }
        }
        #endregion
    }
}