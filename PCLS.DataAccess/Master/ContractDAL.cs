﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.DataAccess.Master
{
    public partial class ContractDAL : OracleConnectionDAL
    {
        public ContractDAL()
        {
            InitializeComponent();
        }

        public ContractDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        #region Contract
        #region ContractSave
        public DataTable ContractSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataSet I_DATATABLE_T,
                                          DataSet I_DATATABLE_P,
                                          DataTable dtUpload
            )
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region CHECK
                cmdContract.CommandText = @"USP_M_CONTRACT_CHECK";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    if (dr.IsNull("MESSAGE"))
                    {
                        #region SAVE
                        cmdContract.CommandText = @"USP_M_CONTRACT_SAVE";

                        cmdContract.Parameters.Clear();
                        cmdContract.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_ID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                        });

                        cmdContract.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_USERID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = I_USERID,
                        });

                        cmdContract.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_DATATABLE",
                            IsNullable = true,
                            OracleType = OracleType.Clob,
                            Value = I_DATATABLE.GetXml(),
                        });
                        cmdContract.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Output,
                            ParameterName = "O_CUR",
                            IsNullable = true,
                            OracleType = OracleType.Cursor
                        });
                        #endregion

                        dbManager.BeginTransaction();
                        dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            I_ID = dt.Rows[0]["MESSAGE"] + string.Empty;

                            //#region USP_M_CONTRACT_T_SAVE
                            //cmdContract.CommandText = @"USP_M_CONTRACT_T_SAVE";

                            //cmdContract.Parameters.Clear();
                            //cmdContract.Parameters.Add(new OracleParameter()
                            //{
                            //    Direction = ParameterDirection.Input,
                            //    ParameterName = "I_CONTRACT_ID",
                            //    IsNullable = true,
                            //    OracleType = OracleType.Number,
                            //    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                            //});

                            //cmdContract.Parameters.Add(new OracleParameter()
                            //{
                            //    Direction = ParameterDirection.Input,
                            //    ParameterName = "I_USERID",
                            //    IsNullable = true,
                            //    OracleType = OracleType.Number,
                            //    Value = I_USERID,
                            //});

                            //cmdContract.Parameters.Add(new OracleParameter()
                            //{
                            //    Direction = ParameterDirection.Input,
                            //    ParameterName = "I_DATATABLE",
                            //    IsNullable = true,
                            //    OracleType = OracleType.Clob,
                            //    Value = I_DATATABLE_T.GetXml(),
                            //});
                            //cmdContract.Parameters.Add(new OracleParameter()
                            //{
                            //    Direction = ParameterDirection.Output,
                            //    ParameterName = "O_CUR",
                            //    IsNullable = true,
                            //    OracleType = OracleType.Cursor
                            //});
                            //dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                            //#endregion

                            //#region USP_M_CONTRACT_P_SAVE
                            //cmdContract.CommandText = @"USP_M_CONTRACT_P_SAVE";

                            //cmdContract.Parameters.Clear();
                            //cmdContract.Parameters.Add(new OracleParameter()
                            //{
                            //    Direction = ParameterDirection.Input,
                            //    ParameterName = "I_CONTRACT_ID",
                            //    IsNullable = true,
                            //    OracleType = OracleType.Number,
                            //    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                            //});

                            //cmdContract.Parameters.Add(new OracleParameter()
                            //{
                            //    Direction = ParameterDirection.Input,
                            //    ParameterName = "I_USERID",
                            //    IsNullable = true,
                            //    OracleType = OracleType.Number,
                            //    Value = I_USERID,
                            //});

                            //cmdContract.Parameters.Add(new OracleParameter()
                            //{
                            //    Direction = ParameterDirection.Input,
                            //    ParameterName = "I_DATATABLE",
                            //    IsNullable = true,
                            //    OracleType = OracleType.Clob,
                            //    Value = I_DATATABLE_P.GetXml(),
                            //});
                            //cmdContract.Parameters.Add(new OracleParameter()
                            //{
                            //    Direction = ParameterDirection.Output,
                            //    ParameterName = "O_CUR",
                            //    IsNullable = true,
                            //    OracleType = OracleType.Cursor
                            //});
                            //dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                            //#endregion

                            UploadDAL.Instance.UploadAdd(I_ID, "CONTRACT", dtUpload, I_USERID, dbManager);
                            dt.Rows[0]["MESSAGE"] = DBNull.Value;
                        }

                        dbManager.CommitTransaction();
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region ContractSelect
        public DataTable ContractSelect(string I_VENDOR_CODE,
                                          string I_SEARCH,
                                          string I_CACTIVE,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region USP_M_Contract_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_CODE,
                });
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COLUMN_NAME",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_COLUMN_NAME,
                });
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ASC",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ASC,
                });
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSelectByID
        public DataSet ContractSelectByID(string I_ID)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;
                cmdContract.CommandText = @"USP_M_CONTRACT_ID_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "CONTRACT");
                DataSet ds = new DataSet("DS");
                ds.Tables.Add(dt.Copy());

                #region USP_M_CONTRACT_T_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_T_SELECT";

                cmdContract.Parameters.Clear();

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? -1 : int.Parse(I_ID),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dt = dbManager.ExecuteDataTable(cmdContract, "CONTRACT_T");
                ds.Tables.Add(dt.Copy());
                #endregion

                #region USP_M_CONTRACT_P_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_P_SELECT";

                cmdContract.Parameters.Clear();

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? -1 : int.Parse(I_ID),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dt = dbManager.ExecuteDataTable(cmdContract, "CONTRACT_P");
                ds.Tables.Add(dt.Copy());
                #endregion

                #region USP_M_CONTRACT_T_CON_ID_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_T_CON_ID_SELECT";

                cmdContract.Parameters.Clear();

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? -1 : int.Parse(I_ID),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dt = dbManager.ExecuteDataTable(cmdContract, "CONTRACT_CON_T");
                ds.Tables.Add(dt.Copy());
                #endregion

                #region USP_M_CONTRACT_T_OUT_ID_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_T_OUT_ID_SELECT";

                cmdContract.Parameters.Clear();

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? -1 : int.Parse(I_ID),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dt = dbManager.ExecuteDataTable(cmdContract, "CONTRACT_OUT_T");
                ds.Tables.Add(dt.Copy());
                #endregion
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region ContractNo
        #region ContractNoSave
        public DataTable ContractNoSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region CHECK
                cmdContract.CommandText = @"USP_M_CONTRACT_NO_CHECK";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    if (dr.IsNull("MESSAGE"))
                    {
                        #region SAVE
                        cmdContract.CommandText = @"USP_M_CONTRACT_NO_SAVE";

                        cmdContract.Parameters.Clear();
                        cmdContract.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_ID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                        });

                        cmdContract.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_USERID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = I_USERID,
                        });

                        cmdContract.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_DATATABLE",
                            IsNullable = true,
                            OracleType = OracleType.Clob,
                            Value = I_DATATABLE.GetXml(),
                        });
                        cmdContract.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Output,
                            ParameterName = "O_CUR",
                            IsNullable = true,
                            OracleType = OracleType.Cursor
                        });
                        #endregion

                        dbManager.BeginTransaction();
                        dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                        dbManager.CommitTransaction();
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region ContractNoSelect
        public DataTable ContractNoSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_NO_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_NO_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractNoSelectByID
        public DataTable ContractNoSelectByID(
                                          string I_ID
                                          )
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;
                cmdContract.CommandText = @"USP_M_CONTRACT_NO_ID_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractNoNotSelect
        /// <summary>
        /// CONTRACT_NO ที่ยังไม่ถูกใช้งานใน M_CONTRACT
        /// </summary>
        /// <param name="I_SEARCH"></param>
        /// <param name="I_CACTIVE"></param>
        /// <returns></returns>
        public DataTable ContractNoNotSelect(string I_SEARCH, string I_IS_ACTIVE, string I_CONTRACT_NO_CODE)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_NO_NOT_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_NO_NOT_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_ACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_IS_ACTIVE) ? -1 : int.Parse(I_IS_ACTIVE),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_NO_CODE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CONTRACT_NO_CODE) ? -1 : int.Parse(I_CONTRACT_NO_CODE),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region ContractTypeSelect
        public DataTable ContractTypeSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_NO_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_TYPE_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractEmploymentSelect
        public DataTable ContractEmploymentSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_NO_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_EPM_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractSectorSelect
        public DataTable ContractSectorSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_NO_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_SECTOR_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractCategorySelect
        public DataTable ContractCategorySelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                cmdContract.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_NO_SELECT
                cmdContract.CommandText = @"USP_M_CONTRACT_CATEGORY_SELECT";

                cmdContract.Parameters.Clear();
                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });

                cmdContract.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContract, "cmdContract");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + Instance +
        private static ContractDAL _instance;
        public static ContractDAL Instance
        {
            get
            {
                _instance = new ContractDAL();
                return _instance;
            }
        }
        #endregion
    }
}