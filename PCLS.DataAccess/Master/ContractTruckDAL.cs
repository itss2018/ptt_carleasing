﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.DataAccess.Master
{
    public partial class ContractTruckDAL : OracleConnectionDAL
    {
        #region ContractTruckDAL
        public ContractTruckDAL()
        {
            InitializeComponent();
        }

        public ContractTruckDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region + Instance +
        private static ContractTruckDAL _instance;
        public static ContractTruckDAL Instance
        {
            get
            {
                _instance = new ContractTruckDAL();
                return _instance;
            }
        }
        #endregion

        #region ContractTrucRequest
        #region ContractTruckRequestSave
        public DataTable ContractTruckRequestSave(int I_USERID,
                                          int I_SYSTEM_ID,
                                          DataSet I_DATATABLE,
                                          DataSet I_DATATABLE_TRUCK
            )
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;

                #region CHECK
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_TRUCK_R_CHECK";

                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE_TRUCK",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE_TRUCK.GetXml(),
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                DataTable dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_CONTRACT_TRUCK_R_CHECK");
                #endregion

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    if (dr.IsNull("MESSAGE"))
                    {
                        #region SAVE
                        cmdContractTruck.CommandText = @"USP_M_CONTRACT_TRUCK_R_SAVE";

                        cmdContractTruck.Parameters.Clear();

                        cmdContractTruck.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_USERID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = I_USERID,
                        });
                        cmdContractTruck.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_SYSTEM_ID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = I_SYSTEM_ID,
                        });
                        cmdContractTruck.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_DATATABLE",
                            IsNullable = true,
                            OracleType = OracleType.Clob,
                            Value = I_DATATABLE.GetXml(),
                        });
                        cmdContractTruck.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_DATATABLE_TRUCK",
                            IsNullable = true,
                            OracleType = OracleType.Clob,
                            Value = I_DATATABLE_TRUCK.GetXml(),
                        });
                        cmdContractTruck.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Output,
                            ParameterName = "O_CUR",
                            IsNullable = true,
                            OracleType = OracleType.Cursor
                        });
                        #endregion

                        dbManager.BeginTransaction();
                        dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_CONTRACT_TRUCK_R_SAVE");

                        dbManager.CommitTransaction();
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region ContractTruckRequestSelect
        public DataTable ContractTruckRequestSelect(int I_SYSTEM_ID, string I_SEARCH,
                                          string I_VENDOR_CODE,
                                          string I_CONTRACT_ID,
                                          string I_REQUESTTYPE_CODE,
                                          string I_CACTIVE,
                                          string I_CONTRACT_DATE,
                                          string I_CONTRACT_DATE_TO,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_TRUCK_R_SELECT
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_TRUCK_R_SELECT";

                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_CODE,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CONTRACT_ID) ? -1 : int.Parse(I_CONTRACT_ID),
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REQUESTTYPE_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_REQUESTTYPE_CODE,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_DATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_CONTRACT_DATE,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_DATE_TO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_CONTRACT_DATE_TO,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_ACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COLUMN_NAME",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_COLUMN_NAME,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ASC",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ASC,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_CONTRACT_TRUCK_R_SELECT");
                #endregion

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractTruckRequestSelectByOrderID
        public DataTable ContractTruckRequestSelectByOrderID(
                                          int I_SYSTEM_ID,
                                          string I_ORDER_ID
                                          )
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_TR_ORID_SELECT";

                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ORDER_ID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ORDER_ID,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_CONTRACT_TR_ORID_SELECT");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region ContractTypeTruckSelect
        public DataTable ContractTypeTruckSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_NO_SELECT
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_TYPE_T_SELECT";

                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_ACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContractTruck, "cmdContract");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractRequestSelect
        public DataTable ContractRequestSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_NO_SELECT
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_R_T_SELECT";

                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_IS_ACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContractTruck, "cmdContract");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region TruckSelectByContract
        public DataTable TruckSelectByContract(string I_CONTRACT_ID,
                                          int I_SYSTEM_ID, string I_VENDOR_CODE)
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;

                #region USP_M_TRUCK_CONT_SELECT
                cmdContractTruck.CommandText = @"USP_M_TRUCK_CONT_SELECT";

                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_CONTRACT_ID) ? int.Parse(I_CONTRACT_ID) : -1,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_CODE,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_TRUCK_CONT_SELECT");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ContractTruckCarCountCheck
        public DataTable ContractTruckCarCountCheck(string I_CONTRACT_ID,
                                          int I_SYSTEM_ID)
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;

                #region USP_M_CONTRACT_T_CAR_COUNT
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_T_CAR_COUNT";

                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_CONTRACT_ID) ? int.Parse(I_CONTRACT_ID) : -1,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_CONTRACT_T_CAR_COUNT");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Interface SAP
        #region +TU+
        public DataSet ContractTruckAddRemoveTU(string I_CONTRACT_DATE, string I_ORDER_ID)
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_TRUCK_TU";
                cmdContractTruck.Parameters.Clear();

                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_DATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_CONTRACT_DATE) ? DateTime.Today.ToString("dd/MM/yyyy") : I_CONTRACT_DATE,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ORDER_ID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_ORDER_ID) ? string.Empty : I_ORDER_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_TU",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_INLET",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataSet ds = dbManager.ExecuteDataSet(cmdContractTruck);
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region +Veh+
        public DataTable ContractTruckAddRemoveVeh(string I_CONTRACT_DATE, string I_ORDER_ID)
        {
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_TRUCK_VEH";
                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_DATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_CONTRACT_DATE) ? DateTime.Today.ToString("dd/MM/yyyy") : I_CONTRACT_DATE,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ORDER_ID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_ORDER_ID) ? string.Empty : I_ORDER_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_CONTRACT_TRUCK_VEH");
                return dt;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region +LPG Sotred+
        public DataTable ContractTruckAdd(string I_CONTRACT_DATE, string I_ORDER_ID, int? I_TRUCK_MAPPING_ID)
        {
            DataTable dt;
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_TRUCK_SAVE";
                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_DATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_CONTRACT_DATE) ? DateTime.Today.ToString("dd/MM/yyyy") : I_CONTRACT_DATE,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ORDER_ID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_ORDER_ID) ? string.Empty : I_ORDER_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TRUCK_MAPPING_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_TRUCK_MAPPING_ID == null ? -1 : I_TRUCK_MAPPING_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_CONTRACT_TRUCK_SAVE");

            }
            catch (Exception ex)
            {
                dt = new DataTable();
                dt.Columns.Add("STATUS");
                dt.Columns.Add("MESSAGE");
                dt.Rows.Add("E");
                dt.Rows.Add(ex.Message);
                //throw new Exception(ex.Message);
            }
            return dt;
        }

        public DataTable ContractTruckRemove(string I_CONTRACT_DATE, string I_ORDER_ID, int? I_TRUCK_MAPPING_ID, int I_CONTRACT_ID_REMOVE)
        {
            DataTable dt;
            try
            {
                cmdContractTruck.CommandType = CommandType.StoredProcedure;
                cmdContractTruck.CommandText = @"USP_M_CONTRACT_TRUCK_REMOVE";
                cmdContractTruck.Parameters.Clear();
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_DATE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_CONTRACT_DATE) ? DateTime.Today.ToString("dd/MM/yyyy") : I_CONTRACT_DATE,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ORDER_ID",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_ORDER_ID) ? string.Empty : I_ORDER_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TRUCK_MAPPING_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_TRUCK_MAPPING_ID == null ? -1 : I_TRUCK_MAPPING_ID,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CONTRACT_ID_REMOVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_CONTRACT_ID_REMOVE,
                });
                cmdContractTruck.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                dt = dbManager.ExecuteDataTable(cmdContractTruck, "USP_M_CONTRACT_TRUCK_REMOVE");

            }
            catch (Exception ex)
            {
                dt = new DataTable();
                dt.Columns.Add("STATUS");
                dt.Columns.Add("MESSAGE");
                dt.Rows.Add("E");
                dt.Rows.Add(ex.Message);
                //throw new Exception(ex.Message);
            }
            return dt;
        }
        #endregion
        #endregion
    }
}
