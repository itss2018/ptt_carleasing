﻿namespace PCLS.DataAccess.Master
{
    partial class DriverDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdDriver = new System.Data.OracleClient.OracleCommand();
            this.cmdLogInterface = new System.Data.OracleClient.OracleCommand();
            this.cmdLogInterfaceTU = new System.Data.OracleClient.OracleCommand();
            this.cmdLogInterfaceVEH = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdLogInterface
            // 
            this.cmdLogInterface.CommandText = "SELECT * FROM VW_T_INTERFACE_DRIVER_SELECT WHERE 1= 1 ";
            // 
            // cmdLogInterfaceTU
            // 
            this.cmdLogInterfaceTU.CommandText = "SELECT * FROM VW_T_INTERFACE_HEAD_TU_SELECT WHERE 1= 1 ";
            // 
            // cmdLogInterfaceVEH
            // 
            this.cmdLogInterfaceVEH.CommandText = "SELECT * FROM VW_T_INTERFACE_HEAD_VEH_SELECT WHERE 1= 1 ";

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdDriver;
        private System.Data.OracleClient.OracleCommand cmdLogInterface;
        private System.Data.OracleClient.OracleCommand cmdLogInterfaceTU;
        private System.Data.OracleClient.OracleCommand cmdLogInterfaceVEH;
    }
}
