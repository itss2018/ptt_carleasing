﻿using dbManager;
using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;

namespace PCLS.DataAccess.Master
{
    public partial class DriverDAL : OracleConnectionDAL
    {
        #region DriverDAL
        public DriverDAL()
        {
            InitializeComponent();
        }

        public DriverDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region + Instance +
        private static DriverDAL _instance;
        public static DriverDAL Instance
        {
            get
            {
                _instance = new DriverDAL();
                return _instance;
            }
        }
        #endregion

        #region DriverRequest
        #region DriverRequestSelect
        public DataTable DriverRequestSelect(
                                          int I_SYSTEM_ID,
                                          string I_VENDOR_CODE,
                                          string I_SEARCH,
                                          string I_DATEFROM,
                                          string I_DATETO,
                                          string I_STATUS,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_REQUEST_SELECT";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_CODE,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATEFROM",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DATEFROM,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATETO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DATETO,
                });

                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_STATUS) ? "-1" : I_STATUS,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COLUMN_NAME",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_COLUMN_NAME,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ASC",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ASC,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverRequestSelectByID
        public DataTable DriverRequestSelectByID(int I_SYSTEM_ID, string I_ID)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_REQUEST_ID_SELECT";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });

                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverRequestSave
        public DataTable DriverRequestSave(string I_ID,
                                          int I_SYSTEM_ID,
                                          int I_USERID,
                                          string I_VENDOR_CODE,
                                          int I_STATUS_TYPE,
                                          int I_STATUS,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload,
                                          DataSet dsLog,
                                          DataSet dsLogDetail)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_REQUEST_SAVE";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_CODE,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_STATUS_TYPE,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_STATUS,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");
                UploadDAL.Instance.UploadAdd(dt.Rows[0]["MESSAGE"] + string.Empty, "DRIVER", dtUpload, I_USERID, dbManager);
                DriverHistorySave(dbManager, I_USERID, dsLog, dsLogDetail);
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region DriverRequestStatusUpdate
        public DataTable DriverRequestStatusUpdate(string I_ID,
                                          int I_SYSTEM_ID,
                                          string I_REMARK,
                                          int I_STATUS_TYPE,
                                          int I_STATUS,
                                          int I_USERID,
                                          DataSet dsLog,
                                          DataSet dsLogDetail)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_R_STATUS_SAVE";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });

                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REMARK",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_REMARK,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_STATUS_TYPE,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_STATUS,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");
                DriverHistorySave(dbManager, I_USERID, dsLog, dsLogDetail);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region DriverCheckCardNull
        public DataTable DriverCheckCardNull(string I_ID, string I_DATA)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_UPDATE_CARD";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATA",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DATA,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverUpdateCardByID
        public DataTable DriverUpdateCardByID(string I_ID)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_UPD_CARD_EX";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region Driver
        #region DriverSelect
        public DataTable DriverSelect(
                                          int I_SYSTEM_ID,
                                          string I_VENDOR_CODE,
                                          string I_SEARCH,
                                          string I_DATEFROM,
                                          string I_DATETO,
                                          string I_STATUS,
                                          string I_EMPLOY,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_SELECT";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_CODE,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATEFROM",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DATEFROM,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATETO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DATETO,
                });

                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = string.IsNullOrEmpty(I_STATUS) ? "-1" : I_STATUS,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_EMPLOY",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_EMPLOY,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COLUMN_NAME",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_COLUMN_NAME,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ASC",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ASC,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public DataSet SelectLogInterfaceSapDAL(string Condition)
        {
            string Query = cmdLogInterface.CommandText;
            try
            {
                DataTable dtLog = new DataTable();
                cmdLogInterface.CommandText += Condition;
                dtLog = dbManager.ExecuteDataTable(cmdLogInterface, "cmdLogInterface");
                DataSet ds = new DataSet();
                ds.Tables.Add(dtLog.Copy());

                ds.Tables[0].TableName = "dtLog";

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdLogInterface.CommandText = Query;
            }
        }

        public DataSet SelectLogInterfaceSapTuDAL(string Condition)
        {
            string Query = cmdLogInterfaceTU.CommandText;
            try
            {
                DataTable dtLog2 = new DataTable();
                cmdLogInterfaceTU.CommandText += Condition;
                dtLog2 = dbManager.ExecuteDataTable(cmdLogInterfaceTU, "cmdLogInterfaceTU");
                DataSet ds = new DataSet();
                ds.Tables.Add(dtLog2.Copy());

                ds.Tables[0].TableName = "dtLog2";

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdLogInterface.CommandText = Query;
            }
        }

        public DataSet SelectLogInterfaceSapVehDAL(string Condition)
        {
            string Query = cmdLogInterfaceVEH.CommandText;
            try
            {
                DataTable dtLog3 = new DataTable();
                cmdLogInterfaceVEH.CommandText += Condition;
                dtLog3 = dbManager.ExecuteDataTable(cmdLogInterfaceVEH, "cmdLogInterfaceVEH");
                DataSet ds = new DataSet();
                ds.Tables.Add(dtLog3.Copy());

                ds.Tables[0].TableName = "dtLog3";

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdLogInterface.CommandText = Query;
            }
        }

        #region DriverSelectExpire
        public DataTable DriverSelectExpire(
                                          int I_SYSTEM_ID,
                                          string I_VENDOR_CODE,
                                          string I_SEARCH,
                                          string I_DATEFROM,
                                          string I_DATETO,
                                          string I_EMPLOY,
                                          string I_COLUMN_NAME,
                                          string I_ASC)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_SELECT_EXPIRE";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_CODE,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATEFROM",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DATEFROM,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATETO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DATETO,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_EMPLOY",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_EMPLOY,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_COLUMN_NAME",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_COLUMN_NAME,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ASC",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ASC,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverSelectByID
        public DataTable DriverSelectByID(int I_SYSTEM_ID, string I_ID)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_ID_SELECT";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });

                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverSelectByCardNo
        public DataTable DriverSelectByCardNo(
                                          int I_SYSTEM_ID,
                                          string I_CARD_NO
                                          , int I_VENDOR_ID
                                          )
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_CARD_NO_SELECT";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CARD_NO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_CARD_NO,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_VENDOR_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverRequestSelectByCardNo
        public DataTable DriverRequestSelectByCardNo(
                                          int I_SYSTEM_ID,
                                          string I_CARD_NO
                                          )
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_R_CARDNO_SELECT";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CARD_NO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_CARD_NO,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverSave
        public DataTable DriverSave(string I_ID,
                                          string I_REQUEST_ID,
                                          int I_SYSTEM_ID,
                                          int I_USERID,
                                          string I_VENDOR_CODE,
                                          string I_DRIVER_CODE_SAP,
                                          string I_STATUS,
                                          int I_STATUS_REQUEST,
                                          DataSet I_DATATABLE,
                                          DataTable dtUpload,
                                          DataSet dsLog,
                                          DataSet dsLogDetail)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_SAVE";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_REQUEST_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_REQUEST_ID) ? 0 : int.Parse(I_REQUEST_ID),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_VENDOR_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_VENDOR_CODE,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DRIVER_CODE_SAP",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_DRIVER_CODE_SAP,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = !string.IsNullOrEmpty(I_STATUS) ? int.Parse(I_STATUS) : 0,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_STATUS_REQUEST",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_STATUS_REQUEST,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");
                UploadDAL.Instance.UploadAdd(dt.Rows[0]["MESSAGE"] + string.Empty, "DRIVER", dtUpload, I_USERID, dbManager);
                DriverHistorySave(dbManager, I_USERID, dsLog, dsLogDetail);
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region DriverEmployUpdate
        public DataTable DriverEmployUpdate(string I_ID,
                                          int I_SYSTEM_ID,
                                          int I_USERID,
                                          DataSet dsLog,
                                          DataSet dsLogDetail
                                          )
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                cmdDriver.CommandText = @"USP_M_DRIVER_EMPLOY_UPDATE";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });

                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SYSTEM_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_SYSTEM_ID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "cmdDriver");
                DriverHistorySave(dbManager, I_USERID, dsLog, dsLogDetail);
                dbManager.CommitTransaction();
                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
        #endregion

        #region DriverHistory
        #region DriverHistorySelect
        public DataSet DriverHistorySelect(
                                          string I_CARD_NO)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                #region USP_M_DRIVER_HISTORY_SELECT
                cmdDriver.CommandText = @"USP_M_DRIVER_HISTORY_SELECT";
                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CARD_NO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_CARD_NO,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdDriver, "DRIVER_H");
                DataSet ds = new DataSet();
                ds.Tables.Add(dt.Copy());
                #endregion

                #region USP_M_DRIVER_H_D_SELECT
                cmdDriver.CommandText = @"USP_M_DRIVER_H_D_SELECT";
                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CARD_NO",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_CARD_NO,
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dt = dbManager.ExecuteDataTable(cmdDriver, "DRIVER_H_D");
                ds.Tables.Add(dt.Copy());
                #endregion

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region DriverHistorySave
        public DataTable DriverHistorySave(DBManager db, int I_USERID,
                                          DataSet I_DATATABLE,
                                          DataSet I_DATATABLE_DETAIL)
        {
            try
            {
                cmdDriver.CommandType = CommandType.StoredProcedure;
                #region USP_M_DRIVER_HISTORY_SAVE
                cmdDriver.CommandText = @"USP_M_DRIVER_HISTORY_SAVE";

                cmdDriver.Parameters.Clear();
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });

                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdDriver.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = db.ExecuteDataTable(cmdDriver, "cmdDriver");
                #endregion

                if (dt != null && dt.Rows.Count > 0 && I_DATATABLE_DETAIL != null)
                {
                    int id = int.Parse(dt.Rows[0]["MESSAGE"] + string.Empty);
                    #region USP_M_DRIVER_H_D_SAVE
                    cmdDriver.CommandText = @"USP_M_DRIVER_H_D_SAVE";

                    cmdDriver.Parameters.Clear();
                    cmdDriver.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_DRIVER_HISTORY_ID",
                        IsNullable = true,
                        OracleType = OracleType.Number,
                        Value = id,
                    });
                    cmdDriver.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_USERID",
                        IsNullable = true,
                        OracleType = OracleType.Number,
                        Value = I_USERID,
                    });
                    cmdDriver.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_DATATABLE",
                        IsNullable = true,
                        OracleType = OracleType.Clob,
                        Value = I_DATATABLE_DETAIL.GetXml(),
                    });
                    cmdDriver.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });

                    dt = db.ExecuteDataTable(cmdDriver, "cmdDriver");
                    #endregion
                }

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
    }
}
