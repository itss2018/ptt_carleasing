﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class FileFormatTypeDAL : OracleConnectionDAL
    {
        public FileFormatTypeDAL()
        {
            InitializeComponent();
        }

        public FileFormatTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable FileFormatTypeSelectDAL(string Condition)
        {
            string Query = cmdFileFormatTypeSelect.CommandText;
            try
            {
                cmdFileFormatTypeSelect.CommandText = @"SELECT * FROM VW_M_FILE_FORMAT_TYPE_SELECT WHERE 1=1";

                cmdFileFormatTypeSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdFileFormatTypeSelect, "cmdFileFormatTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdFileFormatTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static FileFormatTypeDAL _instance;
        public static FileFormatTypeDAL Instance
        {
            get
            {
                _instance = new FileFormatTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}