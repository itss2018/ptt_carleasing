﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class FillTypeDAL : OracleConnectionDAL
    {
        public FillTypeDAL()
        {
            InitializeComponent();
        }

        public FillTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable FillTypeSelectDAL(string Condition)
        {
            string Query = cmdFillTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdFillTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdFillTypeSelect, "cmdFillTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdFillTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static FillTypeDAL _instance;
        public static FillTypeDAL Instance
        {
            get
            {
                _instance = new FillTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}