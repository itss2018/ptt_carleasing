﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class FormulaDAL : OracleConnectionDAL
    {
        public FormulaDAL()
        {
            InitializeComponent();
        }

        public FormulaDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable FormulaSelectDAL(string Condition)
        {
            string Query = cmdFormulaSelect.CommandText;
            try
            {
                cmdFormulaSelect.CommandText = @"SELECT * FROM VW_M_FORMULA_SELECT WHERE 1=1";

                cmdFormulaSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdFormulaSelect, "cmdFormulaSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdFormulaSelect.CommandText = Query;
            }
        }

        public void FormulaAddDAL(int FormulaID, string FormulaName, string Remark, string IsActive, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdFormulaAdd.CommandType = CommandType.StoredProcedure;
                cmdFormulaAdd.CommandText = "USP_M_FORMULA";

                cmdFormulaAdd.Parameters["I_FORMULA_ID"].Value = FormulaID;
                cmdFormulaAdd.Parameters["I_FORMULA_NAME"].Value = FormulaName;
                cmdFormulaAdd.Parameters["I_REMARK"].Value = Remark;
                cmdFormulaAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdFormulaAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdFormulaAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static FormulaDAL _instance;
        public static FormulaDAL Instance
        {
            get
            {
                _instance = new FormulaDAL();
                return _instance;
            }
        } 
	    #endregion
    }
}