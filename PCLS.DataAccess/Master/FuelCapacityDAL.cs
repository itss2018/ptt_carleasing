﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.DataAccess.Master
{
    public partial class FuelCapacityDAL : OracleConnectionDAL
    {
        public FuelCapacityDAL()
        {
            InitializeComponent();
        }

        public FuelCapacityDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable FuelCapacitySelectDAL(string Condition)
        {
            string Query = cmdFuelCapacitySelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdFuelCapacitySelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdFuelCapacitySelect, "cmdFuelCapacitySelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdFuelCapacitySelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static FuelCapacityDAL _instance;
        public static FuelCapacityDAL Instance
        {
            get
            {
                _instance = new FuelCapacityDAL();
                return _instance;
            }
        }
        #endregion
    }
}