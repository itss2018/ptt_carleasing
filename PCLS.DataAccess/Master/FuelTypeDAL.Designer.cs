﻿namespace PCLS.DataAccess.Master
{
    partial class FuelTypeDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdFuelTypeSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdFuelTypeSelect
            // 
            this.cmdFuelTypeSelect.CommandText = "SELECT * FROM VW_M_FUEL_TYPE_SELECT WHERE 1=1";
            this.cmdFuelTypeSelect.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdFuelTypeSelect;
    }
}
