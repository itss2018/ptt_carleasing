﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class FuelTypeDAL : OracleConnectionDAL
    {
        public FuelTypeDAL()
        {
            InitializeComponent();
        }

        public FuelTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable FuelTypeSelectDAL(string Condition)
        {
            string Query = cmdFuelTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdFuelTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdFuelTypeSelect, "cmdFuelTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdFuelTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static FuelTypeDAL _instance;
        public static FuelTypeDAL Instance
        {
            get
            {
                _instance = new FuelTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}