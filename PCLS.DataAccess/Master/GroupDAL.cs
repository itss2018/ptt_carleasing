﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.DataAccess.Master
{
    public partial class GroupDAL : OracleConnectionDAL
    {
        #region GroupDAL
        public GroupDAL()
        {
            InitializeComponent();
        }

        public GroupDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region + Instance +
        private static GroupDAL _instance;
        public static GroupDAL Instance
        {
            get
            {
                _instance = new GroupDAL();
                return _instance;
            }
        }
        #endregion

        #region Group
        #region GroupSave
        public DataTable GroupSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;

                #region CHECK
                cmdGroup.CommandText = @"USP_M_GROUP_CHECK";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                #endregion

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    if (dr.IsNull("MESSAGE"))
                    {
                        #region SAVE
                        cmdGroup.CommandText = @"USP_M_GROUP_SAVE";

                        cmdGroup.Parameters.Clear();
                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_ID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                        });

                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_USERID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = I_USERID,
                        });

                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_DATATABLE",
                            IsNullable = true,
                            OracleType = OracleType.Clob,
                            Value = I_DATATABLE.GetXml(),
                        });
                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Output,
                            ParameterName = "O_CUR",
                            IsNullable = true,
                            OracleType = OracleType.Cursor
                        });
                        #endregion

                        dbManager.BeginTransaction();
                        dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                        dbManager.CommitTransaction();
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GroupSelect
        public DataTable GroupSelect(string I_SEARCH,
                                          string I_CACTIVE, string I_GROUP_WORK_CODE)
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;

                #region USP_M_GROUP_SELECT
                cmdGroup.CommandText = @"USP_M_GROUP_SELECT";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_GROUP_WORK_CODE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_GROUP_WORK_CODE,
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupSelectByID
        public DataTable GroupSelectByID(
                                          string I_ID
                                          )
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;
                cmdGroup.CommandText = @"USP_M_GROUP_ID_SELECT";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region GroupWork
        #region GroupWorkSave
        public DataTable GroupWorkSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;

                #region CHECK
                cmdGroup.CommandText = @"USP_M_GROUP_W_CHECK";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                #endregion

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    if (dr.IsNull("MESSAGE"))
                    {
                        #region SAVE
                        cmdGroup.CommandText = @"USP_M_GROUP_W_SAVE";

                        cmdGroup.Parameters.Clear();
                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_ID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                        });

                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_USERID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = I_USERID,
                        });

                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_DATATABLE",
                            IsNullable = true,
                            OracleType = OracleType.Clob,
                            Value = I_DATATABLE.GetXml(),
                        });
                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Output,
                            ParameterName = "O_CUR",
                            IsNullable = true,
                            OracleType = OracleType.Cursor
                        });
                        #endregion

                        dbManager.BeginTransaction();
                        dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                        dbManager.CommitTransaction();
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GroupWorkSelect
        public DataTable GroupWorkSelect(string I_SEARCH,
                                          string I_CACTIVE)
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;

                #region USP_M_GROUP_W_SELECT
                cmdGroup.CommandText = @"USP_M_GROUP_W_SELECT";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_SEARCH,
                });

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_CACTIVE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_CACTIVE) ? -1 : int.Parse(I_CACTIVE),
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupWorkSelectByID
        public DataTable GroupWorkSelectByID(
                                          string I_ID
                                          )
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;
                cmdGroup.CommandText = @"USP_M_GROUP_W_ID_SELECT";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion

        #region GroupBind
        #region GroupBindSave
        public DataTable GroupBindSave(string I_ID,
                                          int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;

                #region CHECK
                cmdGroup.CommandText = @"USP_M_GROUP_B_CHECK";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                dbManager.Open();
                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                #endregion

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    if (dr.IsNull("MESSAGE"))
                    {
                        #region SAVE
                        cmdGroup.CommandText = @"USP_M_GROUP_B_SAVE";

                        cmdGroup.Parameters.Clear();
                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_ID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                        });

                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_USERID",
                            IsNullable = true,
                            OracleType = OracleType.Number,
                            Value = I_USERID,
                        });

                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Input,
                            ParameterName = "I_DATATABLE",
                            IsNullable = true,
                            OracleType = OracleType.Clob,
                            Value = I_DATATABLE.GetXml(),
                        });
                        cmdGroup.Parameters.Add(new OracleParameter()
                        {
                            Direction = ParameterDirection.Output,
                            ParameterName = "O_CUR",
                            IsNullable = true,
                            OracleType = OracleType.Cursor
                        });
                        #endregion

                        dbManager.BeginTransaction();
                        dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                        dbManager.CommitTransaction();
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GroupBindCancel
        public DataTable GroupBindCancel(int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;

                #region Cancel
                cmdGroup.CommandText = @"USP_M_GROUP_B_CANCEL";

                cmdGroup.Parameters.Clear();

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                dbManager.CommitTransaction();

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion

        #region GroupBindSelect
        public DataTable GroupBindSelect(string I_GROUPWORK,
                                          string I_GROUP)
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;

                #region USP_M_GROUP_B_SELECT
                cmdGroup.CommandText = @"USP_M_GROUP_B_SELECT";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_GROUPWORK",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_GROUPWORK,
                });

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_GROUP",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_GROUP,
                });
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");
                #endregion
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region GroupBindSelectByID
        public DataTable GroupBindSelectByID(
                                          string I_ID
                                          )
        {
            try
            {
                cmdGroup.CommandType = CommandType.StoredProcedure;
                cmdGroup.CommandText = @"USP_M_GROUP_B_ID_SELECT";

                cmdGroup.Parameters.Clear();
                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_ID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_ID) ? 0 : int.Parse(I_ID),
                });

                cmdGroup.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdGroup, "cmdGroup");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #endregion
    }
}
