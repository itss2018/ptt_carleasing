﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class InletDAL : OracleConnectionDAL
    {
        public InletDAL()
        {
            InitializeComponent();
        }

        public InletDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable InletSelectDAL(string Condition)
        {
            string Query = cmdInletSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdInletSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdInletSelect, "cmdInletSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdInletSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static InletDAL _instance;
        public static InletDAL Instance
        {
            get
            {
                _instance = new InletDAL();
                return _instance;
            }
        }
        #endregion
    }
}