﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class InletLevelDAL : OracleConnectionDAL
    {
        public InletLevelDAL()
        {
            InitializeComponent();
        }

        public InletLevelDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable InletLevelSelectDAL(string Condition)
        {
            string Query = cmdInletLevelSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdInletLevelSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdInletLevelSelect, "cmdInletLevelSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdInletLevelSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static InletLevelDAL _instance;
        public static InletLevelDAL Instance
        {
            get
            {
                _instance = new InletLevelDAL();
                return _instance;
            }
        }
        #endregion
    }
}