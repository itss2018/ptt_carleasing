﻿namespace PCLS.DataAccess.Master
{
    partial class InsuranceDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdInsuranceSelect = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdInsuranceSelect
            // 
            this.cmdInsuranceSelect.CommandText = "SELECT * FROM VW_M_INSURANCE_SELECT WHERE 1=1";
            this.cmdInsuranceSelect.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdInsuranceSelect;
    }
}
