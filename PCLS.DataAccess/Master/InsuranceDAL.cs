﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class InsuranceDAL : OracleConnectionDAL
    {
        public InsuranceDAL()
        {
            InitializeComponent();
        }

        public InsuranceDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable InsuranceSelectDAL(string Condition)
        {
            string Query = cmdInsuranceSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdInsuranceSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdInsuranceSelect, "cmdInsuranceSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdInsuranceSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static InsuranceDAL _instance;
        public static InsuranceDAL Instance
        {
            get
            {
                _instance = new InsuranceDAL();
                return _instance;
            }
        }
        #endregion
    }
}