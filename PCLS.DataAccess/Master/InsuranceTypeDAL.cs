﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class InsuranceTypeDAL : OracleConnectionDAL
    {
        public InsuranceTypeDAL()
        {
            InitializeComponent();
        }

        public InsuranceTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable InsuranceTypeSelectDAL(string Condition)
        {
            string Query = cmdInsuranceTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdInsuranceTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdInsuranceTypeSelect, "cmdInsuranceTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdInsuranceTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static InsuranceTypeDAL _instance;
        public static InsuranceTypeDAL Instance
        {
            get
            {
                _instance = new InsuranceTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}