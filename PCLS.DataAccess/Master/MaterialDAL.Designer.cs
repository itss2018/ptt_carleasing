﻿namespace PCLS.DataAccess.Master
{
    partial class MaterialDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdMaterialSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdMaterialAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdCustomerFormulaAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdMaterialAdd
            // 
            this.cmdMaterialAdd.Connection = this.OracleConn;
            this.cmdMaterialAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_MATERIAL_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CUSTOMER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MATERIAL_NAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_SHORT_NAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_MATERIAL_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdCustomerFormulaAdd
            // 
            this.cmdCustomerFormulaAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_CUSTOMER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MATERIAL_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_FORMULA_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REMARK", System.Data.OracleClient.OracleType.VarChar, 2000),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdMaterialSelect;
        private System.Data.OracleClient.OracleCommand cmdMaterialAdd;
        private System.Data.OracleClient.OracleCommand cmdCustomerFormulaAdd;
    }
}
