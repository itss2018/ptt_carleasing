﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class MaterialDAL : OracleConnectionDAL
    {
        public MaterialDAL()
        {
            InitializeComponent();
        }

        public MaterialDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable MaterialSelectDAL(string Condition)
        {
            string Query = cmdMaterialSelect.CommandText;
            try
            {
                cmdMaterialSelect.CommandText = @"SELECT * FROM VW_M_MATERIAL_SELECT WHERE 1=1";

                cmdMaterialSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdMaterialSelect, "cmdMaterialSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdMaterialSelect.CommandText = Query;
            }
        }

        public void MaterialAddDAL(int MaterialID, int CustomerID, string MaterialName, string ShortName, int MaterialTypeID, string IsActive, int CreateBy, DataTable dtFormula)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdMaterialAdd.CommandType = CommandType.StoredProcedure;
                cmdMaterialAdd.CommandText = "USP_M_MATERIAL";

                cmdMaterialAdd.Parameters["I_MATERIAL_ID"].Value = MaterialID;
                cmdMaterialAdd.Parameters["I_CUSTOMER_ID"].Value = CustomerID;
                cmdMaterialAdd.Parameters["I_MATERIAL_NAME"].Value = MaterialName;
                cmdMaterialAdd.Parameters["I_SHORT_NAME"].Value = ShortName;
                cmdMaterialAdd.Parameters["I_MATERIAL_TYPE_ID"].Value = MaterialTypeID;
                cmdMaterialAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdMaterialAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                DataTable dtResult = dbManager.ExecuteDataTable(cmdMaterialAdd, "cmdMaterialAdd");

                cmdCustomerFormulaAdd.CommandType = CommandType.StoredProcedure;
                cmdCustomerFormulaAdd.CommandText = "USP_M_CUSTOMER_FORMULA";

                for (int i = 0; i < dtFormula.Rows.Count; i++)
                {
                    cmdCustomerFormulaAdd.Parameters["I_CUSTOMER_ID"].Value = dtFormula.Rows[i]["CUSTOMER_ID"].ToString();
                    cmdCustomerFormulaAdd.Parameters["I_MATERIAL_ID"].Value = dtResult.Rows[0]["MATERIAL_ID"].ToString();
                    cmdCustomerFormulaAdd.Parameters["I_FORMULA_ID"].Value = dtFormula.Rows[i]["FORMULA_ID"].ToString();
                    cmdCustomerFormulaAdd.Parameters["I_REMARK"].Value = dtFormula.Rows[i]["REMARK"].ToString();
                    cmdCustomerFormulaAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                    dbManager.ExecuteNonQuery(cmdCustomerFormulaAdd);
                }

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static MaterialDAL _instance;
        public static MaterialDAL Instance
        {
            get
            {
                _instance = new MaterialDAL();
                return _instance;
            }
        }
        #endregion
    }
}