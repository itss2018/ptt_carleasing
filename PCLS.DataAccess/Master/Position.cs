﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class PositionDAL : OracleConnectionDAL
    {
        public PositionDAL()
        {
            InitializeComponent();
        }

        public PositionDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable PositionSelectDAL(string Condition)
        {
            string Query = cmdPositionSelect.CommandText;
            try
            {
                cmdPositionSelect.CommandText = @"SELECT * FROM VW_M_POSITION_SELECT WHERE 1=1";

                cmdPositionSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdPositionSelect, "cmdPositionSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdPositionSelect.CommandText = Query;
            }
        }

        public void PositionAddDAL(int PositionID, string PositionName, string IsActive, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdPositionAdd.CommandType = CommandType.StoredProcedure;
                cmdPositionAdd.CommandText = "USP_M_POSITION";

                cmdPositionAdd.Parameters["I_POSITION_ID"].Value = PositionID;
                cmdPositionAdd.Parameters["I_POSITION_NAME"].Value = PositionName;
                cmdPositionAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdPositionAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdPositionAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static PositionDAL _instance;
        public static PositionDAL Instance
        {
            get
            {
                _instance = new PositionDAL();
                return _instance;
            }
        }
        #endregion
    }
}