﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class PressureDAL : OracleConnectionDAL
    {
        public PressureDAL()
        {
            InitializeComponent();
        }

        public PressureDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable PressureSelectDAL(string Condition)
        {
            string Query = cmdPressureSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdPressureSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdPressureSelect, "cmdPressureSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdPressureSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static PressureDAL _instance;
        public static PressureDAL Instance
        {
            get
            {
                _instance = new PressureDAL();
                return _instance;
            }
        }
        #endregion
    }
}