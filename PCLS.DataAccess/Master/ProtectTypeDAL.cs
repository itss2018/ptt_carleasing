﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class ProtectTypeDAL : OracleConnectionDAL
    {
        public ProtectTypeDAL()
        {
            InitializeComponent();
        }

        public ProtectTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable ProtectTypeSelectDAL(string Condition)
        {
            string Query = cmdProtectTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdProtectTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdProtectTypeSelect, "cmdProtectTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdProtectTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static ProtectTypeDAL _instance;
        public static ProtectTypeDAL Instance
        {
            get
            {
                _instance = new ProtectTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}