﻿namespace PCLS.DataAccess.Master
{
    partial class ProvinceDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdProvinceSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdProvinceAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdProvinceAdd
            // 
            this.cmdProvinceAdd.Connection = this.OracleConn;
            this.cmdProvinceAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_PROVINCE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_PROVINCE_NAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_COUNTRY_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdProvinceSelect;
        private System.Data.OracleClient.OracleCommand cmdProvinceAdd;
    }
}
