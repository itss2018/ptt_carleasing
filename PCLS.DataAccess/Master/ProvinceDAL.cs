﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class ProvinceDAL : OracleConnectionDAL
    {
        public ProvinceDAL()
        {
            InitializeComponent();
        }

        public ProvinceDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable ProvinceSelectDAL(string Condition)
        {
            string Query = cmdProvinceSelect.CommandText;
            try
            {
                cmdProvinceSelect.CommandText = @"SELECT * FROM VW_M_PROVINCE_SELECT WHERE 1=1";

                cmdProvinceSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdProvinceSelect, "cmdProvinceSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdProvinceSelect.CommandText = Query;
            }
        }

        public void ProvinceAddDAL(int ProvinceID, string ProvinceName, int CountryID, string IsActive, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdProvinceAdd.CommandType = CommandType.StoredProcedure;
                cmdProvinceAdd.CommandText = "USP_M_PROVINCE";

                cmdProvinceAdd.Parameters["I_PROVINCE_ID"].Value = ProvinceID;
                cmdProvinceAdd.Parameters["I_PROVINCE_NAME"].Value = ProvinceName;
                cmdProvinceAdd.Parameters["I_COUNTRY_ID"].Value = CountryID;
                cmdProvinceAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdProvinceAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdProvinceAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static ProvinceDAL _instance;
        public static ProvinceDAL Instance
        {
            get
            {
                _instance = new ProvinceDAL();
                return _instance;
            }
        }
        #endregion
    }
}