﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.DataAccess.Master
{
    public partial class PumpTypeDAL : OracleConnectionDAL
    {
        public PumpTypeDAL()
        {
            InitializeComponent();
        }

        public PumpTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable PumpTypeSelectDAL(string Condition)
        {
            string Query = cmdPumpTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdPumpTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdPumpTypeSelect, "cmdPumpTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdPumpTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static PumpTypeDAL _instance;
        public static PumpTypeDAL Instance
        {
            get
            {
                _instance = new PumpTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}