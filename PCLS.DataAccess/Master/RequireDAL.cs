﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCLS.DataAccess.Master
{
    public partial class RequireDAL : OracleConnectionDAL
    {
        #region RequireDAL
        public RequireDAL()
        {
            InitializeComponent();
        }

        public RequireDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        #endregion

        #region + Instance +
        private static RequireDAL _instance;
        public static RequireDAL Instance
        {
            get
            {
                _instance = new RequireDAL();
                return _instance;
            }
        }
        #endregion

        #region FieldSelect
        public DataTable FieldSelect(string I_FIELD_TYPE)
        {
            try
            {
                cmdRequire.CommandType = CommandType.StoredProcedure;
                cmdRequire.CommandText = @"USP_M_FIELD_SELECT";

                cmdRequire.Parameters.Clear();
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_FIELD_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_FIELD_TYPE,
                });
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdRequire, "cmdRequire");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireFieldSelect
        public DataTable RequireFieldSelect(string I_FIELD_TYPE, string I_FIELD_REFERENCE)
        {
            try
            {
                cmdRequire.CommandType = CommandType.StoredProcedure;
                cmdRequire.CommandText = @"USP_M_FIELD_REQUIRE_SELECT";

                cmdRequire.Parameters.Clear();
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_FIELD_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_FIELD_TYPE,
                });
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_FIELD_REFERENCE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_FIELD_REFERENCE) ? (object)DBNull.Value : (object)int.Parse(I_FIELD_REFERENCE),
                });
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdRequire, "cmdRequire");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region RequireSave
        public DataTable RequireSave(int I_USERID,
                                          DataSet I_DATATABLE)
        {
            try
            {
                cmdRequire.CommandType = CommandType.StoredProcedure;

                #region USP_M_FIELD_REQUIRE_SAVE
                cmdRequire.CommandText = @"USP_M_FIELD_REQUIRE_SAVE";

                cmdRequire.Parameters.Clear();

                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_USERID",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = I_USERID,
                });

                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_DATATABLE",
                    IsNullable = true,
                    OracleType = OracleType.Clob,
                    Value = I_DATATABLE.GetXml(),
                });
                cmdRequire.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });
                #endregion
                dbManager.Open();
                dbManager.BeginTransaction();
                DataTable dt = dbManager.ExecuteDataTable(cmdRequire, "cmdRequire");
                dbManager.CommitTransaction();

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }
        #endregion
    }
}
