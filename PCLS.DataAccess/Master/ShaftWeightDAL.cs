﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class ShaftWeightDAL : OracleConnectionDAL
    {
        public ShaftWeightDAL()
        {
            InitializeComponent();
        }

        public ShaftWeightDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable ShaftWeightSelectDAL(string Condition)
        {
            string Query = cmdShaftWeightSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdShaftWeightSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdShaftWeightSelect, "cmdShaftWeightSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdShaftWeightSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static ShaftWeightDAL _instance;
        public static ShaftWeightDAL Instance
        {
            get
            {
                _instance = new ShaftWeightDAL();
                return _instance;
            }
        }
        #endregion
    }
}