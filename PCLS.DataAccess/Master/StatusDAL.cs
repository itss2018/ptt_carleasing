﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class StatusDAL : OracleConnectionDAL
    {
        public StatusDAL()
        {
            InitializeComponent();
        }

        public StatusDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable StatusSelectDAL(string Condition)
        {
            string Query = cmdStatusSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdStatusSelect.CommandText += Condition;

                cmdStatusSelect.CommandText += " ORDER BY ROW_ORDER, STATUS_ID";

                dt = dbManager.ExecuteDataTable(cmdStatusSelect, "cmdStatusSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdStatusSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static StatusDAL _instance;
        public static StatusDAL Instance
        {
            get
            {
                _instance = new StatusDAL();
                return _instance;
            }
        }
        #endregion
    }
}