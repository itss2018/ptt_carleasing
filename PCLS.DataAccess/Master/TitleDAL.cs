﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class TitleDAL : OracleConnectionDAL
    {
        public TitleDAL()
        {
            InitializeComponent();
        }

        public TitleDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable TitleSelectDAL(string Condition)
        {
            string Query = cmdTitleSelect.CommandText;
            try
            {
                cmdTitleSelect.CommandText = @"SELECT * FROM VW_M_TITLE_SELECT WHERE 1=1";

                cmdTitleSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdTitleSelect, "cmdTitleSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTitleSelect.CommandText = Query;
            }
        }

        public void TitleAddDAL(int TitleID, string TitleName, string IsActive, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdTitleAdd.CommandType = CommandType.StoredProcedure;
                cmdTitleAdd.CommandText = "USP_M_TITLE";

                cmdTitleAdd.Parameters["I_TITLE_ID"].Value = TitleID;
                cmdTitleAdd.Parameters["I_TITLE_NAME"].Value = TitleName;
                cmdTitleAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdTitleAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdTitleAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static TitleDAL _instance;
        public static TitleDAL Instance
        {
            get
            {
                _instance = new TitleDAL();
                return _instance;
            }
        }
        #endregion
    }
}