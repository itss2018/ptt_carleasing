﻿namespace PCLS.DataAccess.Master
{
    partial class TruckDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdTruckTempToTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckTempSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckTempStatusUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckInletAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckTempInletSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckInletDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckInletSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckMappingSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckHeadSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckMappingAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckTempMappingSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckTempMappingStatusUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckTempMappingToTruck = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckDetailSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckGraphicSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckStatusIvmsSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckUpdateStatus = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckSatusUpdate = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckSelectM = new System.Data.OracleClient.OracleCommand();
            this.cmdTruckInletSelectM = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdTruckTempToTruck
            // 
            this.cmdTruckTempToTruck.Connection = this.OracleConn;
            this.cmdTruckTempToTruck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TEMP_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckAdd
            // 
            this.cmdTruckAdd.Connection = this.OracleConn;
            // 
            // cmdTruckTempSelect
            // 
            this.cmdTruckTempSelect.CommandText = "SELECT * FROM VW_M_TRUCK_TEMP_SELECT WHERE 1=1";
            this.cmdTruckTempSelect.Connection = this.OracleConn;
            // 
            // cmdTruckTempStatusUpdate
            // 
            this.cmdTruckTempStatusUpdate.Connection = this.OracleConn;
            this.cmdTruckTempStatusUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TEMP_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_REMARK", System.Data.OracleClient.OracleType.VarChar, 4000, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, ""),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckInletAdd
            // 
            this.cmdTruckInletAdd.Connection = this.OracleConn;
            this.cmdTruckInletAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TEMP_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_NO", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_HIGH", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_LOW", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MEDIUM", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckTempInletSelect
            // 
            this.cmdTruckTempInletSelect.CommandText = "SELECT * FROM VW_M_TRUCK_TEMP_INLET_SELECT WHERE 1=1";
            this.cmdTruckTempInletSelect.Connection = this.OracleConn;
            // 
            // cmdTruckInletDelete
            // 
            this.cmdTruckInletDelete.Connection = this.OracleConn;
            this.cmdTruckInletDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TEMP_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_NO", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_HIGH", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_LOW", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MEDIUM", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckSelect
            // 
            this.cmdTruckSelect.CommandText = "SELECT * FROM VW_M_TRUCK_SELECT WHERE 1=1";
            this.cmdTruckSelect.Connection = this.OracleConn;
            // 
            // cmdTruckInletSelect
            // 
            this.cmdTruckInletSelect.CommandText = "SELECT * FROM VW_M_TRUCK_INLET_SELECT WHERE 1=1";
            this.cmdTruckInletSelect.Connection = this.OracleConn;
            // 
            // cmdTruckMappingSelect
            // 
            this.cmdTruckMappingSelect.Connection = this.OracleConn;
            // 
            // cmdTruckHeadSelect
            // 
            this.cmdTruckHeadSelect.Connection = this.OracleConn;
            this.cmdTruckHeadSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdTruckMappingAdd
            // 
            this.cmdTruckMappingAdd.Connection = this.OracleConn;
            this.cmdTruckMappingAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TEMP_MAPPING_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_ID_HEAD", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_ID_DETAIL", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TYPE_MAPPING_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdTruckTempMappingSelect
            // 
            this.cmdTruckTempMappingSelect.CommandText = "SELECT * FROM VW_M_TRUCK_TEMP_MAPPING_SELECT WHERE 1=1";
            this.cmdTruckTempMappingSelect.Connection = this.OracleConn;
            // 
            // cmdTruckTempMappingStatusUpdate
            // 
            this.cmdTruckTempMappingStatusUpdate.Connection = this.OracleConn;
            this.cmdTruckTempMappingStatusUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TEMP_MAPPING_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckTempMappingToTruck
            // 
            this.cmdTruckTempMappingToTruck.Connection = this.OracleConn;
            this.cmdTruckTempMappingToTruck.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TEMP_MAPPING_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckDetailSelect
            // 
            this.cmdTruckDetailSelect.Connection = this.OracleConn;
            this.cmdTruckDetailSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar)});
            // 
            // cmdTruckGraphicSelect
            // 
            this.cmdTruckGraphicSelect.Connection = this.OracleConn;
            this.cmdTruckGraphicSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_STEP", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_TRUCK_TABLE", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_SYSTEM_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});
            // 
            // cmdTruckStatusIvmsSelect
            // 
            this.cmdTruckStatusIvmsSelect.Connection = this.OracleConn;
            // 
            // cmdTruckUpdateStatus
            // 
            this.cmdTruckUpdateStatus.Connection = this.OracleConn;
            // 
            // cmdTruckSatusUpdate
            // 
            this.cmdTruckSatusUpdate.Connection = this.OracleConn;
            this.cmdTruckSatusUpdate.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPDATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdTruckSelectM
            // 
            this.cmdTruckSelectM.Connection = this.OracleConn;
            // 
            // cmdTruckInletSelectM
            // 
            this.cmdTruckInletSelectM.Connection = this.OracleConn;
            this.cmdTruckInletSelectM.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_TRUCK_ID", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdTruckTempToTruck;
        private System.Data.OracleClient.OracleCommand cmdTruckAdd;
        private System.Data.OracleClient.OracleCommand cmdTruckTempSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckTempStatusUpdate;
        private System.Data.OracleClient.OracleCommand cmdTruckInletAdd;
        private System.Data.OracleClient.OracleCommand cmdTruckTempInletSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckInletDelete;
        private System.Data.OracleClient.OracleCommand cmdTruckSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckInletSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckMappingSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckHeadSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckMappingAdd;
        private System.Data.OracleClient.OracleCommand cmdTruckTempMappingSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckTempMappingStatusUpdate;
        private System.Data.OracleClient.OracleCommand cmdTruckTempMappingToTruck;
        private System.Data.OracleClient.OracleCommand cmdTruckDetailSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckGraphicSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckStatusIvmsSelect;
        private System.Data.OracleClient.OracleCommand cmdTruckUpdateStatus;
        private System.Data.OracleClient.OracleCommand cmdTruckSatusUpdate;
        private System.Data.OracleClient.OracleCommand cmdTruckSelectM;
        private System.Data.OracleClient.OracleCommand cmdTruckInletSelectM;
    }
}
