﻿using PCLS.DataAccess.ConnectionDAL;
using PCLS.Entity.Transaction.Truck;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.Reflection;

namespace PCLS.DataAccess.Master
{
    public partial class TruckDAL : OracleConnectionDAL
    {
        public TruckDAL()
        {
            InitializeComponent();
        }

        public TruckDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable TruckAddDAL(TruckEntity truckEntity, DataTable dtUpload, DataTable dtInlet, string TruckTable)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdTruckAdd.CommandType = CommandType.StoredProcedure;
                cmdTruckAdd.CommandText = "USP_M_TRUCK";

                cmdTruckAdd.Parameters.Clear();
                foreach (PropertyInfo propertyInfo in truckEntity.GetType().GetProperties())
                {
                    string PropName = "I_" + propertyInfo.Name;
                    Type PropType = propertyInfo.PropertyType;
                    object PropValue = propertyInfo.GetValue(truckEntity);

                    if (PropValue == null || string.Equals(PropValue, string.Empty))
                        cmdTruckAdd.Parameters.Add(PropName, System.Data.OracleClient.OracleType.VarChar, 0, string.Empty).Value = DBNull.Value;
                    else
                        cmdTruckAdd.Parameters.Add(PropName, System.Data.OracleClient.OracleType.VarChar, 0, string.Empty).Value = PropValue;
                }
                cmdTruckAdd.Parameters.Add("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, string.Empty);
                cmdTruckAdd.Parameters["O_CUR"].Direction = ParameterDirection.Output;

                DataTable dtResult = dbManager.ExecuteDataTable(cmdTruckAdd, "cmdTruckAdd");

                if (string.Equals(dtResult.Rows[0]["FLAG"].ToString(), "S"))
                {
                    cmdTruckInletDelete.CommandText = @"DELETE FROM M_TRUCK_TEMP_INLET
                                                        WHERE TRUCK_TEMP_ID =:I_TRUCK_TEMP_ID";
                    cmdTruckInletDelete.Parameters["I_TRUCK_TEMP_ID"].Value = dtResult.Rows[0]["MESSAGE"].ToString();
                    dbManager.ExecuteNonQuery(cmdTruckInletDelete);

                    cmdTruckInletAdd.CommandText = @"INSERT INTO M_TRUCK_TEMP_INLET (TRUCK_TEMP_ID, NO, HIGH, LOW, MEDIUM)
                                                     VALUES (:I_TRUCK_TEMP_ID, :I_NO, :I_HIGH, :I_LOW, :I_MEDIUM)";

                    for (int i = 0; i < dtInlet.Rows.Count; i++)
                    {
                        cmdTruckInletAdd.Parameters["I_TRUCK_TEMP_ID"].Value = dtResult.Rows[0]["MESSAGE"].ToString();
                        cmdTruckInletAdd.Parameters["I_NO"].Value = dtInlet.Rows[i]["NO"].ToString();

                        if (string.Equals(dtInlet.Rows[i]["HIGH"].ToString(), string.Empty))
                            cmdTruckInletAdd.Parameters["I_HIGH"].Value = DBNull.Value;
                        else
                            cmdTruckInletAdd.Parameters["I_HIGH"].Value = dtInlet.Rows[i]["HIGH"].ToString();

                        if (string.Equals(dtInlet.Rows[i]["LOW"].ToString(), string.Empty))
                            cmdTruckInletAdd.Parameters["I_LOW"].Value = DBNull.Value;
                        else
                            cmdTruckInletAdd.Parameters["I_LOW"].Value = dtInlet.Rows[i]["LOW"].ToString();

                        if (string.Equals(dtInlet.Rows[i]["MEDIUM"].ToString(), string.Empty))
                            cmdTruckInletAdd.Parameters["I_MEDIUM"].Value = DBNull.Value;
                        else
                            cmdTruckInletAdd.Parameters["I_MEDIUM"].Value = dtInlet.Rows[i]["MEDIUM"].ToString();

                        dbManager.ExecuteNonQuery(cmdTruckInletAdd);
                    }

                    UploadDAL.Instance.UploadAdd(TruckTable + "_" + dtResult.Rows[0]["MESSAGE"].ToString(), "CREATE_TRUCK", dtUpload, int.Parse(truckEntity.USER_ID.ToString()), dbManager);
                }

                dbManager.CommitTransaction();

                return dtResult;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        public DataSet TruckTempSelectDAL(string Condition, string ConditionInlet)
        {
            string Query = cmdTruckTempSelect.CommandText;
            string QueryInlet = cmdTruckTempInletSelect.CommandText;
            try
            {
                DataTable dtTruck = new DataTable();
                cmdTruckTempSelect.CommandText += Condition;
                dtTruck = dbManager.ExecuteDataTable(cmdTruckTempSelect, "cmdTruckTempSelect");

                DataTable dtInlet = new DataTable();
                if (!string.Equals(ConditionInlet, string.Empty))
                {
                    cmdTruckTempInletSelect.CommandText += ConditionInlet;
                    dtInlet = dbManager.ExecuteDataTable(cmdTruckTempInletSelect, "cmdTruckTempInletSelect");
                }

                DataSet ds = new DataSet();
                ds.Tables.Add(dtTruck.Copy());
                ds.Tables.Add(dtInlet.Copy());

                ds.Tables[0].TableName = "dtTruck";
                ds.Tables[1].TableName = "dtInlet";

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckTempSelect.CommandText = Query;
                cmdTruckTempInletSelect.CommandText = QueryInlet;
            }
        }
        public DataTable TruckUpdateStatusDAL(string I_ID, string I_TYPE, string I_TRUCK_MAPPING)
        {
            try
            {
                cmdTruckUpdateStatus.CommandType = CommandType.StoredProcedure;
                cmdTruckUpdateStatus.CommandText = @"USP_M_TRUCK_UPDATE_EXPIRE";

                cmdTruckUpdateStatus.Parameters.Clear();
                cmdTruckUpdateStatus.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_SEARCH",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_ID,
                });
                cmdTruckUpdateStatus.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TYPE",
                    IsNullable = true,
                    OracleType = OracleType.Number,
                    Value = string.IsNullOrEmpty(I_TYPE) ? 0 : int.Parse(I_TYPE),
                });
                cmdTruckUpdateStatus.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Input,
                    ParameterName = "I_TRUCK_MAPPING",
                    IsNullable = true,
                    OracleType = OracleType.VarChar,
                    Value = I_TRUCK_MAPPING,
                });
                cmdTruckUpdateStatus.Parameters.Add(new OracleParameter()
                {
                    Direction = ParameterDirection.Output,
                    ParameterName = "O_CUR",
                    IsNullable = true,
                    OracleType = OracleType.Cursor
                });

                DataTable dt = dbManager.ExecuteDataTable(cmdTruckUpdateStatus, "cmdTruckUpdateStatus");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable TruckTempMappingSelectDAL(string Condition)
        {
            string Query = cmdTruckTempMappingSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdTruckTempMappingSelect.CommandText += Condition;
                dt = dbManager.ExecuteDataTable(cmdTruckTempMappingSelect, "cmdTruckTempMappingSelect");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckTempMappingSelect.CommandText = Query;
            }
        }

        public void TruckTempUpdateStatusDAL(int TruckTempID, string TypeUpdate, int UpdateBy, string Remark)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                if (string.Equals(TypeUpdate, "APPROVE"))
                {
                    cmdTruckTempStatusUpdate.CommandText = @"UPDATE M_TRUCK_TEMP
                                                             SET STATUS_ID = CASE WHEN STATUS_ID = 1 THEN 2 WHEN STATUS_ID = 5 THEN 6 END
                                                               , APPROVE_CREATE_BY =:I_UPDATE_BY
                                                               , APPROVE_CREATE_DATETIME = SYSDATE
                                                               , DOC_REMARK =:I_REMARK
                                                             WHERE TRUCK_TEMP_ID =:I_TRUCK_TEMP_ID";

                    cmdTruckTempToTruck.CommandText = "USP_M_TRUCK_TEMP_TO_TRUCK";
                    cmdTruckTempToTruck.CommandType = CommandType.StoredProcedure;

                    cmdTruckTempToTruck.Parameters["I_TRUCK_TEMP_ID"].Value = TruckTempID;
                    cmdTruckTempToTruck.Parameters["I_CREATE_BY"].Value = UpdateBy;

                    dbManager.ExecuteNonQuery(cmdTruckTempToTruck);
                }
                else if (string.Equals(TypeUpdate, "REJECT"))
                {
                    cmdTruckTempStatusUpdate.CommandText = @"UPDATE M_TRUCK_TEMP
                                                             SET STATUS_ID = CASE WHEN STATUS_ID = 1 THEN 3 WHEN STATUS_ID = 5 THEN 7 END
                                                               , REJECT_CREATE_BY =:I_UPDATE_BY
                                                               , REJECT_CREATE_DATETIME = SYSDATE
                                                               , DOC_REMARK =:I_REMARK
                                                             WHERE TRUCK_TEMP_ID =:I_TRUCK_TEMP_ID";
                }
                else if (string.Equals(TypeUpdate, "DOCUMENT"))
                {
                    cmdTruckTempStatusUpdate.CommandText = @"UPDATE M_TRUCK_TEMP
                                                             SET STATUS_ID = CASE WHEN STATUS_ID = 1 THEN 4 WHEN STATUS_ID = 5 THEN 8 END
                                                               , REQUEST_DOCUMENT_BY =:I_UPDATE_BY
                                                               , REQUEST_DOCUMENT_DATETIME = SYSDATE
                                                               , DOC_REMARK =:I_REMARK
                                                             WHERE TRUCK_TEMP_ID =:I_TRUCK_TEMP_ID";
                }

                cmdTruckTempStatusUpdate.Parameters["I_TRUCK_TEMP_ID"].Value = TruckTempID;
                cmdTruckTempStatusUpdate.Parameters["I_REMARK"].Value = Remark;
                cmdTruckTempStatusUpdate.Parameters["I_UPDATE_BY"].Value = UpdateBy;

                dbManager.ExecuteNonQuery(cmdTruckTempStatusUpdate);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void TruckSatusUpdateDAL(int TruckID, int UpdateBy, int ISACTIVE)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdTruckSatusUpdate.CommandText = "USP_M_TRUCK_STATUS_UPDATE";
                cmdTruckSatusUpdate.CommandType = CommandType.StoredProcedure;

                cmdTruckSatusUpdate.Parameters["I_TRUCK_ID"].Value = TruckID;
                cmdTruckSatusUpdate.Parameters["I_UPDATE_BY"].Value = UpdateBy;
                cmdTruckSatusUpdate.Parameters["I_IS_ACTIVE"].Value = ISACTIVE;

                dbManager.ExecuteNonQuery(cmdTruckSatusUpdate);


                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public void TruckTempMappingUpdateStatusDAL(int TruckTempMappingID, string TypeUpdate, int UpdateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                if (string.Equals(TypeUpdate, "APPROVE"))
                {
                    cmdTruckTempMappingStatusUpdate.CommandText = @"UPDATE M_TRUCK_TEMP_MAPPING
                                                                    SET STATUS_ID = CASE WHEN STATUS_ID = 1 THEN 2 WHEN STATUS_ID = 5 THEN 6 END
                                                                      , APPROVE_CREATE_BY =:I_UPDATE_BY
                                                                      , APPROVE_CREATE_DATETIME = SYSDATE
                                                                      WHERE TRUCK_TEMP_MAPPING_ID =:I_TRUCK_TEMP_MAPPING_ID";

                    cmdTruckTempMappingToTruck.CommandText = "USP_M_TRUCK_TEMP_MAP_TO_TRUCK";
                    cmdTruckTempMappingToTruck.CommandType = CommandType.StoredProcedure;

                    cmdTruckTempMappingToTruck.Parameters["I_TRUCK_TEMP_MAPPING_ID"].Value = TruckTempMappingID;
                    cmdTruckTempMappingToTruck.Parameters["I_CREATE_BY"].Value = UpdateBy;

                    dbManager.ExecuteNonQuery(cmdTruckTempMappingToTruck);
                }
                else if (string.Equals(TypeUpdate, "REJECT"))
                {
                    cmdTruckTempMappingStatusUpdate.CommandText = @"UPDATE M_TRUCK_TEMP_MAPPING
                                                                    SET STATUS_ID = CASE WHEN STATUS_ID = 1 THEN 3 WHEN STATUS_ID = 5 THEN 7 END
                                                                      , REJECT_CREATE_BY =:I_UPDATE_BY
                                                                      , REJECT_CREATE_DATETIME = SYSDATE
                                                                    WHERE TRUCK_TEMP_MAPPING_ID =:I_TRUCK_TEMP_MAPPING_ID";
                }

                cmdTruckTempMappingStatusUpdate.Parameters["I_TRUCK_TEMP_MAPPING_ID"].Value = TruckTempMappingID;
                cmdTruckTempMappingStatusUpdate.Parameters["I_UPDATE_BY"].Value = UpdateBy;

                dbManager.ExecuteNonQuery(cmdTruckTempMappingStatusUpdate);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataSet TruckSelectDAL(string Condition, string ConditionInlet, int? TruckID)
        {
            string Query = cmdTruckSelect.CommandText;
            string QueryInlet = cmdTruckInletSelect.CommandText;
            try
            {
                DataTable dtTruck = new DataTable();

                cmdTruckSelect.CommandText += Condition;

                dtTruck = dbManager.ExecuteDataTable(cmdTruckSelect, "cmdTruckSelect");

                DataTable dtInlet = new DataTable();
                if (!string.Equals(ConditionInlet, string.Empty))
                {
                    cmdTruckInletSelect.CommandText += ConditionInlet;
                    dtInlet = dbManager.ExecuteDataTable(cmdTruckInletSelect, "cmdTruckInletSelect");
                }

                DataTable dtIVMS = new DataTable();
                if (!string.IsNullOrEmpty(TruckID + string.Empty))
                {
                    cmdTruckStatusIvmsSelect.CommandText = "USP_M_TRUCK_STATUS_IVMS_SELECT";
                    cmdTruckStatusIvmsSelect.CommandType = CommandType.StoredProcedure;

                    cmdTruckStatusIvmsSelect.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Input,
                        ParameterName = "I_TRUCK_ID",
                        IsNullable = true,
                        OracleType = OracleType.Number,
                        Value = TruckID,
                    });

                    cmdTruckStatusIvmsSelect.Parameters.Add(new OracleParameter()
                    {
                        Direction = ParameterDirection.Output,
                        ParameterName = "O_CUR",
                        IsNullable = true,
                        OracleType = OracleType.Cursor
                    });

                    dtIVMS = dbManager.ExecuteDataTable(cmdTruckStatusIvmsSelect, "cmdTruckStatusIvmsSelect");
                }

                DataSet ds = new DataSet();
                ds.Tables.Add(dtTruck.Copy());
                ds.Tables.Add(dtInlet.Copy());
                ds.Tables.Add(dtIVMS.Copy());

                ds.Tables[0].TableName = "dtTruck";
                ds.Tables[1].TableName = "dtInlet";
                ds.Tables[2].TableName = "dtIVMS";

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckSelect.CommandText = Query;
                cmdTruckInletSelect.CommandText = QueryInlet;
            }
        }

        public DataTable TruckMappingSelectDAL(string Condition)
        {
            string Query = cmdTruckMappingSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();

                cmdTruckMappingSelect.CommandText = @"SELECT TRUCK_MAPPING.* 
                                                           , M_CONTRACT_TRUCK.TRUCK_MAPPING_ID AS CONTRACT_TRUCK_MAPPING_ID
                                                      FROM VW_M_TRUCK_MAPPING_SELECT TRUCK_MAPPING
                                                           LEFT JOIN M_CONTRACT_TRUCK ON TRUCK_MAPPING.TRUCK_MAPPING_ID = M_CONTRACT_TRUCK.TRUCK_MAPPING_ID
                                                      WHERE 1=1";

                cmdTruckMappingSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdTruckMappingSelect, "cmdTruckMappingSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckMappingSelect.CommandText = Query;
            }
        }

        public DataTable TruckHeadSelectDAL(int TruckTypeID, string VendorID, bool IncludeInContract)
        {
            string Query = cmdTruckHeadSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();

                cmdTruckHeadSelect.CommandText = @"SELECT TRUCK_ID, NVL(LICENSE, '-') AS LICENSE, NVL(CHASSIS, '-') AS CHASSIS
                                                         FROM M_TRUCK
                                                         WHERE TRUCK_TYPE_ID =:I_TRUCK_TYPE_ID AND VENDOR_ID = :I_VENDOR_ID";

                if (!IncludeInContract)
                    cmdTruckHeadSelect.CommandText += " AND TRUCK_ID NOT IN (SELECT TRUCK_ID_HEAD FROM M_TRUCK_MAPPING WHERE IS_ACTIVE = 1) AND TRUCK_ID NOT IN (SELECT TRUCK_ID_HEAD FROM M_TRUCK_TEMP_MAPPING WHERE STATUS_ID IN (1,5))";

                cmdTruckHeadSelect.Parameters["I_TRUCK_TYPE_ID"].Value = TruckTypeID;
                cmdTruckHeadSelect.Parameters["I_VENDOR_ID"].Value = VendorID;

                dt = dbManager.ExecuteDataTable(cmdTruckHeadSelect, "cmdTruckHeadDetailSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckHeadSelect.CommandText = Query;
            }
        }

        public DataTable TruckHeadSelectContractDAL(int TruckTypeID, string VendorID, bool IncludeInContract)
        {
            string Query = cmdTruckHeadSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();

                cmdTruckHeadSelect.CommandText = @"SELECT TRUCK_ID, NVL(LICENSE, '-') AS LICENSE, NVL(CHASSIS, '-') AS CHASSIS
                                                         FROM M_TRUCK
                                                         WHERE TRUCK_TYPE_ID =:I_TRUCK_TYPE_ID AND VENDOR_ID = :I_VENDOR_ID";

                if (!IncludeInContract)
                    cmdTruckHeadSelect.CommandText += " AND TRUCK_ID NOT IN (SELECT TRUCK_ID_HEAD FROM M_TRUCK_MAPPING WHERE IS_ACTIVE = 1) AND TRUCK_ID NOT IN (SELECT TRUCK_ID_HEAD FROM M_TRUCK_TEMP_MAPPING WHERE STATUS_ID IN (2,6,7))";

                cmdTruckHeadSelect.Parameters["I_TRUCK_TYPE_ID"].Value = TruckTypeID;
                cmdTruckHeadSelect.Parameters["I_VENDOR_ID"].Value = VendorID;

                dt = dbManager.ExecuteDataTable(cmdTruckHeadSelect, "cmdTruckHeadDetailSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckHeadSelect.CommandText = Query;
            }
        }

        public DataTable TruckDetailSelectDAL(int TruckTypeID, string VendorID, bool IncludeInContract)
        {
            string Query = cmdTruckDetailSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();

                cmdTruckDetailSelect.CommandText = @"SELECT TRUCK_ID, NVL(LICENSE, '-') AS LICENSE, NVL(CHASSIS, '-') AS CHASSIS
                                                     FROM M_TRUCK
                                                     WHERE TRUCK_TYPE_ID =:I_TRUCK_TYPE_ID AND VENDOR_ID =:I_VENDOR_ID";

                if (!IncludeInContract)
                    cmdTruckDetailSelect.CommandText += " AND TRUCK_ID NOT IN (SELECT TRUCK_ID_DETAIL FROM M_TRUCK_MAPPING WHERE IS_ACTIVE = 1) AND TRUCK_ID NOT IN (SELECT TRUCK_ID_DETAIL FROM M_TRUCK_TEMP_MAPPING WHERE STATUS_ID IN (1,5)) ";

                cmdTruckDetailSelect.Parameters["I_TRUCK_TYPE_ID"].Value = TruckTypeID;
                cmdTruckDetailSelect.Parameters["I_VENDOR_ID"].Value = VendorID;

                dt = dbManager.ExecuteDataTable(cmdTruckDetailSelect, "cmdTruckDetailSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckDetailSelect.CommandText = Query;
            }
        }

        public DataTable TruckDetailSelectContractDAL(int TruckTypeID, string VendorID, bool IncludeInContract)
        {
            string Query = cmdTruckDetailSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();

                cmdTruckDetailSelect.CommandText = @"SELECT TRUCK_ID, NVL(LICENSE, '-') AS LICENSE, NVL(CHASSIS, '-') AS CHASSIS
                                                     FROM M_TRUCK
                                                     WHERE TRUCK_TYPE_ID =:I_TRUCK_TYPE_ID AND VENDOR_ID =:I_VENDOR_ID";

                if (!IncludeInContract)
                    cmdTruckDetailSelect.CommandText += " AND TRUCK_ID NOT IN (SELECT TRUCK_ID_DETAIL FROM M_TRUCK_MAPPING WHERE IS_ACTIVE = 1) AND TRUCK_ID NOT IN (SELECT TRUCK_ID_DETAIL FROM M_TRUCK_TEMP_MAPPING WHERE STATUS_ID IN (2,6,7)) ";

                cmdTruckDetailSelect.Parameters["I_TRUCK_TYPE_ID"].Value = TruckTypeID;
                cmdTruckDetailSelect.Parameters["I_VENDOR_ID"].Value = VendorID;

                dt = dbManager.ExecuteDataTable(cmdTruckDetailSelect, "cmdTruckDetailSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckDetailSelect.CommandText = Query;
            }
        }

        public DataTable TruckMappingAddDAL(int TruckTempMappingID, int TruckIDHead, int TruckIDDetail, int CreateBy, int TruckTypeMappingID, int VendorID, int IsActive)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdTruckMappingAdd.CommandType = CommandType.StoredProcedure;
                cmdTruckMappingAdd.CommandText = "USP_M_TRUCK_MAPPING";

                cmdTruckMappingAdd.Parameters["I_TRUCK_TEMP_MAPPING_ID"].Value = TruckTempMappingID;
                cmdTruckMappingAdd.Parameters["I_TRUCK_ID_HEAD"].Value = TruckIDHead;
                cmdTruckMappingAdd.Parameters["I_TRUCK_ID_DETAIL"].Value = TruckIDDetail;
                cmdTruckMappingAdd.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdTruckMappingAdd.Parameters["I_TRUCK_TYPE_MAPPING_ID"].Value = TruckTypeMappingID;
                cmdTruckMappingAdd.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdTruckMappingAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;

                DataTable dt = new DataTable();
                dt = dbManager.ExecuteDataTable(cmdTruckMappingAdd, "cmdTruckMappingAdd");

                dbManager.CommitTransaction();

                return dt;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        public DataTable TruckGraphicSelectDAL(int Step, int TruckTypeID, int TruckIDHead, string TruckTable, int SystemID)
        {
            try
            {
                DataTable dt = new DataTable();

                cmdTruckGraphicSelect.CommandText = "USP_M_TRUCK_GRAPHIC_SELECT";
                cmdTruckGraphicSelect.CommandType = CommandType.StoredProcedure;

                cmdTruckGraphicSelect.Parameters["I_STEP"].Value = Step;
                cmdTruckGraphicSelect.Parameters["I_TRUCK_TYPE_ID"].Value = TruckTypeID;
                cmdTruckGraphicSelect.Parameters["I_TRUCK_ID"].Value = TruckIDHead;
                cmdTruckGraphicSelect.Parameters["I_TRUCK_TABLE"].Value = TruckTable;
                cmdTruckGraphicSelect.Parameters["I_SYSTEM_ID"].Value = SystemID;

                dt = dbManager.ExecuteDataTable(cmdTruckGraphicSelect, "cmdTruckGraphicSelect");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable LogInterfaceSAPDAL(string Condition)
        {
            string Query = cmdTruckTempMappingSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdTruckTempMappingSelect.CommandText += Condition;
                dt = dbManager.ExecuteDataTable(cmdTruckTempMappingSelect, "cmdTruckTempMappingSelect");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckTempMappingSelect.CommandText = Query;
            }
        }

        public DataSet TruckSelectMobile_DAL()
        {
            try
            {
                DataSet ds = new DataSet();

                cmdTruckSelectM.CommandText = "SELECT * FROM VW_M_TRUCK_MAPPING_SELECT_M";

                DataTable dt = dbManager.ExecuteDataTable(cmdTruckSelectM, "cmdTruckSelectM");
                ds.Tables.Add(dt.Copy());
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        public DataSet TruckInletSelectMobile_DAL(int TruckID)
        {
            try
            {
                DataSet ds = new DataSet();

                cmdTruckInletSelectM.CommandText = "SELECT * FROM VW_M_TRUCK_INLET_SELECT WHERE TRUCK_ID=:I_TRUCK_ID";

                cmdTruckInletSelectM.Parameters["I_TRUCK_ID"].Value = TruckID;

                DataTable dt = dbManager.ExecuteDataTable(cmdTruckInletSelectM, "cmdTruckInletSelectM");
                ds.Tables.Add(dt.Copy());
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                dbManager.Close();
            }
        }

        #region + Instance +
        private static TruckDAL _instance;
        public static TruckDAL Instance
        {
            get
            {
                _instance = new TruckDAL();
                return _instance;
            }
        }
        #endregion
    }
}