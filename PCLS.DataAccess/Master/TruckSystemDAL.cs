﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class TruckSystemDAL : OracleConnectionDAL
    {
        public TruckSystemDAL()
        {
            InitializeComponent();
        }

        public TruckSystemDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable TruckSystemSelectDAL()
        {
            try
            {
                DataTable dt = new DataTable();

                cmdTruckSystemSelect.CommandText = @"SELECT TRUCK_SYS_ID, REPLACE(TRUCK_SYS_NAME, '&nbsp;', '') AS TRUCK_SYS_NAME
                                                     FROM M_TRUCK_SYS";

                dt = dbManager.ExecuteDataTable(cmdTruckSystemSelect, "cmdTruckSystemSelect");

                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static TruckSystemDAL _instance;
        public static TruckSystemDAL Instance
        {
            get
            {
                _instance = new TruckSystemDAL();
                return _instance;
            }
        }
        #endregion
    }
}