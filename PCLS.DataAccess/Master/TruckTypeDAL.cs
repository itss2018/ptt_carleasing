﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class TruckTypeDAL : OracleConnectionDAL
    {
        public TruckTypeDAL()
        {
            InitializeComponent();
        }

        public TruckTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable TruckTypeSelectDAL(string Condition)
        {
            string Query = cmdTruckTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdTruckTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdTruckTypeSelect, "cmdTruckTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static TruckTypeDAL _instance;
        public static TruckTypeDAL Instance
        {
            get
            {
                _instance = new TruckTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}