﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class TruckTypeMappingDAL : OracleConnectionDAL
    {
        public TruckTypeMappingDAL()
        {
            InitializeComponent();
        }

        public TruckTypeMappingDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable TruckTypeMappingSelectDAL(string Condition)
        {
            string Query = cmdTruckTypeMappingSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdTruckTypeMappingSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdTruckTypeMappingSelect, "cmdTruckTypeMappingSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdTruckTypeMappingSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static TruckTypeMappingDAL _instance;
        public static TruckTypeMappingDAL Instance
        {
            get
            {
                _instance = new TruckTypeMappingDAL();
                return _instance;
            }
        }
        #endregion
    }
}