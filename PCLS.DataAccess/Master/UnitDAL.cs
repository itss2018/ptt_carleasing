﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class UnitDAL : OracleConnectionDAL
    {
        public UnitDAL()
        {
            InitializeComponent();
        }

        public UnitDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UnitSelectDAL(string Condition)
        {
            string Query = cmdUnitSelect.CommandText;
            try
            {
                cmdUnitSelect.CommandText = @"SELECT * FROM VW_M_UNIT_SELECT WHERE 1=1";

                cmdUnitSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUnitSelect, "cmdUnitSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUnitSelect.CommandText = Query;
            }
        }

        public void UnitAddDAL(int UnitID, string UnitName, string IsActive, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUnitAdd.CommandType = CommandType.StoredProcedure;
                cmdUnitAdd.CommandText = "USP_M_UNIT";

                cmdUnitAdd.Parameters["I_UNIT_ID"].Value = UnitID;
                cmdUnitAdd.Parameters["I_UNIT_NAME"].Value = UnitName;
                cmdUnitAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdUnitAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdUnitAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UnitDAL _instance;
        public static UnitDAL Instance
        {
            get
            {
                _instance = new UnitDAL();
                return _instance;
            }
        } 
	    #endregion
    }
}