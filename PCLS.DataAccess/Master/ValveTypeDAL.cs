﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class ValveTypeDAL : OracleConnectionDAL
    {
        public ValveTypeDAL()
        {
            InitializeComponent();
        }

        public ValveTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable ValveTypeSelectDAL(string Condition)
        {
            string Query = cmdValveTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdValveTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdValveTypeSelect, "cmdValveTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdValveTypeSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static ValveTypeDAL _instance;
        public static ValveTypeDAL Instance
        {
            get
            {
                _instance = new ValveTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}