﻿namespace PCLS.DataAccess.Master
{
    partial class VendorDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdVendorSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdVendorAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdVendorSelect
            // 
            this.cmdVendorSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.VarChar, 10),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_NAME", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.VarChar, 5)});
            // 
            // cmdVendorAdd
            // 
            this.cmdVendorAdd.Connection = this.OracleConn;
            this.cmdVendorAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_CODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_NAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_TELEPHONE", System.Data.OracleClient.OracleType.VarChar, 500, System.Data.ParameterDirection.Input, "", System.Data.DataRowVersion.Current, true, null),
            new System.Data.OracleClient.OracleParameter("I_FAX", System.Data.OracleClient.OracleType.VarChar, 500, System.Data.ParameterDirection.Input, "", System.Data.DataRowVersion.Current, true, null),
            new System.Data.OracleClient.OracleParameter("I_EMAIL", System.Data.OracleClient.OracleType.VarChar, 500, System.Data.ParameterDirection.Input, "", System.Data.DataRowVersion.Current, true, null),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdVendorSelect;
        private System.Data.OracleClient.OracleCommand cmdVendorAdd;
    }
}
