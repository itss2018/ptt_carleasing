﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class VendorDAL : OracleConnectionDAL
    {
        public VendorDAL()
        {
            InitializeComponent();
        }

        public VendorDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable VendorSelectDAL(string VendorID, string VendorCode, string VendorName, string IsActive)
        {
            string Query = cmdVendorSelect.CommandText;
            try
            {
                cmdVendorSelect.CommandText = @"SELECT *
                                                FROM VW_M_VENDOR_SELECT
                                                WHERE VENDOR_ID LIKE :I_VENDOR_ID
                                                AND VENDOR_CODE LIKE :I_VENDOR_CODE
                                                AND VENDOR_NAME LIKE :I_VENDOR_NAME
                                                AND IS_ACTIVE LIKE :I_IS_ACTIVE";

                cmdVendorSelect.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdVendorSelect.Parameters["I_VENDOR_CODE"].Value = VendorCode;
                cmdVendorSelect.Parameters["I_VENDOR_NAME"].Value = VendorName;
                cmdVendorSelect.Parameters["I_IS_ACTIVE"].Value = IsActive;

                return dbManager.ExecuteDataTable(cmdVendorSelect, "cmdVendorSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdVendorSelect.CommandText = Query;
            }
        }

        public void VendorAddDAL(int VendorID, string VendorCode, string VendorName, string Telephone, string Fax, string Email, string IsActive, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdVendorAdd.CommandType = CommandType.StoredProcedure;
                cmdVendorAdd.CommandText = "USP_M_VENDOR";

                cmdVendorAdd.Parameters["I_VENDOR_ID"].Value = VendorID;
                cmdVendorAdd.Parameters["I_VENDOR_CODE"].Value = VendorCode;
                cmdVendorAdd.Parameters["I_VENDOR_NAME"].Value = VendorName;
                cmdVendorAdd.Parameters["I_TELEPHONE"].Value = Telephone;
                cmdVendorAdd.Parameters["I_FAX"].Value = Fax;
                cmdVendorAdd.Parameters["I_EMAIL"].Value = Email;
                cmdVendorAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdVendorAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdVendorAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static VendorDAL _instance;
        public static VendorDAL Instance
        {
            get
            {
                _instance = new VendorDAL();
                return _instance;
            }
        }
        #endregion
    }
}