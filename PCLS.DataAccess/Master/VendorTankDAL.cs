﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class VendorTankDAL : OracleConnectionDAL
    {
        public VendorTankDAL()
        {
            InitializeComponent();
        }

        public VendorTankDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable VendorTankSelectDAL(string Condition)
        {
            string Query = cmdVendorTankSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdVendorTankSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdVendorTankSelect, "cmdVendorTankSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdVendorTankSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static VendorTankDAL _instance;
        public static VendorTankDAL Instance
        {
            get
            {
                _instance = new VendorTankDAL();
                return _instance;
            }
        }
        #endregion
    }
}