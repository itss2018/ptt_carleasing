﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class VibrateDAL : OracleConnectionDAL
    {
        public VibrateDAL()
        {
            InitializeComponent();
        }

        public VibrateDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable VibrateSelectDAL(string Condition)
        {
            string Query = cmdVibrateSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdVibrateSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdVibrateSelect, "cmdVibrateSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdVibrateSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static VibrateDAL _instance;
        public static VibrateDAL Instance
        {
            get
            {
                _instance = new VibrateDAL();
                return _instance;
            }
        }
        #endregion
    }
}