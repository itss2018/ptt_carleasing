﻿namespace PCLS.DataAccess.Master
{
    partial class WarehouseDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdWarehouseSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdWarehouseAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdWarehouseAdd
            // 
            this.cmdWarehouseAdd.Connection = this.OracleConn;
            this.cmdWarehouseAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_WAREHOUSE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_WAREHOUSE_NAME", System.Data.OracleClient.OracleType.VarChar, 2000),
            new System.Data.OracleClient.OracleParameter("I_SHORT_NAME", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_PREFIX_SALE", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_PREFIX_CONTRACT", System.Data.OracleClient.OracleType.VarChar, 20),
            new System.Data.OracleClient.OracleParameter("I_PREFIX_ORDER", System.Data.OracleClient.OracleType.VarChar, 20)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdWarehouseSelect;
        private System.Data.OracleClient.OracleCommand cmdWarehouseAdd;
    }
}
