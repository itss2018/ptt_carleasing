﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class WarehouseDAL : OracleConnectionDAL
    {
        public WarehouseDAL()
        {
            InitializeComponent();
        }

        public WarehouseDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable WarehouseSelectDAL(string Condition)
        {
            string Query = cmdWarehouseSelect.CommandText;
            try
            {
                cmdWarehouseSelect.CommandText = @"SELECT * FROM VW_M_WAREHOUSE_SELECT WHERE 1=1";

                cmdWarehouseSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdWarehouseSelect, "cmdWarehouseSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdWarehouseSelect.CommandText = Query;
            }
        }

        public void WarehouseAddDAL(int WarehouseID, string WarehouseName, string ShortName, string IsActive, int CreateBy, string PrefixSale, string PrefixContract, string PrefixOrder)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdWarehouseAdd.CommandType = CommandType.StoredProcedure;
                cmdWarehouseAdd.CommandText = "USP_M_WAREHOUSE";

                cmdWarehouseAdd.Parameters["I_WAREHOUSE_ID"].Value = WarehouseID;
                cmdWarehouseAdd.Parameters["I_WAREHOUSE_NAME"].Value = WarehouseName;
                cmdWarehouseAdd.Parameters["I_SHORT_NAME"].Value = ShortName;
                cmdWarehouseAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdWarehouseAdd.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdWarehouseAdd.Parameters["I_PREFIX_SALE"].Value = PrefixSale;
                cmdWarehouseAdd.Parameters["I_PREFIX_CONTRACT"].Value = PrefixContract;
                cmdWarehouseAdd.Parameters["I_PREFIX_ORDER"].Value = PrefixOrder;

                dbManager.ExecuteNonQuery(cmdWarehouseAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static WarehouseDAL _instance;
        public static WarehouseDAL Instance
        {
            get
            {
                _instance = new WarehouseDAL();
                return _instance;
            }
        } 
	    #endregion
    }
}