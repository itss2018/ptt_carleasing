﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.PIS
{
    public partial class PISDAL : OracleConnectionDAL_PIS
    {
        public PISDAL()
        {
            InitializeComponent();
        }

        public PISDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable PisSelectDAL(string Condition)
        {
            try
            {
                DataTable dt = new DataTable();

                cmdPISSelect.CommandText = @"SELECT P.INAME, P.FNAME, P.LNAME, P.OFFICETEL, P.HOMETEL
                                                  , P.PERSCODE, E.EMAIL, U.UNITCODE, U.UNITNAME, U.UNITTABBR
                                             FROM PIS.PERSONEL P LEFT JOIN PIS.UNIT U ON P.UNITCODE = U.UNITCODE
                                                                 LEFT JOIN PIS.EMAIL E ON P.CODE = E.IDCODE
                                             WHERE 1=1 {0}";

                cmdPISSelect.CommandText = string.Format(cmdPISSelect.CommandText, Condition);

                dt = dbManager.ExecuteDataTable(cmdPISSelect, "cmdPISSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static PISDAL _instance;
        public static PISDAL Instance
        {
            get
            {
                _instance = new PISDAL();
                return _instance;
            }
        }
        #endregion
    }
}