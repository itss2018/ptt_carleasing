﻿namespace PCLS.DataAccess.SystemData
{
    partial class AuthenDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdAuthenDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdAuthenInsert = new System.Data.OracleClient.OracleCommand();
            this.cmdAuthenSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdAuthenSelectTemplate = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdAuthenDelete
            // 
            this.cmdAuthenDelete.Connection = this.OracleConn;
            this.cmdAuthenDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdAuthenInsert
            // 
            this.cmdAuthenInsert.Connection = this.OracleConn;
            this.cmdAuthenInsert.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_MENU_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_VISIBLE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_READ", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_WRITE", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdAuthenSelect
            // 
            this.cmdAuthenSelect.Connection = this.OracleConn;
            this.cmdAuthenSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdAuthenSelectTemplate
            // 
            this.cmdAuthenSelectTemplate.Connection = this.OracleConn;

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdAuthenDelete;
        private System.Data.OracleClient.OracleCommand cmdAuthenInsert;
        private System.Data.OracleClient.OracleCommand cmdAuthenSelect;
        private System.Data.OracleClient.OracleCommand cmdAuthenSelectTemplate;
    }
}
