﻿using dbManager;
using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.SystemData
{
    public partial class UploadDAL : OracleConnectionDAL
    {
        public UploadDAL()
        {
            InitializeComponent();
        }

        public UploadDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UploadRequestFileDAL(string Condition)
        {
            try
            {
                cmdUploadRequestSelect.CommandText = @"SELECT M_REQUEST_FILE.UPLOAD_TYPE, M_UPLOAD_TYPE.UPLOAD_ID, M_UPLOAD_TYPE.UPLOAD_NAME, M_UPLOAD_TYPE.EXTENTION, M_UPLOAD_TYPE.MAX_FILE_SIZE, M_UPLOAD_TYPE.DOWNLOAD_FORM,M_REQUEST_FILE.REF_ID
                                                       FROM  M_REQUEST_FILE INNER JOIN M_UPLOAD_TYPE ON M_REQUEST_FILE.UPLOAD_ID = M_UPLOAD_TYPE.UPLOAD_ID
                                                       WHERE (M_UPLOAD_TYPE.IS_ACTIVE = 1)";

                cmdUploadRequestSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUploadRequestSelect, "cmdUploadRequestSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void UploadAdd(string DocID, string UploadType, DataTable dtUpload, int CreateBy, DBManager db)
        {
            try
            {
                cmdUploadDelete.Parameters["I_DOC_ID"].Value = DocID;
                cmdUploadDelete.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                db.ExecuteNonQuery(cmdUploadDelete);

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    cmdUploadAdd.Parameters["FILENAME_SYSTEM"].Value = dtUpload.Rows[i]["FILENAME_SYSTEM"].ToString();
                    cmdUploadAdd.Parameters["FILENAME_USER"].Value = dtUpload.Rows[i]["FILENAME_USER"].ToString();
                    cmdUploadAdd.Parameters["UPLOAD_ID"].Value = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    cmdUploadAdd.Parameters["FULLPATH"].Value = dtUpload.Rows[i]["FULLPATH"].ToString();
                    cmdUploadAdd.Parameters["REF_INT"].Value = 0;
                    cmdUploadAdd.Parameters["REF_STR"].Value = DocID;
                    cmdUploadAdd.Parameters["ISACTIVE"].Value = "1";
                    cmdUploadAdd.Parameters["CREATE_BY"].Value = CreateBy;

                    db.ExecuteNonQuery(cmdUploadAdd);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UploadSelect(string RefStr, string UploadType, string Condition)
        {
            string Query = cmdUploadSelect.CommandText;
            try
            {
                cmdUploadSelect.CommandText += Condition;

                cmdUploadSelect.Parameters["I_REF_STR"].Value = RefStr;
                cmdUploadSelect.Parameters["I_UPLOAD_TYPE"].Value = UploadType;

                DataTable dt = dbManager.ExecuteDataTable(cmdUploadSelect, "cmdUploadSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUploadSelect.CommandText = Query;
            }
        }

        #region + Instance +
        private static UploadDAL _instance;
        public static UploadDAL Instance
        {
            get
            {
                _instance = new UploadDAL();
                return _instance;
            }
        }
        #endregion
    }
}