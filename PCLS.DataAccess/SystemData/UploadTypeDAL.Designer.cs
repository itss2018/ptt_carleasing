﻿namespace PCLS.DataAccess.SystemData
{
    partial class UploadTypeDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdUploadTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeHeaderSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUploadTypeAdd = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdUploadTypeSelect
            // 
            this.cmdUploadTypeSelect.Connection = this.OracleConn;
            this.cmdUploadTypeSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdUploadTypeHeaderSelect
            // 
            this.cmdUploadTypeHeaderSelect.Connection = this.OracleConn;
            this.cmdUploadTypeHeaderSelect.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255)});
            // 
            // cmdUploadTypeAdd
            // 
            this.cmdUploadTypeAdd.Connection = this.OracleConn;
            this.cmdUploadTypeAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_NAME", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_EXTENTION", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_MAX_FILE_SIZE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_UPLOAD_TYPE_TH", System.Data.OracleClient.OracleType.VarChar, 255),
            new System.Data.OracleClient.OracleParameter("I_REQUIRE", System.Data.OracleClient.OracleType.Number)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdUploadTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeHeaderSelect;
        private System.Data.OracleClient.OracleCommand cmdUploadTypeAdd;
    }
}
