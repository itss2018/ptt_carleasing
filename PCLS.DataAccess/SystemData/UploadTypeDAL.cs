﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.SystemData
{
    public partial class UploadTypeDAL : OracleConnectionDAL
    {
        public UploadTypeDAL()
        {
            InitializeComponent();
        }

        public UploadTypeDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UploadTypeSelectDAL(string Condition)
        {
            string Query = cmdUploadTypeSelect.CommandText;
            try
            {
                cmdUploadTypeSelect.CommandText = @"SELECT * FROM VW_M_UPLOAD_TYPE WHERE 1=1";

                cmdUploadTypeSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUploadTypeSelect, "cmdUploadTypeSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUploadTypeSelect.CommandText = Query;
            }
        }

        public DataTable UploadTypeHeaderSelectDAL(string Condition)
        {
            string Query = cmdUploadTypeHeaderSelect.CommandText;
            try
            {
                cmdUploadTypeHeaderSelect.CommandText = @"SELECT * FROM VW_M_UPLOAD_TYPE_HEADER WHERE 1=1";

                cmdUploadTypeHeaderSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUploadTypeHeaderSelect, "cmdUploadTypeHeaderSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUploadTypeSelect.CommandText = Query;
            }
        }

        public void UploadTypeAddDAL(string UploadType, string UploadName, string Extention, int MaxFileSize, string IsActive, int CreateBy, string UploadTypeTH, string Require)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUploadTypeAdd.CommandType = CommandType.StoredProcedure;
                cmdUploadTypeAdd.CommandText = "USP_M_UPLOAD_TYPE";

                cmdUploadTypeAdd.Parameters["I_UPLOAD_TYPE"].Value = UploadType;
                cmdUploadTypeAdd.Parameters["I_UPLOAD_NAME"].Value = UploadName;
                cmdUploadTypeAdd.Parameters["I_EXTENTION"].Value = Extention;
                cmdUploadTypeAdd.Parameters["I_MAX_FILE_SIZE"].Value = MaxFileSize;
                cmdUploadTypeAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdUploadTypeAdd.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdUploadTypeAdd.Parameters["I_UPLOAD_TYPE_TH"].Value = UploadTypeTH;
                cmdUploadTypeAdd.Parameters["I_REQUIRE"].Value = Require;

                dbManager.ExecuteNonQuery(cmdUploadTypeAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UploadTypeDAL _instance;
        public static UploadTypeDAL Instance
        {
            get
            {
                _instance = new UploadTypeDAL();
                return _instance;
            }
        }
        #endregion
    }
}