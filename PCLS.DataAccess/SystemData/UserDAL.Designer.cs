﻿namespace PCLS.DataAccess.SystemData
{
    partial class UserDAL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdUserSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUserSystemSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUserPlantSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUserTypeSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUserAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdUserContractDelete = new System.Data.OracleClient.OracleCommand();
            this.cmdUserContractAdd = new System.Data.OracleClient.OracleCommand();
            this.cmdUserContractSelect = new System.Data.OracleClient.OracleCommand();
            this.cmdUserSelect2 = new System.Data.OracleClient.OracleCommand();
            // 
            // cmdUserSelect
            // 
            this.cmdUserSelect.CommandText = "SELECT * FROM VW_M_USER_SELECT WHERE 1=1";
            this.cmdUserSelect.Connection = this.OracleConn;
            // 
            // cmdUserSystemSelect
            // 
            this.cmdUserSystemSelect.Connection = this.OracleConn;
            // 
            // cmdUserPlantSelect
            // 
            this.cmdUserPlantSelect.CommandText = "SELECT * FROM VW_M_USER_PLANT_SELECT WHERE 1=1";
            this.cmdUserPlantSelect.Connection = this.OracleConn;
            // 
            // cmdUserTypeSelect
            // 
            this.cmdUserTypeSelect.CommandText = "SELECT * FROM VW_M_USER_TYPE_SELECT WHERE 1=1";
            this.cmdUserTypeSelect.Connection = this.OracleConn;
            // 
            // cmdUserAdd
            // 
            this.cmdUserAdd.Connection = this.OracleConn;
            this.cmdUserAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_USER_NAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_PASSWORD", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_DATE_OF_BIRTH", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_TITLE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_FIRSTNAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_LASTNAME", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_ADDRESS", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_SUB_DISTRICT", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_DISTRICT", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_PROVINCE", System.Data.OracleClient.OracleType.VarChar, 200),
            new System.Data.OracleClient.OracleParameter("I_ZIPCODE", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_COUNTRY", System.Data.OracleClient.OracleType.VarChar, 100),
            new System.Data.OracleClient.OracleParameter("I_TELEPHONE", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_EMAIL", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_USERGROUP_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_SPECIAL_STATUS", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IS_ACTIVE", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_IMAGE_PATH", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("I_USER_TYPE_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 50, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null),
            new System.Data.OracleClient.OracleParameter("I_VENDOR_ID", System.Data.OracleClient.OracleType.Number, 0, System.Data.ParameterDirection.Input, "", System.Data.DataRowVersion.Current, true, null)});
            // 
            // cmdUserContractDelete
            // 
            this.cmdUserContractDelete.Connection = this.OracleConn;
            this.cmdUserContractDelete.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USER_ID", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdUserContractAdd
            // 
            this.cmdUserContractAdd.Connection = this.OracleConn;
            this.cmdUserContractAdd.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USER_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CONTRACT_ID", System.Data.OracleClient.OracleType.Number),
            new System.Data.OracleClient.OracleParameter("I_CREATE_BY", System.Data.OracleClient.OracleType.Number)});
            // 
            // cmdUserContractSelect
            // 
            this.cmdUserContractSelect.CommandText = "SELECT * FROM VW_M_USER_CONTRACT_SELECT WHERE 1=1";
            this.cmdUserContractSelect.Connection = this.OracleConn;
            // 
            // cmdUserSelect2
            // 
            this.cmdUserSelect2.CommandText = "SELECT * FROM VW_M_USER_SELECT WHERE 1=1";
            this.cmdUserSelect2.Connection = this.OracleConn;
            this.cmdUserSelect2.Parameters.AddRange(new System.Data.OracleClient.OracleParameter[] {
            new System.Data.OracleClient.OracleParameter("I_USERNAME", System.Data.OracleClient.OracleType.VarChar, 50),
            new System.Data.OracleClient.OracleParameter("I_PASSWORD", System.Data.OracleClient.OracleType.VarChar, 500),
            new System.Data.OracleClient.OracleParameter("O_CUR", System.Data.OracleClient.OracleType.Cursor, 0, System.Data.ParameterDirection.Output, false, ((byte)(0)), ((byte)(0)), "", System.Data.DataRowVersion.Current, null)});

        }

        #endregion

        private System.Data.OracleClient.OracleCommand cmdUserSelect;
        private System.Data.OracleClient.OracleCommand cmdUserSystemSelect;
        private System.Data.OracleClient.OracleCommand cmdUserPlantSelect;
        private System.Data.OracleClient.OracleCommand cmdUserTypeSelect;
        private System.Data.OracleClient.OracleCommand cmdUserAdd;
        private System.Data.OracleClient.OracleCommand cmdUserContractDelete;
        private System.Data.OracleClient.OracleCommand cmdUserContractAdd;
        private System.Data.OracleClient.OracleCommand cmdUserContractSelect;
        private System.Data.OracleClient.OracleCommand cmdUserSelect2;
    }
}
