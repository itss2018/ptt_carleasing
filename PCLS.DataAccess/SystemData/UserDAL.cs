﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.SystemData
{
    public partial class UserDAL : OracleConnectionDAL
    {
        public UserDAL()
        {
            InitializeComponent();
        }

        public UserDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UserSelectDAL(string Condition)
        {
            string Query = cmdUserSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdUserSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdUserSelect, "cmdUserSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserSelect.CommandText = Query;
            }
        }

        public DataTable UserContractSelectDAL(string Condition)
        {
            string Query = cmdUserContractSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdUserContractSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdUserContractSelect, "cmdUserContractSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserContractSelect.CommandText = Query;
            }
        }

        public DataTable UserSystemSelectDAL(string Condition)
        {
            try
            {
                DataTable dt = new DataTable();
                cmdUserSystemSelect.CommandText = @"SELECT M_USER_SYSTEM.ID, M_USER_SYSTEM.USER_ID, M_USER_SYSTEM.SYSTEM_ID, M_SYSTEM_TYPE.SYSTEM_NAME, M_SYSTEM_TYPE.LOGO_NAME
                                                    FROM M_USER_SYSTEM INNER JOIN M_SYSTEM_TYPE ON M_USER_SYSTEM.SYSTEM_ID = M_SYSTEM_TYPE.SYSTEM_ID
                                                    AND M_SYSTEM_TYPE.IS_ACTIVE = 1" + Condition;

                dt = dbManager.ExecuteDataTable(cmdUserSystemSelect, "cmdUserSystemSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UserSelect2DAL(string Username, string Password)
        {
            try
            {
                DataTable dt = new DataTable();

                cmdUserSelect2.CommandType = CommandType.StoredProcedure;
                cmdUserSelect2.CommandText = "USP_T_LOGIN_CHECK";

                cmdUserSelect2.Parameters["I_USERNAME"].Value = Username;
                cmdUserSelect2.Parameters["I_PASSWORD"].Value = Password;

                dt = dbManager.ExecuteDataTable(cmdUserSelect2, "cmdUserSelect2");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable UserPlantSelectDAL(string Condition)
        {
            string Query = cmdUserPlantSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdUserPlantSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdUserPlantSelect, "cmdUserPlantSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserPlantSelect.CommandText = Query;
            }
        }

        public DataTable UserTypeSelectDAL(string Condition)
        {
            string Query = cmdUserTypeSelect.CommandText;
            try
            {
                DataTable dt = new DataTable();
                cmdUserTypeSelect.CommandText += Condition;

                dt = dbManager.ExecuteDataTable(cmdUserTypeSelect, "cmdUserTypeSelect");
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserSelect.CommandText = Query;
            }
        }

        public DataTable UserAddDAL(int UserID, string UserName, string Password, string DateOfBirth, int TitleID, string FirstName, string LastName
                             , string Address, string SubDistrict, string District, string Province, string ZipCode, string Country, string Telephone
                             , string Email, int UserGroupID, int IsActive, int CreateBy, string ImagePath, int UserTypeID, DataTable dtUpload, int VendorID)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUserAdd.CommandType = CommandType.StoredProcedure;
                cmdUserAdd.CommandText = "USP_M_USER";

                cmdUserAdd.Parameters["I_USER_ID"].Value = UserID;
                cmdUserAdd.Parameters["I_USER_NAME"].Value = UserName;
                cmdUserAdd.Parameters["I_PASSWORD"].Value = Password;
                cmdUserAdd.Parameters["I_DATE_OF_BIRTH"].Value = DateOfBirth;
                cmdUserAdd.Parameters["I_TITLE_ID"].Value = TitleID;
                cmdUserAdd.Parameters["I_FIRSTNAME"].Value = FirstName;
                cmdUserAdd.Parameters["I_LASTNAME"].Value = LastName;
                cmdUserAdd.Parameters["I_ADDRESS"].Value = Address;
                cmdUserAdd.Parameters["I_SUB_DISTRICT"].Value = SubDistrict;
                cmdUserAdd.Parameters["I_DISTRICT"].Value = District;
                cmdUserAdd.Parameters["I_PROVINCE"].Value = Province;
                cmdUserAdd.Parameters["I_ZIPCODE"].Value = ZipCode;
                cmdUserAdd.Parameters["I_COUNTRY"].Value = Country;
                cmdUserAdd.Parameters["I_TELEPHONE"].Value = Telephone;
                cmdUserAdd.Parameters["I_EMAIL"].Value = Email;
                cmdUserAdd.Parameters["I_USERGROUP_ID"].Value = UserGroupID;
                cmdUserAdd.Parameters["I_SPECIAL_STATUS"].Value = 0;
                cmdUserAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdUserAdd.Parameters["I_CREATE_BY"].Value = CreateBy;
                cmdUserAdd.Parameters["I_IMAGE_PATH"].Value = ImagePath;
                cmdUserAdd.Parameters["I_USER_TYPE_ID"].Value = UserTypeID;

                if (VendorID == 0)
                    cmdUserAdd.Parameters["I_VENDOR_ID"].Value = DBNull.Value;
                else
                    cmdUserAdd.Parameters["I_VENDOR_ID"].Value = VendorID;

                DataTable dtResult = dbManager.ExecuteDataTable(cmdUserAdd, "cmdUserAdd");

                if (string.Equals(dtResult.Rows[0]["FLAG"].ToString(), "N"))
                    throw new Exception(dtResult.Rows[0]["MESSAGE"].ToString());

                UploadDAL.Instance.UploadAdd(dtResult.Rows[0]["MESSAGE"].ToString(), "USER_LOGIN", dtUpload, CreateBy, dbManager);

                dbManager.CommitTransaction();

                return dtResult;
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UserDAL _instance;
        public static UserDAL Instance
        {
            get
            {
                _instance = new UserDAL();
                return _instance;
            }
        }
        #endregion
    }
}