﻿using PCLS.DataAccess.ConnectionDAL;
using System;
using System.ComponentModel;
using System.Data;

namespace PCLS.DataAccess.Master
{
    public partial class UserGroupDAL : OracleConnectionDAL
    {
        public UserGroupDAL()
        {
            InitializeComponent();
        }

        public UserGroupDAL(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public DataTable UserGroupSelectDAL(string Condition)
        {
            string Query = cmdUserGroupSelect.CommandText;
            try
            {
                cmdUserGroupSelect.CommandText = @"SELECT * FROM VW_M_USERGROUP_SELECT WHERE 1=1";

                cmdUserGroupSelect.CommandText += Condition;

                return dbManager.ExecuteDataTable(cmdUserGroupSelect, "cmdUserGroupSelect");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                cmdUserGroupSelect.CommandText = Query;
            }
        }

        public void UserGroupAddDAL(int UserGroupID, string UserGroupName, string IsAdmin, string IsActive, int CreateBy)
        {
            try
            {
                dbManager.Open();
                dbManager.BeginTransaction();

                cmdUserGroupAdd.CommandType = CommandType.StoredProcedure;
                cmdUserGroupAdd.CommandText = "USP_M_USERGROUP";

                cmdUserGroupAdd.Parameters["I_USERGROUP_ID"].Value = UserGroupID;
                cmdUserGroupAdd.Parameters["I_USERGROUP_NAME"].Value = UserGroupName;
                cmdUserGroupAdd.Parameters["I_IS_ADMIN"].Value = IsAdmin;
                cmdUserGroupAdd.Parameters["I_IS_ACTIVE"].Value = IsActive;
                cmdUserGroupAdd.Parameters["I_CREATE_BY"].Value = CreateBy;

                dbManager.ExecuteNonQuery(cmdUserGroupAdd);

                dbManager.CommitTransaction();
            }
            catch (Exception ex)
            {
                dbManager.RollbackTransaction();
                throw new Exception(ex.Message);
            }
        }

        #region + Instance +
        private static UserGroupDAL _instance;
        public static UserGroupDAL Instance
        {
            get
            {
                _instance = new UserGroupDAL();
                return _instance;
            }
        }
        #endregion
    }
}