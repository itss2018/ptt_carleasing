﻿using System;
using System.Collections.Generic;

namespace PCLS.Entity.Mobile.Truck
{
    [Serializable]
    public class TruckInfo
    {
        public TruckInfo() { }

        public string HEADNO { get; set; }
        public string TRAILERNO { get; set; }
        public string VEHICLE_TYPE { get; set; }
        public string VEHICLE_TYPE_SAP { get; set; }
        public string CARRIER_NAME { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CLASS_GROUP { get; set; }
        public string VOLUME { get; set; }
        public DateTime ENTRY_DATE { get; set; }
        public DateTime LAST_UPDATEDATE { get; set; }

        public TUInfo TU_INFO { get; set; }
    }

    public class TUInfo
    {
        public TUInfo() { }

        public string HEADNO { get; set; }
        public string TRAILERNO { get; set; }
        public string CALIBRATE_CODE { get; set; }
        public DateTime CALIBRATE_DATE { get; set; }
        public DateTime CALIBRATE_ENDDATE { get; set; }

        public List<CompartmentInfo> COMP_INFO { get; set; }
    }

    public class CompartmentInfo
    {
        public CompartmentInfo() { }

        public string HEADNO { get; set; }
        public string TRAILERNO { get; set; }
        public int COMPNO { get; set; }
        public decimal VOLUME { get; set; }
    }
}