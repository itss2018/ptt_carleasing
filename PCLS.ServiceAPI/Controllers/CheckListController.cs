﻿using LTMS.Bussiness.Master;
using LTMS.Entity.Mobile.Truck;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.Http;

namespace LTMS.ServiceAPI.Controllers
{
    public class CheckListController : ApiController
    {
        [HttpGet]
        public string CheckListSelect(int CheckTypeID, int TruckSysID)
        {
            //https://pttwebtest9.pttplc.com/PTT-TMSLubeBulkWS_test/api/CheckList/CheckListSelect/?CheckTypeID=3&TruckSysID=2

            try
            {
                DataTable dtCheckList = CheckListBLL.Instance.CheckListSelectBLL(CheckTypeID, TruckSysID);

                return JsonConvert.SerializeObject(dtCheckList, Formatting.Indented);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex.Message, Formatting.Indented);
            }
        }

        [HttpGet]
        public string Test_Image_Get()
        {
            //https://pttwebtest9.pttplc.com/PTT-TMSLubeBulkWS_test/api/CheckList/Test_Image_Get
            try
            {
                return JsonConvert.SerializeObject("https://ptttms.pttplc.com/TMS/UploadFile/Vendor/1279/2018-01-24%2017-13-19-880.jpg", Formatting.Indented);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex.Message, Formatting.Indented);
            }
        }

        [HttpGet]
        public bool SaveDocument(Byte[] bytes, string DocName)
        {
            //https://pttwebtest9.pttplc.com/PTT-TMSLubeBulkWS_test/api/CheckList/SaveDocument/?bytes=System.IO.File.ReadAllBytes&DocName=xxx.jpg
            try
            {
                System.Web.UI.Page page = new System.Web.UI.Page();
                string PathUpload = page.Server.MapPath("~") + "\\" + "UploadFile";

                //bytes = System.IO.File.ReadAllBytes(PathUpload + "\\" + "Test.jpg");

                using (FileStream stream = new FileStream(PathUpload + "\\" + DocName, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                
                return true;
                //File Path Item
            }
            catch
            {
                return false;
            }
        }

        [HttpGet]
        public byte[] GetDocument(string FolderName, string FileName)
        {//https://pttwebtest9.pttplc.com/PTT-TMSLubeBulkWS_test/api/CheckList/GetDocument/?FolderName=2018&FileName=Test.pdf
            try
            {
                System.Web.UI.Page page = new System.Web.UI.Page();
                string PathDownload = page.Server.MapPath("~") + "\\" + "UploadFile";

                if (!string.Equals(FolderName, string.Empty))
                    PathDownload += "\\" + FolderName;

                PathDownload += "\\" + FileName;

                Byte[] bytes = System.IO.File.ReadAllBytes(PathDownload);
                return bytes;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}