﻿using LTMS.Bussiness.Authentication;
using LTMS.Bussiness.Log;
using LTMS.WebPC.Helper;
using Newtonsoft.Json;
using OTM.Bussiness.SystemData;
using System;
using System.Data;
using System.Web.Http;

namespace LTMS.ServiceAPI.Controllers
{
    public class LoginController : ApiController
    {
        [HttpGet]
        public string CheckLogin(string Domain, string UserName, string Password, string isUserTMS)
        {
            //https://pttwebtest9.pttplc.com/PTT-TMSLubeBulkWS_test/api/Login/CheckLogin/?Domain=pttdigital&UserName=zsakchai.p&Password=PingAut1&isUserTMS=0

            Domain += @"\";
            UserName = UserName.ToLower();

            try
            {
                DataTable dtUser = new DataTable();
                DataTable dtTemp = new DataTable();

                if (string.Equals(isUserTMS, "0"))
                {//Domain
                    if (DomainHelper.CheckDomain(Domain + UserName, Password))
                        dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + UserName + "'" + " AND IS_ACTIVE = 1");
                }
                else
                {//Legacy
                    dtTemp = UserBLL.Instance.UserSelect2BLL(UserName, MD5Helper.GetMD5Hash(Password));

                    if (dtTemp.Rows.Count > 0)
                    {
                        if (string.Equals(dtTemp.Rows[0]["FLAG"].ToString(), "N"))
                            throw new Exception(dtTemp.Rows[0]["MESSAGE"].ToString());

                        dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + UserName + "'" + " AND PASSWORD = '" + MD5Helper.GetMD5Hash(Password) + "'" + " AND IS_ACTIVE = 1");
                    }
                }

                if (dtUser.Rows.Count > 0)
                {
                    LoginBLL.Instance.ClearLoginBLL(UserName);
                    LogBLL.Instance.LoginLogBLL(int.Parse(dtUser.Rows[0]["USER_ID"].ToString()));
                }
                else
                    throw new Exception("Not Authorize...");

                dtUser.TableName = "dtUser";
                return JsonConvert.SerializeObject(dtUser, Formatting.Indented);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex.Message, Formatting.Indented);
            }
        }

        [HttpGet]
        public string Test(string ABC)
        {
            try
            {
                DataTable dtUser = new DataTable();
                DataTable dtTemp = new DataTable();

                dtUser = new DataTable();
                dtUser.Columns.Add("COL1");
                dtUser.Columns.Add("COL2");

                dtUser.Rows.Add("Sakchai", "Pieamsombat");
                dtUser.Rows.Add("Kiattisak", "Chaisomboon");

                return JsonConvert.SerializeObject(dtUser, Formatting.Indented);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        //public string Get(string Domain, string UserName, string Password, string isUserLocal)
        //{
        //    Domain += @"\";
            
        //    try
        //    {
        //        DataTable dtUser = new DataTable();
        //        DataTable dtTemp = new DataTable();

        //        if (string.Equals(isUserLocal, "0"))
        //        {//Domain
        //            if (DomainHelper.CheckDomain(Domain + UserName, Password))
        //                dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + UserName + "'" + " AND IS_ACTIVE = 1");
        //        }
        //        else
        //        {//Legacy
        //            dtTemp = UserBLL.Instance.UserSelect2BLL(UserName, MD5Helper.GetMD5Hash(Password));

        //            if (dtTemp.Rows.Count > 0)
        //            {
        //                if (string.Equals(dtTemp.Rows[0]["FLAG"].ToString(), "N"))
        //                    throw new Exception(dtTemp.Rows[0]["MESSAGE"].ToString());

        //                dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + UserName + "'" + " AND PASSWORD = '" + MD5Helper.GetMD5Hash(Password) + "'" + " AND IS_ACTIVE = 1");
        //            }
        //        }

        //        if (dtUser.Rows.Count > 0)
        //            LoginBLL.Instance.ClearLoginBLL(UserName);
        //        else
        //            throw new Exception("Not Authorize...");

        //        dtUser.TableName = "dtUser";
        //        return JsonConvert.SerializeObject(dtUser, Formatting.Indented);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
    }
}