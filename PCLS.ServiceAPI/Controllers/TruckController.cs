﻿using LTMS.Bussiness.Master;
using LTMS.Entity.Mobile.Truck;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace LTMS.ServiceAPI.Controllers
{
    public class TruckController : ApiController
    {
        [HttpGet]
        public string TruckSelect()
        {
            //https://pttwebtest9.pttplc.com/PTT-TMSLubeBulkWS_test/api/Truck/TruckSelect

            try
            {
                DataSet dsTruck = new DataSet();
                DataTable dtTruck = new DataTable();

                DataSet dsInlet = new DataSet();
                DataTable dtInlet = new DataTable();

                dsTruck = TruckBLL.Instance.TruckSelectMobile_BLL();
                dtTruck = dsTruck.Tables[0];

                dtTruck.TableName = "dtTruck";

                TruckInfo[] truckInfoList = new TruckInfo[dtTruck.Rows.Count];
                TruckInfo truckInfo = new TruckInfo();

                for (int i = 0; i < dtTruck.Rows.Count; i++)
			    {
                    truckInfo = new TruckInfo();

                    //Truck Info
                    truckInfo.HEADNO = dtTruck.Rows[i]["HEADNO"].ToString();
                    truckInfo.TRAILERNO = dtTruck.Rows[i]["TRAILERNO"].ToString();
                    truckInfo.VEHICLE_TYPE = dtTruck.Rows[i]["VEHICLE_TYPE"].ToString();
                    truckInfo.VEHICLE_TYPE_SAP = dtTruck.Rows[i]["VEHICLE_TYPE_SAP"].ToString();
                    truckInfo.CARRIER_NAME = dtTruck.Rows[i]["CARRIER_NAME"].ToString();
                    truckInfo.CONTRACT_NO = dtTruck.Rows[i]["CONTRACT_NO"].ToString();
                    truckInfo.CLASS_GROUP = dtTruck.Rows[i]["CLASS_GROUP"].ToString();
                    truckInfo.VOLUME = "-1";
                    truckInfo.ENTRY_DATE = DateTime.Now;

                    //TU Info
                    TUInfo tuInfo = new TUInfo();
                    tuInfo.HEADNO = dtTruck.Rows[i]["HEADNO"].ToString();
                    tuInfo.TRAILERNO = dtTruck.Rows[i]["TRAILERNO"].ToString();
                    tuInfo.CALIBRATE_CODE = "?";
                    tuInfo.CALIBRATE_DATE = DateTime.Now;
                    tuInfo.CALIBRATE_ENDDATE = DateTime.Now;
                    truckInfo.TU_INFO = tuInfo;

                    //Compartment
                    dsInlet = TruckBLL.Instance.TruckInletSelectMobile_BLL(int.Parse(dtTruck.Rows[i]["TRUCK_ID_DETAIL"].ToString()));
                    dtInlet = dsInlet.Tables[0];

                    truckInfo.TU_INFO.COMP_INFO = new List<CompartmentInfo>();
                    for (int j = 0; j < dtInlet.Rows.Count; j++)
                    {
                        CompartmentInfo compartmentInfo = new CompartmentInfo()
                        {
                            HEADNO = dtTruck.Rows[i]["HEADNO"].ToString(),
                            TRAILERNO = dtTruck.Rows[i]["TRAILERNO"].ToString(),
                            COMPNO = int.Parse(dtInlet.Rows[j]["NO"].ToString()),
                            VOLUME = int.Parse(dtInlet.Rows[j]["HIGH"].ToString())
                        };
                        truckInfo.TU_INFO.COMP_INFO.Add(compartmentInfo);
                    }

                    truckInfoList[i] = truckInfo;
                }

                return JsonConvert.SerializeObject(truckInfoList, Formatting.Indented);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex.Message, Formatting.Indented);
            }
        }
    }
}