﻿using LTMS.Bussiness.Master;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web.Http;

namespace LTMS.ServiceAPI.Controllers
{
    public class VendorController : ApiController
    {
        [HttpGet]
        public string VendorSelect(string VendorID, string VendorCode, string VendorName, string IsActive)
        {
            //https://pttwebtest9.pttplc.com/PTT-TMSLubeBulkWS_test/api/Vendor/VendorSelect/?VendorID=%&VendorCode=0010002212&VendorName=%&IsActive=1

            try
            {
                DataTable dtVendor = VendorBLL.Instance.VendorSelectBLL(VendorID, VendorCode, VendorName, IsActive);

                return JsonConvert.SerializeObject(dtVendor, Formatting.Indented);
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(ex.Message, Formatting.Indented);
            }
        }
    }
}