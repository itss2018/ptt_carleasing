﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PCLS.ServiceTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //Trust Certificate
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();

                //Check Login
                wsMobile.wsMobile ws = new wsMobile.wsMobile();
                ws.Url = "http://ptt-ecustom-p01.ptt.corp/LTMS_Service/wsMobile.svc";
                wsMobile.TMSAuthentication authen = new wsMobile.TMSAuthentication()
                {
                    Username = "TMS",
                    Password = "9e5dfc0ebc078e4f03650d778f13d412"
                };

                string UserList = ws.CheckLogin(authen, @"pttdigital\", "zsakchai.p", "PingAut1", "0");
                MessageBox.Show(UserList);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }

    public class TrustAllCertificatePolicy : System.Net.ICertificatePolicy
    {
        public TrustAllCertificatePolicy()
        {
        }

        public bool CheckValidationResult(ServicePoint sp,
                  System.Security.Cryptography.X509Certificates.X509Certificate cert,
                  WebRequest req,
                  int problem)
        {
            return true;
        }
    }
}