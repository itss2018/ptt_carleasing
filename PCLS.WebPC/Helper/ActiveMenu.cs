﻿using System;
using System.Web.UI.HtmlControls;

namespace PCLS.WebPC.Helper
{
    public static class ActiveMenu
    {
        public static void SetActiveMenu(System.Web.UI.MasterPage Master, string TabName, string MenuName)
        {
            try
            {
                string[] name = new string[] { "tabMaster"
                                                          , "liMasterialType", "liTitle", "liCurrency"
                                             , "tabTransaction"
                                                          , "liInquiry", "", ""
                                             , "tabSystem"
                                                          , "liAuthentication", "liUser", "liUserGroup"};
                RemoveClass(Master, name);

                AddClass(Master, TabName);
                AddClass(Master, MenuName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static void RemoveClass(System.Web.UI.MasterPage Master, string[] name)
        {
            try
            {
                HtmlGenericControl Menu;
                string ControlName = string.Empty;
                for (int i = 0; i < name.Length; i++)
                {
                    ControlName = name[i];
                    Menu = (HtmlGenericControl)Master.FindControl(ControlName);
                    if (Menu != null)
                        Menu.Attributes.Remove("class");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static void AddClass(System.Web.UI.MasterPage Master, string ControlName)
        {
            try
            {
                HtmlGenericControl Menu;
                Menu = (HtmlGenericControl)Master.FindControl(ControlName);
                if (Menu != null)
                    Menu.Attributes.Add("class", "active");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}