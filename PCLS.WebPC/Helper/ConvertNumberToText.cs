﻿using System;

namespace PCLS.WebPC.Helper
{
    public static class ConvertNumberToText
    {
        public static string NumberToText(int number, bool isUK)
        {
            try
            {
                if (number == 0) return "Zero";
                string and = isUK ? "and " : ""; // deals with UK or US numbering
                if (number == -2147483648) return "Minus Two Billion One Hundred " + and +
                "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
                "Six Hundred " + and + "Forty Eight";
                int[] num = new int[4];
                int first = 0;
                int u, h, t;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                if (number < 0)
                {
                    sb.Append("Minus ");
                    number = -number;
                }
                string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };
                string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
                string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
                string[] words3 = { "Thousand ", "Million ", "Billion " };
                num[0] = number % 1000;           // units
                num[1] = number / 1000;
                num[2] = number / 1000000;
                num[1] = num[1] - 1000 * num[2];  // thousands
                num[3] = number / 1000000000;     // billions
                num[2] = num[2] - 1000 * num[3];  // millions
                for (int i = 3; i > 0; i--)
                {
                    if (num[i] != 0)
                    {
                        first = i;
                        break;
                    }
                }
                for (int i = first; i >= 0; i--)
                {
                    if (num[i] == 0) continue;
                    u = num[i] % 10;              // ones
                    t = num[i] / 10;
                    h = num[i] / 100;             // hundreds
                    t = t - 10 * h;               // tens
                    if (h > 0) sb.Append(words0[h] + "Hundred ");
                    if (u > 0 || t > 0)
                    {
                        if (h > 0 || i < first) sb.Append(and);
                        if (t == 0)
                            sb.Append(words0[u]);
                        else if (t == 1)
                            sb.Append(words1[u]);
                        else
                            sb.Append(words2[t - 2] + words0[u]);
                    }
                    if (i != 0) sb.Append(words3[i - 1]);
                }
                return sb.ToString().TrimEnd();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static string NumberToThaiBaht(string txt)
        {
            string bahtTxt, n, bahtTH = "";
            double amount;
            try { amount = Convert.ToDouble(txt); }
            catch { amount = 0; }
            bahtTxt = amount.ToString("####.00");
            string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
            string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
            string[] temp = bahtTxt.Split('.');
            string intVal = temp[0];
            string decVal = temp[1];
            if (Convert.ToDouble(bahtTxt) == 0)
                bahtTH = "ศูนย์บาทถ้วน";
            else
            {
                for (int i = 0; i < intVal.Length; i++)
                {
                    n = intVal.Substring(i, 1);
                    if (n != "0")
                    {
                        if ((i == (intVal.Length - 1)) && (n == "1"))
                            bahtTH += "เอ็ด";
                        else if ((i == (intVal.Length - 2)) && (n == "2"))
                            bahtTH += "ยี่";
                        else if ((i == (intVal.Length - 2)) && (n == "1"))
                            bahtTH += "";
                        else
                            bahtTH += num[Convert.ToInt32(n)];
                        bahtTH += rank[(intVal.Length - i) - 1];
                    }
                }
                bahtTH += "บาท";
                if (decVal == "00")
                    bahtTH += "ถ้วน";
                else
                {
                    for (int i = 0; i < decVal.Length; i++)
                    {
                        n = decVal.Substring(i, 1);
                        if (n != "0")
                        {
                            if ((i == decVal.Length - 1) && (n == "1"))
                                bahtTH += "เอ็ด";
                            else if ((i == (decVal.Length - 2)) && (n == "2"))
                                bahtTH += "ยี่";
                            else if ((i == (decVal.Length - 2)) && (n == "1"))
                                bahtTH += "";
                            else
                                bahtTH += num[Convert.ToInt32(n)];
                            bahtTH += rank[(decVal.Length - i) - 1];
                        }
                    }
                    bahtTH += "สตางค์";
                }
            }
            return bahtTH;
        }
    }
}