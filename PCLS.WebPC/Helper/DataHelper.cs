﻿using System;

namespace PCLS.WebPC.Helper
{
    public static class DataHelper
    {
        public static bool IsInt(string str)
        {
            try
            {
                int tmp;
                if (int.TryParse(str, out tmp))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool IsDecimal(string str)
        {
            try
            {
                decimal tmp;
                if (decimal.TryParse(str, out tmp))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}