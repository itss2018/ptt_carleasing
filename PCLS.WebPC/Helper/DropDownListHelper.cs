﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Helper
{
    public class DropDownListHelper
    {
        public static void BindDropDown(ref DropDownList cbo, DataTable dtSource, string valueFlied, string valueText, bool isHeader, string ChooseText)
        {
            try
            {
                if (dtSource == null)
                {
                    cbo.Items.Clear();
                    cbo.DataBind();

                    if (isHeader)
                    {
                        dtSource = new DataTable();
                        dtSource.Columns.Add(valueFlied);
                        dtSource.Columns.Add(valueText);

                        DataRow dr = dtSource.NewRow();
                        dr[valueText] = ChooseText;
                        dtSource.Rows.InsertAt(dr, 0);
                    }
                    cbo.DataSource = dtSource;
                    cbo.DataValueField = valueFlied;
                    cbo.DataTextField = valueText;
                    cbo.DataBind();
                }
                else
                {
                    if (isHeader)
                    {
                        DataRow dr = dtSource.NewRow();
                        dr[valueText] = ChooseText;
                        dtSource.Rows.InsertAt(dr, 0);
                    }
                    cbo.DataSource = dtSource;
                    cbo.DataValueField = valueFlied;
                    cbo.DataTextField = valueText;
                    cbo.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public static void BindRadioButtonList(ref RadioButtonList rdo, DataTable dtSource, string valueFlied, string valueText, bool isHeader, string ChooseText)
        {
            try
            {
                if (isHeader)
                {
                    DataRow dr = dtSource.NewRow();
                    dr[valueText] = ChooseText;
                    dtSource.Rows.InsertAt(dr, 0);
                }
                rdo.DataSource = dtSource;
                rdo.DataValueField = valueFlied;
                rdo.DataTextField = valueText;
                rdo.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void BindCheckBoxList(ref CheckBoxList rdo, DataTable dtSource, string valueFlied, string valueText, bool isHeader, string ChooseText)
        {
            try
            {
                if (isHeader)
                {
                    DataRow dr = dtSource.NewRow();
                    dr[valueText] = ChooseText;
                    dtSource.Rows.InsertAt(dr, 0);
                }
                rdo.DataSource = dtSource;
                rdo.DataValueField = valueFlied;
                rdo.DataTextField = valueText;
                rdo.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}