﻿using SelectPdf;
using System;
using System.Data;
using System.IO;

namespace PCLS.WebPC.Helper
{
    public static class ExportPDFHeper
    {
        public static string Export(string PathExport, string PathFileFormat, int MarginSize, string FileName, DataTable dtReplace, int PageHeight)
        {
            try
            {
                string ContentForm = File.ReadAllText(PathFileFormat);
                for (int i = 0; i < dtReplace.Rows.Count; i++)
                    ContentForm = ContentForm.Replace(dtReplace.Rows[i]["PARAMETER"].ToString(), dtReplace.Rows[i]["VALUE"].ToString());

                if (PageHeight > 0)
                {
                    ContentForm = "<div style=\"height:" + PageHeight.ToString() + "cm\">" + ContentForm;
                    ContentForm = ContentForm.Replace("{P1_END}", "</div>");
                }

                if (MarginSize == 0)
                    MarginSize = 45;
                
                HtmlToPdf converter = new HtmlToPdf();
                converter.Options.MarginTop = MarginSize;
                converter.Options.MarginLeft = MarginSize - 35;
                converter.Options.MarginBottom = MarginSize;
                converter.Options.MarginRight = MarginSize - 35;

                PdfDocument doc = converter.ConvertHtmlString(ContentForm);
                
                if (!Directory.Exists(PathExport))
                    Directory.CreateDirectory(PathExport);

                //string FullPath = PathExport + "\\" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".pdf";
                string FullPath = PathExport + "\\" + FileName.Replace(".pdf", string.Empty) + ".pdf";

                doc.Save(FullPath);
                doc.Close();

                return FullPath;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}