﻿using System;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Helper
{
    public static class FileUploadHelper
    {
        private static void ValidateUploadFile(string ExtentionUser, int FileSize, DataTable dtUploadType, DropDownList cboUploadType)
        {
            try
            {
                string Extention = dtUploadType.Rows[cboUploadType.SelectedIndex]["Extention"].ToString();
                int MaxSize = int.Parse(dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

                if (FileSize > MaxSize)
                    throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[cboUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

                if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                    throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string CheckPath(string PathUpload)
        {
            try
            {
                if (!Directory.Exists(PathUpload))
                    Directory.CreateDirectory(PathUpload);

                return PathUpload;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void StartUpload(string PathUpload, FileUpload fileUpload, DataTable dtUploadType, DropDownList cboUploadType, DataTable dtUpload, GridView dgvUploadFile)
        {
            try
            {
                if (cboUploadType.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, "ประเภทไฟล์เอกสาร"));

                if (fileUpload.HasFile)
                {
                    string FileNameUser = fileUpload.PostedFile.FileName;
                    string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                    ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload.PostedFile.ContentLength, dtUploadType, cboUploadType);
                    CheckPath(PathUpload);

                    fileUpload.SaveAs(PathUpload + "\\" + FileNameSystem);

                    dtUpload.Rows.Add(cboUploadType.SelectedValue, cboUploadType.SelectedItem, Path.GetFileName(PathUpload + "\\" + FileNameSystem), FileNameUser, PathUpload + "\\" + FileNameSystem);
                    GridViewHelper.BindGridView(dgvUploadFile, dtUpload, false);
                }
                else
                {
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, "ไฟล์ที่ต้องการอัพโหลด"));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void CheckRequestFile(GridView dgvUploadRequestFile, DataTable dtUploadRequestFile, DataTable dtUpload)
        {
            try
            {
                string UploadID = string.Empty;
                CheckBox chk = new CheckBox();

                for (int j = 0; j < dtUploadRequestFile.Rows.Count; j++)
                {
                    chk = (CheckBox)dgvUploadRequestFile.Rows[j].FindControl("chkUploaded");
                    if (chk != null)
                        chk.Checked = false;
                }

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    UploadID = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    for (int j = 0; j < dtUploadRequestFile.Rows.Count; j++)
                    {
                        if (string.Equals(UploadID, dtUploadRequestFile.Rows[j]["UPLOAD_ID"].ToString()))
                        {
                            chk = (CheckBox)dgvUploadRequestFile.Rows[j].FindControl("chkUploaded");
                            if (chk != null)
                                chk.Checked = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}