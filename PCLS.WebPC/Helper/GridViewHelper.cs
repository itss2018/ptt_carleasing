﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Helper
{
    public static class GridViewHelper
    {
        public static void BindGridView(GridView dgvSource, DataTable dataSource, bool AssignColor)
        {
            if (dataSource != null)
            {
                dgvSource.DataSource = dataSource;
                dgvSource.DataBind();

                if (AssignColor)
                    SetColor(dgvSource, dataSource);
            }
            else
            {
                dgvSource.DataSource = null;
                dgvSource.DataBind();
            }
        }

        public static void SetColor(GridView dgvSource, DataTable dataSource)
        {
            try
            {
                string ColorName = string.Empty;
                //var converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(Color));

                for (int i = 0; i < dgvSource.Rows.Count; i++)
                {
                    if (!string.Equals(dataSource.Rows[i]["ROW_COLOR"].ToString(), string.Empty))
                    {
                        ColorName = dataSource.Rows[i]["ROW_COLOR"].ToString();
                        dgvSource.Rows[i].ForeColor = System.Drawing.Color.FromName(ColorName);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static int GetColumnIndexByName(GridView dgv, string ColumnName)
        {
            int i = 0;
            string currentDataField = string.Empty;

            foreach (Object bfName in dgv.Columns)
            {
                if (string.Equals(bfName.GetType().ToString(), "System.Web.UI.WebControls.BoundField"))
                {
                    currentDataField = ((BoundField)bfName).DataField;
                    if (string.Equals(((BoundField)bfName).DataField, ColumnName))
                        return i;
                }

                i++;
            }

            return -1;
        }
    }
}