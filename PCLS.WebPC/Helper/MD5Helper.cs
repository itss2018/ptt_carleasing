﻿
using System.Security.Cryptography;
using System.Text;
namespace PCLS.WebPC.Helper
{
    public static class MD5Helper
    {
        public static string GetMD5Hash(string input)
        {
            MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = Encoding.UTF8.GetBytes(input);
            bs = md5.ComputeHash(bs);
            StringBuilder s = new StringBuilder();
            foreach (byte b in bs)
                s.Append(b.ToString("x2").ToLower());

            return s.ToString();
        }
    }
}