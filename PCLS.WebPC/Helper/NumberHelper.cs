﻿using System;

namespace PCLS.WebPC.Helper
{
    public static class NumberHelper
    {
        public static bool IsInt(string Input)
        {
            try
            {
                int tmp;
                return int.TryParse(Input, out tmp);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static bool IsDecimal(string Input)
        {
            try
            {
                decimal tmp;
                return decimal.TryParse(Input, out tmp);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}