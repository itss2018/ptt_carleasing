﻿using System;
using System.Text;
using System.Web.Security;

namespace PCLS.WebPC
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
                {
                    string RedirectURL = Encoding.UTF8.GetString(MachineKey.Decode(Request.QueryString["URL"], MachineKeyProtection.All));
                    Response.Redirect("Pages/Authentication/Login.aspx?URL=" + MachineKey.Encode(Encoding.UTF8.GetBytes(RedirectURL), MachineKeyProtection.All), false);
                }
                else
                    Response.Redirect("Pages/Authentication/Login.aspx");
            }
            catch
            {
                Response.Redirect("Pages/Authentication/Login.aspx");
            }
        }
    }
}