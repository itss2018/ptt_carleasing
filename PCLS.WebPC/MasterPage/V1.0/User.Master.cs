﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.MasterPage
{
    public partial class User : System.Web.UI.MasterPage
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    byte[] plaintextBytes = Encoding.UTF8.GetBytes("Driver");
                    string str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    //aDriver.HRef = "../../Pages/Master/Driver.aspx?str=" + str;
                    //aDriver2.HRef = "../../Pages/Master/Driver.aspx?str=" + str;
                    //plaintextBytes = Encoding.UTF8.GetBytes("REQUEST");
                    //str = MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
                    //aDriverRequest.HRef = "../../Pages/Master/Driver.aspx?str=" + str;
                    if (Session["UserLogin"] != null)
                    {
                        DataTable dtUserLogin = (DataTable)Session["UserLogin"];

                        lblLoginName.Text = dtUserLogin.Rows[0]["FULLNAME"].ToString();
                        lblLoginName2.Text = dtUserLogin.Rows[0]["FULLNAME"].ToString();

                        string ImagePath = dtUserLogin.Rows[0]["IMAGE_PATH"].ToString();
                        if (string.Equals(ImagePath, string.Empty))
                            ImagePath = "~/Images/UserProfile/imgProfileDefault.png";

                        imgProfile.Src = ImagePath;
                        imgProfile2.Src = ImagePath;
                        imgProfile3.Src = ImagePath;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void aLogout_ServerClick(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();

            Response.Redirect("../../Index.aspx");
        }
    }
}