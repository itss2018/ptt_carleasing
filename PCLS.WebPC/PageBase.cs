﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using System;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Linq;
using PCLS.WebPC.Properties;
using GemBox.Spreadsheet;
using System.Drawing;
using System.IO;
using PCLS.Bussiness.SystemData;

namespace PCLS.WebPC
{
    public class PageBase : System.Web.UI.Page
    {
        #region + View State +
        public bool CanRead
        {
            get
            {
                return (bool)ViewState["CanRead"];
            }
            set
            {
                ViewState["CanRead"] = value;
            }
        }

        public bool CanWrite
        {
            get
            {
                return (bool)ViewState["CanWrite"];
            }
            set
            {
                ViewState["CanWrite"] = value;
            }
        }

        public int UserID
        {
            get
            {
                return (int)ViewState["UserID"];
            }
            set
            {
                ViewState["UserID"] = value;
            }
        }

        public String UserName
        {
            get
            {
                return (String)ViewState["UserName"];
            }
            set
            {
                ViewState["UserName"] = value;
            }
        }


        public string FullName
        {
            get
            {
                return ViewState["FullName"].ToString();
            }
            set
            {
                ViewState["FullName"] = value;
            }
        }

        public int VendorID
        {
            get
            {
                return (int)ViewState["VendorID"];
            }
            set
            {
                ViewState["VendorID"] = value;
            }
        }

        public string VendorCode
        {
            get
            {
                return (string)ViewState["VendorCode"];
            }
            set
            {
                ViewState["VendorCode"] = value;
            }
        }
        #endregion

        #region OnPreInit
        protected override void OnPreInit(EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo(ConfigurationManager.AppSettings["Culture"]);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(ConfigurationManager.AppSettings["Culture"]);
            base.OnPreInit(e);
        }
        #endregion

        #region + OnLoad +
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    this.CheckAuthen();

                    CanRead = true;
                    CanWrite = true;

                    if (Session["UserLogin"] != null)
                    {
                        DataTable dtUserLogin = (DataTable)Session["UserLogin"];

                        UserID = int.Parse(dtUserLogin.Rows[0]["USER_ID"].ToString());
                        FullName = dtUserLogin.Rows[0]["FULLNAME"].ToString();
                        VendorID = (string.Equals(dtUserLogin.Rows[0]["VENDOR_ID"].ToString(), string.Empty)) ? -1 : int.Parse(dtUserLogin.Rows[0]["VENDOR_ID"].ToString());
                        VendorCode = dtUserLogin.Rows[0]["VENDOR_CODE"] + string.Empty;
                    }
                    else
                    {
                        //Session.Abandon();
                        //Session.Clear();
                        Session.RemoveAll();
                    }
                }
                base.OnLoad(e);
            }
            catch (Exception ex)
            {
                alertFail("ระบบเกิดความผิดพลาด :" + RemoveSpecialCharacters(ex.Message));
            }
        }
        #endregion

        #region + Check Authen +
        private void CheckAuthen()
        {
            //try
            //{
            //    string[] file = Request.CurrentExecutionFilePath.Split('/');
            //    string URL = "/" + file[file.Length - 2] + "/" + file[file.Length - 1];

            //    if (URL.Contains("ChoosePlant.aspx") 
            //     || URL.Contains("Dashboard.aspx")
            //     || URL.Contains("ShowReport.aspx")
            //     || URL.Contains("ShowReport2.aspx")
            //     || URL.Contains("GasTypeAddEdit.aspx")
            //     || URL.Contains("GasTankAddEdit.aspx")
            //     || URL.Contains("GasRepairTypeAddEdit.aspx")
            //     || URL.Contains("WarehouseAddEdit.aspx")
            //     || URL.Contains("PlantAddEdit.aspx")
            //     || URL.Contains("S_logAddEdit.aspx")
            //     || URL.Contains("VendorAddEdit.aspx")
            //     || URL.Contains("CustomerAddEdit.aspx")
            //     || URL.Contains("VendorAddEdit.aspx")
            //     || URL.Contains("UserAddEdit.aspx")
            //     || URL.Contains("UserGroupAddEdit.aspx"))
            //        return;

            //    DataTable dtAuthen = (DataTable)Session["dtAuthen"];
            //    DataRow[] dr = dtAuthen.Select("VISIBLE = 1 AND MENU_URL LIKE '%" + URL + "'");
            //    if (dr.Length == 0)
            //        Response.Redirect("../Other/NotAuthorize.aspx");

            //    CanRead = string.Equals(dr[0]["READ"].ToString(), "1") ? true : false;
            //    CanWrite = string.Equals(dr[0]["WRITE"].ToString(), "1") ? true : false;
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}
        }
        #endregion

        #region + Alert +
        public void alertSuccess(string title, string msg, string url)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alertSuccess('" + title + "','" + msg + "','" + url + "')", true);
        }

        public void alertSuccess(string msg)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alertSuccess('','" + msg + "','')", true);
        }
        public void alertSuccess(string title, string msg)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alertSuccess('" + RemoveSpecialCharacters(title) + "','" + RemoveSpecialCharacters(msg) + "','')", true);
        }
        public void alertFail(string msg)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alertFail('" + msg + "')", true);
        }
        public void alertFail(string title, string msg)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alertFailTitle('" + title + "','" + msg + "')", true);
        }
        #endregion

        #region + RemoveSpecialCharacters +
        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9_. ก-๙]+", "", RegexOptions.Compiled);
        }
        #endregion

        #region + EncodeQueryString +
        /// <summary>
        /// ใช้ Encode String โดยส่งผ่าน QueryString รูปแบบ String1&String2&String3
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        protected string EncodeQueryString(string str)
        {
            byte[] plaintextBytes = Encoding.UTF8.GetBytes(str);
            return MachineKey.Encode(plaintextBytes, MachineKeyProtection.All);
        }
        #endregion

        #region + DecodeQueryString +
        /// <summary>
        /// ช้ Decode String ที่ส่งผ่าน QueryString รูปแบบ String1&String2&String3
        /// </summary>
        /// <param name="str"></param>
        protected string DecodeQueryString(string str)
        {
            byte[] plaintextBytes = MachineKey.Decode(str, MachineKeyProtection.All);
            str = Encoding.UTF8.GetString(plaintextBytes);
            return str;
        }
        #endregion

        #region + Enable Control +
        public static void EnableControls(System.Web.UI.Control control)
        {
            foreach (System.Web.UI.Control c in control.Controls)
            {
                // Get the Enabled property by reflection.
                Type type = c.GetType();
                PropertyInfo prop = type.GetProperty("Enabled");

                // Set it to False to disable the control.
                if (prop != null)
                    prop.SetValue(c, true, null);

                // Recurse into child controls.
                if (c.Controls.Count > 0)
                    EnableControls(c);
            }
        }
        #endregion

        #region + Disable Control +
        public static void DisableControls(System.Web.UI.Control control)
        {
            try
            {
                foreach (System.Web.UI.Control c in control.Controls)
                {
                    // Get the Enabled property by reflection.

                    if (!string.Equals(c.ID, "cmdLogout") && !string.Equals(c.ID, "cmdModalClose") && !string.Equals(c.ID, "cmdModalSave"))
                    {//ContentPlaceHolder1_dgvUploadFile_imgView_0
                        Type type = c.GetType();
                        PropertyInfo prop = type.GetProperty("Enabled");

                        // Set it to False to disable the control.
                        if (prop != null)
                            prop.SetValue(c, false, null);
                    }

                    // Recurse into child controls.
                    if (c.Controls.Count > 0)
                        DisableControls(c);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + InitialStatus +
        public void InitialStatus(DropDownList ddlStatus, string Condition)
        {
            try
            {
                DataTable dtStatus = StatusBLL.Instance.StatusSelectBLL(Condition);
                DropDownListHelper.BindDropDown(ref ddlStatus, dtStatus, "STATUS_VALUE", "STATUS_NAME", true, Properties.Resources.DropDownChooseText);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void InitialStatus(RadioButtonList radStatus, string Condition)
        {
            try
            {
                DataTable dtStatus = StatusBLL.Instance.StatusSelectBLL(Condition);
                radStatus.DataSource = dtStatus;
                radStatus.DataValueField = "STATUS_VALUE";
                radStatus.DataTextField = "STATUS_NAME";
                radStatus.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region + InitialBrand +
        public void InitialBrand(DropDownList ddlBrand, string Condition)
        {
            try
            {
                DataTable dtBrand = CarBrandBLL.Instance.CarBrandSelectBLL(Condition);
                DropDownListHelper.BindDropDown(ref ddlBrand, dtBrand, "BRANDID", "BRANDNAME", true, Properties.Resources.DropDownChooseText);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void InitialBrand(RadioButtonList radBrand, string Condition)
        {
            try
            {
                DataTable dtCarBrand = CarBrandBLL.Instance.CarBrandSelectBLL(Condition);
                radBrand.DataSource = dtCarBrand;
                radBrand.DataValueField = "BRANDID";
                radBrand.DataTextField = "BRANDNAME";
                radBrand.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        #region + InitialCarType +
        public void InitialCarType(DropDownList ddlCarType, string Condition)
        {
            try
            {
                DataTable dtCarType = CarTypeBLL.Instance.CarTypeSelectBLL(Condition);
                DropDownListHelper.BindDropDown(ref ddlCarType, dtCarType, "CARTYPEID", "CARTYPE", true, Properties.Resources.DropDownChooseText);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void InitialCarType(RadioButtonList radCarType, string Condition)
        {
            try
            {
                DataTable dtCarType = CarTypeBLL.Instance.CarTypeSelectBLL(Condition);
                radCarType.DataSource = dtCarType;
                radCarType.DataValueField = "CARTYPEID";
                radCarType.DataTextField = "CARTYPE";
                radCarType.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        public void AssignDropDown(DataTable dtTruck, ref DropDownList ddl, string ColumnName)
        {
            try
            {
                if (!string.Equals(dtTruck.Rows[0][ColumnName].ToString(), string.Empty))
                    ddl.SelectedValue = dtTruck.Rows[0][ColumnName].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region AssignTextBox
        /// <summary>
        /// Set ค่า จาก DataTable Row 0 ให้ TextBox ตาม ColumnName ที่ส่งมา
        /// </summary>
        /// <param name="dtTruck"></param>
        /// <param name="txt"></param>
        /// <param name="ColumnName"></param>
        public void AssignTextBox(DataTable dtTruck, ref TextBox txt, string ColumnName)
        {
            try
            {
                if (!string.Equals(dtTruck.Rows[0][ColumnName].ToString(), string.Empty))
                    txt.Text = dtTruck.Rows[0][ColumnName].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AssignCheckBox
        /// <summary>
        /// Set ค่า True ให้ CheckBox โดย 1 = True ,0 = False จาก DataTable Row 0 ตาม ColumnName ที่ส่งมา
        /// </summary>
        /// <param name="dtTruck"></param>
        /// <param name="chk"></param>
        /// <param name="ColumnName"></param>
        public void AssignCheckBox(DataTable dtTruck, ref CheckBox chk, string ColumnName)
        {
            try
            {
                if (string.Equals(dtTruck.Rows[0][ColumnName].ToString(), (int)IS_ACTIVE_STATUS.Active + string.Empty))
                    chk.Checked = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region AssignDropDownText
        /// <summary>
        /// Set ค่า จาก DataTable Row 0 ให้ DropDownList(Text) ตาม ColumnName ที่ส่งมา
        /// </summary>
        /// <param name="dtTruck"></param>
        /// <param name="ddl"></param>
        /// <param name="ColumnName"></param>
        public void AssignDropDownText(DataTable dtTruck, ref DropDownList ddl, string ColumnName)
        {
            try
            {
                if (!string.Equals(dtTruck.Rows[0][ColumnName].ToString(), string.Empty))
                    ddl.SelectedItem.Text = dtTruck.Rows[0][ColumnName].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        protected void SetDropDownList_Vendor_IS_ACTIVE(ref DropDownList ddl, int VendorID)
        {
            // กรณีผู้ขนส่งถูกระงับใช้งาน ให้แสดงชื่อผู้ขนส่งใน Dropdown List
            string StrWhere = "%";
            if (VendorID != 0)
            {
                StrWhere = VendorID.ToString();
            }

            DataTable dt = VendorBLL.Instance.VendorSelectBLL(StrWhere, "%", "%", "%");
            DropDownListHelper.BindDropDown(ref ddl, dt, "VENDOR_ID", "VENDOR_NAME", true, Resources.DropDownChooseText);
        }

        protected enum DRIVER_REQUEST
        {

            NewRequest = 0,//ผขส. ขอเพิ่มข้อมูล
            EditRequest = 1//ผขส. ขอแก้ไขข้อมูล
        }

        protected enum IS_ACTIVE_STATUS
        {
            Active = 1,               //ใช้งาน
            Inactive = 0,             //ไม่ใช้งาน
        }

        protected enum REQUIRE_TYPE
        {
            REQUIRE = 1, OPTIONAL = 2, DISABLE = 3
        }

        #region + M_STATUS +
        protected enum Mode
        {
            View, Edit, Add, Copy
        }

        protected enum TRUCK_REQUEST_STATUS
        {
            RequestNew = 1,                                                         //ผขส. ขอเพิ่มข้อมูล
            RequestApprove = 2,                                                     //ขปน. อนุมัติ (เพิ่มข้อมูล)
            RequestReject = 3,                                                      //ขปน. ไม่อนุมัติ (เพิ่มข้อมูล)
            RequestDocument = 4,                                                    //ขปน. ขอเอกสาร (เพิ่มข้อมูล)
            EditNew = 5,                                                            //ผขส. ขอแก้ไขข้อมูล
            EditApprove = 6,                                                        //ขปน. อนุมัติ (แก้ไขข้อมูล)
            EditReject = 7,                                                         //ขปน. ไม่อนุมัติ (แก้ไขข้อมูล)
            EditDocument = 8                                                        //ขปน. ขอเอกสาร (แก้ไขข้อมูล)
        }

        protected enum TRUCK_STATUS
        {
            InActive = 0,           // ระงับใช้งาน
            Active = 1,             // พร้อมใช้งาน
            Blacklist = 2,          // BLACK LIST
            NotAvailable = 3,       // ยังไม่พร้อมใช้งาน
            TruckAgeOf8Years = 4    // age of 8 years
        }

        protected enum TRUCK_TYPE
        {
            Type1 = 1,                                                              //รถเดี่ยว
            Type2 = 2,                                                              //หัวลาก
            Type3 = 3                                                               //หางลาก
        }

        protected enum DRIVER_STATUS
        {
            Active = 1,               //ใช้งาน
            UnActive = 0,             //ระงับใช้งาน
            BlackList = 2,              //BLACK LIST

        }

        /// <summary>
        /// จาก database table m_status
        /// </summary>
        protected enum DRIVER_REQUEST_STATUS
        {
            NewRequest = 0,//ผขส. ขอเพิ่มข้อมูล
            EditRequest = 1,//ผขส. ขอแก้ไขข้อมูล
            NewRequestDocument = 2,//ขปน. ขอเอกสาร (เพิ่มข้อมูล)
            EditRequestDocument = 3,//ขปน. ขอเอกสาร (แก้ไขข้อมูล)
            NewApprove = 4,//ขปน. อนุมัติ (เพิ่มข้อมูล)
            EditApprove = 5,//ขปน. อนุมัติ (แก้ไขข้อมูล)
            NewNotApprove = 6,//ขปน. ไม่อนุมัติ (เพิ่มข้อมูล)
            EditNotApprove = 7//ขปน. ไม่อนุมัติ (แก้ไขข้อมูล)
        }
        #endregion

        public void GetVendor(DropDownList ddlVendor, string VendorIDSearch, string VendorCodeSearch, string VendorNameSearch, string IsActiveSearch)
        {
            try
            {
                try
                {
                    DataTable dtVendor = VendorBLL.Instance.VendorSelectBLL(VendorIDSearch, VendorCodeSearch, VendorNameSearch, IsActiveSearch);
                    DropDownListHelper.BindDropDown(ref ddlVendor, dtVendor, "VENDOR_ID", "VENDOR_NAME", true, Properties.Resources.DropDownChooseText);

                    if (VendorID > 0)
                    {
                        ddlVendor.SelectedValue = VendorID.ToString();
                        ddlVendor.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #region ExportExcel
        /// <summary>
        /// ExportExcel ตาม Field ในหน้าจอ Field.aspx Column IS_EXPORT โดยเรียงลำดับตาม ListNo และListNo ต้องเรียงกันให้ถูกต้องไม่มีการข้ามตัวเลขและต้องเริ้มจาก 1 เช่น 1,2,3,4.....
        /// FileTemplate กรณีไม่มี string.Empty กรณีมี ให้ส่งแต่ชื่อไฟล์ไม่มีนามสกุลเช่น Driver โดยเก็บไฟล์ไว้ที่ ~/FileFormat 
        /// วิธีการเรียกให้ ~/FileFormat/" + FileTemplate + ".xlsx
        /// dtData ข้อมูลที่ต้องการ Export
        /// StartRow ข้อมูลเริ่มแสดงใน Excel  row ไหน เช่น 1
        /// StartColumn ข้อมูลเริ่มแสดงใน Excel  Column ไหน เช่น 1 คือ Column A
        /// </summary>
        /// <param name="FieldType"></param>
        /// <param name="FileTemplate"></param>
        /// <param name="dtData"></param>
        /// <param name="StartRow"></param>
        /// <param name="StartColumn"></param>
        protected void ExportExcel(string FieldType, string FileTemplate, DataTable dtData, int StartRow, int StartColumn)
        {
            SpreadsheetInfo.SetLicense("EQU2-1000-0000-000U");
            ExcelFile workbook = new ExcelFile();
            ExcelWorksheet worksheet;
            if (string.IsNullOrEmpty(FileTemplate))
            {
                workbook = new ExcelFile();
                workbook.Worksheets.Add("Export");
                worksheet = workbook.Worksheets["Export"];
            }
            else
            {
                workbook = ExcelFile.Load(Server.MapPath("~/FileFormat/" + FileTemplate + ".xlsx"));
                worksheet = workbook.Worksheets["Sheets"];
            }

            worksheet.InsertDataTable(dtData, new InsertDataTableOptions(StartRow, StartColumn) { ColumnHeaders = false });
            for (int j = 0; j < dtData.Columns.Count; j++)
            {//Export Detail
                this.SetFormatCell(worksheet.Cells[0, j], dtData.Columns[j].ColumnName, VerticalAlignmentStyle.Top, HorizontalAlignmentStyle.Center, false);
                worksheet.Cells[0, j].Style.FillPattern.SetPattern(FillPatternStyle.Solid, Color.Yellow, Color.Black);
                worksheet.Columns[j].AutoFit();
            }
            string Path = this.CheckPath();
            string FileName = FieldType + "_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xlsx";

            workbook.Save(Path + "\\" + FileName);
            this.DownloadFile(Path, FileName);
        }

        /// <summary>
        /// DownloadFile Excal เท่านั้น
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="FileName"></param>
        private void DownloadFile(string Path, string FileName)
        {
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(Path + "\\" + FileName);
            Response.End();
        }
        /// <summary>
        /// CheckPath ที่จะ ไว้เก็บไฟล์ Excel ที่ต้อง Export
        /// </summary>
        /// <returns></returns>
        private string CheckPath()
        {
            try
            {
                string PathExport = Server.MapPath("~") + "\\" + "Export";
                if (!Directory.Exists(PathExport))
                    Directory.CreateDirectory(PathExport);

                return PathExport;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// กรณีต้องการ Set สีให้ Excel
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="value"></param>
        /// <param name="VerticalAlign"></param>
        /// <param name="HorizontalAlign"></param>
        /// <param name="WrapText"></param>
        private void SetFormatCell(ExcelCell cell, string value, VerticalAlignmentStyle VerticalAlign, HorizontalAlignmentStyle HorizontalAlign, bool WrapText)
        {
            try
            {
                cell.Value = value;
                cell.Style.VerticalAlignment = VerticalAlign;
                cell.Style.HorizontalAlignment = HorizontalAlign;
                cell.Style.WrapText = WrapText;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region UploadFile
        #region GetUploadType
        /// <summary>
        /// ใช้ Set DrowDownList ของ Type เอกสารที่ upload Return Datatable ของ ข้อมูล File Upload
        /// </summary>
        /// <param name="ddlUploadType"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        protected DataTable GetUploadType(ref DropDownList ddlUploadType, string type)
        {
            DataTable dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL(" AND UPLOAD_TYPE = '" + type + "'");
            DropDownListHelper.BindDropDown(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true, Properties.Resources.DropDownChooseText);

            return dtUploadType;
        }
        #endregion

        #region GetTableUpload
        /// <summary>
        /// Create DataTable สำหรับ Upload File
        /// </summary>
        /// <returns></returns>
        protected DataTable GetTableUpload()
        {
            DataTable dtUpload = new DataTable();
            dtUpload.Columns.Add("UPLOAD_ID");
            dtUpload.Columns.Add("UPLOAD_NAME");
            dtUpload.Columns.Add("FILENAME_SYSTEM");
            dtUpload.Columns.Add("FILENAME_USER");
            dtUpload.Columns.Add("FULLPATH");

            return dtUpload;
        }
        #endregion

        #region GetUploadRequire
        /// <summary>
        /// Set ค่าให้ GridView ของ RequestFile ตาม UPLOAD_TYPE และ GridView ที่ส่งเข้ามา
        /// </summary>
        /// <param name="dgvUploadRequestFile"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        protected DataTable GetUploadRequire(ref GridView dgvUploadRequestFile, string type)
        {
            DataTable dtUploadRequestFile = UploadBLL.Instance.UploadRequestFileBLL(" AND M_REQUEST_FILE.UPLOAD_TYPE = '" + type + "'");
            GridViewHelper.BindGridView(dgvUploadRequestFile, dtUploadRequestFile, false);
            return dtUploadRequestFile;
        }
        #endregion

        #region ValidateUpload
        /// <summary>
        /// ValidateUpload ตรวจสอบว่าเลือก ประเภทไฟล์เอกสาร หรือไม่
        /// </summary>
        /// <param name="ddlUploadType"></param>
        protected void ValidateUpload(ref DropDownList ddlUploadType)
        {
            try
            {
                if (ddlUploadType.SelectedIndex == 0)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, "ประเภทไฟล์เอกสาร"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region ValidateUploadFile
        /// <summary>
        /// ตรวจสอบ ขนาดไฟล์และ นามสกุล
        /// </summary>
        /// <param name="ExtentionUser"></param>
        /// <param name="FileSize"></param>
        /// <param name="dtUploadType"></param>
        /// <param name="ddlUploadType"></param>
        protected void ValidateUploadFile(string ExtentionUser, int FileSize, DataTable dtUploadType, DropDownList ddlUploadType)
        {
            try
            {
                string Extention = dtUploadType.Rows[ddlUploadType.SelectedIndex]["Extention"].ToString();
                int MaxSize = int.Parse(dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString()) * 1024 * 1024; //Convert to Byte

                if (FileSize > MaxSize)
                    throw new Exception("ไฟล์มีขนาดใหญ่เกินที่กำหนด (ต้องไม่เกิน " + dtUploadType.Rows[ddlUploadType.SelectedIndex]["MAX_FILE_SIZE"].ToString() + " MB)");

                if (!Extention.Contains("*.*") && !Extention.Contains(ExtentionUser))
                    throw new Exception("ไฟล์นามสกุล " + ExtentionUser + " ไม่สามารถอัพโหลดได้");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region CheckPath
        /// <summary>
        /// ตรวจสอบ Pathที่จะวางไฟล์
        /// </summary>
        /// <param name="FieldType"></param>
        /// <returns></returns>
        protected string CheckPath(string FieldType)
        {
            try
            {
                string PathExport = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + FieldType;
                if (!Directory.Exists(PathExport))
                    Directory.CreateDirectory(PathExport);

                return PathExport;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region StartUpload
        /// <summary>
        /// Upload และแสดง ไฟล์ที่ Upload ใน Grid
        /// </summary>
        /// <param name="ddlUploadType"></param>
        /// <param name="fileUpload1"></param>
        /// <param name="dtUploadType"></param>
        /// <param name="dtUploadRequestFile"></param>
        /// <param name="dtUpload"></param>
        /// <param name="dgvUploadFile"></param>
        /// <param name="dgvUploadRequestFile"></param>
        /// <returns></returns>
        protected DataTable StartUpload(ref DropDownList ddlUploadType, FileUpload fileUpload1, DataTable dtUploadType, DataTable dtUploadRequestFile, DataTable dtUpload, ref GridView dgvUploadFile, ref GridView dgvUploadRequestFile)
        {
            ValidateUpload(ref ddlUploadType);

            if (fileUpload1.HasFile)
            {
                string FileNameUser = fileUpload1.PostedFile.FileName;
                string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                ValidateUploadFile(System.IO.Path.GetExtension(FileNameUser), fileUpload1.PostedFile.ContentLength, dtUploadType, ddlUploadType);

                string Path = this.CheckPath();
                fileUpload1.SaveAs(Path + "\\" + FileNameSystem);

                dtUpload.Rows.Add(ddlUploadType.SelectedValue, ddlUploadType.SelectedItem, System.IO.Path.GetFileName(Path + "\\" + FileNameSystem), FileNameUser, Path + "\\" + FileNameSystem);
                GridViewHelper.BindGridView(dgvUploadFile, dtUpload, false);

                this.CheckRequestFile(ref dgvUploadRequestFile, dtUploadRequestFile, dtUpload);

            }
            else
            {
                alertFail(Properties.Resources.FileUploadNotFound);
            }
            return dtUpload;

        }
        #endregion

        #region CheckRequestFile
        /// <summary>
        /// ตรวจสอบว่า ไฟล์ที่ Request ได้ถูก Upload แล้วหรือไม่ ถ้า Upแล้ว จะ ติ๊กถูกที่ CheckBox
        /// </summary>
        /// <param name="dgvUploadRequestFile"></param>
        /// <param name="dtUploadRequestFile"></param>
        /// <param name="dtUpload"></param>
        protected void CheckRequestFile(ref GridView dgvUploadRequestFile, DataTable dtUploadRequestFile, DataTable dtUpload)
        {
            try
            {
                string UploadID = string.Empty;
                CheckBox chk = new CheckBox();

                for (int j = 0; j < dtUploadRequestFile.Rows.Count; j++)
                {
                    chk = (CheckBox)dgvUploadRequestFile.Rows[j].FindControl("chkUploaded");
                    if (chk != null)
                        chk.Checked = false;
                }

                for (int i = 0; i < dtUpload.Rows.Count; i++)
                {
                    UploadID = dtUpload.Rows[i]["UPLOAD_ID"].ToString();
                    for (int j = 0; j < dtUploadRequestFile.Rows.Count; j++)
                    {
                        if (string.Equals(UploadID, dtUploadRequestFile.Rows[j]["UPLOAD_ID"].ToString()))
                        {
                            chk = (CheckBox)dgvUploadRequestFile.Rows[j].FindControl("chkUploaded");
                            if (chk != null)
                                chk.Checked = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #endregion

        #region DropDrown & RadioButtonList
        protected void SetRadioButtonListIs_Active(ref RadioButtonList rbl, string STATUS_TYPE, bool ASC)
        {
            DataTable dt = StatusBLL.Instance.StatusSelectBLL(" AND STATUS_TYPE = '" + STATUS_TYPE + "'");

            dt.DefaultView.Sort = "STATUS_VALUE " + (ASC ? "ASC" : "DESC");
            DropDownListHelper.BindRadioButtonList(ref rbl, dt, "STATUS_VALUE", "STATUS_NAME", false, string.Empty);
        }

        protected void SetDropDownListIs_Active(ref DropDownList ddl, string STATUS_TYPE, bool ASC)
        {
            DataTable dt = StatusBLL.Instance.StatusSelectBLL(" AND STATUS_TYPE = '" + STATUS_TYPE + "'");
            dt.DefaultView.Sort = "STATUS_VALUE " + (ASC ? "ASC" : "DESC");
            DropDownListHelper.BindDropDown(ref ddl, dt, "STATUS_VALUE", "STATUS_NAME", true, Resources.DropDownChooseText);
        }

        protected void SetDropDownListCOLUMN_NAME(ref DropDownList ddl, object ViewStateDataField, bool ASC)
        {
            DataTable dt = (DataTable)ViewStateDataField;
            if (dt != null)
            {
                DataRow[] drs = dt.Select("LISTNO > 0 AND IS_VIEW = " + ((int)IS_ACTIVE_STATUS.Active + string.Empty));
                if (drs.Any())
                {
                    dt = drs.CopyToDataTable();
                    dt.DefaultView.Sort = "LISTNO " + (ASC ? "ASC" : "DESC");
                    DropDownListHelper.BindDropDown(ref ddl, dt, "COLUMN_NAME", "CONTROL_LABEL_NAME", true, Resources.DropDownChooseText);
                }
            }
        }

        protected void SetDropDownList_Vendor_IS_ACTIVE(ref DropDownList ddl, int SystemID, string VendorCode)
        {
            // กรณีผู้ขนส่งถูกระงับใช้งาน ให้แสดงชื่อผู้ขนส่งใน Dropdown List
            string StrWhere = "%";
            if (!string.IsNullOrEmpty(VendorCode))
            {
                StrWhere = VendorCode.ToString();
            }

            DataTable dt = VendorBLL.Instance.VendorSelectBLL("%", StrWhere, "%", "%");
            DropDownListHelper.BindDropDown(ref ddl, dt, "VENDOR_CODE", "VENDOR_NAME", true, Resources.DropDownChooseText);
        }

        protected void SetDropDownList_Vendor_IS_ACTIVE(ref DropDownList ddl, int SystemID, int VendorID)
        {
            // กรณีผู้ขนส่งถูกระงับใช้งาน ให้แสดงชื่อผู้ขนส่งใน Dropdown List
            string StrWhere = "%";
            if (VendorID != 0)
            {
                StrWhere = VendorID.ToString();
            }

            DataTable dt = VendorBLL.Instance.VendorSelectBLL(StrWhere, "%", "%", "%");
            DropDownListHelper.BindDropDown(ref ddl, dt, "VENDOR_ID", "VENDOR_NAME", true, Resources.DropDownChooseText);
        }
        #endregion

        protected string ValidateFile(GridView dgvUploadRequestFile)
        {
            string mess = string.Empty;
            CheckBox chk = new CheckBox();
            for (int i = 0; i < dgvUploadRequestFile.Rows.Count; i++)
            {
                chk = (CheckBox)dgvUploadRequestFile.Rows[i].FindControl("chkUploaded");
                if (chk != null)
                {
                    if (!chk.Checked)
                        mess += string.Format(Resources.FileRequire, dgvUploadRequestFile.Rows[i].Cells[2].Text.Trim()) + "<br/>";
                }
            }

            return mess;
        }

        #region GetFormatMessageBox
        /// <summary>
        /// ใช้จัดรูปแบบ Text ที่ แสดงใน MessageBox ตามรูปแบบ BootStrap
        /// title ค่าจาก Label
        /// data ค่าจาก TextBox หรือ DropDown ต่างๆ ที่ Key  เข้ามา
        /// </summary>
        /// <param name="title"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        protected string GetFormatMessageBox(string title, string data)
        {
            return "<div class=\"row\"><label class=\"control-label col-md-4 text-right\">" + title + " :</label><label class=\"control-label col-md-8\">" + data + " </label></div>";
        }
        #endregion

    }
}