﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PCLS.WebPC.Pages.Authentication.Login" Theme="Blue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../../plugins/login-form/css/style.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
        <ContentTemplate>
            <%--<div id="divLogin" runat="server">
                <div style="width:100%; height:250px;" class="navbar navbar-default navbar-fixed-top">
                    <asp:Image ID="imgBanner" runat="server" ImageUrl="~/Images/Index/imgHeader.png" Width="100%" Height="100%" />
                    <div style="padding-top:20px; padding-right:5%; font-family:'Century Gothic'">
                        <div style="float:left;">
                            
                        </div>
                        <div style="background-color:#FF99CC; width:350px; border-radius:20px; float:right;">
                            <br />
                            <br />
                            <center>
                                <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" Width="80%"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" Width="80%"></asp:TextBox>
                                <br />
                                <asp:Button ID="cmdLogin" runat="server" Text="Login" Width="80%" SkinID="null" style="font-size:20px" UseSubmitBehavior="true" OnClick="cmdLogin_Click" />
                                <br />
                                <br />
                                <br />
                            </center>
                        </div>
                    </div>
                </div>
            </div>--%>
            <div id="divLogin" runat="server">
                <div style="width: 100%; height: 250px;" class="navbar navbar-default navbar-fixed-top">
                    <%--<asp:Image ID="imgBanner" runat="server" ImageUrl="~/Images/Index/imgHeader.png" Width="100%" Height="100%" />--%>
                    <div style="padding-top: 20px; padding-right: 5%; font-family: 'Century Gothic'">
                        <div style="float: none;">
                            <div class="container" style="padding-top: 20px">
                                <section id="content">
                                    <%--<h1>Login Form</h1>--%>
                                    <div align="center">
                                        <asp:Image ID="imgLogo" runat="server" ImageUrl="~/Images/ptt-logo.png" />
                                        <p style="color: navy; font-size: 180%">Car Leasing Web</p>
                                    </div>
                                    </br>
                                        <div align="center">
                                            <%--<input type="text" placeholder="Username" required="" id="username" />--%>
                                            <asp:DropDownList ID="ddlDomain" runat="server" Width="80%">
                                                <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                                <asp:ListItem>PTT\</asp:ListItem>
                                                <asp:ListItem>PTTDIGITAL\</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </br>
                                    <div align="center">
                                        <%--<input type="text" placeholder="Username" required="" id="username" />--%>
                                        <asp:TextBox ID="txtUsername" runat="server" placeholder="Username" Width="80%"></asp:TextBox>
                                    </div>
                                    </br>
                                    <div align="center">
                                        <%--<input type="password" placeholder="Password" required="" id="password" />--%>
                                        <asp:TextBox ID="txtPassword" runat="server" placeholder="Password" TextMode="Password" Width="80%"></asp:TextBox>
                                    </div>
                                    <div>&nbsp;</div>
                                    <div class="button">
                                        <div>&nbsp;</div>
                                        <asp:Button ID="cmdLogin" runat="server" Text="Login" Width="80%" SkinID="null" Style="font-size: 20px" UseSubmitBehavior="true" OnClick="cmdLogin_Click" />
                                        <%--<asp:Button ID="cmdExport" runat="server" Text="Export" Width="80%" SkinID="null" style="font-size:20px" UseSubmitBehavior="true" OnClick="cmdExport_Click" />--%>
                                        <div>&nbsp;</div>
                                        <%--<a href="http://www.pttplc.com" target="_blank">http://www.pttplc.com</a>--%>
                                        <p style="color: navy">Copyright © 2018 PTT. All rights reserved.</p>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script src="../../plugins/login-form/js/index.js"></script>
</asp:Content>
