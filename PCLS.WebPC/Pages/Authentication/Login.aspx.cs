﻿using PCLS.Bussiness.SystemData;
using PCLS.WebPC.Helper;
using SelectPdf;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using PCLS.Bussiness.Authentication;

namespace PCLS.WebPC.Pages.Authentication
{
    public partial class Login : PageBase
    {
        #region + View State +
        private DataTable dtDomain
        {
            get
            {
                if ((DataTable)ViewState["dtDomain"] != null)
                    return (DataTable)ViewState["dtDomain"];
                else
                    return null;
            }
            set
            {
                ViewState["dtDomain"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //if (!Page.IsPostBack)
                //    this.InitialForm();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    Session.Abandon();
                    Session.Clear();

                    ddlDomain.SelectedIndex = 1;
                    txtUsername.Text = "zsakchai.p";
                    txtPassword.Text = "PingAut2";

                    dtDomain = DomainBLL.Instance.DomainSelectBLL(" AND M_DOMAIN.IS_ACTIVE = 1");
                    DropDownListHelper.BindDropDown(ref ddlDomain, dtDomain, "ID", "DOMAIN_NAME", true, Properties.Resources.DropDownChooseText);
                    ddlDomain.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void ShowErrorMessage(string ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + PageBase.RemoveSpecialCharacters(ex) + "')", true);
        }

        protected void cmdLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Pages/Master/Car.aspx");
                //this.CheckLogin(ddlDomain.SelectedItem.ToString(), txtUsername.Text.Trim(), txtPassword.Text.Trim(), dtDomain.Rows[ddlDomain.SelectedIndex]["REQUIRE_PASSWORD"].ToString());
            }
            catch (Exception ex)
            {
                //this.ShowErrorMessage(ex.Message);
            }
        }

        private void CheckLogin(string Domain, string UserName, string Password, string isUserLocal)
        {
            #region + Old Code +
            //try
            //{
            //    this.ValidateLogin();
            //    DataTable dtUser = new DataTable();
            //    DataTable dtTemp = new DataTable();

            //    dtTemp = UserBLL.Instance.UserSelect2BLL(txtUsername.Text.Trim(), MD5Helper.GetMD5Hash(txtPassword.Text.Trim()));

            //    if (dtTemp.Rows.Count > 0)
            //    {
            //        if (string.Equals(dtTemp.Rows[0]["FLAG"].ToString(), "N"))
            //        {
            //            alertFail(dtTemp.Rows[0]["MESSAGE"].ToString());
            //            return;
            //        }
            //    }
            //    else
            //        return;

            //    dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + txtUsername.Text.Trim() + "'" + " AND PASSWORD = '" + MD5Helper.GetMD5Hash(txtPassword.Text.Trim()) + "'" + " AND IS_ACTIVE = 1");

            //    if (dtUser.Rows.Count == 0)
            //    {
            //        alertSuccess(string.Empty, "Not Authorize...", "~/Pages/Authentication/Login.aspx");
            //    }

            //    Session["UserLogin"] = dtUser;
            //    Response.Redirect("~/Pages/Other/BlankPage.aspx", false);
            //}
            //catch (Exception ex)
            //{
            //    alertFail(ex.Message);
            //}
            #endregion

            //try
            //{
            //    this.ValidateLogin();
            //    DataTable dtUser = new DataTable();
            //    DataTable dtTemp = new DataTable();

            //    if (string.Equals(isUserLocal, "0"))
            //    {//Domain
            //        if (DomainHelper.CheckDomain(Domain + UserName, Password))
            //            dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + UserName + "'" + " AND IS_ACTIVE = 1");
            //    }
            //    else
            //    {//Legacy
            //        dtTemp = UserBLL.Instance.UserSelect2BLL(UserName, MD5Helper.GetMD5Hash(Password));

            //        if (dtTemp.Rows.Count > 0)
            //        {
            //            if (string.Equals(dtTemp.Rows[0]["FLAG"].ToString(), "N"))
            //            {
            //                this.ShowErrorMessage(dtTemp.Rows[0]["MESSAGE"].ToString());
            //                return;
            //            }
            //        }
            //        else
            //            return;

            //        dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + UserName + "'" + " AND PASSWORD = '" + MD5Helper.GetMD5Hash(Password) + "'" + " AND IS_ACTIVE = 1");
            //    }

            //    if (dtUser.Rows.Count > 0)
            //    {
            //        LoginBLL.Instance.ClearLoginBLL(UserName);
            //        LogBLL.Instance.LoginLogBLL(int.Parse(dtUser.Rows[0]["USER_ID"].ToString()));
            //        Session["UserLogin"] = dtUser;
            //        Response.Redirect("~/Pages/Other/BlankPage.aspx", false);
            //    }
            //    else
            //    {
            //        Response.Redirect("~/Pages/Other/NotAuthorize.aspx", false);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception(ex.Message);
            //}            
        }

        private void ValidateLogin()
        {
            try
            {
                if (string.Equals(txtUsername.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(PCLS.WebPC.Properties.Resources.TextBoxRequire, "ชื่อผู้ใช้งาน"));

                if (string.Equals(txtPassword.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(PCLS.WebPC.Properties.Resources.TextBoxRequire, "รหัสผ่าน"));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}