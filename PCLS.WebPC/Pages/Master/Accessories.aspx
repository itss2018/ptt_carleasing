﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="Accessories.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.Accessories" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลอุปกรณ์</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>การตั้งค่าและข้อมูล</li>
                    <li class="active">ข้อมูลอุปกรณ์</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Search Option</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblCheckType" runat="server" Text="ชื่ออุปกรณ์"></asp:Label>
                                    <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ"></asp:Label>
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="form-control">
                                        <asp:ListItem>ใช้งาน</asp:ListItem>
                                        <asp:ListItem>ไม่ใช้งาน</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdAdd" runat="server" Text="เพิ่ม" SkinID="ButtonSuccess" OnClick="cmdAdd_Click" />
                        <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" OnClick="cmdSearch_Click" />
                        <asp:Button ID="cmdClear" runat="server" Text="ล้างข้อมูล" SkinID="ButtonWarning"  OnClick="cmdClear_Click"  />
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">รายการข้อมูลอุปกรณ์</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body" style="overflow-x:auto">
                        <asp:GridView ID="dgvData" runat="server" class="table table-bordered" SkinID="GridNoPaging" OnRowDataBound="dgvData_RowDataBound" DataKeyNames="ID" AutoGenerateColumns="False">
                            <HeaderStyle Wrap="false" />
                            <RowStyle Wrap="false" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgView.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID" HeaderText="รหัส"></asp:BoundField>
                                <asp:BoundField DataField="NAME" HeaderText="ชื่ออุปกรณ์"></asp:BoundField>
                                <asp:BoundField DataField="STATUS" HeaderText="สถานะการใช้งาน"></asp:BoundField>                              
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

