﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="Car.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.Car" Theme="Blue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-car"></i><span class="caption-subject bold uppercase">&nbsp;ประวัติรถ</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>ประวัติรถ</li>
                    <li class="active">ประวัติรถ</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Search Option</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="lblCheckType" runat="server" Text="ทะเบียนรถ"></asp:Label>
                                    <asp:TextBox ID="txtSubject0" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="ยี่ห้อ"></asp:Label>
                                    <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="รุ่น"></asp:Label>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="เลขตัวถัง"></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblTruckSys" runat="server" Text="ประเภทรถ"></asp:Label>
                                    <asp:DropDownList ID="ddlTruckSys" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="รหัสพนักงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="ชื่อพนักงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="นามสกุลพนักงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblSubject" runat="server" Text="เลขที่สัญญา"></asp:Label>
                                    <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ"></asp:Label>
                                    <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" Text="วันที่จบสัญญา ตั้งแต่"></asp:Label>
                                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="วันที่จบสัญญา ถึงวันที่"></asp:Label>
                                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdAdd" runat="server" Text="เพิ่ม" SkinID="ButtonSuccess" OnClick="cmdAdd_Click"/>
                        <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" />
                        <asp:Button ID="cmdClear" runat="server" Text="ล้างข้อมูล" SkinID="ButtonWarning"  OnClick="cmdClear_Click"/>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Show Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body" style="overflow-x:auto">
                        <asp:GridView ID="dgvData" runat="server" class="table table-bordered" SkinID="GridNoPaging" OnRowDataBound="dgvData_RowDataBound" DataKeyNames="ID">
                            <HeaderStyle Wrap="false" />
                            <RowStyle Wrap="false" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgView.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID" HeaderText="รหัส"></asp:BoundField>
                                <asp:BoundField DataField="License" HeaderText="ทะเบียนรถ"></asp:BoundField>
                                <asp:BoundField DataField="ChassisNo" HeaderText="เลขตัวถัง"></asp:BoundField>
                                <asp:BoundField DataField="EngineNo" HeaderText="เลขเครื่องยนต์"></asp:BoundField>
                                <asp:BoundField DataField="Brand" HeaderText="ยี่ห้อ"></asp:BoundField>
                                <asp:BoundField DataField="Model" HeaderText="รุ่น"></asp:BoundField>
                                <asp:BoundField DataField="Description" HeaderText="รายละเอียดเพิ่มเติม"></asp:BoundField>
                                <asp:BoundField DataField="TypeCar" HeaderText="ประเภทรถ"></asp:BoundField>  
                                <asp:BoundField DataField="Email" HeaderText="อีเมล์"></asp:BoundField>                                
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
