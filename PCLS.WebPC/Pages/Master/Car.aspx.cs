﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace PCLS.WebPC.Pages.Master
{
    public partial class Car :  PageBase
    {
        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                
                this.GetData();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("License");
                dt.Columns.Add("ChassisNo");
                dt.Columns.Add("EngineNo");
                dt.Columns.Add("Brand");
                dt.Columns.Add("Model");
                dt.Columns.Add("Description");
                dt.Columns.Add("TypeCar");
                dt.Columns.Add("Email");

                DataRow row = dt.NewRow();
                row["ID"] = 1200;
                row["License"] = "1กจ 347";
                row["ChassisNo"] = "ABCD-32585";
                row["EngineNo"] = "ABCD-32585";
                row["Brand"] = "HONDA";
                row["Model"] = "CITY";
                row["Description"] = "TEST";
                row["TypeCar"] = "TEST";
                row["Email"] = "SURIRAT.A@GMAIL.COM";
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string str = string.Empty;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");

                if (aView != null)
                {
                    str = Mode.View + "&" + row["ID"];//Mode & ID
                    aView.HRef = "CarAddEdit.aspx?str=" + EncodeQueryString(str);
                }

                if (aEdit != null)
                {
                    str = Mode.Edit + "&" + row["ID"];//Mode & ID
                    aEdit.HRef = "CarAddEdit.aspx?str=" + EncodeQueryString(str);
                }
            }
        }
        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            FormHelper.OpenForm("CarAddEdit.aspx" + "?type=" + EncodeQueryString(Mode.Add.ToString()) + "&ID=" + EncodeQueryString("0"), Page);
        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {
                //txtName.Text = string.Empty;
                ddlStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }
    }
}