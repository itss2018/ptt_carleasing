﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="CarAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.CarAddEdit" Theme="Blue" %>
<%@ Register Src="~/UserControl/CarModal.ascx" TagPrefix="uc3" TagName="ModalPopup" %>
<%@ Register Src="~/UserControl/Insurance1Modal.ascx" TagPrefix="uc4" TagName="ModalPopup" %>
<%@ Register Src="~/UserControl/Insurance2Modal.ascx" TagPrefix="uc5" TagName="ModalPopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลรถ</span></h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>ประวัติรถ</li>
                    <li class="active">ข้อมูลรถ</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Input Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                            <div class="col-md-12">                                   
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#Car" data-toggle="tab">ข้อมูลรถยนต์</a></li>
                                        <li><a href="#Leasing" data-toggle="tab">ข้อมูลเช่าซื้อ</a></li>
                                        <li><a href="#Tax" data-toggle="tab">ข้อมูลทะเบียนภาษี</a></li>
                                        <li><a href="#Insurance" data-toggle="tab">ข้อมูลประกันภัย</a></li>
                                        <li><a href="#OwnCar" data-toggle="tab">ข้อมูลผู้ครอบครอง</a></li>
                                        <li><a href="#PrepaidCard" data-toggle="tab">ข้อมูลบัตรน้ำมัน</a></li>
                                        <li><a href="#Issued" data-toggle="tab">ข้อมูลการจำหน่าย</a></li>
                                    </ul>

                                    <div class="tab-content">

                                        <div class="active tab-pane" id="Car">

                                            <div class="row">
                                                <div class="col-md-4">
                                                     <div class="form-group">
                                                     <label for="exampleInputEmail1">ทะเบียนรถ</label>
                                                            <asp:TextBox runat="server" ID="txtLICENSE_CODE" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">

                                                    <div class="form-group">
                                                     <label for="exampleInputEmail1">จังหวัด</label>
                                                            <asp:DropDownList runat="server" ID="DropDownList27" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                     <div class="form-group">
                                                     <label>การได้มา</label>
                                                            <asp:DropDownList runat="server" ID="DropDownList1" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                     <label>ประเภทการจัดหา</label>
                                                            <asp:DropDownList runat="server" ID="DropDownList2" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                     <label>ประเภทการใช้งาน</label>
                                                            <asp:DropDownList runat="server" ID="DropDownList3" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                         
                                            <div class="row">
                                                <div class="col-md-4">
                                                     <div class="form-group">
                                                     <label>ยี่ห้อ</label>
                                                            <asp:DropDownList runat="server" ID="DropDownList4" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                     <label>รุ่นรถ</label>
                                                            <asp:DropDownList runat="server" ID="DropDownList5" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                 <div class="col-md-4">
                                                    <div class="form-group">
                                                     <label>สีรถ</label>
                                                            <asp:DropDownList runat="server" ID="DropDownList6" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>                                          

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ประเภทรถ</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList7" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ขนาดเครื่องยนต์</label>
                                                        <asp:TextBox runat="server" ID="TextBox1" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ชนิดเครื่องยนต์</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList12" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ชนิดเชื้อเพลิง</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList10" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>เลขตัวรถ</label>
                                                        <asp:TextBox runat="server" ID="TextBox2" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>เลขเครื่องยนต์</label>
                                                        <asp:TextBox runat="server" ID="TextBox3" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>เลขมอเตอร์(ไฮบริด)</label>
                                                        <asp:TextBox runat="server" ID="TextBox4" class="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>เลขถังก๊าซ</label>
                                                        <asp:TextBox runat="server" ID="TextBox5" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>มูลค่ารถยนต์</label>
                                                        <asp:TextBox runat="server" ID="TextBox6" CssClass="form-control" />  
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่รับรถ</label>
                                                        <asp:TextBox runat="server" ID="txtDATEFROM_SEARCH" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>สถานะรถ</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList11" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>หมายเหตุ</label>
                                                        <asp:TextBox id="TextArea1" CssClass="form-control" TextMode="multiline" Columns="50" Rows="5" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                             
                                            <div class="row">
                                               <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">ประเภทไฟล์เอกสาร</label>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="DropDownList9"  runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:FileUpload ID="fileUpload3" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class='glyphicon glyphicon-plus'></span>
                                                                    <asp:Button ID="Button5" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" />
                                                                </div>
                                                            </div>
                                                     </div>
                                               </div>                                               
                                            </div>
                                        
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                       <asp:Button ID="Button14" runat="server" Text="บันทึก" SkinID="ButtonSuccess"  />
                                                        <asp:Button ID="Button15" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  />
                                                    </div>                                                    
                                                </div>
                                            </div>
                                                                                  
                                          </div><!-- /.tab-pane -->

                                        <div class="tab-pane" id="Leasing">

                                            <div class="row">
                                                <div class="col-md-4">
                                                     <div class="form-group">
                                                        <label>เลขที่สัญญาปตท</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList23" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>ครั้งที่</label>
                                                        <asp:TextBox runat="server" ID="TextBox51" class="form-control" />
                                                    </div>
                                                </div>
                                                 <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>จำนวนปี</label>
                                                        <asp:TextBox runat="server" ID="TextBox52" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>สถานะสัญญา</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList13" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>บริษัทคู่สัญญา</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList14" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่เริ่มสัญญา</label>
                                                       <asp:TextBox runat="server" ID="TextBox12" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่สิ้นสุดสัญญา</label>
                                                         <asp:TextBox runat="server" ID="TextBox13" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>เลขที่สัญญาเช่าลีสซิ่ง</label>
                                                        <asp:TextBox runat="server" ID="TextBox14" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                         <label>มูลค่าสัญญา</label>
                                                        <asp:TextBox runat="server" ID="TextBox15" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>คืนภาษีได้</label>
                                                         <asp:DropDownList runat="server" ID="DropDownList16" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>                                              
                                            </div>                                          

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>สกุลเงิน</label>
                                                       <asp:DropDownList runat="server" ID="DropDownList15" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>  
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                         <label>อัตราแลกเปลี่ยน</label>
                                                        <asp:TextBox runat="server" ID="TextBox18" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                         <label>เงินดาวน์</label>
                                                        <asp:TextBox runat="server" ID="TextBox17" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                         <label>อัตราดอกเบี้ย</label>
                                                        <asp:TextBox runat="server" ID="TextBox16" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>มูลค่าซาก</label>
                                                        <asp:TextBox runat="server" ID="TextBox19" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ประเภทการจ่าย</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList17" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>  
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>จำนวนงวดต่อปี</label>
                                                        <asp:TextBox runat="server" ID="TextBox20" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>จำนวนงวดทั้งหมด</label>
                                                        <asp:TextBox runat="server" ID="TextBox21" CssClass="form-control" />
                                                    </div>
                                                </div>                                                                                          
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>เงินงวด</label>
                                                        <asp:TextBox runat="server" ID="TextBox22" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>งวดแรก</label>
                                                        <asp:TextBox runat="server" ID="TextBox23" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>งวดสุดท้าย</label>
                                                        <asp:TextBox runat="server" ID="TextBox24" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                     <div class="form-group">
                                                        <label>เลขที่บันทึกข้อตกลง</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList25" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>บริษัทคู่สัญญา</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList20" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                     <div class="form-group">
                                                        <label>สัญญาค้ำประกันเลขที่</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList32" class="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>มูลค่าหลักประกันสัญญา</label>
                                                       <asp:TextBox runat="server" ID="TextBox32" SkinID="null" CssClass="form-control" />
                                                    </div>
                                                </div>
                                              
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ธนาคาร</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList33" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>สาขา</label>
                                                        <asp:TextBox runat="server" ID="TextBox31" CssClass="form-control" />
                                                    </div>
                                                </div>  
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่เริ่มสัญญา</label>
                                                       <asp:TextBox runat="server" ID="TextBox29" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่สิ้นสุดสัญญา</label>
                                                        <asp:TextBox runat="server" ID="TextBox30" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                               <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">ประเภทไฟล์เอกสาร</label>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="DropDownList30"  runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:FileUpload ID="fileUpload6" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class='glyphicon glyphicon-plus'></span>
                                                                    <asp:Button ID="Button2" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" />
                                                                </div>
                                                            </div>
                                                     </div>
                                               </div>                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                       <asp:Button ID="Button6" runat="server" Text="บันทึก" SkinID="ButtonSuccess"  />
                                                        <asp:Button ID="Button7" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  />
                                                    </div>                                                    
                                                </div>
                                            </div>

                                        </div><!-- /.tab-pane -->

                                        <div class="tab-pane" id="Tax">

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ประเภทการจดทะเบียน</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList18" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>คุณลักษณะ</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList19" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่จดทะเบียน</label>
                                                        <asp:TextBox runat="server" ID="TextBox25" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>                                                
                                            </div>  

                                            <div class="row">                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ค่าธรรมเนียมภาษี</label>
                                                        <asp:TextBox runat="server" ID="TextBox26" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ค่าตรวจสภาพ</label>
                                                        <asp:TextBox runat="server" ID="TextBox27" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>น้ำหนักรถ</label>
                                                        <asp:TextBox runat="server" ID="TextBox28" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>เสียภาษีล่วงหน้า (วัน)</label>
                                                         <asp:DropDownList runat="server" ID="DropDownList8" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>แจ้งก่อนล่วงหน้า (วัน)</label>
                                                         <asp:DropDownList runat="server" ID="DropDownList28" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>   
                                                
                                            </div>

                                            <div class="row">
                                               <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">ประเภทไฟล์เอกสาร</label>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="DropDownList31"  runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:FileUpload ID="fileUpload7" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class='glyphicon glyphicon-plus'></span>
                                                                    <asp:Button ID="Button8" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" />
                                                                </div>
                                                            </div>
                                                     </div>
                                               </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                       <asp:Button ID="Button9" runat="server" Text="บันทึก" SkinID="ButtonSuccess"  />
                                                        <asp:Button ID="Button10" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  />
                                                    </div>                                                    
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="box">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">รายการการต่อภาษี</h3>
                                                             <div class="pull-right box-tools">
                                                                <button type="button" class="btn btn-info btn-sm"  title="เพิ่มรายการการต่อภาษี" data-toggle="modal" data-target="#modalConfirmTaxSave">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <asp:GridView ID="dgvDataTax" runat="server" SkinID="GridNoPaging" class="table table-bordered" AutoGenerateColumns="False" >
                                                            <HeaderStyle Wrap="false" />
                                                            <RowStyle Wrap="false" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No.">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex + 1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="DuedateTax" HeaderText="วันครบกำหนดเสียภาษี "></asp:BoundField>
                                                                <asp:BoundField DataField="TaxAmount" HeaderText="ค่าภาษี"></asp:BoundField>
                                                                <asp:BoundField DataField="ExtraAmount" HeaderText="เงินเพิ่ม"></asp:BoundField>
                                                                <asp:BoundField DataField="CARInspectionStatus" HeaderText="สถานะการตรวจสภาพรถ"></asp:BoundField>
                                                                <asp:BoundField DataField="CARInspectionDate" HeaderText="วันที่ตรวจสภาพ "></asp:BoundField>
                                                                <asp:BoundField DataField="NGVInspectionStatus" HeaderText="สถานะการตรวจถัง NGV"></asp:BoundField>
                                                                <asp:BoundField DataField="NGVCARInspectionDate" HeaderText="วันที่ตรวจสภาพ NGV"></asp:BoundField>
                                                            </Columns>
                                                            </asp:GridView>
                                                        </div>   
                                                        <div class="box-footer clearfix">
                                                            <ul class="pagination pagination-sm no-margin pull-right">
                                                                <li><a href="#">&laquo;</a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">&raquo;</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="example-modal">
                                                <div class="modal">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                <h4 class="modal-title">Default Modal</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>One fine body&hellip;</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div><!-- /.tab-pane -->

                                        <div class="tab-pane" id="Insurance">
                                            <h3 class="box-title">ประกันภัยภาคบังคับ (พ.ร.บ.)</h3>                                          

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="box">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">รายการต่อประกันภัยภาคบังคับ (พ.ร.บ.)</h3>
                                                             <div class="pull-right box-tools">
                                                                <button type="button" class="btn btn-info btn-sm"  title="เพิ่มรายการการต่อประกันภัยภาคบังคับ (พ.ร.บ.)" data-toggle="modal" data-target="#modalInsurance1">
                                                                    <i class="fa fa-plus"></i>
                                                                </button> 
                                                            </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <asp:GridView ID="dgvDataInsurance1" runat="server" SkinID="GridNoPaging" class="table table-bordered" AutoGenerateColumns="False" >
                                                            <HeaderStyle Wrap="false" />
                                                            <RowStyle Wrap="false" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No.">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex + 1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="PolicyNo" HeaderText="กรมธรรม์เลขที่"></asp:BoundField>
                                                                <asp:BoundField DataField="Datebegin" HeaderText="วันที่เริ่มต้น"></asp:BoundField>
                                                                <asp:BoundField DataField="DateEnd" HeaderText="วันที่สิ้นสุด"></asp:BoundField>
                                                                <asp:BoundField DataField="InsuranceCompany" HeaderText="บริษัทประกันภัย"></asp:BoundField>                             
                                                            </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                        <div class="box-footer clearfix">
                                                            <ul class="pagination pagination-sm no-margin pull-right">
                                                                <li><a href="#">&laquo;</a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">&raquo;</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>        
                                            
                                            <h3 class="box-title">ประกันภัยภาคสมัครใจ</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="box">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">รายการต่อประกันภัยภาคสมัครใจ</h3>
                                                             <div class="pull-right box-tools">
                                                                <button type="button" class="btn btn-info btn-sm"  title="เพิ่มรายการการต่อประกันภัยภาคสมัครใจ" data-toggle="modal" data-target="#modalInsurance2">
                                                                    <i class="fa fa-plus"></i>
                                                                </button> 
                                                            </div>
                                                        </div>
                                                        <div class="box-body">
                                                            <asp:GridView ID="dgvDataInsurance2" runat="server" SkinID="GridNoPaging" class="table table-bordered" AutoGenerateColumns="False" >
                                                            <HeaderStyle Wrap="false" />
                                                            <RowStyle Wrap="false" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No.">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex + 1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="PolicyNo" HeaderText="กรมธรรม์เลขที่"></asp:BoundField>
                                                                <asp:BoundField DataField="Datebegin" HeaderText="วันที่เริ่มต้น"></asp:BoundField>
                                                                <asp:BoundField DataField="DateEnd" HeaderText="วันที่สิ้นสุด"></asp:BoundField>
                                                                <asp:BoundField DataField="TypeInsurance" HeaderText="ประเภท"></asp:BoundField>
                                                                <asp:BoundField DataField="Coverage" HeaderText="ทุนประกัน"></asp:BoundField>
                                                                <asp:BoundField DataField="InsuranceCompany" HeaderText="บริษัทประกันภัย"></asp:BoundField>                             
                                                            </Columns>
                                                            </asp:GridView>
                                                        </div>                                                       
                                                        <div class="box-footer clearfix">
                                                            <ul class="pagination pagination-sm no-margin pull-right">
                                                                <li><a href="#">&laquo;</a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">&raquo;</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                      </div><!-- /.tab-pane -->

                                        <div class="tab-pane" id="OwnCar">
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>รหัสผู้ครอบครอง</label>
                                                        <asp:TextBox runat="server" ID="TextBox34" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>ชื่อผู้ครอบครอง</label>
                                                        <asp:TextBox runat="server" ID="TextBox33" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                 <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>ตำแหน่ง</label>
                                                        <asp:TextBox runat="server" ID="TextBox37" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>เบอร์โทรศัพท์</label>
                                                        <asp:TextBox runat="server" ID="TextBox40" CssClass="form-control" />
                                                    
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>มือถือ</label>
                                                        <asp:TextBox runat="server" ID="TextBox41" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>รหัสหน่วยงาน</label>
                                                        <asp:TextBox runat="server" ID="TextBox42" CssClass="form-control" />
                                                    
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>ชื่อย่อหน่วยงาน</label>
                                                        <asp:TextBox runat="server" ID="TextBox43" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>ชื่อเต็มหน่วยงาน</label>
                                                        <asp:TextBox runat="server" ID="TextBox44" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                               <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">Cost Center</label>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <asp:TextBox runat="server" ID="TextBox38" CssClass="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <asp:TextBox runat="server" ID="TextBox48" CssClass="form-control" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <asp:TextBox runat="server" ID="TextBox49" CssClass="form-control" />
                                                                </div>
                                                            </div>
                                                     </div>
                                               </div>
                                               <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่เริ่มครอบครอง</label>
                                                        <asp:TextBox runat="server" ID="TextBox45" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                               <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">ประเภทไฟล์เอกสาร</label>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="DropDownList24"  runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:FileUpload ID="fileUpload1" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class='glyphicon glyphicon-plus'></span>
                                                                    <asp:Button ID="Button3" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" />
                                                                </div>
                                                            </div>
                                                     </div>
                                               </div>
                                               
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                      <asp:Button ID="Button12" runat="server" Text="บันทึก" SkinID="ButtonSuccess"  />
                                                        <asp:Button ID="Button13" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  />
                                                    </div>                                                    
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="box">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">รายชื่อผู้ครอบครอง</h3>
                                                             <!--<div class="pull-right box-tools">
                                                                <button type="button" class="btn btn-info btn-sm"  title="เพิ่มรายการการต่อภาษี" data-toggle="modal" data-target="#modalConfirmTaxSave">
                                                                    <i class="fa fa-plus"></i>
                                                                </button> 
                                                            </div>-->
                                                        </div>
                                                        <div class="box-body">
                                                            <asp:GridView ID="dgvDataOwn" runat="server" SkinID="GridNoPaging" class="table table-bordered" AutoGenerateColumns="False" >
                                                            <HeaderStyle Wrap="false" />
                                                            <RowStyle Wrap="false" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No.">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex + 1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="OwnID" HeaderText="รหัสผู้ครอบครอง"></asp:BoundField>
                                                                <asp:BoundField DataField="OwnName" HeaderText="ชื่อผู้ครอบครอง"></asp:BoundField>
                                                                <asp:BoundField DataField="Position" HeaderText="ตำแหน่ง"></asp:BoundField>
                                                                <asp:BoundField DataField="TelNo" HeaderText="เบอร์โทรศัพท์"></asp:BoundField>
                                                                <asp:BoundField DataField="Division" HeaderText="หน่วยงาน"></asp:BoundField>  
                                                            </Columns>
                                                            </asp:GridView>
                                                        </div>                                                   
                                                        <div class="box-footer clearfix">
                                                            <ul class="pagination pagination-sm no-margin pull-right">
                                                                <li><a href="#">&laquo;</a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">&raquo;</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                                             
                                        </div><!-- /.tab-pane -->

                                        <div class="tab-pane" id="PrepaidCard">

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>เลขที่บัตร</label>
                                                        <asp:TextBox runat="server" ID="TextBox47" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ประเภทบัตร</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList26" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>รหัสผู้ถือบัตร</label>
                                                        <asp:TextBox runat="server" ID="TextBox11" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>ชื่อผู้ถือบัตร</label>
                                                        <asp:TextBox runat="server" ID="TextBox7" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่เริ่ม</label>
                                                        <asp:TextBox runat="server" ID="TextBox50" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วันที่หมดอายุ</label>
                                                        <asp:TextBox runat="server" ID="TextBox10" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">ประเภทไฟล์เอกสาร</label>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="DropDownList22"  runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:FileUpload ID="fileUpload2" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class='glyphicon glyphicon-plus'></span>
                                                                    <asp:Button ID="Button1" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>                                               
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:Button ID="Button16" runat="server" Text="บันทึก" SkinID="ButtonSuccess"  />
                                                        <asp:Button ID="Button17" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  />
                                                    </div>                                                    
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="box">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">รายการบัตรเติมน้ำมัน</h3>
                                                             <!--<div class="pull-right box-tools">
                                                                <button type="button" class="btn btn-info btn-sm"  title="เพิ่มรายการการต่อภาษี" data-toggle="modal" data-target="#modalConfirmTaxSave">
                                                                    <i class="fa fa-plus"></i>
                                                                </button> 
                                                            </div>-->
                                                        </div>
                                                        <div class="box-body">
                                                            <asp:GridView ID="dgvDataPrepaidCard" runat="server" SkinID="GridNoPaging" class="table table-bordered" AutoGenerateColumns="False" >
                                                            <HeaderStyle Wrap="false" />
                                                            <RowStyle Wrap="false" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="">
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <ItemTemplate>
                                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="No.">
                                                                    <ItemTemplate>
                                                                        <%# Container.DataItemIndex + 1 %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="CardNo" HeaderText="เลขที่บัตร"></asp:BoundField>
                                                                <asp:BoundField DataField="CardType" HeaderText="ประเภทบัตร"></asp:BoundField>
                                                                <asp:BoundField DataField="EmpCode" HeaderText="รหัสพนักงาน"></asp:BoundField>
                                                                <asp:BoundField DataField="EmpName" HeaderText="ผู้ถือบัตร"></asp:BoundField>
                                                                <asp:BoundField DataField="ExpDate" HeaderText="วันหมดอายุ"></asp:BoundField>  
                                                            </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                        <div class="box-footer clearfix">
                                                            <ul class="pagination pagination-sm no-margin pull-right">
                                                                <li><a href="#">&laquo;</a></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">&raquo;</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div><!-- /.tab-pane -->                                       
                                        
                                        <div class="tab-pane" id="Issued">

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>วิธีการจำหน่าย</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList21" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ประเภทการใช้สิทธิ์</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList35" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>รหัสผู้ซื้อ</label>
                                                        <asp:TextBox runat="server" ID="TextBox36" CssClass="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label>ชื่อผู้ซื้อ</label>
                                                        <asp:TextBox runat="server" ID="TextBox39" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">ประเภทไฟล์เอกสาร</label>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="DropDownList37"  runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:FileUpload ID="fileUpload4" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class='glyphicon glyphicon-plus'></span>
                                                                    <asp:Button ID="Button20" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>                                               
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                       <asp:Button ID="Button4" runat="server" Text="บันทึก" SkinID="ButtonSuccess"  />
                                                        <asp:Button ID="Button11" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  />
                                                    </div>                                                    
                                                </div>
                                            </div>

                                        </div><!-- /.tab-pane -->

                                    </div><!-- /.tab-content -->
                                </div><!-- /.nav-tabs-custom -->
                            </div><!-- /.nav-tabs-custom -->
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.box-body -->
          

            


            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    <uc3:ModalPopup runat="server" ID="modalConfirmTaxSave" IDModel="modalConfirmTaxSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" 
        TextDetail="" ImageName="imgTitleConfirm.png" />

    <uc4:ModalPopup runat="server" ID="modalInsurance1" IDModel="modalInsurance1"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" 
        TextDetail="" ImageName="imgTitleConfirm.png" />
    <uc5:ModalPopup runat="server" ID="modalInsurance2" IDModel="modalInsurance2"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" 
        TextDetail="" ImageName="imgTitleConfirm.png" />
</asp:Content>
