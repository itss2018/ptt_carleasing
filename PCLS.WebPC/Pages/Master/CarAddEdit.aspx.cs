﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.Master
{
    public partial class CarAddEdit : PageBase
    {
        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {

                this.GetDataInsurance1();
                this.GetDataInsurance2();
                this.GetDataTAX();
                this.GetDataOwn();
                this.GetDataCard();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetDataInsurance1()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("PolicyNo");
                dt.Columns.Add("Datebegin");
                dt.Columns.Add("DateEnd");
                dt.Columns.Add("InsuranceCompany");

                DataRow row = dt.NewRow();

                row["PolicyNo"] = "ASED-61852882";
                row["Datebegin"] = "01/09/2017";
                row["DateEnd"] = "01/09/2018";
                row["InsuranceCompany"] = "บริษัท ABCDEF";

                dt.Rows.Add(row);

                this.dgvDataInsurance1.Visible = true;

                dgvDataInsurance1.DataSource = dt;

                dgvDataInsurance1.DataBind();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void GetDataInsurance2()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("PolicyNo");
                dt.Columns.Add("Datebegin");
                dt.Columns.Add("DateEnd");
                dt.Columns.Add("TypeInsurance");
                dt.Columns.Add("Coverage");
                dt.Columns.Add("InsuranceCompany");

                DataRow row = dt.NewRow();

                row["PolicyNo"] = "ASED-61852882";
                row["Datebegin"] = "01/09/2017";
                row["DateEnd"] = "01/09/2018";
                row["TypeInsurance"] = "ประกันชั้น 1";
                row["Coverage"] = "550,000";
                row["InsuranceCompany"] = "บริษัท ABCDEF";

                dt.Rows.Add(row);

                this.dgvDataInsurance2.Visible = true;

                dgvDataInsurance2.DataSource = dt;

                dgvDataInsurance2.DataBind();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void GetDataTAX()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("DuedateTax");
                dt.Columns.Add("TaxAmount");
                dt.Columns.Add("ExtraAmount");
                dt.Columns.Add("CARInspectionStatus");
                dt.Columns.Add("CARInspectionDate");
                dt.Columns.Add("NGVInspectionStatus");
                dt.Columns.Add("NGVInspectionDate");
                DataRow row = dt.NewRow();

                row["DuedateTax"] = "01/09/2018";
                row["TaxAmount"] = "12,000";
                row["ExtraAmount"] = "840";
                row["CARInspectionStatus"] = "ผ่าน";
                row["CARInspectionDate"] = "01/08/2018";
                row["NGVInspectionStatus"] = "";
                row["NGVInspectionDate"] = "";

                dt.Rows.Add(row);

                this.dgvDataTax.Visible = true;

                dgvDataTax.DataSource = dt;

                dgvDataTax.DataBind();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
        
        private void GetDataOwn()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("OwnID");
                dt.Columns.Add("OwnName");
                dt.Columns.Add("Position");
                dt.Columns.Add("TelNo");
                dt.Columns.Add("Division");
                DataRow row = dt.NewRow();

                row["OwnID"] = "1200";
                row["OwnName"] = "John Doe";
                row["Position"] = "หัวหน้า";
                row["TelNo"] = "086-823-78569";
                row["Division"] = "หน่วยงาน";

                dt.Rows.Add(row);

                this.dgvDataOwn.Visible = true;

                dgvDataOwn.DataSource = dt;

                dgvDataOwn.DataBind();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void GetDataCard()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("CardNo");
                dt.Columns.Add("CardType");
                dt.Columns.Add("EmpCode");
                dt.Columns.Add("EmpName");
                dt.Columns.Add("ExpDate");
                DataRow row = dt.NewRow();

                row["CardNo"] = "4569-7894-2188-9875";
                row["CardType"] = "บัตร Prepaid";
                row["EmpCode"] = "1200";
                row["EmpName"] = "John Doe";
                row["ExpDate"] = "01/02/2019";

                dt.Rows.Add(row);

                this.dgvDataPrepaidCard.Visible = true;

                dgvDataPrepaidCard.DataSource = dt;

                dgvDataPrepaidCard.DataBind();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
    }
}