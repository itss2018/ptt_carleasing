﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="CarColorAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.CarColorAddEdit" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลสีรถ</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>การตั้งค่าและข้อมูล</li>
                    <li class="active">ข้อมูลสีรถ</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Input Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblName" runat="server" Text="ชื่อสีรถ"></asp:Label>
                                    <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ"></asp:Label><b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" SkinID="ButtonSuccess" OnClick="cmdSave_Click" data-toggle="modal" data-target="#modalConfirmSave" UseSubmitBehavior="false" />
                        <asp:Button ID="cmdNew" runat="server" Text="สร้างใหม่" OnClick="cmdNew_Click" data-toggle="modal" data-target="#modalConfirmSave" UseSubmitBehavior="false" />
                        <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" SkinID="ButtonDanger" OnClick="cmdCancel_Click"  />
                    </div>
                </div>

            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>