﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.Master
{
    public partial class EmailTemplate1 : PageBase
    {
        #region Member
        string FieldType = "EmailTemplate", str = string.Empty;
        #endregion

        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {

                this.GetData();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("TYPE");
                dt.Columns.Add("NAME");
                dt.Columns.Add("STATUS");
                DataRow row = dt.NewRow();

                row["ID"] = "0001";
                row["TYPE"] = "แจ้งเกี่ยวกับรถยนต์";
                row["NAME"] = "แจ้งให้นำรถไปตรวจสภาพ ตรอ.";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0002";
                row["TYPE"] = "แจ้งเกี่ยวกับรถยนต์";
                row["NAME"] = "แจ้งให้นำรถไปตรวจสภาพ NGV";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0003";
                row["TYPE"] = "แจ้งเกี่ยวกับรถยนต์";
                row["NAME"] = "แจ้งให้นำรถไปซ่อม เช็ค ระยะ เปลี่ยนยาง";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0004";
                row["TYPE"] = "แจ้งเกี่ยวกับรถยนต์";
                row["NAME"] = "แจ้งให้มารับป้ายภาษีรถยนต์";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0005";
                row["TYPE"] = "แจ้งเกี่ยวกับรถยนต์";
                row["NAME"] = "แจ้งให้มาส่งคืนรถ/รับมอบรถยนต์";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0006";
                row["TYPE"] = "แจ้งเกี่ยวกับรถยนต์";
                row["NAME"] = "แจ้งให้ตรวจสอบข้อมูลรถยนต์";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);


                row = dt.NewRow();
                row["ID"] = "0007";
                row["TYPE"] = "ขอเปลี่ยนแปลงข้อมูลการครอบครองยานพาหนะ";
                row["NAME"] = "แจ้งเตือนตาม Workflow";
                row["STATUS"] = "ไม่ใช้ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0008";
                row["TYPE"] = "ขออนุมัติยืมกุญแจสำรอง";
                row["NAME"] = "แจ้งให้ผู้ Request มารับกุญแจ";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0009";
                row["TYPE"] = "ขออนุมัติยืมกุญแจสำรอง";
                row["NAME"] = "แจ้งให้ผู้ Request มาคืนกุญแจ";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0010";
                row["TYPE"] = "ขออนุมัติยืมกุญแจสำรอง";
                row["NAME"] = "แจ้งหลังจากรับกุญแจแล้ว";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");
                if (aView != null)
                {
                    str = Mode.View + "&" + row["ID"];//Mode & ID
                    aView.HRef = "EmailTemplateAddEdit.aspx?str=" + EncodeQueryString(str);
                }
                if (aEdit != null)
                {
                    str = Mode.Edit + "&" + row["ID"];//Mode & ID
                    aEdit.HRef = "EmailTemplateAddEdit.aspx?str=" + EncodeQueryString(str);
                }
            }

        }

        #region dgvData_PageIndexChanging
        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvData.PageIndex = e.NewPageIndex;
            cmdSearch_Click(null, null);

        }
        #endregion

        protected void dgvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            FormHelper.OpenForm("EmailTemplateAddEdit.aspx" + "?type=" + EncodeQueryString(Mode.Add.ToString()) + "&ID=" + EncodeQueryString("0"), Page);
        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtName.Text = string.Empty;
                ddlStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }
    }
}