﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="EmailTemplateAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.EmailTemplateAddEdit1" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลรูปแบบอีเมล์</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>การตั้งค่าและข้อมูล</li>
                    <li class="active">ข้อมูลรูปแบบอีเมล์</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Input Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblCheckType" runat="server" Text="ประเภทอีเมล์"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="DropDownList1" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="ชื่อรูปแบบอีเมล์"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:Label ID="lblSubject" runat="server" Text="Subject"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtSubject" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="ข้อความ"></asp:Label>
                                    <b style="color:red">*</b>
                                    <textarea id="txtEditor" name="txtEditor" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="สถานะ"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="form-control">
                                        <asp:ListItem>ใช้งาน</asp:ListItem>
                                        <asp:ListItem>ไม่ใช้งาน</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" SkinID="ButtonSuccess" OnClick="cmdSave_Click" data-toggle="modal" data-target="#modalConfirmSave" UseSubmitBehavior="false" />
                        <asp:Button ID="cmdNew" runat="server" Text="สร้างใหม่" OnClick="cmdNew_Click" data-toggle="modal" data-target="#modalConfirmSave" UseSubmitBehavior="false" />
                        <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" SkinID="ButtonDanger" OnClick="cmdCancel_Click"  />
                    </div>
                </div>

            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
