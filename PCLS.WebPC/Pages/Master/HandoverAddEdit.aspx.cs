﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace PCLS.WebPC.Pages.Transaction
{
    public partial class HandoverAddEdit : PageBase
    {

        #region Member
        string FieldType = "HandoverAddEdit", str = string.Empty;
        #endregion

        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {

                this.GetData();
                this.GetData2();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        private void GetData()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("DOCUMENT");
                dt.Columns.Add("FILENAME");


                DataRow row = dt.NewRow();
                row["ID"] = "1";
                row["DOCUMENT"] = "ใบส่งมอบ";
                row["FILENAME"] = "1234.PDF";
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
        private void GetData2()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("DOCUMENT");
                dt.Columns.Add("FILENAME");


                DataRow row = dt.NewRow();
                row["ID"] = "1";
                row["DOCUMENT"] = "ประแจปากตาย";
                row["FILENAME"] = "1234.PDF";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "2";
                row["DOCUMENT"] = "ไขควง";
                row["FILENAME"] = "1234.PDF";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "3";
                row["DOCUMENT"] = "คีม";
                row["FILENAME"] = "1234.PDF";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "4";
                row["DOCUMENT"] = "วิทยุ-ซีดี";
                row["FILENAME"] = "1234.PDF";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "5";
                row["DOCUMENT"] = "สามเหลี่ยมสะท้อนแสง";
                row["FILENAME"] = "1234.PDF";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "6";
                row["DOCUMENT"] = "อุปกรณ์นำศูนย์น็อตล้อ";
                row["FILENAME"] = "1234.PDF";
                dt.Rows.Add(row);

                this.GridView1.Visible = true;
                GridView1.DataSource = dt;
                GridView1.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");
                if (aView != null)
                {
                    str = Mode.View + "&" + row["ID"];//Mode & ID
                    aView.HRef = "HandoverAddEdit.aspx?str=" + EncodeQueryString(str);
                }
                if (aEdit != null)
                {
                    str = Mode.Edit + "&" + row["ID"];//Mode & ID
                    aEdit.HRef = "HandoverAddEdit.aspx?str=" + EncodeQueryString(str);
                }
            }

        }

        #region dgvData_PageIndexChanging
        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvData.PageIndex = e.NewPageIndex;

        }
        #endregion

        protected void dgvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {

        }
        protected void cmdClear_Click(object sender, EventArgs e)
        {

        }
        protected void btnDocument_Click(object sender, EventArgs e)
        {

        }
        protected void btnReject_Click(object sender, EventArgs e)
        {

        }
    }
}