﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="MenuCar.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.MenuCar1" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;Car</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Transaction</li>
                    <li class="active">Car</li>
                </ol>
            </section>
            <section class="content">
        <div class="row">
            <div class="col-md-3">
                <!-- /. box -->
                <div class="box box-solid">
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li runat="server" id="li1"><a href="../../Pages/Master/Brand.aspx" runat="server"><i class="fa fa-circle-o text-light-blue btn-primary"></i>ข้อมูลยี่ห้อรถ</a></li>
                            <li runat="server" id="li3"><a href="../../Pages/Master/Model.aspx" runat="server"><i class="fa fa-circle-o text-light-blue"></i>ข้อมูลรุ่นรถ</a></li>                            
                            <li runat="server" id="li53"><a href="../../Pages/Master/CarColor.aspx"><i class="fa fa-circle-o text-light-blue"></i>ข้อมูลสีรถ</a></li>
                            <li runat="server" id="li4"><a href="../../Pages/Master/TypeEngine.aspx"><i class="fa fa-circle-o text-light-blue"></i>ข้อมูลชนิดเครื่องยนต์</a></li>
                            <li runat="server" id="li5"><a href="../../Pages/Master/TypeFuel.aspx"><i class="fa fa-circle-o text-light-blue"></i>ข้อมูลชนิดเชื้อเพลิง</a></li>
                            <li runat="server" id="li42"><a href="../../Pages/Master/TypeCar.aspx"><i class="fa fa-circle-o text-light-blue"></i>ข้อมูลประเภทรถ</a></li>
                            <li runat="server" id="li6"><a href="../../Pages/Master/StatusCar.aspx"><i class="fa fa-circle-o text-light-blue"></i>ข้อมูลสถานะรถ</a></li>
                            <li runat="server" id="li8"><a href="../../Pages/Master/TypeRegistration.aspx"><i class="fa fa-circle-o text-light-blue"></i>ข้อมูลประเภทการจดทะเบียน</a></li>
                            <li runat="server" id="li2"><a href="../../Pages/Master/Feature.aspx"><i class="fa fa-circle-o text-light-blue"></i>ข้อมูลคุณลักษณะการจดทะเบียน</a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            
        </div>
        <!-- /.row -->
    </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
