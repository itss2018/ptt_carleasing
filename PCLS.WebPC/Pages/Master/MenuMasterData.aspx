﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="MenuMasterData.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.MenuCar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลตั้งต้น</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>การตั้งค่าและข้อมูล</li>
                    <li class="active">ข้อมูลตั้งต้น</li>
                </ol>
            </section>
            <section class="content">
        <div class="row">

            <div class="col-md-3">
        
          
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ข้อมูลทั่วไป
              </h3>
            </div>
            <div class="box-body">
              
                            <a href="../../Pages/Master/Province.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลจังหวัด</a>
                            <a href="../../Pages/Master/Bank.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลธนาคาร</a>
                            <a href="../../Pages/Master/Currency.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลสกุลเงิน</a>
                            <a href="../../Pages/Master/TypeDocument.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทเอกสาร</a>
                            <a href="../../Pages/Master/Uploadfile.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลเอกสารแนบ</a>
                            <a href="../../Pages/Master/TypeEmail.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทอีเมล์</a>
                            <a href="../../Pages/Master/EmailTemplate.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลรูปแบบอีเมล์</a>
                            <a href="../../Pages/Master/Reason.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลเหตุผลในการไม่อนุมัติ</a>
                            <a href="../../Pages/Master/Department.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลหน่วยงาน</a>
              
              <br>

             
            </div>
          </div>

        </div>

            <div class="col-md-3">
        
          
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ข้อมูลสัญญา
              </h3>
            </div>
            <div class="box-body">
               <a href="../../Pages/Master/Partner.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลบริษัทคู่สัญญา</a>
                            <a href="../../Pages/Master/TypeInsurance.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทประกันภัย</a>
                            <a href="../../Pages/Master/TypeContract.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทสัญญา</a>
                            
              
              <br>

             
            </div>
          </div>

        </div>
           <div class="col-md-3">
        
          
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">ข้อมูลเกี่ยวกับรถ
              </h3>
            </div>
            <div class="box-body">
               <a href="../../Pages/Master/Brand.aspx"  class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลยี่ห้อรถ</a>
                            <a href="../../Pages/Master/Model.aspx"  class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลรุ่นรถ</a>                    
                            <a href="../../Pages/Master/CarColor.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลสีรถ</a>
                            <a href="../../Pages/Master/TypeEngine.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลชนิดเครื่องยนต์</a>
                            <a href="../../Pages/Master/TypeFuel.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลชนิดเชื้อเพลิง</a>
                            <a href="../../Pages/Master/TypeCar.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทรถ</a>
                            <a href="../../Pages/Master/TypeRegistration.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทการจดทะเบียน</a>
                            <a href="../../Pages/Master/Feature.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลคุณลักษณะการจดทะเบียน</a>
                <a href="../../Pages/Master/TypeAcquisition.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทการได้มา</a>
                            <a href="../../Pages/Master/TypeSupply.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทการจัดหา</a>
                            <a href="../../Pages/Master/TypePay.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทการจ่าย</a>
                            <a href="../../Pages/Master/TypeLeasing.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทการเช่าซื้อ</a>
                            <a href="../../Pages/Master/TypeSale.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลวิธีการจำหน่าย</a>
                            <a href="../../Pages/Master/TypePurchase.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทการใช้สิทธิซื้อ</a>
                            <a href="../../Pages/Master/TypeRepair.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทการซ่อม</a>
                            <a href="../../Pages/Master/Charge.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลข้อหา</a>
                            <a href="../../Pages/Master/TypeTrafficTicket.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทใบสั่ง</a>                       
                            <a href="../../Pages/Master/TypeCard.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลประเภทบัตรเติมน้ำมัน</a>
                            <a href="../../Pages/Master/Accessories.aspx" class="btn btn-block btn-social btn-twitter">
                <i class="fa fa-minus"></i>ข้อมูลอุปกรณ์</a>
              
              <br>

             
            </div>
          </div>

        </div>
            
        </div>
        <!-- /.row -->
    </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
