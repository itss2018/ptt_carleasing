﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.Master
{
    public partial class PurchaseCar : PageBase
    {
        #region Member
        string FieldType = "PurchaseCar", str = string.Empty;
        #endregion

        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {

                this.GetData();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("NAME");
                dt.Columns.Add("LICENSE");
                dt.Columns.Add("PROVINCE");
                dt.Columns.Add("BRAND");
                dt.Columns.Add("MODEL");
                dt.Columns.Add("TYPECAR");
                dt.Columns.Add("TYPEFUEL");
                dt.Columns.Add("TYPESALE");
                dt.Columns.Add("TYPEPURCHASE");
                dt.Columns.Add("REMARK");
                DataRow row = dt.NewRow();

                row["id"] = "0001";
                row["NAME"] = "John Doe";
                row["LICENSE"] = "6กท 2579";
                row["PROVINCE"] = "กรุงเทพฯ";
                row["BRAND"] = "BENZ";
                row["MODEL"] = "S280";
                row["TYPECAR"] = "รถเก๋ง";
                row["TYPEFUEL"] = "เบนซิล 91";
                row["TYPESALE"] = "";
                row["TYPEPURCHASE"] = "";
                row["REMARK"] = "ครบสัญญาเช่า";
                dt.Rows.Add(row);

                row = dt.NewRow();

                row["id"] = "0002";
                row["NAME"] = "Jane Doe";
                row["LICENSE"] = "1กท 1234";
                row["PROVINCE"] = "กรุงเทพฯ";
                row["BRAND"] = "BENZ";
                row["MODEL"] = "S280";
                row["TYPECAR"] = "รถเก๋ง";
                row["TYPEFUEL"] = "เบนซิล 91";
                row["TYPESALE"] = "";
                row["TYPEPURCHASE"] = "";
                row["REMARK"] = "สละสิทธิ์ซื้อ";
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");
                if (aView != null)
                {
                    str = Mode.View + "&" + row["ID"];//Mode & ID
                    aView.HRef = "PurchaseCarAddEdit.aspx?str=" + EncodeQueryString(str);
                }
                if (aEdit != null)
                {
                    str = Mode.Edit + "&" + row["ID"];//Mode & ID
                    aEdit.HRef = "PurchaseCarAddEdit.aspx?str=" + EncodeQueryString(str);
                }
            }

        }

        #region dgvData_PageIndexChanging
        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvData.PageIndex = e.NewPageIndex;
            cmdSearch_Click(null, null);

        }
        #endregion

        protected void dgvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            FormHelper.OpenForm("PurchaseCarAddEdit.aspx" + "?type=" + EncodeQueryString(Mode.Add.ToString()) + "&ID=" + EncodeQueryString("0"), Page);
        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtName.Text = string.Empty;
                ddlStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }
    }
}