﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.Master
{
    public partial class TypeCarAddEdit : PageBase
    {
        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion

        #region + View State +
        private string ActionType
        {
            get
            {
                if ((string)ViewState["ActionType"] != null)
                    return (string)ViewState["ActionType"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionType"] = value;
            }
        }

        private string ActionKey
        {
            get
            {
                if ((string)ViewState["ActionKey"] != null)
                    return (string)ViewState["ActionKey"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionKey"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                Helper.ActiveMenu.SetActiveMenu(Master, "tabMaster", "liCarBrand");

                InitialStatus(ddlStatus, " AND IS_ACTIVE = 1 AND STATUS_TYPE = 'ACTIVE_STATUS' ");
                this.CheckQueryString();
                this.GetData();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                if (string.Equals(ActionType, Mode.Add.ToString()))
                {
                    //txtName.Enabled = true;
                }
                else
                {
                    //txtName.Enabled = true;

                    DataTable dtCarType = CarTypeBLL.Instance.CarTypeSelectBLL(this.GetCondition());
                    if (dtCarType.Rows.Count > 0)
                    {
                        txtName.Text = dtCarType.Rows[0]["CARTYPE"].ToString();
                        ddlStatus.SelectedValue = dtCarType.Rows[0]["ISACTIVE"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void CheckQueryString()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
                {
                    ActionType = DecodeQueryString(Request.QueryString["type"]);
                    ActionKey = DecodeQueryString(Request.QueryString["id"]);

                    if (string.Equals(ActionType, Mode.View.ToString()))
                    {

                        DisableControls(Page);
                        cmdSave.Visible = false;
                        cmdCancel.Visible = false;
                        cmdNew.Visible = false;
                    }
                }
                else
                    Response.Redirect("../Other/NotAuthorize.aspx");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string GetCondition()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" AND CARTYPEID = '" + ActionKey + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.ValidateSave();
                UserName = "CAR1";
                CarTypeBLL.Instance.CarTypeAddBLL(int.Parse(ActionKey), txtName.Text.Trim(), ddlStatus.SelectedValue, UserName, 0);
                alertSuccess(string.Empty, Properties.Resources.SaveSuccess, "TypeCar.aspx");
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.ValidateSave();
                UserName = "CAR1"; //
                CarTypeBLL.Instance.CarTypeAddBLL(int.Parse(ActionKey), txtName.Text.Trim(), ddlStatus.SelectedValue, UserName, 1);
                alertSuccess(string.Empty, Properties.Resources.SaveSuccess, "TypeCar.aspx");
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("TypeCar.aspx");
        }

        private void ValidateSave()
        {
            try
            {
                if (string.Equals(txtName.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblName.Text));

                if (ddlStatus.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblStatus.Text));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
