﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace PCLS.WebPC.Pages.Master
{
    public partial class TypeEngine : PageBase
    {
        #region Member
        string FieldType = "TypeEngine", str = string.Empty;
        #endregion

        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                Helper.ActiveMenu.SetActiveMenu(Master, "tabMaster", "liCarBrand");

                this.GetData();
                InitialStatus(ddlStatus, " AND IS_ACTIVE = 1 AND STATUS_TYPE = 'ACTIVE_STATUS'");
                //this.AssignAuthen();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                dtData = CarEngineBLL.Instance.CarEngineSelectBLL(this.GetConditionSearch());
                GridViewHelper.BindGridView(dgvData, dtData, false);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private string GetConditionSearch()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                if (!string.Equals(txtName.Text.Trim(), string.Empty))
                    sb.Append(" AND TYPEENGINENAME LIKE '%" + txtName.Text.Trim() + "%'");

                if (ddlStatus.SelectedIndex > 0)
                    sb.Append(" AND ISACTIVE = " + ddlStatus.SelectedValue);

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            DataRowView row = (DataRowView)e.Row.DataItem;
            HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
            HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");

            if (aView != null)
                aView.HRef = "TypeEngineAddEdit.aspx?" + "type=" + EncodeQueryString(Mode.View.ToString()) + "&id=" + EncodeQueryString(row["TYPEENGINEID"].ToString());

            if (aEdit != null)
                aEdit.HRef = "TypeEngineAddEdit.aspx?" + "type=" + EncodeQueryString(Mode.Edit.ToString()) + "&id=" + EncodeQueryString(row["TYPEENGINEID"].ToString());

        }

        #region dgvData_PageIndexChanging
        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvData.PageIndex = e.NewPageIndex;
            cmdSearch_Click(null, null);

        }
        #endregion

        protected void dgvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            FormHelper.OpenForm("TypeEngineAddEdit.aspx" + "?type=" + EncodeQueryString(Mode.Add.ToString()) + "&id=" + EncodeQueryString("0"), Page);
        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtName.Text = string.Empty;
                ddlStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }

        protected void dgvData_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void dgvData_SelectedIndexChanged2(object sender, EventArgs e)
        {

        }
    }
}