﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.Master
{
    public partial class TypeEngineAddEdit : PageBase
    {
        #region + View State +
        private string ActionType
        {
            get
            {
                if ((string)ViewState["ActionType"] != null)
                    return (string)ViewState["ActionType"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionType"] = value;
            }
        }

        private string ActionKey
        {
            get
            {
                if ((string)ViewState["ActionKey"] != null)
                    return (string)ViewState["ActionKey"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionKey"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                    this.InitialForm();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                Helper.ActiveMenu.SetActiveMenu(Master, "tabMaster", "liCarBrand");

                InitialStatus(ddlStatus, " AND IS_ACTIVE = 1 AND STATUS_TYPE = 'ACTIVE_STATUS' ");
                this.CheckQueryString();
                this.GetData();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                if (string.Equals(ActionType, Mode.Add.ToString()))
                {
                    //txtName.Enabled = true;
                }
                else
                {
                    //txtName.Enabled = false;

                    DataTable dtCarTypeEngine= CarEngineBLL.Instance.CarEngineSelectBLL(this.GetCondition());
                    if (dtCarTypeEngine.Rows.Count > 0)
                    {
                        txtName.Text = dtCarTypeEngine.Rows[0]["TYPEENGINENAME"].ToString();
                        ddlStatus.SelectedValue = dtCarTypeEngine.Rows[0]["ISACTIVE"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CheckQueryString()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
                {
                    ActionType = DecodeQueryString(Request.QueryString["type"]);
                    ActionKey = DecodeQueryString(Request.QueryString["id"]);

                    if (string.Equals(ActionType, Mode.View.ToString()))
                    {
                        DisableControls(Page);
                        cmdSave.Visible = false;
                        cmdNew.Visible = false;
                        cmdCancel.Visible = false;
                    }
                }
                else
                    Response.Redirect("../Other/NotAuthorize.aspx");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string GetCondition()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" AND TYPEENGINEID = '" + ActionKey + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.ValidateSave();
                UserName = "CAR1";
                CarEngineBLL.Instance.CarEngineAddBLL(int.Parse(ActionKey), txtName.Text.Trim(), ddlStatus.SelectedValue, UserName, 0);
                alertSuccess(string.Empty, Properties.Resources.SaveSuccess, "TypeEngine.aspx");
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdNew_Click(object sender, EventArgs e)
        {
            try
            {
                this.ValidateSave();
                UserName = "CAR1"; //
                CarEngineBLL.Instance.CarEngineAddBLL(int.Parse(ActionKey), txtName.Text.Trim(), ddlStatus.SelectedValue, UserName, 1);
                alertSuccess(string.Empty, Properties.Resources.SaveSuccess, "TypeEngine.aspx");
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("TypeEngine.aspx");
        }

        private void ValidateSave()
        {
            try
            {
                if (string.Equals(txtName.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblName.Text));

                if (ddlStatus.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblStatus.Text));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}