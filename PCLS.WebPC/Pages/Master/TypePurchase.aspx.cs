﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.Master
{
    public partial class TypePurchase :  PageBase
    {
        #region Member
        string FieldType = "TypePurchase", str = string.Empty;
        #endregion

        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {

                this.GetData();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("TYPE");
                dt.Columns.Add("NAME");
                dt.Columns.Add("STATUS");
                DataRow row = dt.NewRow();

                row["ID"] = "0001";
                row["TYPE"] = "จำหน่ายพนักงาน";
                row["NAME"] = "จับฉลาก";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0002";
                row["TYPE"] = "จำหน่ายพนักงาน";
                row["NAME"] = "สิทธิ์ผู้บริหาร";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0003";
                row["TYPE"] = "ขายทอดตลาด";
                row["NAME"] = "Vendor ซื้อ";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["ID"] = "0004";
                row["TYPE"] = "รับซื้อคืน";
                row["NAME"] = "รับซื้อคืน";
                row["STATUS"] = "ใช้งาน";
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");
                if (aView != null)
                {
                    str = Mode.View + "&" + row["ID"];//Mode & ID
                    aView.HRef = "TypePurchaseAddEdit.aspx?str=" + EncodeQueryString(str);
                }
                if (aEdit != null)
                {
                    str = Mode.Edit + "&" + row["ID"];//Mode & ID
                    aEdit.HRef = "TypePurchaseAddEdit.aspx?str=" + EncodeQueryString(str);
                }
            }

        }

        #region dgvData_PageIndexChanging
        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvData.PageIndex = e.NewPageIndex;
            cmdSearch_Click(null, null);

        }
        #endregion

        protected void dgvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            FormHelper.OpenForm("TypePurchaseAddEdit.aspx" + "?type=" + EncodeQueryString(Mode.Add.ToString()) + "&ID=" + EncodeQueryString("0"), Page);
        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtName.Text = string.Empty;
                ddlStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }
    }
}
