﻿using System;

namespace PCLS.WebPC
{
    public partial class BlankPage : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                Helper.ActiveMenu.SetActiveMenu(Master, string.Empty, "liDashboard");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}