﻿using System;
using System.IO;

namespace PCLS.WebPC.Pages.Other
{
    public partial class DownloadFile :  PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["FILE_DOWNLOAD"] != null)
            {
                string FullPath = Session["FILE_DOWNLOAD"].ToString();
                this.Download(FullPath);

                Session.Remove("FILE_DOWNLOAD");
            }
        }

        private void Download(string FullPath)
        {
            string FileName = Path.GetFileName(FullPath);

            Response.ContentType = "APPLICATION/OCTET-STREAM";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + FileName);
            Response.TransmitFile(FullPath);
            Response.End();
        }
    }
}