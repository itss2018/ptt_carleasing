﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NotAuthorize.aspx.cs" Inherits="PCLS.WebPC.Pages.Other.NotAuthorize" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Not Authorize</title>

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../FontAwesome/css/font-awesome.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/AdminLTE.css" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
        <div class="container">
            <div class="modal fade" id="ShowInformation" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--%>
                                    <h4 class="modal-title">
                                        <asp:Image ID="imgInformation" runat="server" ImageUrl="~/Images/imgNotAuthorize.png" Width="48" Height="48" />
                                        <asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="modal-footer">
                                    <button id="cmdLogin" runat="server" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onserverclick="cmdLogin_ServerClick">Login Again</button>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <!-- jQuery 2.2.3 -->
        <script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>

        <!-- Bootstrap 3.3.6 -->
        <script src="../../bootstrap/js/bootstrap.min.js"></script>

        <!-- AdminLTE App -->
        <script src="../../dist/js/app.min.js"></script>

        <!-- AdminLTE for demo purposes -->
        <script src="../../dist/js/demo.js"></script>
    </form>
</body>
</html>