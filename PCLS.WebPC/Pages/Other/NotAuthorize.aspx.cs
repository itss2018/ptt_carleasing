﻿using System;
using System.Web.UI;

namespace PCLS.WebPC.Pages.Other
{
    public partial class NotAuthorize : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lblModalTitle.Text = "Not Authorize";
                lblModalBody.Text = "Your user information no access to the system.";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ShowInformation", "$('#ShowInformation').modal();", true);
                upModal.Update();
            }
        }

        protected void cmdLogin_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("../Authentication/Login.aspx");
        }
    }
}