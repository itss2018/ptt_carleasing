﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="ReportInsurance.aspx.cs" Inherits="PCLS.WebPC.Pages.Reports.ReportInsurance" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;รายงานข้อมูลการชำระภาษีรถยนต์ประจำปี</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>รายงาน</li>
                    <li class="active">รายงานข้อมูลการชำระภาษีรถยนต์ประจำปี</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Search Option</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="ทะเบียน"></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="ยี่ห้อ"></asp:Label>
                                    <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="ประเภททะเบียน"></asp:Label>
                                    <asp:DropDownList ID="DropDownList2" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" Text="บริษัทลีสซิ่ง"></asp:Label>
                                    <asp:DropDownList ID="DropDownList5" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="วันที่ครบกำหนดชำระภาษี ตั้งแต่"></asp:Label>
                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="ถึงวันที่"></asp:Label>
                                    <asp:TextBox ID="TextBox6" runat="server" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="ประเภทการจดทะเบียน"></asp:Label>
                                    <asp:DropDownList ID="DropDownList1" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" Text="การได้มา"></asp:Label>
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="สถานะ"></asp:Label>
                                    <asp:DropDownList ID="DropDownList3" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                       

                        <div class="row">
                            
                           
                        </div>

                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdAdd" runat="server" Text="Export"  SkinID="ButtonSuccess" OnClick="cmdAdd_Click" Visible="False" />
                        <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา"  OnClick="cmdSearch_Click" />
                        <asp:Button ID="cmdClear" runat="server" Text="ล้างข้อมูล" SkinID="ButtonWarning" OnClick="cmdClear_Click"  />
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">รายงานข้อมูลการชำระภาษีรถยนต์ประจำปี</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body" style="overflow-x:auto">
                        <asp:GridView ID="dgvData" runat="server" class="table table-bordered" SkinID="GridNoPaging" OnRowDataBound="dgvData_RowDataBound" DataKeyNames="ID" AutoGenerateColumns="False" OnSelectedIndexChanged="dgvData_SelectedIndexChanged1">
                            <HeaderStyle Wrap="false" />
                            <RowStyle Wrap="false" />
                            <Columns>
                                <asp:BoundField DataField="ID" HeaderText="ลำดับ"></asp:BoundField>
                                <asp:BoundField DataField="LICENSE" HeaderText="ทะเบียน"></asp:BoundField>
                                <asp:BoundField DataField="PROVINCE" HeaderText="จังหวัด"></asp:BoundField>
                                <asp:BoundField DataField="BRAND" HeaderText="ยี่ห้อ"></asp:BoundField>
                                <asp:BoundField DataField="MODEL" HeaderText="รุ่น"></asp:BoundField>
                                <asp:BoundField DataField="TYPELICENSE" HeaderText="ประเภททะเบียน"></asp:BoundField>
                                <asp:BoundField DataField="UNITCODE" HeaderText="รหัสหน่วยงาน"></asp:BoundField>
                                <asp:BoundField DataField="UNIT" HeaderText="หน่วยงาน"></asp:BoundField>
                                <asp:BoundField DataField="BUSINESS" HeaderText="ธุรกิจ"></asp:BoundField>
                                <asp:BoundField DataField="IO" HeaderText="IO"></asp:BoundField>
                                <asp:BoundField DataField="COSTCENTER" HeaderText="COSTCENTER"></asp:BoundField>
                                <asp:BoundField DataField="DUEDATE" HeaderText="วันที่ครบกำหนด"></asp:BoundField>
                                <asp:BoundField DataField="TYPERECEIVE" HeaderText="การได้มา"></asp:BoundField>
                                <asp:BoundField DataField="VENDERCODE" HeaderText="VENDER Code"></asp:BoundField>
                                <asp:BoundField DataField="ISNGV" HeaderText="ติด NGV"></asp:BoundField>
                                <asp:BoundField DataField="STATUS" HeaderText="สถานะ"></asp:BoundField>
                                <asp:BoundField DataField="TAXPAYDATE" HeaderText="ชำระภาษีล่าสุด"></asp:BoundField>
                                <asp:BoundField DataField="PLANSTATUS" HeaderText="ตามแผน/ล่าช้า"></asp:BoundField>
                                <asp:BoundField DataField="TAXPRICE" HeaderText="ค่าภาษี"></asp:BoundField>
                                <asp:BoundField DataField="REMARK" HeaderText="หมายเหตุ"></asp:BoundField>              
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

                <div class="box box-primary" runat="server" id="divButton">
                    <div class="box-header with-border">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div id="divButtonSave" runat="server" style="text-align: right">
                                    <asp:Button ID="cmdExport" runat="server" Text="Export" SkinID="ButtonSuccess"  OnClick="cmdExport_Click" />
                                    <asp:Button ID="Button2" runat="server" Text="ยกเลิก" SkinID="ButtonDanger" OnClick="cmdClear_Click"  />                                        
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

