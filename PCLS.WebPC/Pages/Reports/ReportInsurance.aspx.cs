﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.Reports
{
    public partial class ReportInsurance : PageBase
    {
        #region Member
        string FieldType = "BorrowKey", str = string.Empty;
        #endregion

        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {

                this.GetData();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("LICENSE");
                dt.Columns.Add("PROVINCE");
                dt.Columns.Add("BRAND");
                dt.Columns.Add("MODEL");
                dt.Columns.Add("TYPELICENSE");
                dt.Columns.Add("UNITCODE");
                dt.Columns.Add("UNIT");
                dt.Columns.Add("BUSINESS");
                dt.Columns.Add("IO");
                dt.Columns.Add("COSTCENTER");
                dt.Columns.Add("DUEDATE");
                dt.Columns.Add("TYPERECEIVE");
                dt.Columns.Add("VENDERCODE");
                dt.Columns.Add("ISNGV");
                dt.Columns.Add("STATUS");
                dt.Columns.Add("TAXPAYDATE");
                dt.Columns.Add("PLANSTATUS");
                dt.Columns.Add("TAXPRICE");
                dt.Columns.Add("REMARK");

                DataRow row = dt.NewRow();

                row["id"] = "1";
                row["LICENSE"] = "2กม-6837";
                row["PROVINCE"] = "กรุงเทพมหานคร";
                row["BRAND"] = "KIA";
                row["MODEL"] = "PICANTO";
                row["TYPELICENSE"] = "รย.1";
                row["UNITCODE"] = "80000087";
                row["UNIT"] = "ผ.บด.กด.";
                row["BUSINESS"] = "สนญ.";
                row["IO"] = "103260035003";
                row["COSTCENTER"] = "10326003";
                row["DUEDATE"] = "24/10/2556";
                row["TYPERECEIVE"] = "ซื้อ";
                row["VENDERCODE"] = "";
                row["ISNGV"] = "ไม่ติด";
                row["STATUS"] = "ต่อภาษีแล้ว";
                row["TAXPAYDATE"] = "15/8/2017";
                row["PLANSTATUS"] = "ตามแผน";
                row["TAXPRICE"] = "2,544.00";
                row["REMARK"] = "";
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["id"] = "2";
                row["LICENSE"] = "3กย-2965";
                row["PROVINCE"] = "กรุงเทพมหานคร";
                row["BRAND"] = "HONDA";
                row["MODEL"] = "CIVIC";
                row["TYPELICENSE"] = "รย.1";
                row["UNITCODE"] = "80000547";
                row["UNIT"] = "ปท.5 ปตต.";
                row["BUSINESS"] = "สนญ.";
                row["IO"] = "301100005006";
                row["COSTCENTER"] = "30113003";
                row["DUEDATE"] = "15/10/2557";
                row["TYPERECEIVE"] = "เช่า";
                row["VENDERCODE"] = "KL";
                row["ISNGV"] = "ติด";
                row["STATUS"] = "ต่อภาษีแล้ว";
                row["TAXPAYDATE"] = "15/8/2017";
                row["PLANSTATUS"] = "ตามแผน";
                row["TAXPRICE"] = "3,145.50";
                row["REMARK"] = "";
                dt.Rows.Add(row);


                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {


        }

        #region dgvData_PageIndexChanging
        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvData.PageIndex = e.NewPageIndex;
            cmdSearch_Click(null, null);

        }
        #endregion

        protected void dgvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
                        
        }

        protected void cmdExport_Click(object sender, EventArgs e)
        {

            FormHelper.OpenForm("../../Pages/Reports/Insurance/report รายงานค่าภาษีรถยนต์ v2.xlsx", Page);

        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void dgvData_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }
    }
}