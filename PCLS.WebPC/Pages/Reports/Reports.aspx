﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.Reports"  Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;รายงานแสดงข้อมูล</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li class="active">รายงานแสดงข้อมูล</li>
                </ol>
            </section>
             <section class="content">
                <div class="box box-primary">
                    
                    
                    <div class="box-body">
                     
                            <div class="col-md-6">
                                <a class="btn btn-lg btn-primary big-btn-1" href="#">
  				                      <img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานการครอบครองยานพาหนะ</small></div></a>

                                &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="#"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานข้อมูลรถยนต์</small></div></a>
                                &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="#"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานการเปลี่ยนแปลงข้อมูลการครอบครองยานพาหนะ</small></div></a>
                                &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="#x"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานสัญญารถยนต์</small></div></a>
                                &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="ReportInsurance.aspx"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานข้อมูลการชำระภาษีรถยนต์ประจำปี</small></div></a>
                                &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="#x"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานสรุปค่างวด/ค่าเช่ารถ</small></div></a>
                            &nbsp;&nbsp;</div>
                        <div class="col-md-6">
                               
                                <a class="btn btn-lg btn-primary big-btn-1" href="#">
  				                      <img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานข้อมูลประกันภัยรถยนต์</small></div></a>
                                &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="#"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานประวัติการซ่อมรถยนต์</small></div></a>
                                &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="#"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานข้อมููลยานพาหนะที่ไม่ถูกต้อง</small></div></a>
                                &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="#"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานการซ่อมรถ</small></div></a>
                                 &nbsp;&nbsp;<a class="btn btn-lg btn-primary big-btn-1" href="#"><img width="60px" class="pull-left" src="../../dist/img/icon/report.png"><div class="btn-text"><small>รายงานการส่งมอบรถยนต์</small></div></a>
                            &nbsp;&nbsp;</div>
                        
                       
                    </div>
             
                </div>
               
            </section>

            
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

