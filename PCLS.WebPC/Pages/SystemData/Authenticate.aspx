﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="Authenticate.aspx.cs" Inherits="PCLS.WebPC.Pages.SystemData.Authenticate" Theme="Blue" %>

<%@ Register Src="~/UserControl/ConfirmModal.ascx" TagPrefix="uc3" TagName="ModalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;Authentication</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>System</li>
                    <li class="active">Authentication</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-CurrencyType">Configuration</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#UserGroup" data-toggle="tab" aria-expanded="true">User Group</a></li>
                                    <li class=""><a href="#TabMaster" data-toggle="tab" aria-expanded="false">ประวัติรถ</a></li>
                                    <li class=""><a href="#TabSystem1" data-toggle="tab" aria-expanded="false">งานทะเบียนรถ</a></li>
                                    <li class=""><a href="#TabSystem2" data-toggle="tab" aria-expanded="false">ข้อมูลสัญญา</a></li>
                                    <li class=""><a href="#TabSystem3" data-toggle="tab" aria-expanded="false">ประวัติการซ่อม</a></li>
                                    <li class=""><a href="#TabSystem4" data-toggle="tab" aria-expanded="false">รายงาน</a></li>
                                    <li class=""><a href="#TabSystem5" data-toggle="tab" aria-expanded="false">ยื่นคำร้อง</a></li>
                                    <li class=""><a href="#TabSystem6" data-toggle="tab" aria-expanded="false">รายการคำร้อง</a></li>
                                    <li class=""><a href="#TabSystem7" data-toggle="tab" aria-expanded="false">ข้อมูลตั้งต้น</a></li>
                                    <li class=""><a href="#TabSystem8" data-toggle="tab" aria-expanded="false">System</a></li>
                                </ul>

                                <div class="tab-content" style="padding-left: 30px; padding-right:30px">
                                    <div class="tab-pane fade active in" id="UserGroup">
                                        <br />
                                        <br />
                                        <asp:DropDownList ID="ddlUserGroup" runat="server" CssClass="form-control" Style="width: 30%" AutoPostBack="true" OnSelectedIndexChanged="ddlUserGroup_SelectedIndexChanged">
                                            <asp:ListItem>Administrator - ผู้ดูแลระบบ</asp:ListItem>
                                            <asp:ListItem>กด.</asp:ListItem>
                                            <asp:ListItem>BSA</asp:ListItem>
                                            <asp:ListItem>USER - ผู้ใช้ทั่วไป</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                    </div>
                                    <div class="tab-pane fade" id="TabMaster" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvMaster" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="TabSystem1" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvTransaction" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="TabSystem2" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvSystem1" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="TabSystem3" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvSystem2" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="TabSystem4" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvSystem3" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="TabSystem5" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvSystem4" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="TabSystem6" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvSystem5" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="TabSystem7" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvSystem6" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="TabSystem8" style="overflow-x:auto">
                                        <br />
                                        <div class="dataTable_wrapper">
                                            <asp:GridView ID="dgvSystem7" runat="server" DataKeyNames="MENU_ID_LEVEL1,MENU_ID_LEVEL2">
                                                <HeaderStyle Wrap="false" />
                                                <RowStyle Wrap="false" />
                                                <Columns>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL1" HeaderText="Function (Level 1)"></asp:BoundField>
                                                    <asp:BoundField DataField="MENU_NAME_LEVEL2" HeaderText="Function (Level 2)"></asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION"></asp:BoundField>
                                                    <asp:TemplateField HeaderText="VISIBLE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkVisible" runat="server" Checked='<%# Eval("VISIBLE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="READ">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRead" runat="server" Checked='<%# Eval("READ").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="WRITE">
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkWrite" runat="server" Checked='<%# Eval("WRITE").ToString().Equals("1")  %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdCloseAll" runat="server" Text="ปิดทั้งหมด" SkinID="ButtonWarning" OnClick="cmdCloseAll_Click" />
                        <asp:Button ID="cmdOpenAll" runat="server" Text="เปิดทั้งหมด" SkinID="ButtonSuccess" OnClick="cmdOpenAll_Click" />
                        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" SkinID="ButtonSuccess" data-toggle="modal" data-target="#ModalConfirmBeforeSave" />
                        <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" OnClick="cmdCancel_Click" />
                    </div>
                </div>
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    <uc3:ModalPopup runat="server" ID="mpConfirmSave" IDModel="ModalConfirmBeforeSave"
                IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
                TextTitle="ยืนยันการบันทึก" TextDetail="คุณต้องการบันทึกข้อมูล ใช่หรือไม่ ?" />
</asp:Content>