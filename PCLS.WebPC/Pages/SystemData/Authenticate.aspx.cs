﻿using PCLS.Bussiness.System;
using PCLS.Bussiness.SystemData;
using PCLS.WebPC.Helper;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.SystemData
{
    public partial class Authenticate : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                //Helper.ActiveMenu.SetActiveMenu(Master, "tabSystem", "liAuthentication");

                //DataTable dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(string.Empty);
                //DropDownListHelper.BindDropDown(ref ddlUserGroup, dtUserGroup, "USERGROUP_ID", "USERGROUP_NAME", true, Properties.Resources.DropDownChooseText);

                //this.VisibleButtom(false);
                ////this.AssignAuthen();
                DataTable dt = new DataTable();
                dt.Columns.Add("MENU_ID_LEVEL1");
                dt.Columns.Add("MENU_ID_LEVEL2");
                dt.Columns.Add("MENU_NAME_LEVEL1");
                dt.Columns.Add("MENU_NAME_LEVEL2");
                dt.Columns.Add("DESCRIPTION");
                dt.Columns.Add("VISIBLE");
                dt.Columns.Add("READ");
                dt.Columns.Add("WRITE");

                DataRow row = dt.NewRow();
                row["MENU_ID_LEVEL1"] = 1;
                row["MENU_ID_LEVEL2"] = 1;
                row["MENU_NAME_LEVEL1"] = "ประวัติรถ";
                row["MENU_NAME_LEVEL2"] = "";
                row["DESCRIPTION"] = "";
                row["VISIBLE"] = 1;
                row["READ"] = 1;
                row["WRITE"] = 1;
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["MENU_ID_LEVEL1"] = 2;
                row["MENU_ID_LEVEL2"] = 2;
                row["MENU_NAME_LEVEL1"] = "ข้อมูลผู้ซื้อรถ";
                row["MENU_NAME_LEVEL2"] = "";
                row["DESCRIPTION"] = "";
                row["VISIBLE"] = 1;
                row["READ"] = 1;
                row["WRITE"] = 1;
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["MENU_ID_LEVEL1"] = 3;
                row["MENU_ID_LEVEL2"] = 3;
                row["MENU_NAME_LEVEL1"] = "การส่งมอบรถยนต์";
                row["MENU_NAME_LEVEL2"] = "";
                row["DESCRIPTION"] = "";
                row["VISIBLE"] = 1;
                row["READ"] = 1;
                row["WRITE"] = 1;
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["MENU_ID_LEVEL1"] = 4;
                row["MENU_ID_LEVEL2"] = 4;
                row["MENU_NAME_LEVEL1"] = "การครอบครองรถยนต์";
                row["MENU_NAME_LEVEL2"] = "";
                row["DESCRIPTION"] = "";
                row["VISIBLE"] = 1;
                row["READ"] = 1;
                row["WRITE"] = 1;
                dt.Rows.Add(row);

                this.dgvMaster.Visible = true;
                dgvMaster.DataSource = dt;
                dgvMaster.DataBind();

                ////////////////////////////////////////////////////////////////////////////
                DataTable dt2 = new DataTable();
                dt2.Columns.Add("MENU_ID_LEVEL1");
                dt2.Columns.Add("MENU_ID_LEVEL2");
                dt2.Columns.Add("MENU_NAME_LEVEL1");
                dt2.Columns.Add("MENU_NAME_LEVEL2");
                dt2.Columns.Add("DESCRIPTION");
                dt2.Columns.Add("VISIBLE");
                dt2.Columns.Add("READ");
                dt2.Columns.Add("WRITE");

                DataRow row2 = dt2.NewRow();
                row2["MENU_ID_LEVEL1"] = 1;
                row2["MENU_ID_LEVEL2"] = 1;
                row2["MENU_NAME_LEVEL1"] = "ภาษีรถยนต์";
                row2["MENU_NAME_LEVEL2"] = "";
                row2["DESCRIPTION"] = "";
                row2["VISIBLE"] = 1;
                row2["READ"] = 1;
                row2["WRITE"] = 1;
                dt2.Rows.Add(row2);

                row2 = dt2.NewRow();
                row2["MENU_ID_LEVEL1"] = 2;
                row2["MENU_ID_LEVEL2"] = 2;
                row2["MENU_NAME_LEVEL1"] = "ประกันภัย";
                row2["MENU_NAME_LEVEL2"] = "";
                row2["DESCRIPTION"] = "";
                row2["VISIBLE"] = 1;
                row2["READ"] = 1;
                row2["WRITE"] = 1;
                dt2.Rows.Add(row2);

                row2 = dt2.NewRow();
                row2["MENU_ID_LEVEL1"] = 3;
                row2["MENU_ID_LEVEL2"] = 3;
                row2["MENU_NAME_LEVEL1"] = "ใบสั่งจราจร";
                row2["MENU_NAME_LEVEL2"] = "";
                row2["DESCRIPTION"] = "";
                row2["VISIBLE"] = 1;
                row2["READ"] = 1;
                row2["WRITE"] = 1;
                dt2.Rows.Add(row2);

                this.dgvTransaction.Visible = true;
                dgvTransaction.DataSource = dt2;
                dgvTransaction.DataBind();
                dt2.Clear();

                ////////////////////////////////////////////////////////////////////////////
                DataTable dt3 = new DataTable();
                dt3.Columns.Add("MENU_ID_LEVEL1");
                dt3.Columns.Add("MENU_ID_LEVEL2");
                dt3.Columns.Add("MENU_NAME_LEVEL1");
                dt3.Columns.Add("MENU_NAME_LEVEL2");
                dt3.Columns.Add("DESCRIPTION");
                dt3.Columns.Add("VISIBLE");
                dt3.Columns.Add("READ");
                dt3.Columns.Add("WRITE");

                DataRow row3 = dt3.NewRow();
                row3["MENU_ID_LEVEL1"] = 1;
                row3["MENU_ID_LEVEL2"] = 1;
                row3["MENU_NAME_LEVEL1"] = "ข้อมูลสัญญา";
                row3["MENU_NAME_LEVEL2"] = "";
                row3["DESCRIPTION"] = "";
                row3["VISIBLE"] = 1;
                row3["READ"] = 1;
                row3["WRITE"] = 1;
                dt3.Rows.Add(row3);


                this.dgvSystem1.Visible = true;
                dgvSystem1.DataSource = dt3;
                dgvSystem1.DataBind();
                ////////////////////////////////////////////////////////////////////////////


                DataTable dt4 = new DataTable();
                dt4.Columns.Add("MENU_ID_LEVEL1");
                dt4.Columns.Add("MENU_ID_LEVEL2");
                dt4.Columns.Add("MENU_NAME_LEVEL1");
                dt4.Columns.Add("MENU_NAME_LEVEL2");
                dt4.Columns.Add("DESCRIPTION");
                dt4.Columns.Add("VISIBLE");
                dt4.Columns.Add("READ");
                dt4.Columns.Add("WRITE");

                DataRow row4 = dt4.NewRow();
                row4["MENU_ID_LEVEL1"] = 1;
                row4["MENU_ID_LEVEL2"] = 1;
                row4["MENU_NAME_LEVEL1"] = "ประวัติการซ่อม";
                row4["MENU_NAME_LEVEL2"] = "";
                row4["DESCRIPTION"] = "";
                row4["VISIBLE"] = 1;
                row4["READ"] = 1;
                row4["WRITE"] = 1;
                dt4.Rows.Add(row4);


                this.dgvSystem2.Visible = true;
                dgvSystem2.DataSource = dt4;
                dgvSystem2.DataBind();
                ////////////////////////////////////////////////////////////////////////////

                DataTable dt5 = new DataTable();
                dt5.Columns.Add("MENU_ID_LEVEL1");
                dt5.Columns.Add("MENU_ID_LEVEL2");
                dt5.Columns.Add("MENU_NAME_LEVEL1");
                dt5.Columns.Add("MENU_NAME_LEVEL2");
                dt5.Columns.Add("DESCRIPTION");
                dt5.Columns.Add("VISIBLE");
                dt5.Columns.Add("READ");
                dt5.Columns.Add("WRITE");

                DataRow row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "ใบรายงานการครอบครองยานพาหนะ";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                 row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานข้อมูลรถยนต์";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                 row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานการเปลี่ยนแปลงข้อมูลการครอบครองยานพาหนะ";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                 row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานสัญญารถยนต์";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                 row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานข้อมูลการชำระภาษีรถยนต์ประจำปี";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานสรุปค่างวด/ค่าเช่ารถ";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานข้อมูลประกันภัยรถยนต์";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานประวัติการซ่อมรถยนต์";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานข้อมููลยานพาหนะที่ไม่ถูกต้อง";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานการซ่อมรถ";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                row5 = dt5.NewRow();
                row5["MENU_ID_LEVEL1"] = 1;
                row5["MENU_ID_LEVEL2"] = 1;
                row5["MENU_NAME_LEVEL1"] = "รายงานการส่งมอบรถยนต์";
                row5["MENU_NAME_LEVEL2"] = "";
                row5["DESCRIPTION"] = "";
                row5["VISIBLE"] = 1;
                row5["READ"] = 1;
                row5["WRITE"] = 1;
                dt5.Rows.Add(row5);

                this.dgvSystem3.Visible = true;
                dgvSystem3.DataSource = dt5;
                dgvSystem3.DataBind();

                ////////////////////////////////////////////////////////////////////////////

                DataTable dt7= new DataTable();
                dt7.Columns.Add("MENU_ID_LEVEL1");
                dt7.Columns.Add("MENU_ID_LEVEL2");
                dt7.Columns.Add("MENU_NAME_LEVEL1");
                dt7.Columns.Add("MENU_NAME_LEVEL2");
                dt7.Columns.Add("DESCRIPTION");
                dt7.Columns.Add("VISIBLE");
                dt7.Columns.Add("READ");
                dt7.Columns.Add("WRITE");

                DataRow row7 = dt7.NewRow();
                row7["MENU_ID_LEVEL1"] = 1;
                row7["MENU_ID_LEVEL2"] = 1;
                row7["MENU_NAME_LEVEL1"] = "ยื่นคำร้อง";
                row7["MENU_NAME_LEVEL2"] = "";
                row7["DESCRIPTION"] = "";
                row7["VISIBLE"] = 1;
                row7["READ"] = 1;
                row7["WRITE"] = 1;
                dt7.Rows.Add(row7);


                this.dgvSystem4.Visible = true;
                dgvSystem4.DataSource = dt7;
                dgvSystem4.DataBind();
                ////////////////////////////////////////////////////////////////////////////

                DataTable dt6 = new DataTable();
                dt6.Columns.Add("MENU_ID_LEVEL1");
                dt6.Columns.Add("MENU_ID_LEVEL2");
                dt6.Columns.Add("MENU_NAME_LEVEL1");
                dt6.Columns.Add("MENU_NAME_LEVEL2");
                dt6.Columns.Add("DESCRIPTION");
                dt6.Columns.Add("VISIBLE");
                dt6.Columns.Add("READ");
                dt6.Columns.Add("WRITE");

                DataRow row6 = dt6.NewRow();
                row6["MENU_ID_LEVEL1"] = 1;
                row6["MENU_ID_LEVEL2"] = 1;
                row6["MENU_NAME_LEVEL1"] = "ทั้งหมด";
                row6["MENU_NAME_LEVEL2"] = "";
                row6["DESCRIPTION"] = "";
                row6["VISIBLE"] = 1;
                row6["READ"] = 1;
                row6["WRITE"] = 1;
                dt6.Rows.Add(row6);

                row6 = dt6.NewRow();
                row6["MENU_ID_LEVEL1"] = 1;
                row6["MENU_ID_LEVEL2"] = 1;
                row6["MENU_NAME_LEVEL1"] = "ร่าง";
                row6["MENU_NAME_LEVEL2"] = "";
                row6["DESCRIPTION"] = "";
                row6["VISIBLE"] = 1;
                row6["READ"] = 1;
                row6["WRITE"] = 1;
                dt6.Rows.Add(row6);

                row6 = dt6.NewRow();
                row6["MENU_ID_LEVEL1"] = 1;
                row6["MENU_ID_LEVEL2"] = 1;
                row6["MENU_NAME_LEVEL1"] = "ระหว่างอนุมัติ";
                row6["MENU_NAME_LEVEL2"] = "";
                row6["DESCRIPTION"] = "";
                row6["VISIBLE"] = 1;
                row6["READ"] = 1;
                row6["WRITE"] = 1;
                dt6.Rows.Add(row6);

                row6 = dt6.NewRow();
                row6["MENU_ID_LEVEL1"] = 1;
                row6["MENU_ID_LEVEL2"] = 1;
                row6["MENU_NAME_LEVEL1"] = "เสร็จสิ้น";
                row6["MENU_NAME_LEVEL2"] = "";
                row6["DESCRIPTION"] = "";
                row6["VISIBLE"] = 1;
                row6["READ"] = 1;
                row6["WRITE"] = 1;
                dt6.Rows.Add(row6);

                row6 = dt6.NewRow();
                row6["MENU_ID_LEVEL1"] = 1;
                row6["MENU_ID_LEVEL2"] = 1;
                row6["MENU_NAME_LEVEL1"] = "เรียกกลับ";
                row6["MENU_NAME_LEVEL2"] = "";
                row6["DESCRIPTION"] = "";
                row6["VISIBLE"] = 1;
                row6["READ"] = 1;
                row6["WRITE"] = 1;
                dt6.Rows.Add(row6);

                this.dgvSystem5.Visible = true;
                dgvSystem5.DataSource = dt6;
                dgvSystem5.DataBind();

                ////////////////////////////////////////////////////////////////////////////
                DataTable dt9 = new DataTable();
                dt9.Columns.Add("MENU_ID_LEVEL1");
                dt9.Columns.Add("MENU_ID_LEVEL2");
                dt9.Columns.Add("MENU_NAME_LEVEL1");
                dt9.Columns.Add("MENU_NAME_LEVEL2");
                dt9.Columns.Add("DESCRIPTION");
                dt9.Columns.Add("VISIBLE");
                dt9.Columns.Add("READ");
                dt9.Columns.Add("WRITE");

                DataRow row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลจังหวัด"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลธนาคาร"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลสกุลเงิน"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทการได้มา"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทการจัดหา"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทการจ่าย"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทการเช่าซื้อ"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลวิธีการจำหน่าย"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทการใช้สิทธิซื้อ"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทการซ่อม"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลข้อหา"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทใบสั่ง"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทบัตรเติมน้ำมัน"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลบริษัทคู่สัญญา"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทประกันภัย"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทสัญญา"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลสถานะสัญญา"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทเอกสาร"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลเอกสารแนบ"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทอีเมล์"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลรูปแบบอีเมล์"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลเหตุผลในการไม่อนุมัติ"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลยี่ห้อรถ"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลรุ่นรถ"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลสีรถ"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลชนิดเครื่องยนต์"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลชนิดเชื้อเพลิง"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทรถ"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลประเภทการจดทะเบียน"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลคุณลักษณะการจดทะเบียน"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                row9 = dt9.NewRow();
                row9["MENU_ID_LEVEL1"] = 1; row9["MENU_ID_LEVEL2"] = 1; row9["MENU_NAME_LEVEL1"] = "ข้อมูลหน่วยงาน"; row9["MENU_NAME_LEVEL2"] = ""; row9["DESCRIPTION"] = ""; row9["VISIBLE"] = 1; row9["READ"] = 1; row9["WRITE"] = 1;
                dt9.Rows.Add(row9);

                this.dgvSystem6.Visible = true;
                dgvSystem6.DataSource = dt9;
                dgvSystem6.DataBind();

                ////////////////////////////////////////////////////////////////////////////
                DataTable dt8 = new DataTable();
                dt8.Columns.Add("MENU_ID_LEVEL1");
                dt8.Columns.Add("MENU_ID_LEVEL2");
                dt8.Columns.Add("MENU_NAME_LEVEL1");
                dt8.Columns.Add("MENU_NAME_LEVEL2");
                dt8.Columns.Add("DESCRIPTION");
                dt8.Columns.Add("VISIBLE");
                dt8.Columns.Add("READ");
                dt8.Columns.Add("WRITE");

                DataRow row8 = dt8.NewRow();
                row8["MENU_ID_LEVEL1"] = 1;
                row8["MENU_ID_LEVEL2"] = 1;
                row8["MENU_NAME_LEVEL1"] = "Authentication";
                row8["MENU_NAME_LEVEL2"] = "";
                row8["DESCRIPTION"] = "";
                row8["VISIBLE"] = 1;
                row8["READ"] = 1;
                row8["WRITE"] = 1;
                dt8.Rows.Add(row8);

                row8 = dt8.NewRow();
                row8["MENU_ID_LEVEL1"] = 2;
                row8["MENU_ID_LEVEL2"] = 2;
                row8["MENU_NAME_LEVEL1"] = "User";
                row8["MENU_NAME_LEVEL2"] = "";
                row8["DESCRIPTION"] = "";
                row8["VISIBLE"] = 1;
                row8["READ"] = 1;
                row8["WRITE"] = 1;
                dt8.Rows.Add(row8);

                row8 = dt8.NewRow();
                row8["MENU_ID_LEVEL1"] = 3;
                row8["MENU_ID_LEVEL2"] = 3;
                row8["MENU_NAME_LEVEL1"] = "User Group";
                row8["MENU_NAME_LEVEL2"] = "";
                row8["DESCRIPTION"] = "";
                row8["VISIBLE"] = 1;
                row8["READ"] = 1;
                row8["WRITE"] = 1;
                dt8.Rows.Add(row8);

                this.dgvSystem7.Visible = true;
                dgvSystem7.DataSource = dt8;
                dgvSystem7.DataBind();
                ////////////////////////////////////////////////////////////////////////////
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void AssignAuthen()
        {
            try
            {
                if (!CanWrite)
                {
                    cmdCloseAll.Visible = false;
                    cmdOpenAll.Visible = false;
                    cmdSave.Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void VisibleButtom(bool visible)
        {
            try
            {
                cmdCloseAll.Visible = visible;
                cmdOpenAll.Visible = visible;
                cmdSave.Visible = visible;
                cmdCancel.Visible = visible;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void ddlUserGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //if (ddlUserGroup.SelectedIndex > 0)
                //{
                //    this.VisibleButtom(true);
                //    this.LoadAuthen();
                //}
                //else
                //{
                //    this.VisibleButtom(false);

                //    GridViewHelper.BindGridView(dgvMaster, null, false);
                //    GridViewHelper.BindGridView(dgvTransaction, null, false);
                //    GridViewHelper.BindGridView(dgvSystem, null, false);
                //}
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void LoadAuthen()
        {
            try
            {
                DataTable dtAuthen = AuthenBLL.Instance.AuthenSelectBLL(ddlUserGroup.SelectedValue);
                if (dtAuthen.Rows.Count == 0)
                    dtAuthen = AuthenBLL.Instance.AuthenSelectTemplateBLL();

                DataTable dtMaster = dtAuthen.Select("MENU_NAME = 'Master Data'").CopyToDataTable();
                GridViewHelper.BindGridView(dgvMaster, dtMaster, false);

                DataTable dtTransaction = dtAuthen.Select("MENU_NAME = 'Transaction'").CopyToDataTable();
                GridViewHelper.BindGridView(dgvTransaction, dtTransaction, false);

                DataTable dtSystem = dtAuthen.Select("MENU_NAME = 'System'").CopyToDataTable();
                GridViewHelper.BindGridView(dgvSystem1, dtSystem, false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void cmdCloseAll_Click(object sender, EventArgs e)
        {
            this.ActionAll(false);
        }

        protected void cmdOpenAll_Click(object sender, EventArgs e)
        {
            this.ActionAll(true);
        }

        private void ActionAll(bool IsCheck)
        {
            try
            {
                CheckBox chk;
                for (int i = 0; i < dgvMaster.Rows.Count; i++)
                {
                    chk = (CheckBox)dgvMaster.Rows[i].FindControl("chkVisible");
                    if (chk != null)
                        chk.Checked = IsCheck;

                    chk = (CheckBox)dgvMaster.Rows[i].FindControl("chkRead");
                    if (chk != null)
                        chk.Checked = IsCheck;

                    chk = (CheckBox)dgvMaster.Rows[i].FindControl("chkWrite");
                    if (chk != null)
                        chk.Checked = IsCheck;
                }

                for (int i = 0; i < dgvTransaction.Rows.Count; i++)
                {
                    chk = (CheckBox)dgvTransaction.Rows[i].FindControl("chkVisible");
                    if (chk != null)
                        chk.Checked = IsCheck;

                    chk = (CheckBox)dgvTransaction.Rows[i].FindControl("chkRead");
                    if (chk != null)
                        chk.Checked = IsCheck;

                    chk = (CheckBox)dgvTransaction.Rows[i].FindControl("chkWrite");
                    if (chk != null)
                        chk.Checked = IsCheck;
                }

                for (int i = 0; i < dgvSystem1.Rows.Count; i++)
                {
                    chk = (CheckBox)dgvSystem1.Rows[i].FindControl("chkVisible");
                    if (chk != null)
                        chk.Checked = IsCheck;

                    chk = (CheckBox)dgvSystem1.Rows[i].FindControl("chkRead");
                    if (chk != null)
                        chk.Checked = IsCheck;

                    chk = (CheckBox)dgvSystem1.Rows[i].FindControl("chkWrite");
                    if (chk != null)
                        chk.Checked = IsCheck;
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtSave = new DataTable();
                dtSave.Columns.Add("MENU_ID");
                dtSave.Columns.Add("VISIBLE");
                dtSave.Columns.Add("READ");
                dtSave.Columns.Add("WRITE");

                string MENU_ID_LEVEL1 = string.Empty;
                string MENU_ID_LEVEL2 = string.Empty;

                CheckBox chkVisible, chkRead, chkWrite;

                for (int i = 0; i < dgvMaster.Rows.Count; i++)
                {
                    MENU_ID_LEVEL1 = dgvMaster.DataKeys[i].Values["MENU_ID_LEVEL1"].ToString();
                    MENU_ID_LEVEL2 = dgvMaster.DataKeys[i].Values["MENU_ID_LEVEL2"].ToString();

                    chkVisible = (CheckBox)dgvMaster.Rows[i].FindControl("chkVisible");
                    chkRead = (CheckBox)dgvMaster.Rows[i].FindControl("chkRead");
                    chkWrite = (CheckBox)dgvMaster.Rows[i].FindControl("chkWrite");

                    dtSave.Rows.Add(string.Equals(MENU_ID_LEVEL2, string.Empty) ? MENU_ID_LEVEL1 : MENU_ID_LEVEL2
                                  , chkVisible.Checked ? 1 : 0
                                  , chkRead.Checked ? 1 : 0
                                  , chkWrite.Checked ? 1 : 0);
                }

                for (int i = 0; i < dgvTransaction.Rows.Count; i++)
                {
                    MENU_ID_LEVEL1 = dgvTransaction.DataKeys[i].Values["MENU_ID_LEVEL1"].ToString();
                    MENU_ID_LEVEL2 = dgvTransaction.DataKeys[i].Values["MENU_ID_LEVEL2"].ToString();

                    chkVisible = (CheckBox)dgvTransaction.Rows[i].FindControl("chkVisible");
                    chkRead = (CheckBox)dgvTransaction.Rows[i].FindControl("chkRead");
                    chkWrite = (CheckBox)dgvTransaction.Rows[i].FindControl("chkWrite");

                    dtSave.Rows.Add(string.Equals(MENU_ID_LEVEL2, string.Empty) ? MENU_ID_LEVEL1 : MENU_ID_LEVEL2
                                  , chkVisible.Checked ? 1 : 0
                                  , chkRead.Checked ? 1 : 0
                                  , chkWrite.Checked ? 1 : 0);
                }

                for (int i = 0; i < dgvSystem1.Rows.Count; i++)
                {
                    MENU_ID_LEVEL1 = dgvSystem1.DataKeys[i].Values["MENU_ID_LEVEL1"].ToString();
                    MENU_ID_LEVEL2 = dgvSystem1.DataKeys[i].Values["MENU_ID_LEVEL2"].ToString();

                    chkVisible = (CheckBox)dgvSystem1.Rows[i].FindControl("chkVisible");
                    chkRead = (CheckBox)dgvSystem1.Rows[i].FindControl("chkRead");
                    chkWrite = (CheckBox)dgvSystem1.Rows[i].FindControl("chkWrite");

                    dtSave.Rows.Add(string.Equals(MENU_ID_LEVEL2, string.Empty) ? MENU_ID_LEVEL1 : MENU_ID_LEVEL2
                                  , chkVisible.Checked ? 1 : 0
                                  , chkRead.Checked ? 1 : 0
                                  , chkWrite.Checked ? 1 : 0);
                }

                AuthenBLL.Instance.AuthenInsertBLL(ddlUserGroup.SelectedValue, dtSave, UserID);
                alertSuccess(Properties.Resources.SaveSuccess);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Authenticate.aspx");
        }
    }
}