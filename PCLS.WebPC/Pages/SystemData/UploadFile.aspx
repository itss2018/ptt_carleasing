﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="UploadFile.aspx.cs" Inherits="PCLS.WebPC.Pages.SystemData.UploadFile" Theme="Blue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;Upload File</span></h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>การตั้งค่าและข้อมูล</li>
                    <li class="active">Upload File</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search Option</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUploadType" runat="server" Text="กลุ่มเอกสาร"></asp:Label>
                                    <asp:DropDownList ID="ddlUploadType" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <asp:Button ID="cmdAdd" runat="server" Text="เพิ่ม" SkinID="ButtonSuccess" OnClick="cmdAdd_Click" />
                        <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" OnClick="cmdSearch_Click" />
                        <asp:Button ID="cmdClear" runat="server" Text="ล้างข้อมูล" SkinID="ButtonWarning" OnClick="cmdClear_Click" />
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Show Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body" style="overflow-x:auto">
                        <asp:GridView ID="dgvData" runat="server" OnRowDataBound="dgvData_RowDataBound" OnPageIndexChanging="dgvData_PageIndexChanging" DataKeyNames="UPLOAD_ID">
                            <HeaderStyle Wrap="false" />
                            <RowStyle Wrap="false" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="#" id="aView" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgView.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="#" id="aEdit" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="UPLOAD_ID" HeaderText="" Visible="false"></asp:BoundField>
                                <asp:BoundField DataField="UPLOAD_TYPE_TH" HeaderText="กลุ่มเอกสาร"></asp:BoundField>
                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ไฟล์เอกสาร"></asp:BoundField>
                                <asp:TemplateField HeaderText="Require">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" Checked='<%# Convert.ToString(Eval("REQUIRE")) == "1" ? true : false %>' Enabled="false" />   
                                    </ItemTemplate>   
                                </asp:TemplateField>
                                <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์สูงสุด (MB)"></asp:BoundField>
                                <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุล"></asp:BoundField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" Checked='<%# Convert.ToString(Eval("IS_ACTIVE")) == "1" ? true : false %>' Enabled="false" />   
                                    </ItemTemplate>   
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>