﻿using PCLS.Bussiness.System;
using PCLS.Bussiness.SystemData;
using PCLS.WebPC.Helper;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.SystemData
{
    public partial class UploadFile : PageBase
    {
        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                //Helper.ActiveMenu.SetActiveMenu(Master, "tabMaster", "liUploadFile");

                //this.GetData();
                ////this.AssignAuthen();
                DataTable dt = new DataTable();
                dt.Columns.Add("UPLOAD_ID");
                dt.Columns.Add("UPLOAD_TYPE_TH");
                dt.Columns.Add("UPLOAD_NAME");
                dt.Columns.Add("REQUIRE");
                dt.Columns.Add("MAX_FILE_SIZE");
                dt.Columns.Add("EXTENTION");
                dt.Columns.Add("IS_ACTIVE");

                DataRow row = dt.NewRow();
                row["UPLOAD_ID"] = "1";
                row["UPLOAD_TYPE_TH"] = "เอกสารสัญญา";
                row["UPLOAD_NAME"] = "เอกสารสัญญา";
                row["REQUIRE"] = 1;
                row["MAX_FILE_SIZE"] = 5;
                row["EXTENTION"] = "*.*";
                row["IS_ACTIVE"] = 1;
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["UPLOAD_ID"] = "2";
                row["UPLOAD_TYPE_TH"] = "เอกสารสัญญา";
                row["UPLOAD_NAME"] = "เอกสารอื่นๆ";
                row["REQUIRE"] = 0;
                row["MAX_FILE_SIZE"] = 5;
                row["EXTENTION"] = "*.*";
                row["IS_ACTIVE"] = 1;
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                dtData = UploadTypeBLL.Instance.UploadTypeSelectBLL(this.GetConditionSearch());
                GridViewHelper.BindGridView(dgvData, dtData, false);

                DataTable dt = UploadTypeBLL.Instance.UploadTypeHeaderSelectBLL(string.Empty);
                DropDownListHelper.BindDropDown(ref ddlUploadType, dt, "UPLOAD_TYPE", "UPLOAD_TYPE_TH", true, Properties.Resources.DropDownChooseText);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private string GetConditionSearch()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                if (ddlUploadType.SelectedIndex > 0)
                    sb.Append(" AND UPLOAD_TYPE = '" + ddlUploadType.SelectedValue + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");

                if (aView != null)
                    aView.HRef = "UploadFileAddEdit.aspx?" + "type=" + EncodeQueryString(Mode.View.ToString()) + "&id=" + EncodeQueryString(row["UPLOAD_ID"].ToString());

                if (aEdit != null)
                    aEdit.HRef = "UploadFileAddEdit.aspx?" + "type=" + EncodeQueryString(Mode.Edit.ToString()) + "&id=" + EncodeQueryString(row["UPLOAD_ID"].ToString());
            }
        }

        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                dgvData.PageIndex = e.NewPageIndex;
                GridViewHelper.BindGridView(dgvData, dtData, false);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            FormHelper.OpenForm("UploadFileAddEdit.aspx" + "?type=" + EncodeQueryString(Mode.Add.ToString()) + "&id=" + EncodeQueryString("0"), Page);
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {
                ddlUploadType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
    }
}