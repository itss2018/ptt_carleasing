﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="UploadFileAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.SystemData.UploadFileAddEdit" Theme="Blue" %>

<%@ Register Src="~/UserControl/ConfirmModal.ascx" TagPrefix="uc3" TagName="ModalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;Upload File</span></h1>
                <ol class="breadcrumb">
                    <li><a href="../../BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">Master Data</a></li>
                    <li class="active">Upload File</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Input Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                   
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUploadType" runat="server" Text="กลุ่มเอกสาร"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlUploadType" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUploadName" runat="server" Text="ไฟล์เอกสาร"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtUploadName" runat="server"></asp:TextBox>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblExtention" runat="server" Text="นามสกุล"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtExtention" runat="server" placeholder="*.*"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblMaxFileSize" runat="server" Text="ขนาดไฟล์สูงสุด (MB)"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtMaxFileSize" runat="server" style="text-align:center"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblRequire" runat="server" Text="Require"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:RadioButtonList ID="radRequire" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" SkinID="ButtonSuccess" data-toggle="modal" data-target="#modalConfirmSave" UseSubmitBehavior="false" />
                        <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" OnClick="cmdCancel_Click" />
                    </div>
                </div>
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    <uc3:ModalPopup runat="server" ID="modalConfirmSave" IDModel="modalConfirmSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextDetail="คุณต้องการบันทึกข้อมูล ใช่หรือไม่ ?" ImageName="imgTitleConfirm.png" />
</asp:Content>