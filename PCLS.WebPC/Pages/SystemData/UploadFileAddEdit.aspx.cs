﻿using PCLS.Bussiness.SystemData;
using PCLS.WebPC.Helper;
using System;
using System.Data;
using System.Text;
using System.Web.UI;

namespace PCLS.WebPC.Pages.SystemData
{
    public partial class UploadFileAddEdit : PageBase
    {
        #region + View State +
        private string ActionType
        {
            get
            {
                if ((string)ViewState["ActionType"] != null)
                    return (string)ViewState["ActionType"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionType"] = value;
            }
        }

        private string ActionKey
        {
            get
            {
                if ((string)ViewState["ActionKey"] != null)
                    return (string)ViewState["ActionKey"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionKey"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                    this.InitialForm();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                //Helper.ActiveMenu.SetActiveMenu(Master, "tabMaster", "liUploadFile");

                //DataTable dt = UploadTypeBLL.Instance.UploadTypeHeaderSelectBLL(string.Empty);
                //DropDownListHelper.BindDropDown(ref ddlUploadType, dt, "UPLOAD_TYPE", "UPLOAD_TYPE_TH", true, Properties.Resources.DropDownChooseText);

                //InitialStatus(ddlStatus, " AND IS_ACTIVE = 1 AND STATUS_TYPE = 'ACTIVE_STATUS'");
                //this.CheckQueryString();
                //this.GetData();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                if (string.Equals(ActionType, Mode.Add.ToString()))
                {
                    ddlUploadType.Enabled = true;
                }
                else
                {
                    ddlUploadType.Enabled = false;

                    DataTable dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL(this.GetCondition());
                    if (dtUploadType.Rows.Count > 0)
                    {
                        ddlUploadType.SelectedValue = dtUploadType.Rows[0]["UPLOAD_TYPE"].ToString();
                        txtUploadName.Text = dtUploadType.Rows[0]["UPLOAD_NAME"].ToString();
                        radRequire.SelectedValue = dtUploadType.Rows[0]["REQUIRE"].ToString();
                        txtExtention.Text = dtUploadType.Rows[0]["EXTENTION"].ToString();
                        txtMaxFileSize.Text = dtUploadType.Rows[0]["MAX_FILE_SIZE"].ToString();
                        ddlStatus.SelectedValue = dtUploadType.Rows[0]["IS_ACTIVE"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CheckQueryString()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
                {
                    ActionType = DecodeQueryString(Request.QueryString["type"]);
                    ActionKey = DecodeQueryString(Request.QueryString["id"]);

                    if (string.Equals(ActionType, Mode.View.ToString()))
                    {
                        DisableControls(Page);
                        cmdSave.Visible = false;
                        cmdCancel.Visible = false;
                    }
                }
                else
                    Response.Redirect("../Other/NotAuthorize.aspx");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string GetCondition()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" AND UPLOAD_ID = '" + ActionKey + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.ValidateSave();

                UploadTypeBLL.Instance.UploadTypeAddBLL(ddlUploadType.SelectedValue, txtUploadName.Text.Trim(), txtExtention.Text.Trim(), int.Parse(txtMaxFileSize.Text.Trim()), ddlStatus.SelectedValue, UserID, ddlUploadType.SelectedItem.Text, radRequire.SelectedValue);
                alertSuccess(string.Empty, Properties.Resources.SaveSuccess, "UploadFile.aspx");
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("UploadFile.aspx");
        }

        private void ValidateSave()
        {
            try
            {
                if (ddlUploadType.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblUploadType.Text));

                if (string.Equals(txtUploadName.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblUploadName.Text));

                if (string.Equals(txtExtention.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblExtention.Text));

                if (string.Equals(txtMaxFileSize.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblMaxFileSize.Text));

                int tmp;
                if (!int.TryParse(txtMaxFileSize.Text.Trim(), out tmp))
                    throw new Exception(string.Format(Properties.Resources.NumberOnly, lblMaxFileSize.Text));

                if (ddlStatus.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblStatus.Text));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}