﻿using PCLS.Bussiness.System;
using PCLS.Bussiness.SystemData;
using PCLS.WebPC.Helper;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.SystemData
{
    public partial class User : PageBase
    {
        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                //Helper.ActiveMenu.SetActiveMenu(Master, "tabSystem", "liUser");

                //this.GetUserGroup();
                //this.GetData();
                //InitialStatus(ddlStatus, " AND IS_ACTIVE = 1 AND STATUS_TYPE = 'ACTIVE_STATUS'");
                ////this.AssignAuthen();
                DataTable dt = new DataTable();
                dt.Columns.Add("USER_ID");
                dt.Columns.Add("USERNAME");
                dt.Columns.Add("FULLNAME");
                dt.Columns.Add("USERGROUP_NAME");
                dt.Columns.Add("USER_TYPE_NAME");
                dt.Columns.Add("IS_ACTIVE");
                
                DataRow row = dt.NewRow();
                row["USER_ID"] = "1";
                row["USERNAME"] = "WSP001";
                row["FULLNAME"] = "Demo Data";
                row["USERGROUP_NAME"] = "Administrator";
                row["USER_TYPE_NAME"] = "Web User";
                row["IS_ACTIVE"] = 1;
                dt.Rows.Add(row);
               
                row = dt.NewRow();
                row["USER_ID"] = "2";
                row["USERNAME"] = "WSP002";
                row["FULLNAME"] = "Demo Data2";
                row["USERGROUP_NAME"] = "Administrator";
                row["USER_TYPE_NAME"] = "Web User";
                row["IS_ACTIVE"] = 0;
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetUserGroup()
        {
            try
            {
                DataTable dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(" AND IS_ACTIVE = 1");
                DropDownListHelper.BindDropDown(ref ddlUserGroup, dtUserGroup, "USERGROUP_ID", "USERGROUP_NAME", true, Properties.Resources.DropDownChooseText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                dtData = UserBLL.Instance.UserSelectBLL(this.GetConditionSearch());
                GridViewHelper.BindGridView(dgvData, dtData, false);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private string GetConditionSearch()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                if (ddlUserGroup.SelectedIndex > 0)
                    sb.Append(" AND USERGROUP_ID = '" + ddlUserGroup.SelectedValue + "'");

                if (!string.Equals(txtUserName.Text.Trim(), string.Empty))
                    sb.Append(" AND USERNAME LIKE '%" + txtUserName.Text.Trim() + "%'");

                if (!string.Equals(txtFirstName.Text.Trim(), string.Empty))
                    sb.Append(" AND FIRSTNAME LIKE '%" + txtFirstName.Text.Trim() + "%'");

                if (!string.Equals(txtLastName.Text.Trim(), string.Empty))
                    sb.Append(" AND LASTNAME LIKE '%" + txtLastName.Text.Trim() + "%'");

                if (ddlStatus.SelectedIndex > 0)
                    sb.Append(" AND IS_ACTIVE = '" + ddlStatus.SelectedValue + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");

                if (aView != null)
                    aView.HRef = "UserAddEdit.aspx?" + "type=" + EncodeQueryString(Mode.View.ToString()) + "&id=" + EncodeQueryString(row["USER_ID"].ToString());

                if (aEdit != null)
                    aEdit.HRef = "UserAddEdit.aspx?" + "type=" + EncodeQueryString(Mode.Edit.ToString()) + "&id=" + EncodeQueryString(row["USER_ID"].ToString());
            }
        }

        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                dgvData.PageIndex = e.NewPageIndex;
                GridViewHelper.BindGridView(dgvData, dtData, false);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            FormHelper.OpenForm("UserAddEdit.aspx" + "?type=" + EncodeQueryString(Mode.Add.ToString()) + "&id=" + EncodeQueryString("0"), Page);
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {
                ddlUserGroup.SelectedIndex = 0;
                txtUserName.Text = string.Empty;
                txtFirstName.Text = string.Empty;
                txtLastName.Text = string.Empty;
                ddlStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
    }
}