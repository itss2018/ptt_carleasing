﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="UserAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.SystemData.UserAddEdit" Theme="Blue" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/UserControl/ConfirmModal.ascx" TagPrefix="uc3" TagName="ModalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:2000px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;User</span></h1>
                <ol class="breadcrumb">
                    <li><a href="../../BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">System</a></li>
                    <li class="active">User</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Input Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                   
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="updFileUploadUser" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Table runat="server" Width="100%">
                                            <asp:TableRow HorizontalAlign="Center">
                                                <asp:TableCell ColumnSpan="2" style="text-align:center">
                                                    <asp:Image ID="imgProfile" runat="server" Width="256px" Height="256px" CssClass="img-circle" />
                                                </asp:TableCell>
                                                <asp:TableCell></asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow><asp:TableCell>&nbsp;</asp:TableCell></asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell style="float:right; padding-top:8px">
                                                    <asp:FileUpload ID="fileUploadUser" runat="server" />
                                                </asp:TableCell>
                                                <asp:TableCell style="text-align:left">
                                                    <asp:Button ID="cmdUploadUser" runat="server" Text="Upload" Width="80px" CssClass="btn btn-info" OnClick="cmdUploadUser_Click" />
                                                    &nbsp;<asp:Button ID="cmdUploadUserClear" runat="server" Text="Clear" OnClick="cmdUploadUserClear_Click" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                        <br />
                                        <br />
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="cmdUploadUser" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUserType" runat="server" Text="ประเภทผู้ใช้งาน"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUserName" runat="server" Text="Username"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                                    <asp:Button ID="cmdPIS" runat="server" Text="ดึงข้อมูลจาก PIS" SkinID="null" CssClass="btn btn-info" Width="130px" OnClick="cmdPIS_Click" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUserGroup" runat="server" Text="กลุ่มผู้ใช้งาน"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlUserGroup" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlUserGroup_SelectedIndexChanged"></asp:DropDownList>
                                    <asp:DropDownList ID="ddlVendor" runat="server" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblPassword" runat="server" Text="รหัสผ่าน"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblPasswordAgain" runat="server" Text="รหัสผ่าน (อีกครั้ง)"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtPasswordAgain" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblTitle" runat="server" Text="คำนำหน้า"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlTitle" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblBirthDay" runat="server" Text="วันเกิด"></asp:Label>
                                    <asp:TextBox ID="txtBirthDay" runat="server" Width="100%" SkinID="null" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblFirstName" runat="server" Text="ชื่อ"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblLastName" runat="server" Text="นามสกุล"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblAddress" runat="server" Text="ที่อยู่"></asp:Label>
                                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblSubDistrict" runat="server" Text="แขวง/ตำบล"></asp:Label>
                                    <asp:TextBox ID="txtSubDistrict" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblDistrict" runat="server" Text="เขต/อำเภอ"></asp:Label>
                                    <asp:TextBox ID="txtDistrict" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblProvince" runat="server" Text="จังหวัด"></asp:Label>
                                    <asp:TextBox ID="txtProvince" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblZipCode" runat="server" Text="รหัสไปรษณีย์"></asp:Label>
                                    <asp:TextBox ID="txtZipCode" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblCountry" runat="server" Text="ประเทศ"></asp:Label>
                                    <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblTelephone" runat="server" Text="เบอร์โทรศัพท์"></asp:Label>
                                    <asp:TextBox ID="txtTelephone" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" SkinID="ButtonSuccess" data-toggle="modal" data-target="#modalConfirmSave" UseSubmitBehavior="false" />
                        <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" OnClick="cmdCancel_Click" />
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">File Upload (Require)</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                   
                    <div class="box-body">
                        <asp:GridView ID="dgvUploadRequestFile" runat="server">
                            <Columns>
                                <asp:TemplateField HeaderText="แนบเอกสาร">
                                    <HeaderStyle Width="110px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkUploaded" runat="server" Enabled="false" />   
                                    </ItemTemplate>   
                                </asp:TemplateField>
                                <asp:BoundField DataField="UPLOAD_ID" Visible="false"></asp:BoundField>
                                <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร"></asp:BoundField>
                                <asp:BoundField DataField="EXTENTION" HeaderText="นามสกุลไฟล์ที่รองรับ"></asp:BoundField>
                                <asp:BoundField DataField="MAX_FILE_SIZE" HeaderText="ขนาดไฟล์ (ไม่เกิน) MB"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    
                    <div class="box-footer">
                        &nbsp;
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">File Upload</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="box-body">
                                <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUploadType" runat="server" Text="ประเภทไฟล์เอกสาร"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlUploadType" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblChooseFile" runat="server" Text="เลือกไฟล์"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:FileUpload ID="fileUpload" runat="server" />
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:GridView ID="dgvUploadFile" runat="server" DataKeyNames="UPLOAD_ID,FULLPATH"
                                        OnRowDeleting="dgvUploadFile_RowDeleting"
                                        OnRowUpdating="dgvUploadFile_RowUpdating">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <HeaderStyle Width="50px" />
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="">
                                                <HeaderStyle Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/Button/imgDownload.png" Width="24px" Height="24px" CommandName="Update" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <HeaderStyle Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" CommandName="Delete" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="UPLOAD_ID" Visible="false"></asp:BoundField>
                                            <asp:BoundField DataField="UPLOAD_NAME" HeaderText="ประเภทไฟล์เอกสาร"></asp:BoundField>
                                            <asp:BoundField DataField="FILENAME_SYSTEM" HeaderText="ชื่อไฟล์ (ในระบบ)"></asp:BoundField>
                                            <asp:BoundField DataField="FILENAME_USER" HeaderText="ชื่อไฟล์ (ตามผู้ใช้งาน)"></asp:BoundField>
                                            <asp:BoundField DataField="FULLPATH" Visible="false"></asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                    
                            <div class="box-footer">
                                <asp:Button ID="cmdUpload" runat="server" Text="Add" OnClick="cmdUpload_Click" />
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="cmdUpload" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    <uc3:ModalPopup runat="server" ID="modalConfirmSave" IDModel="modalConfirmSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextDetail="คุณต้องการบันทึกข้อมูล ใช่หรือไม่ ?" ImageName="imgTitleConfirm.png" />
</asp:Content>