﻿using PCLS.Bussiness.Master;
using PCLS.Bussiness.System;
using PCLS.Bussiness.SystemData;
using PCLS.WebPC.Helper;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using PCLS.Bussiness.PIS;

namespace PCLS.WebPC.Pages.SystemData
{
    public partial class UserAddEdit : PageBase
    {
        #region + View State +
        private string ActionType
        {
            get
            {
                if ((string)ViewState["ActionType"] != null)
                    return (string)ViewState["ActionType"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionType"] = value;
            }
        }

        private string ActionKey
        {
            get
            {
                if ((string)ViewState["ActionKey"] != null)
                    return (string)ViewState["ActionKey"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionKey"] = value;
            }
        }

        private DataTable dtUserType
        {
            get
            {
                if ((DataTable)ViewState["dtUserType"] != null)
                    return (DataTable)ViewState["dtUserType"];
                else
                    return null;
            }
            set
            {
                ViewState["dtUserType"] = value;
            }
        }

        private DataTable dtUserGroup
        {
            get
            {
                if ((DataTable)ViewState["dtUserGroup"] != null)
                    return (DataTable)ViewState["dtUserGroup"];
                else
                    return null;
            }
            set
            {
                ViewState["dtUserGroup"] = value;
            }
        }

        private string ImageProfilePath
        {
            get
            {
                if ((string)ViewState["ImageProfilePath"] != null)
                    return (string)ViewState["ImageProfilePath"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ImageProfilePath"] = value;
            }
        }

        private string Password
        {
            get
            {
                if ((string)ViewState["Password"] != null)
                    return (string)ViewState["Password"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["Password"] = value;
            }
        }

        private string PathUpload
        {
            get
            {
                if ((string)ViewState["PathUpload"] != null)
                    return (string)ViewState["PathUpload"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["PathUpload"] = value;
            }
        }

        private DataTable dtUploadType
        {
            get
            {
                if ((DataTable)ViewState["dtUploadType"] != null)
                    return (DataTable)ViewState["dtUploadType"];
                else
                    return null;
            }
            set
            {
                ViewState["dtUploadType"] = value;
            }
        }

        private DataTable dtUpload
        {
            get
            {
                if ((DataTable)ViewState["dtUpload"] != null)
                    return (DataTable)ViewState["dtUpload"];
                else
                    return null;
            }
            set
            {
                ViewState["dtUpload"] = value;
            }
        }

        private DataTable dtUploadRequestFile
        {
            get
            {
                if ((DataTable)ViewState["dtUploadRequestFile"] != null)
                    return (DataTable)ViewState["dtUploadRequestFile"];
                else
                    return null;
            }
            set
            {
                ViewState["dtUploadRequestFile"] = value;
            }
        }

        private string UploadType
        {
            get
            {
                if ((string)ViewState["UploadType"] != null)
                    return (string)ViewState["UploadType"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["UploadType"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtPassword.Attributes["value"] = txtPassword.Text;
                txtPasswordAgain.Attributes["value"] = txtPasswordAgain.Text;

                if (!Page.IsPostBack)
                    this.InitialForm();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                //Helper.ActiveMenu.SetActiveMenu(Master, "tabSystem", "liUser");

                //PathUpload = Server.MapPath("~") + "\\" + "UploadFile" + "\\" + "User";

                //this.CheckQueryString();
                //this.GetUserType();
                //this.GetUserGroup();
                //this.GetTitle();
                //this.GetVendor();

                //this.GetUpload();

                //InitialStatus(ddlStatus, " AND IS_ACTIVE = 1 AND STATUS_TYPE = 'ACTIVE_STATUS'");
                //cmdUploadUserClear_Click(null, null);

                //ImageProfilePath = string.Empty;

                //this.GetData();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetUpload()
        {
            try
            {
                UploadType = "USER_LOGIN";
                this.GetUploadType();
                this.GetUploadRequire();
                dtUpload = UploadBLL.Instance.UploadSelectBLL(ActionKey, UploadType, " AND IS_ACTIVE = 1");
                GridViewHelper.BindGridView(dgvUploadFile, dtUpload, false);
                FileUploadHelper.CheckRequestFile(dgvUploadRequestFile, dtUploadRequestFile, dtUpload);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        private void GetUploadType()
        {
            try
            {
                dtUploadType = UploadTypeBLL.Instance.UploadTypeSelectBLL(" AND UPLOAD_TYPE = '" + UploadType + "'" + " AND IS_ACTIVE = 1");
                DropDownListHelper.BindDropDown(ref ddlUploadType, dtUploadType, "UPLOAD_ID", "UPLOAD_NAME", true, Properties.Resources.DropDownChooseText);

                dtUpload = new DataTable();
                dtUpload.Columns.Add("UPLOAD_ID");
                dtUpload.Columns.Add("UPLOAD_NAME");
                dtUpload.Columns.Add("FILENAME_SYSTEM");
                dtUpload.Columns.Add("FILENAME_USER");
                dtUpload.Columns.Add("FULLPATH");

                GridViewHelper.BindGridView(dgvUploadFile, dtUpload, false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetUploadRequire()
        {
            try
            {
                //dtUploadRequestFile = UploadBLL.Instance.UploadRequestFileBLL(" AND M_REQUEST_FILE.UPLOAD_TYPE = '" + UploadType + "'");
                dtUploadRequestFile = UploadTypeBLL.Instance.UploadTypeSelectBLL(" AND REQUIRE = 1 AND UPLOAD_TYPE = '" + UploadType + "'");
                GridViewHelper.BindGridView(dgvUploadRequestFile, dtUploadRequestFile, false);

                ImageButton imgDownload = new ImageButton();
                for (int i = 0; i < dtUploadRequestFile.Rows.Count; i++)
                {
                    if (!string.Equals(dtUploadRequestFile.Rows[i]["DOWNLOAD_FORM"].ToString(), string.Empty))
                    {
                        imgDownload = (ImageButton)dgvUploadRequestFile.Rows[i].FindControl("imgDownload");
                        if (imgDownload != null)
                            imgDownload.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetUserType()
        {
            try
            {
                dtUserType = UserBLL.Instance.UserTypeSelectBLL(" AND IS_ACTIVE = 1");
                DropDownListHelper.BindDropDown(ref ddlUserType, dtUserType, "USER_TYPE_ID", "USER_TYPE_NAME", true, Properties.Resources.DropDownChooseText);

                if (dtUserType.Rows.Count == 2)
                    ddlUserType.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetUserGroup()
        {
            try
            {
                dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(" AND IS_ACTIVE = 1");
                DropDownListHelper.BindDropDown(ref ddlUserGroup, dtUserGroup, "USERGROUP_ID", "USERGROUP_NAME", true, Properties.Resources.DropDownChooseText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetTitle()
        {
            try
            {
                DataTable dtTitle = TitleBLL.Instance.TitleSelectBLL(" AND IS_ACTIVE = 1");
                DropDownListHelper.BindDropDown(ref ddlTitle, dtTitle, "TITLE_ID", "TITLE_NAME", true, Properties.Resources.DropDownChooseText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetVendor()
        {
            try
            {
                DataTable dtVendor = VendorBLL.Instance.VendorSelectBLL("%", "%", "%", "1");
                DropDownListHelper.BindDropDown(ref ddlVendor, dtVendor, "VENDOR_ID", "VENDOR_NAME", true, Properties.Resources.DropDownChooseText);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                if (string.Equals(ActionType, Mode.Add.ToString()))
                {
                    txtUserName.Enabled = true;
                }
                else
                {
                    txtUserName.Enabled = false;

                    DataTable dtUser = UserBLL.Instance.UserSelectBLL(this.GetCondition());
                    if (dtUser.Rows.Count > 0)
                    {
                        ddlUserType.SelectedValue = dtUser.Rows[0]["USER_TYPE_ID"].ToString();
                        if (string.Equals(dtUserType.Rows[ddlUserType.SelectedIndex]["REQUIRE_PASSWORD"].ToString(), "0"))
                            this.EnablePassword(false);
                        else
                            this.EnablePassword(true);

                        ddlUserGroup.SelectedValue = dtUser.Rows[0]["USERGROUP_ID"].ToString();
                        ddlUserGroup_SelectedIndexChanged(null, null);

                        txtUserName.Text = dtUser.Rows[0]["USERNAME"].ToString();
                        if (txtPassword.Enabled)
                        {
                            txtPassword.Attributes["value"] = "********";
                            txtPasswordAgain.Attributes["value"] = "********";
                        }
                        ddlTitle.SelectedValue = dtUser.Rows[0]["TITLE_ID"].ToString();
                        txtBirthDay.Text = dtUser.Rows[0]["DATE_OF_BIRTH_PREVIEW"].ToString();
                        txtFirstName.Text = dtUser.Rows[0]["FIRSTNAME"].ToString();
                        txtLastName.Text = dtUser.Rows[0]["LASTNAME"].ToString();
                        txtAddress.Text = dtUser.Rows[0]["ADDRESS"].ToString();
                        txtSubDistrict.Text = dtUser.Rows[0]["SUB_DISTRICT"].ToString();
                        txtDistrict.Text = dtUser.Rows[0]["DISTRICT"].ToString();
                        txtProvince.Text = dtUser.Rows[0]["PROVINCE"].ToString();
                        txtZipCode.Text = dtUser.Rows[0]["ZIPCODE"].ToString();
                        txtCountry.Text = dtUser.Rows[0]["COUNTRY"].ToString();
                        txtTelephone.Text = dtUser.Rows[0]["TELEPHONE"].ToString();
                        txtEmail.Text = dtUser.Rows[0]["EMAIL"].ToString();
                        ddlStatus.SelectedValue = dtUser.Rows[0]["IS_ACTIVE"].ToString();

                        Password = dtUser.Rows[0]["PASSWORD"].ToString();

                        ImageProfilePath = dtUser.Rows[0]["IMAGE_PATH"].ToString();
                        if (!string.Equals(ImageProfilePath, string.Empty))
                            imgProfile.ImageUrl = ImageProfilePath;
                        else
                            imgProfile.ImageUrl = "~/Images/UserProfile/imgProfileDefault.png";
                        updFileUploadUser.Update();

                        if (!string.Equals(dtUser.Rows[0]["VENDOR_ID"].ToString(), string.Empty))
                        {
                            ddlVendor.Enabled = true;
                            ddlVendor.SelectedValue = dtUser.Rows[0]["VENDOR_ID"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void EnablePassword(bool Enable)
        {
            try
            {
                txtPassword.Text = string.Empty;
                txtPasswordAgain.Text = string.Empty;

                txtPassword.Enabled = Enable;
                txtPasswordAgain.Enabled = Enable;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CheckQueryString()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
                {
                    ActionType = DecodeQueryString(Request.QueryString["type"]);
                    ActionKey = DecodeQueryString(Request.QueryString["id"]);

                    if (string.Equals(ActionType, Mode.View.ToString()))
                    {
                        DisableControls(Page);
                        cmdSave.Visible = false;
                        cmdCancel.Visible = false;
                    }
                }
                else
                    Response.Redirect("../Other/NotAuthorize.aspx");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string GetCondition()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" AND USER_ID = '" + ActionKey + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.ValidateSave();

                string DateOfBirth = string.Empty;
                if (!string.Equals(txtBirthDay.Text.Trim(), string.Empty))
                    DateOfBirth = txtBirthDay.Text.Trim();

                if (!string.Equals(txtPassword.Text.Trim(), "********"))
                {
                    if (txtPassword.Enabled)
                    {
                        if (!string.Equals(Password, MD5Helper.GetMD5Hash(txtPassword.Text.Trim())))
                            Password = MD5Helper.GetMD5Hash(txtPassword.Text.Trim());
                    }
                    else
                        Password = string.Empty;
                }

                int VendorID = 0;
                if (ddlVendor.SelectedIndex > 0)
                    VendorID = int.Parse(ddlVendor.SelectedValue);

                UserBLL.Instance.UserAddBLL(int.Parse(ActionKey), txtUserName.Text.Trim(), Password, DateOfBirth, int.Parse(ddlTitle.SelectedValue), txtFirstName.Text.Trim()
                                          , txtLastName.Text.Trim(), txtAddress.Text.Trim(), txtSubDistrict.Text.Trim(), txtDistrict.Text.Trim(), txtProvince.Text.Trim(), txtZipCode.Text.Trim()
                                          , txtCountry.Text.Trim(), txtTelephone.Text.Trim(), txtEmail.Text.Trim(), int.Parse(ddlUserGroup.SelectedValue)
                                          , int.Parse(ddlStatus.SelectedValue), UserID, ImageProfilePath, int.Parse(ddlUserType.SelectedValue), dtUpload, VendorID);
                alertSuccess(string.Empty, Properties.Resources.SaveSuccess, "User.aspx");
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("User.aspx");
        }

        private void ValidateSave()
        {
            try
            {
                if (ddlUserType.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblUserType.Text));

                if (ddlUserGroup.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblUserGroup.Text));

                if (string.Equals(txtUserName.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblUserName.Text));

                if (txtPassword.Enabled)
                {
                    if (string.Equals(txtPassword.Text.Trim(), string.Empty))
                        throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblPassword.Text));

                    if (string.Equals(txtPasswordAgain.Text.Trim(), string.Empty))
                        throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblPasswordAgain.Text));

                    if (!string.Equals(txtPassword.Text.Trim(), txtPasswordAgain.Text.Trim()))
                        throw new Exception(string.Format(Properties.Resources.TextBoxRequire, "Password ไม่ถูกต้องตรงกัน"));
                }

                if (ddlTitle.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblTitle.Text));

                if (string.Equals(txtFirstName.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblFirstName.Text));

                if (string.Equals(txtLastName.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblLastName.Text));

                if (ddlStatus.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblStatus.Text));

                if (string.Equals(txtEmail.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblEmail.Text));

                if (ddlVendor.Enabled && ddlVendor.SelectedIndex < 1)
                {
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, "ผู้ประกอบการขนส่ง"));
                }

                CheckBox chk = new CheckBox();
                for (int i = 0; i < dgvUploadRequestFile.Rows.Count; i++)
                {
                    chk = (CheckBox)dgvUploadRequestFile.Rows[i].FindControl("chkUploaded");
                    if (chk != null)
                    {
                        if (!chk.Checked)
                            throw new Exception(Properties.Resources.UploadFileRequest);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(dtUserType.Rows[ddlUserType.SelectedIndex]["REQUIRE_PASSWORD"].ToString(), "0"))
                    this.EnablePassword(false);
                else
                    this.EnablePassword(true);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdUploadUser_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileUploadUser.HasFile)
                {
                    string FileNameUser = fileUploadUser.PostedFile.FileName;
                    string FileNameSystem = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss-fff") + System.IO.Path.GetExtension(FileNameUser);

                    string Path = this.CheckPath();
                    fileUploadUser.SaveAs(Path + "\\" + FileNameSystem);

                    imgProfile.ImageUrl = "~/Images/UserProfile/" + FileNameSystem;

                    updFileUploadUser.Update();
                }
                else
                {
                    imgProfile.ImageUrl = "~/Images/UserProfile/" + "imgProfileDefault.png";

                    updFileUploadUser.Update();
                }

                ImageProfilePath = imgProfile.ImageUrl;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private string CheckPath()
        {
            try
            {
                string PathExport = Server.MapPath("~") + "\\" + "Images" + "\\" + "UserProfile";
                if (!Directory.Exists(PathExport))
                    Directory.CreateDirectory(PathExport);

                return PathExport;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void cmdUploadUserClear_Click(object sender, EventArgs e)
        {
            try
            {
                ImageProfilePath = string.Empty;
                imgProfile.ImageUrl = "~/Images/UserProfile/imgProfileDefault.png";
                updFileUploadUser.Update();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdUpload_Click(object sender, EventArgs e)
        {
            try
            {
                FileUploadHelper.StartUpload(PathUpload, fileUpload, dtUploadType, ddlUploadType, dtUpload, dgvUploadFile);
                FileUploadHelper.CheckRequestFile(dgvUploadRequestFile, dtUploadRequestFile, dtUpload);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void dgvUploadFile_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            try
            {
                dtUpload.Rows.RemoveAt(e.RowIndex);
                GridViewHelper.BindGridView(dgvUploadFile, dtUpload, false);

                FileUploadHelper.CheckRequestFile(dgvUploadRequestFile, dtUploadRequestFile, dtUpload);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void dgvUploadFile_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
        {
            string FullPath = dgvUploadFile.DataKeys[e.RowIndex]["FULLPATH"].ToString();

            Session["FILE_DOWNLOAD"] = FullPath;
            FormHelper.OpenForm("../Other/DownloadFile.aspx", Page);
        }

        protected void cmdPIS_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.Equals(txtUserName.Text.Trim(), string.Empty))
                {
                    DataTable dtPIS = new DataTable();
                    dtPIS = PISBLL.Instance.PisSelectBLL(" AND CODE = '" + txtUserName.Text.Trim() + "'");
                    if (dtPIS.Rows.Count == 0)
                        throw new Exception("ไม่พบข้อมูล รหัสพนักงาน " + txtUserName.Text.Trim() + " ในระบบ PIS");

                    this.FindText(ddlTitle, dtPIS.Rows[0]["INAME"].ToString());
                    txtFirstName.Text = dtPIS.Rows[0]["FNAME"].ToString();
                    txtLastName.Text = dtPIS.Rows[0]["LNAME"].ToString();
                    txtEmail.Text = dtPIS.Rows[0]["EMAIL"].ToString();
                    txtTelephone.Text = dtPIS.Rows[0]["HOMETEL"].ToString();
                }
                else
                {
                    ddlTitle.SelectedIndex = 0;
                    txtFirstName.Text = string.Empty;
                    txtLastName.Text = string.Empty;
                    txtEmail.Text = string.Empty;
                    txtTelephone.Text = string.Empty;

                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, txtUserName.Text));
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void FindText(DropDownList ddl, string Text)
        {
            try
            {
                for (int i = 0; i < ddl.Items.Count; i++)
                {
                    if (string.Equals(ddl.Items[i].Text, Text))
                    {
                        ddl.SelectedIndex = i;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void ddlUserGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (string.Equals(dtUserGroup.Rows[ddlUserGroup.SelectedIndex]["IS_ADMIN"].ToString(), "3"))
                {
                    ddlVendor.Enabled = true;
                }
                else
                {
                    ddlVendor.SelectedIndex = 0;
                    ddlVendor.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
    }
}