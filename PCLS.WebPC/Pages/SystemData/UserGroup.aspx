﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="UserGroup.aspx.cs" Inherits="PCLS.WebPC.Pages.SystemData.UserGroup" Theme="Blue" %>

<%--<%@ Register Src="~/UserControl/ConfirmModal.ascx" TagPrefix="uc3" TagName="ModalPopup" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/css/footable.min.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/0.1.0/js/footable.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=GridView1]').footable();
        });
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;User Group</span>
                    <%--<small>(กลุ่มผู้ใช้งาน)</small>--%>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>System</li>
                    <li class="active">User Group</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search Option</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <%--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--%>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUserGroup" runat="server" Text="กลุ่มผู้ใช้งาน"></asp:Label>
                                    <asp:TextBox ID="txtUserGroup" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ"></asp:Label>
                                    <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <asp:Button ID="cmdAdd" runat="server" Text="เพิ่ม" SkinID="ButtonSuccess" OnClick="cmdAdd_Click" />
                        <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" OnClick="cmdSearch_Click" />
                        <asp:Button ID="cmdClear" runat="server" Text="ล้างข้อมูล" SkinID="ButtonWarning" OnClick="cmdClear_Click" />
                        <%--<input id="checkbox_id" runat="server" type="checkbox" name="checkbox" value="value" class="minimal" />
                        <label for="checkbox_id">Text</label>--%>
                        <%--<br />
                        <input id="input-3" class="minimal" type="radio" name="demo-radio" />--%>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Show Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <%--<div class="box-body">
                        <asp:GridView ID="GridView1" CssClass="table table-bordered table-striped table-hover" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="2" style="width:100%">
                            <Columns>
                                <asp:BoundField DataField="Id" HeaderText="Id" />
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Company" HeaderText="Company" />
                                <asp:BoundField DataField="Age" HeaderText="Age"  />
                            </Columns>
                        </asp:GridView>
                    </div>--%>

                    <div class="box-body" style="overflow-x:auto">
                        <asp:GridView ID="dgvData" runat="server" OnRowDataBound="dgvData_RowDataBound" OnPageIndexChanging="dgvData_PageIndexChanging" DataKeyNames="USERGROUP_ID">
                            <HeaderStyle Wrap="false" />
                            <RowStyle Wrap="false" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="#" id="aView" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgView.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="#" id="aEdit" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="USERGROUP_ID" HeaderText="" Visible="false"></asp:BoundField>
                                <asp:BoundField DataField="USERGROUP_NAME" HeaderText="กลุ่มผู้ใช้งาน"></asp:BoundField>
                                <asp:BoundField DataField="IS_ADMIN_TEXT" HeaderText="ประเภท"></asp:BoundField>
                                <asp:TemplateField HeaderText="สถานะ">
                                    <ItemStyle HorizontalAlign="Center" Width="100px" />
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" Checked='<%# Convert.ToString(Eval("IS_ACTIVE")) == "1" ? true : false %>' Enabled="false" />   
                                    </ItemTemplate>   
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                    <%--<div class="box-body">
                        <table id="dgvData" class="table table-bordered table-striped table-hover" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="all">Rendering engine</th>
                                    <th class="in-phone-l">Browser</th>
                                    <th class="in-phone-l">Platform(s)</th>
                                    <th class="in-phone-l">Engine version</th>
                                    <th class="in-phone-l" style="width:24px;"></th>
                                    <th class="in-phone-l" style="width:24px;"></th>
                                    <th class="none">CSS grade</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Trident 1</td>
                                    <td>Internet Explorer 4.0</td>
                                    <td>Win 95+</td>
                                    <td>4</td>
                                    <td style="text-align:center"><asp:ImageButton ID="imgView" runat="server" ImageUrl="~/Images/Button/imgView.png" Width="24px" Height="24px" Style="cursor: pointer" /></td>
                                    <td style="text-align:center"><asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" Style="cursor: pointer" /></td>
                                    <td>X</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="all">Rendering engine</th>
                                    <th class="in-phone-l">Browser</th>
                                    <th class="in-phone-l">Platform(s)</th>
                                    <th class="in-phone-l">Engine version</th>
                                    <th class="in-phone-l" style="width:24px;"></th>
                                    <th class="in-phone-l" style="width:24px;"></th>
                                    <th class="none">CSS grade</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>--%>
                </div>
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    <%--<uc3:ModalPopup runat="server" ID="modalConfirmSave" IDModel="modalConfirmSave"
                IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdClear_Click"
                TextDetail="คุณต้องการบันทึกข้อมูล ใช่หรือไม่ ?" ImageName="imgTitleConfirm.png" />--%>
</asp:Content>