﻿using PCLS.Bussiness.System;
using PCLS.WebPC.Helper;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.SystemData
{
    public partial class UserGroup : PageBase
    {
        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void MakeData()
        {
            //StringBuilder sbHead = new StringBuilder();
            //sbHead.Append("<th class=\"all\">Rendering engine</th>");
            //sbHead.Append("<th class=\"in-phone-l\">Browser</th>");
            //sbHead.Append("<th class=\"in-phone-l\">Platform(s)</th>");
            //sbHead.Append("<th class=\"in-phone-l\">Engine version</th>");
            //sbHead.Append("<th class=\"none\">CSS grade</th>");
            //sbHead.Append("<th class=\"in-phone-l\" style=\"width:24px;\"></th>");
            //sbHead.Append("<th class=\"in-phone-l\" style=\"width:24px;\"></th>");
            
            //ltHead.Text = sbHead.ToString();
            //ltFooter.Text = sbHead.ToString();

            //StringBuilder sbDetail = new StringBuilder();
            //sbDetail.Append("<tr>");
            //sbDetail.Append("<td>Trident 1</td>");
            //sbDetail.Append("<td>Internet Explorer 4.0</td>");
            //sbDetail.Append("<td>Win 95+</td>");
            //sbDetail.Append("<td>4</td>");
            //sbDetail.Append("<td>X</td>");
            //sbDetail.Append("<td style=\"text-align:center\"><img id=\"imgView\" runat=\"server\" src=\"../../Images/Button/imgView.png\" width=\"24px\" height=\"24px\" alt=\"View\" style=\"cursor:pointer\" /></td>");
            //sbDetail.Append("<td style=\"text-align:center\"><img id=\"imgEdit\" runat=\"server\" src=\"../../Images/Button/imgEdit.png\" width=\"24px\" height=\"24px\" alt=\"View\" style=\"cursor:pointer\" /></td>");
            //sbDetail.Append("</tr>");
            //ltDetail.Text += sbDetail.ToString();

            //sbDetail.Remove(0, sbDetail.Length);
            //sbDetail.Append("<tr>");
            //sbDetail.Append("<td>Trident 2</td>");
            //sbDetail.Append("<td>Internet Explorer 4.0</td>");
            //sbDetail.Append("<td>Win 95+</td>");
            //sbDetail.Append("<td>4</td>");
            //sbDetail.Append("<td>X</td>");
            //sbDetail.Append("<td style=\"text-align:center\"><img id=\"imgView\" runat=\"server\" src=\"../../Images/Button/imgView.png\" width=\"24px\" height=\"24px\" alt=\"View\" style=\"cursor:pointer\" /></td>");
            //sbDetail.Append("<td style=\"text-align:center\"><img id=\"imgEdit\" runat=\"server\" src=\"../../Images/Button/imgEdit.png\" width=\"24px\" height=\"24px\" alt=\"View\" style=\"cursor:pointer\" /></td>");
            //sbDetail.Append("</tr>");
            //ltDetail.Text += sbDetail.ToString();
        }

        private void InitialForm()
        {
            try
            {
                //Helper.ActiveMenu.SetActiveMenu(Master, "tabSystem", "liUserGroup");

                //this.GetData();
                //InitialStatus(ddlStatus, " AND IS_ACTIVE = 1 AND STATUS_TYPE = 'ACTIVE_STATUS'");
                ////this.AssignAuthen();
                DataTable dt = new DataTable();
                dt.Columns.Add("USERGROUP_ID");
                dt.Columns.Add("USERGROUP_NAME");
                dt.Columns.Add("IS_ADMIN_TEXT");
                dt.Columns.Add("IS_ACTIVE");

                DataRow row = dt.NewRow();
                row["USERGROUP_ID"] = "1";
                row["USERGROUP_NAME"] = "Administrator";
                row["IS_ADMIN_TEXT"] = "ผู้ดูแลระบบ";
                row["IS_ACTIVE"] = 1;
                dt.Rows.Add(row);

                row = dt.NewRow();
                row["USERGROUP_ID"] = "2";
                row["USERGROUP_NAME"] = "PTT USER";
                row["IS_ADMIN_TEXT"] = "พนักงาน ปตท.";
                row["IS_ACTIVE"] = 0;
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                dtData = UserGroupBLL.Instance.UserGroupSelectBLL(this.GetConditionSearch());
                GridViewHelper.BindGridView(dgvData, dtData, false);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private string GetConditionSearch()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                if (!string.Equals(txtUserGroup.Text.Trim(), string.Empty))
                    sb.Append(" AND USERGROUP_NAME LIKE '%" + txtUserGroup.Text.Trim() + "%'");

                if (ddlStatus.SelectedIndex > 0)
                    sb.Append(" AND IS_ACTIVE = " + ddlStatus.SelectedValue);

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");

                if (aView != null)
                    aView.HRef = "UserGroupAddEdit.aspx?" + "type=" + EncodeQueryString(Mode.View.ToString()) + "&id=" + EncodeQueryString(row["USERGROUP_ID"].ToString());

                if (aEdit != null)
                    aEdit.HRef = "UserGroupAddEdit.aspx?" + "type=" + EncodeQueryString(Mode.Edit.ToString()) + "&id=" + EncodeQueryString(row["USERGROUP_ID"].ToString());
            }
        }

        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                dgvData.PageIndex = e.NewPageIndex;
                GridViewHelper.BindGridView(dgvData, dtData, false);
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {
            FormHelper.OpenForm("UserGroupAddEdit.aspx" + "?type=" + EncodeQueryString(Mode.Add.ToString()) + "&id=" + EncodeQueryString("0"), Page);
        }

        protected void cmdSearch_Click(object sender, EventArgs e)
        {
            this.GetData();
        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtUserGroup.Text = string.Empty;
                ddlStatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }
    }
}