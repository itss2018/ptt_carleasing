﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="UserGroupAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.SystemData.UserGroupAddEdit" Theme="Blue" %>

<%@ Register Src="~/UserControl/ConfirmModal.ascx" TagPrefix="uc3" TagName="ModalPopup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;User Group</span></h1>
                <ol class="breadcrumb">
                    <li><a href="../../BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li><a href="#">System</a></li>
                    <li class="active">User Group</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Input Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                   
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblUserGroup" runat="server" Text="กลุ่มผู้ใช้งาน"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:TextBox ID="txtUserGroupName" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="สถานะ"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="lblStatusSpecial" runat="server" Text="ประเภท"></asp:Label>
                                    <b style="color:red">*</b>
                                    <asp:RadioButtonList ID="radIsAdmin" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>ผู้ดูแลระบบ</asp:ListItem>
                                        <asp:ListItem>พนักงาน ปตท.</asp:ListItem>
                                        <asp:ListItem>ผู้ใช้ทั่วไป</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <%--<input id="input-3" class="minimal" type="radio" name="demo-radio" />--%>

                                   <%--<input type="radio" name="gender" value="male" checked> Male<br>
  <input type="radio" name="gender" value="female"> Female<br>
  <input type="radio" name="gender" value="other"> Other--%>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" SkinID="ButtonSuccess" data-toggle="modal" data-target="#modalConfirmSave" UseSubmitBehavior="false" />
                        <asp:Button ID="cmdCancel" runat="server" Text="ยกเลิก" OnClick="cmdCancel_Click" />
                    </div>
                </div>
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    <uc3:ModalPopup runat="server" ID="modalConfirmSave" IDModel="modalConfirmSave"
        IsButtonTypeSubmit="true" IsOnClick="true" IsShowClose="true" OnClickOK="cmdSave_Click"
        TextDetail="คุณต้องการบันทึกข้อมูล ใช่หรือไม่ ?" ImageName="imgTitleConfirm.png" />
</asp:Content>