﻿using PCLS.Bussiness.System;
using System;
using System.Data;
using System.Text;
using System.Web.UI;

namespace PCLS.WebPC.Pages.SystemData
{
    public partial class UserGroupAddEdit : PageBase
    {
        #region + View State +
        private string ActionType
        {
            get
            {
                if ((string)ViewState["ActionType"] != null)
                    return (string)ViewState["ActionType"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionType"] = value;
            }
        }

        private string ActionKey
        {
            get
            {
                if ((string)ViewState["ActionKey"] != null)
                    return (string)ViewState["ActionKey"];
                else
                    return string.Empty;
            }
            set
            {
                ViewState["ActionKey"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                    this.InitialForm();
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {
                //Helper.ActiveMenu.SetActiveMenu(Master, "tabSystem", "liUserGroup");

                //InitialStatus(ddlStatus, " AND IS_ACTIVE = 1 AND STATUS_TYPE = 'ACTIVE_STATUS'");
                //InitialStatus(radIsAdmin, " AND STATUS_TYPE = 'SPECIAL_STATUS'");
                //this.CheckQueryString();
                //this.GetData();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                if (string.Equals(ActionType, Mode.Add.ToString()))
                {
                    txtUserGroupName.Enabled = true;
                }
                else
                {
                    txtUserGroupName.Enabled = false;

                    DataTable dtUserGroup = UserGroupBLL.Instance.UserGroupSelectBLL(this.GetCondition());
                    if (dtUserGroup.Rows.Count > 0)
                    {
                        txtUserGroupName.Text = dtUserGroup.Rows[0]["USERGROUP_NAME"].ToString();
                        ddlStatus.SelectedValue = dtUserGroup.Rows[0]["IS_ACTIVE"].ToString();
                        radIsAdmin.SelectedValue = dtUserGroup.Rows[0]["IS_ADMIN"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void CheckQueryString()
        {
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString.ToString()))
                {
                    ActionType = DecodeQueryString(Request.QueryString["type"]);
                    ActionKey = DecodeQueryString(Request.QueryString["id"]);

                    if (string.Equals(ActionType, Mode.View.ToString()))
                    {
                        DisableControls(Page);
                        cmdSave.Visible = false;
                        cmdCancel.Visible = false;
                    }
                }
                else
                    Response.Redirect("../Other/NotAuthorize.aspx");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private string GetCondition()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(" AND USERGROUP_ID = '" + ActionKey + "'");

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.ValidateSave();

                UserGroupBLL.Instance.UserGroupAddBLL(int.Parse(ActionKey), txtUserGroupName.Text.Trim(), radIsAdmin.SelectedValue, ddlStatus.SelectedValue, UserID);
                alertSuccess(string.Empty, Properties.Resources.SaveSuccess, "UserGroup.aspx");
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserGroup.aspx");
        }

        private void ValidateSave()
        {
            try
            {
                if (string.Equals(txtUserGroupName.Text.Trim(), string.Empty))
                    throw new Exception(string.Format(Properties.Resources.TextBoxRequire, lblUserGroup.Text));

                if (ddlStatus.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblStatus.Text));

                if (radIsAdmin.SelectedIndex < 1)
                    throw new Exception(string.Format(Properties.Resources.DropDownRequire, lblStatusSpecial.Text));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}