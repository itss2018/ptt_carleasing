﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="BorrowKeyAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Transaction.BorrowKeyAddEdit" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ขออนุมัติยืมกุญแจสำรอง</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Transaction</li>
                    <li class="active">ขออนุมัติยืมกุญแจสำรอง</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลผู้ขออนุมัติยืมกุญแจสำรอง</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="รหัสพนักงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="ชื่อ-นามสกุล"></asp:Label>
                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="ตำแหน่ง"></asp:Label>
                                    <asp:TextBox ID="TextBox3" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="ส่วน"></asp:Label>
                                    <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="ฝ่าย/โครงการ/สำนัก"></asp:Label>
                                    <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="ธุรกิจ"></asp:Label>
                                    <asp:TextBox ID="TextBox6" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="โทรศัพท์"></asp:Label>
                                    <asp:TextBox ID="TextBox9" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="มือถือ"></asp:Label>
                                    <asp:TextBox ID="TextBox10" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลรถยนต์</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">ทะเบียนรถ</label>
                                    <asp:TextBox runat="server" ID="txtLICENSE_CODE" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">จังหวัด</label>
                                    <asp:DropDownList ID="DropDownList4" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ยี่ห้อ</label>
                                    <asp:DropDownList ID="DropDownList3" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>รุ่นรถ</label>
                                    <asp:DropDownList ID="DropDownList1" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ประเภทรถ</label>
                                    <asp:DropDownList ID="DropDownList2" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">วัตถุประสงค์</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">กรุณาระบุเหตุผล</label>
                                    <asp:DropDownList ID="DropDownList9" runat="server" class="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">เหตุผลอื่น ๆ (โปรดระบุ) </label>
                                    <asp:TextBox ID="TextBox25" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>                      
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">สถานะ</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">สถานะ</label>
                                    <asp:TextBox ID="TextBox27" runat="server" class="form-control">รออนุมัติ</asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">หมายเหตุ </label>
                                    <asp:TextBox ID="TextBox26" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>              
                </div>
                <div class="box box-primary" runat="server" id="divButton">
                    <div class="box-header with-border">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div id="divButtonSave" runat="server" style="text-align: right">
                                    <asp:Button ID="cmdAdd" runat="server" Text="บันทึก" Class="btn btn-success" OnClick="cmdAdd_Click" />
                                    <asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" Class="btn btn-danger" OnClick="cmdClear_Click"  />                                        
                                </div>
                                <div id="divButtonRequestSave" runat="server" visible="false"  style="text-align: right">
                                    <asp:Button ID="btnApprove" Text="อนุมัติ" runat="server" Class="btn btn-success" />
                                    <asp:Button ID="btnReject" runat="server" Text="ปฏิเสธ" OnClick="btnReject_Click" Class="btn btn-warning"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="ยกเลิก" Class="btn btn-danger" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>


            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>



