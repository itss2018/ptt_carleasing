﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="BorrowKeyAllowAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Transaction.BorrowKeyAllowAddEdit" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ขออนุมัติยืมกุญแจสำรอง</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>Transaction</li>
                    <li class="active">ขออนุมัติยืมกุญแจสำรอง</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    
                    <div class="box-body">
                        <br>
                        <ol class="timeline">
                            <li class="timeline__step2 done">
                                <input class="timeline__step-radio" id="trigger1{{identifier}}" name="trigger{{identifier}}" type="radio">

                                <label class="timeline__step-label" for="trigger1{{identifier}}">
                                    <span class="timeline__step-content">
                                        12 May 2013</span>
                                </label>
        
                                <span class="timeline__step-title">
                                    Requester</br>ธนชัย ศรีแสง</span>
        
                                <i class="timeline__step-marker">1</i>
                            </li>
                            <li class="timeline__step2 ">
                                <input class="timeline__step-radio" id="trigger2{{identifier}}" name="trigger{{identifier}}" type="radio">
        
                                <label class="timeline__step-label" for="trigger2{{identifier}}">
                                    <span class="timeline__step-content">
                                        14 May 2013</span>
                                </label>
        
                                <span class="timeline__step-title">
                                    ผู้ดูแลข้อมูลรถ <br>ธนชัย ศรีแสง</span>

                                <i class="timeline__step-marker">2</i>
                            </li>
                           
                            
                        </ol><br>
                        <br>
                       

                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลผู้ขออนุมัติยืมกุญแจสำรอง</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="รหัสพนักงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="ชื่อ-นามสกุล"></asp:Label>
                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="ตำแหน่ง"></asp:Label>
                                    <asp:TextBox ID="TextBox3" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" Text="รหัสหน่วยงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox11" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="แผนก"></asp:Label>
                                    <asp:TextBox ID="TextBox12" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" Text="ส่วน"></asp:Label>
                                    <asp:TextBox ID="TextBox13" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="ฝ่าย/โครงการ/สำนัก"></asp:Label>
                                    <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="กลุ่มธุรกิจ"></asp:Label>
                                    <asp:TextBox ID="TextBox6" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="โทรศัพท์"></asp:Label>
                                    <asp:TextBox ID="TextBox9" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="มือถือ"></asp:Label>
                                    <asp:TextBox ID="TextBox10" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลรถยนต์</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">ทะเบียนรถ</label>
                                    <asp:DropDownList ID="DropDownList5" runat="server" class="form-control">
                                        <asp:ListItem>-- เลือกข้อมูล --</asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">จังหวัด</label>
                                    <asp:TextBox ID="TextBox108" runat="server" class="form-control"></asp:TextBox>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ประเภทรถ</label>
                                    <asp:TextBox ID="TextBox28" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>ยี่ห้อ</label>
                                    <asp:TextBox ID="TextBox7" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>รุ่นรถ</label>
                                    <asp:TextBox ID="TextBox8" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                        </div>                   
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">วัตถุประสงค์</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">กรุณาระบุวัตถุประสงค์</label>
                                    <asp:DropDownList ID="DropDownList9" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                        <asp:ListItem> เปลี่ยนโครงสร้างหน่วยงาน </asp:ListItem>
                                        <asp:ListItem> ยุบหน่วยงาน </asp:ListItem>
                                        <asp:ListItem> อื่น ๆ (โปรดระบุ) </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="วัตถุประสงค์อื่น ๆ (โปรดระบุ)"></asp:Label>
                                    <b style="color:red">*</b>
                                    <textarea id="txtEditor" rows="5"  name="txtEditor" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>                  
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">สถานะ</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">สถานะ</label>
                                    <asp:TextBox ID="TextBox27" runat="server" class="form-control">รออนุมัติ</asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">หมายเหตุ </label>
                                    <asp:TextBox ID="TextBox26" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="box-footer">
                         <asp:Button ID="btnApprove" Text="อนุมัติ" runat="server" SkinID="ButtonSuccess"  />
                         <asp:Button ID="btnReject" runat="server" Text="ปฏิเสธ" OnClick="btnReject_Click" Class="btn btn-warning"/>
                         <asp:Button ID="btnCancel" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  />
                    </div>
                </div>
                


            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>



