﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="Contract.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.Contract1" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลสัญญา</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>ข้อมูลสัญญา</li>
                    <li class="active">ข้อมูลสัญญา</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Search Option</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblCheckType" runat="server" Text="เลขที่สัญญา"></asp:Label>
                                    <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="ประเภทสัญญา"></asp:Label>
                                    <asp:DropDownList ID="ddlStatus" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="ตั้งแต่วันที่"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtDATEFROM_SEARCH" SkinID="null" CssClass="datepicker" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="ถึงวันที่"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtDATETO_SEARCH" SkinID="null" CssClass="datepicker" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="บริษัทคู่สัญญา"></asp:Label>
                                    <asp:DropDownList ID="DropDownList2" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="สถานะสัญญา"></asp:Label>
                                    <asp:DropDownList ID="DropDownList1" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdAdd" runat="server" Text="เพิ่ม"  SkinID="ButtonSuccess" OnClick="cmdAdd_Click" />
                        <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา"  OnClick="cmdSearch_Click" />
                        <asp:Button ID="cmdClear" runat="server" Text="ล้างข้อมูล" SkinID="ButtonWarning" OnClick="cmdClear_Click"  />
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">รายชื่อคณะกรรมการตรวจรับ</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body" style="overflow-x:auto">
                        <asp:GridView ID="dgvData" runat="server" class="table table-bordered" SkinID="GridNoPaging" OnRowDataBound="dgvData_RowDataBound" DataKeyNames="ID" AutoGenerateColumns="False">
                            <HeaderStyle Wrap="false" />
                            <RowStyle Wrap="false" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgView.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                            <asp:Image ImageUrl="~/Images/Button/imgEdit.png" Width="24px" Height="24px" runat="server" />
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ID" HeaderText="รหัสสัญญา"></asp:BoundField>
                                <asp:BoundField DataField="TYPECONTRACT" HeaderText="ประเภทสัญญา"></asp:BoundField>
                                <asp:BoundField DataField="PARTNERNAME" HeaderText="บริษัทคู่สัญญา"></asp:BoundField>
                                <asp:BoundField DataField="CONTRACTSTARTDATE" HeaderText="วันที่เริ่มต้นสัญญา"></asp:BoundField>
                                <asp:BoundField DataField="CONTRACTENDDATE" HeaderText="วันที่สิ้นสุดสัญญา"></asp:BoundField>
                                <asp:BoundField DataField="CONTRACTVALUE" HeaderText="มูลค่าสัญญา"></asp:BoundField>
                                <asp:BoundField DataField="STATUS" HeaderText="สถานะสัญญา"></asp:BoundField>                              
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
