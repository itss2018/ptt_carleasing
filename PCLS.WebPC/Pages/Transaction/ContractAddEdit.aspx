﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="ContractAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Master.ContractAddEdit1" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลสัญญา</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>ข้อมูลสัญญา</li>
                    <li class="active">ข้อมูลสัญญา</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">Input Data</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblCheckType" runat="server" Text="เลขที่สัญญา"></asp:Label>
                                    <asp:TextBox ID="txtName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" runat="server" Text="วันที่ลงนามในสัญญา"></asp:Label>
                                    <asp:TextBox runat="server" ID="TextBox3" SkinID="null" CssClass="datepicker" />
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="ประเภทสัญญา"></asp:Label>
                                    <asp:DropDownList ID="DropDownList4" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" Text="มูลค่าสัญญา/มูลค่าหลักประกันสัญญา"></asp:Label>
                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>  
                        </div>
                     
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="ธนาคารผู้ออกหลักค้ำประกัน"></asp:Label>
                                    <asp:DropDownList ID="DropDownList3" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>                                    
                                </div>
                            </div>                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="สาขาของธนาคารผู้ออกหลักค้ำประกัน"></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="วันที่เริ่มต้นสัญญา"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtDATEFROM_SEARCH" SkinID="null" CssClass="datepicker" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="วันที่สิ้นสุดสัญญา"></asp:Label>
                                    <asp:TextBox runat="server" ID="txtDATETO_SEARCH" SkinID="null" CssClass="datepicker" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="บริษัทคู่สัญญา"></asp:Label>
                                    <asp:DropDownList ID="DropDownList2" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="สถานะสัญญา"></asp:Label>
                                    <asp:DropDownList ID="DropDownList1" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <asp:Button ID="cmdAdd" runat="server" Text="บันทึก" SkinID="ButtonSuccess" OnClick="cmdAdd_Click" />
                        <asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" SkinID="ButtonDanger" OnClick="cmdClear_Click"  />
                    </div>

                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">คณะกรรมการตรวจรับ</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">


                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="ตำแหน่ง"></asp:Label>
                                    <asp:DropDownList ID="DropDownList5" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="รหัสพนักงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" Text="ชื่อ - นามสกุล"></asp:Label>
                                    <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer">
                                <asp:Button ID="Button1" runat="server" Text="เพิ่ม" SkinID="ButtonSuccess"  OnClick="cmdAdd_Click" />
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">รายชื่อคณะกรรมการตรวจรับ</h3>
                                            <div class="pull-right box-tools">
                                            <%--<button type="button" class="btn btn-info btn-sm"  title="เพิ่มรายการการต่อประกันภัยภาคบังคับ (พ.ร.บ.)" data-toggle="modal" data-target="#modalInsurance1">
                                                <i class="fa fa-plus"></i>
                                            </button> --%>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <asp:GridView ID="dgvData" runat="server" class="table table-bordered" SkinID="GridNoPaging" OnRowDataBound="dgvData_RowDataBound" DataKeyNames="ID" AutoGenerateColumns="False">
                                            <HeaderStyle Wrap="false" />
                                            <RowStyle Wrap="false" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <HeaderStyle Width="50px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                                            <asp:Image ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" runat="server" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ID" HeaderText="ลำดับ"></asp:BoundField>
                                                <asp:BoundField DataField="POSITION" HeaderText="ตำแหน่ง"></asp:BoundField>
                                                <asp:BoundField DataField="EMPCODE" HeaderText="รหัสพนักงาน"></asp:BoundField>
                                                <asp:BoundField DataField="NAME" HeaderText="ชื่อ"></asp:BoundField>
                                                <asp:BoundField DataField="SURNAME" HeaderText="นามสกุล"></asp:BoundField>                        
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="box-footer clearfix">
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            <li><a href="#">&laquo;</a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">&raquo;</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">นำรถเข้าสัญญา</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">เลือกไฟล์</label>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="DropDownList37"  runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:FileUpload ID="fileUpload4" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class='glyphicon glyphicon-plus'></span>
                                                                    <asp:Button ID="Button20" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>                                               
                                            </div>
                                            

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">รายชื่อรถที่นำเข้าสัญญา</h3>
                                            <div class="pull-right box-tools">
                                            <button type="button" class="btn btn-info btn-sm"  title="เพิ่มรถ" data-toggle="modal" data-target="#modalCar">
                                                <i class="fa fa-plus"></i>
                                            </button> 
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <asp:GridView ID="GridView1" runat="server" class="table table-bordered" SkinID="GridNoPaging" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="ID" AutoGenerateColumns="False">
                                            <HeaderStyle Wrap="false" />
                                            <RowStyle Wrap="false" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <HeaderStyle Width="50px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                                            <asp:Image ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" runat="server" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ID" HeaderText="ลำดับ"></asp:BoundField>
                                                <asp:BoundField DataField="License" HeaderText="ทะเบียนรถ"></asp:BoundField>
                                                <asp:BoundField DataField="ChassisNo" HeaderText="เลขตัวถัง"></asp:BoundField>
                                                <asp:BoundField DataField="EngineNo" HeaderText="เลขเครื่องยนต์"></asp:BoundField>
                                                <asp:BoundField DataField="Brand" HeaderText="ยี่ห้อ"></asp:BoundField>
                                                <asp:BoundField DataField="Model" HeaderText="รุ่น"></asp:BoundField>
                                                <asp:BoundField DataField="Description" HeaderText="รายละเอียดเพิ่มเติม"></asp:BoundField>
                                                <asp:BoundField DataField="TypeCar" HeaderText="ประเภทรถ"></asp:BoundField>                      
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="box-footer clearfix">
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            <li><a href="#">&laquo;</a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">&raquo;</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>

            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


