﻿using PCLS.Bussiness.Master;
using PCLS.WebPC.Helper;
using PCLS.WebPC.Properties;
using System;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PCLS.WebPC.Pages.Master
{
    public partial class ContractAddEdit1 : PageBase
    {
        #region Member
        string FieldType = "Contract", str = string.Empty;
        #endregion

        #region + View State +
        private DataTable dtData
        {
            get
            {
                if ((DataTable)ViewState["dtData"] != null)
                    return (DataTable)ViewState["dtData"];
                else
                    return null;
            }
            set
            {
                ViewState["dtData"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    this.InitialForm();
                }
            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        private void InitialForm()
        {
            try
            {

                this.GetData();
                this.GetData2();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("POSITION");
                dt.Columns.Add("EMPCODE");
                dt.Columns.Add("NAME");
                dt.Columns.Add("SURNAME");

                DataRow row = dt.NewRow();
                row["ID"] = "0001";
                row["POSITION"] = "ทดสอบ";
                row["EMPCODE"] = "1234";
                row["NAME"] = "Jane";
                row["SURNAME"] = "Doe";
                dt.Rows.Add(row);

                this.dgvData.Visible = true;
                dgvData.DataSource = dt;
                dgvData.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

      
        private void GetData2()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("License");
                dt.Columns.Add("ChassisNo");
                dt.Columns.Add("EngineNo");
                dt.Columns.Add("Brand");
                dt.Columns.Add("Model");
                dt.Columns.Add("Description");
                dt.Columns.Add("TypeCar");

                DataRow row = dt.NewRow();
                row["ID"] = 1200;
                row["License"] = "1กจ 347";
                row["ChassisNo"] = "ABCD-32585";
                row["EngineNo"] = "ABCD-32585";
                row["Brand"] = "HONDA";
                row["Model"] = "CITY";
                row["Description"] = "TEST";
                row["TypeCar"] = "TEST";
                dt.Rows.Add(row);

                this.GridView1.Visible = true;
                GridView1.DataSource = dt;
                GridView1.DataBind();

            }
            catch (Exception ex)
            {
                alertFail(ex.Message);
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");
                if (aView != null)
                {
                    str = Mode.View + "&" + row["ID"];//Mode & ID
                    aView.HRef = "ContractAddEdit.aspx?str=" + EncodeQueryString(str);
                }
                if (aEdit != null)
                {
                    str = Mode.Edit + "&" + row["ID"];//Mode & ID
                    aEdit.HRef = "ContractAddEdit.aspx?str=" + EncodeQueryString(str);
                }
            }

        }

        protected void dgvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;
                HtmlAnchor aView = (HtmlAnchor)e.Row.FindControl("aView");
                HtmlAnchor aEdit = (HtmlAnchor)e.Row.FindControl("aEdit");
                if (aView != null)
                {
                    str = Mode.View + "&" + row["ID"];//Mode & ID
                    aView.HRef = "ContractAddEdit.aspx?str=" + EncodeQueryString(str);
                }
                if (aEdit != null)
                {
                    str = Mode.Edit + "&" + row["ID"];//Mode & ID
                    aEdit.HRef = "ContractAddEdit.aspx?str=" + EncodeQueryString(str);
                }
            }

        }

        #region dgvData_PageIndexChanging
        protected void dgvData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvData.PageIndex = e.NewPageIndex;

        }
        #endregion

        protected void dgvData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void cmdAdd_Click(object sender, EventArgs e)
        {

        }

        protected void cmdClear_Click(object sender, EventArgs e)
        {

        }
    }
}