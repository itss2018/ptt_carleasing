﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="Document.aspx.cs" Inherits="PCLS.WebPC.Pages.Transaction.Document"  Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ยื่นคำร้อง</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>เอกสาร</li>
                    <li class="active">ยื่นคำร้อง</li>
                </ol>
            </section>
             <section class="content">
                <div class="box box-primary">
                    
                    
                    <div class="box-body">
                     
                            <div class="col-md-4 ">

                                <h3>1. การดำเดินการเกี่ยวกับรถ</h3>
                                 <a class="btn btn-lg btn-primary big-btn-1" href="../../Pages/Transaction/PossessorRequestsAddEdit.aspx">
  				                      <img width="60px" class="pull-left" src="../../dist/img/icon/user_02.png"><div class="btn-text"><strong>เปลี่ยนข้อมูลการครอบครองรถ</strong></div></a>
                                 <%-- <a class="btn btn-lg btn-primary big-btn-1" href="../../Pages/Transaction/PossessorRequestAddEdit.aspx">
  				                    <img width="60px" class="pull-left" src="../../dist/img/icon/user_01.png"><div class="btn-text"><strong>เปลี่ยนผู้ครอบครองรถ</strong><br><small>ฝ่ายเดียวกัน</small></div></a>--%>
   <a class="btn btn-lg btn-primary big-btn-1" href="../../Pages/Transaction/KeyLending.aspx">
  				                    <img width="60px" class="pull-left" src="../../dist/img/icon/key.png"><div class="btn-text"><strong>ขอยืมกุญแจสำรอง</strong></div></a>
                                  <a class="btn btn-lg btn-primary big-btn-1" href="../../Pages/Transaction/ReportLostAddEdit.aspx">
  				                    <img width="60px" class="pull-left" src="../../dist/img/icon/car.png"><div class="btn-text"><strong>แจ้งภาษี/ป้ายทะเบียนหาย</strong></div></a>
                            </div>
                            <div class="col-md-4 ">

                                <h3>2. แบบฟอร์ม</h3>
                                 <a class="btn btn-lg btn-primary big-btn-1" href="#">
  				                    <img width="60px" class="pull-left" src="../../dist/img/icon/download.png"><div class="btn-text"><strong>ดาวน์โหลดเอกสาร</strong></div></a>
                            </div>
                        
                       
                    </div>
             
                </div>

                <div class="row">
      
        <!-- ./col -->
                    <div class="col-md-12">
                      <div class="box box-solid">
                        <div class="box-header with-border">
                          <i class="fa fa-newspaper-o"></i>

                          <h3 class="box-title">ประกาศ</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                          <ol>
                            <li>ทดสอบระบบเพื่อลงข้อมูล มีผลบังคับใช้ตั้งแต่วันที่ 1 กันยายน 2561 เป็นต้นไป</li>
                            <li>ทดสอบระบบเพื่อลงข้อมูล มีผลบังคับใช้ตั้งแต่วันที่ 1 กันยายน 2561 เป็นต้นไป</li>
                            <li>ทดสอบระบบเพื่อลงข้อมูล มีผลบังคับใช้ตั้งแต่วันที่ 1 กันยายน 2561 เป็นต้นไป</li>
                            <li>ทดสอบระบบเพื่อลงข้อมูล มีผลบังคับใช้ตั้งแต่วันที่ 1 กันยายน 2561 เป็นต้นไป</li>
                            <li>ทดสอบระบบเพื่อลงข้อมูล มีผลบังคับใช้ตั้งแต่วันที่ 1 กันยายน 2561 เป็นต้นไป
                              <ol>
                                <li>ทดสอบรย่อหน้าแบบสั้น</li>
                                <li>ทดสอบรย่อหน้าแบบสั้น</li>
                                <li>ทดสอบรย่อหน้าแบบสั้น</li>
                                <li>ทดสอบรย่อหน้าแบบสั้น</li>
                              </ol>
                            </li>
                          </ol>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->
                    </div>
     
        
      </div>
      <!-- /.row -->
            </section>

            
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

