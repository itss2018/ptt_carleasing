﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="InsuranceAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Transaction.InsuranceAddEdit" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลประกันภัยรถยนต์</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>การจัดการข้อมูล</li>
                    <li class="active">ข้อมูลประกันภัยรถยนต์</li>
                </ol>
            </section>
            <section class="content">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลรถยนต์</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">ทะเบียนรถ</label>
                                    <asp:DropDownList ID="DropDownList1" runat="server" class="form-control">
                                        <asp:ListItem>-- เลือกข้อมูล --</asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">จังหวัด</label>
                                    <asp:TextBox ID="TextBox108" runat="server" class="form-control"></asp:TextBox>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ประเภทรถ</label>
                                    <asp:TextBox ID="TextBox28" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>ยี่ห้อ</label>
                                    <asp:TextBox ID="TextBox7" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>รุ่นรถ</label>
                                    <asp:TextBox ID="TextBox8" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                        </div>                       
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลผู้ครอบครองรถ</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>รหัสผู้ครอบครอง</label>
                                    <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ชื่อผู้ครอบครอง</label>
                                    <asp:TextBox ID="TextBox29" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ตำแหน่ง</label>
                                    <asp:TextBox ID="TextBox30" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>                            
                        </div> 
                         <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="รหัสหน่วยงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="ชื่อย่อหน่วยงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label13" runat="server" Text="กลุ่มธุรกิจ"></asp:Label>
                                    <asp:TextBox ID="TextBox6" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="โทรศัพท์"></asp:Label>
                                    <asp:TextBox ID="TextBox9" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="มือถือ"></asp:Label>
                                    <asp:TextBox ID="TextBox10" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลการต่อประกันภัยรถยนต์</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">กรมธรรม์เลขที่</label>
                                    <asp:TextBox ID="TextBox11" runat="server" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">วันที่เริ่มต้น</label>
                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">วันที่สิ้นสุด</label>
                                    <asp:TextBox ID="TextBox3" runat="server" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                               <div class="form-group">
                                    <label for="exampleInputEmail1">บริษัทประกันภัย</label>
                                    <asp:DropDownList ID="DropDownList5" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                             <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">ทุนประกันภัย</label>
                                    <asp:TextBox ID="TextBox15" runat="server" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                        </div>  
                                           
                    </div>
                </div>


                <div class="box box-primary" runat="server" id="divButton">
                    <div class="box-header with-border">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div id="divButtonSave" runat="server" style="text-align: right">

                                    <asp:Button ID="Button1" runat="server" Text="บันทึก" SkinID="ButtonSuccess" OnClick="cmdAdd_Click" />
                                    <asp:Button ID="Button2" runat="server" Text="ยกเลิก" SkinID="ButtonDanger" OnClick="cmdClear_Click"  />
                                </div>
                                <div id="divButtonRequestSave" runat="server" visible="false"  style="text-align: right">
                                    <asp:Button ID="btnApprove" Text="อนุมัติ" runat="server" Class="btn btn-success" />
                                    <asp:Button ID="btnReject" runat="server" Text="ปฏิเสธ" OnClick="btnReject_Click" Class="btn btn-warning"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="ยกเลิก" Class="btn btn-danger" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>


            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>