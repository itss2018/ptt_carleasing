﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="PossessorRequestAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Transaction.PossessorRequestAddEdit" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa  fa-car"></i><span class="caption-subject bold uppercase">&nbsp;ขออนุมัติเปลี่ยนแปลงข้อมูลการครอบครองรถยนต์ (กรณีย้ายรถในฝ่ายเดียวกัน)</span>
                </h1>

                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>ยื่นคำร้อง</li>
                    <li class="active">ขออนุมัติเปลี่ยนแปลงข้อมูลการครอบครองรถยนต์</li>
                </ol>
                
            </section>
            
            <section class="content">
                <div class="box box-primary">
                    
                    <div class="box-body">
                        <br>
                        <ol class="timeline">
                            <li class="timeline__step7 done">
                                <input class="timeline__step-radio" id="trigger1{{identifier}}" name="trigger{{identifier}}" type="radio">

                                <label class="timeline__step-label" for="trigger1{{identifier}}">
                                    <span class="timeline__step-content">
                                        16 Oct 2018</span>
                                </label>
        
                                <span class="timeline__step-title">
                                    Requester <i class="fa fa-check" style="font-size:15px;color:lightseagreen"></i></br>ธนชัย ศรีแสง</span>
        
                                <i class="timeline__step-marker">1</i>
                            </li>
                            <li class="timeline__step7 ">
                                <input class="timeline__step-radio" id="trigger2{{identifier}}" name="trigger{{identifier}}" type="radio">
        
                                <label class="timeline__step-label" for="trigger2{{identifier}}">
                                    <span class="timeline__step-content">
                                        16 Oct 2018</span>
                                </label>
        
                                <span class="timeline__step-title">
                                    ผู้ดูแลข้อมูลรถ</br>ธนชัย ศรีแสง</span>

                                <i class="timeline__step-marker">2</i>
                            </li>
                            <li class="timeline__step7">
                                <input class="timeline__step-radio" id="trigger3{{identifier}}" name="trigger{{identifier}}" type="radio">
        
                                <label class="timeline__step-label" for="trigger3{{identifier}}">
                                   <span class="timeline__step-content">
                                        16 Oct 2018</span>
                                </label>
        
                                <span class="timeline__step-title">
                                    ผจ.กด</br>ธนชัย ศรีแสง</span>
        
                                <i class="timeline__step-marker">3</i>
                            </li>
                            <li class="timeline__step7">
                                <input class="timeline__step-radio" id="trigger4{{identifier}}" name="trigger{{identifier}}" type="radio">
        
                                <label class="timeline__step-label" for="trigger4{{identifier}}">
                                    <span class="timeline__step-content">
                                        16 Oct 2018</span>
                                </label>
        
                                <span class="timeline__step-title">
                                    ผจ.ส่วน</br>ธนชัย ศรีแสง</span>
        
                                <i class="timeline__step-marker">4</i>
                            </li>
                            <li class="timeline__step7">
                                <input class="timeline__step-radio" id="trigger5{{identifier}}" name="trigger{{identifier}}" type="radio">
        
                                <label class="timeline__step-label" for="trigger5{{identifier}}">
                                    <span class="timeline__step-content">
                                        16 Oct 2018</span>
                                </label>
        
                                <span class="timeline__step-title">
                                    ผจ.ฝ่าย</br>ธนชัย ศรีแสง</span>
        
                                <i class="timeline__step-marker">5</i>
                            </li>
                            <li class="timeline__step7">
                                <input class="timeline__step-radio" id="trigger6{{identifier}}" name="trigger{{identifier}}" type="radio">
        
                                <label class="timeline__step-label" for="trigger6{{identifier}}">
                                    <span class="timeline__step-content">
                                        16 Oct 2018</span>
                                </label>
        
                                <span class="timeline__step-title">
                                    ผจ.ส่วน ฝั่งรับ</br>ธนชัย ศรีแสง
                                </span>
        
                                <i class="timeline__step-marker">6</i>
                            </li>
                            <li class="timeline__step7">
                                <input class="timeline__step-radio" id="trigger7{{identifier}}" name="trigger{{identifier}}" type="radio">
        
                                <label class="timeline__step-label" for="trigger7{{identifier}}">
                                    <span class="timeline__step-content">
                                        16 Oct 2018</span>
                                </label>
        
                                <span class="timeline__step-title">
                                   ผู้ดูแลข้อมูลรถ</br>ธนชัย ศรีแสง</span>
        
                                <i class="timeline__step-marker">7</i>
                            </li>
                            
                        </ol></br>
                        </br>
                        </br>
                        <ul class="list-inline">
                            <li class="pull-right dropdown">
                                <a href="#" class="link-black text-md" data-toggle="dropdown"><i class="fa fa-comments-o margin-r-5"></i>ดูประวัติเอกสาร</a>
                                      <ul class="dropdown-menu dropdown-cart" role="menu">
                                          <li>
                                              <span class="item">
                                                <span class="item-left">
                                                    <img src="/Images/UserProfile/User.png" alt="" />
                                                    <span class="item-info">
                                                        <span>Requester</span>
                                                        <span class="text-light-blue">ธนชัย ศรีแสง</span>
                                                        <span class="text-green">สร้างเอกสาร (15/09/2561)</span>
                                                    </span>
                                                </span>
                                                
                                            </span>
                                          </li>
                            <li>
                                              <span class="item">
                                                <span class="item-left">
                                                    <img src="/Images/UserProfile/User.png" alt="" />
                                                    <span class="item-info">
                                                        <span>ผู้ดูแลข้อมูลรถ</span>
                                                        <span class="text-green">อนุมัติ (15/09/2561)</span>
                                                    </span>
                                                </span>
                                                
                                            </span>
                                          </li>
                                          <li>
                                              <span class="item">
                                                <span class="item-left">
                                                    <img src="/Images/UserProfile/User.png" alt="" />
                                                    <span class="item-info">
                                                        <span>ผจ.กด</span>
                                                        <span class="text-yellow">รออนุมัติ</span>
                                                    </span>
                                                </span>
                                            </span>
                                          </li>
                                          <li>
                                              <span class="item">
                                                <span class="item-left">
                                                    <img src="/Images/UserProfile/User.png" alt="" />
                                                    <span class="item-info">
                                                        <span>ส่วน</span>
                                                        <span class="text-yellow">รออนุมัติ</span>
                                                    </span>
                                                </span>
                                            </span>
                                          </li>
                                          <li>
                                              <span class="item">
                                                <span class="item-left">
                                                    <img src="/Images/UserProfile/User.png" alt="" />
                                                    <span class="item-info">
                                                        <span>ฝ่าย</span>
                                                        <span class="text-yellow">รออนุมัติ</span>
                                                    </span>
                                                </span>
                                            </span>
                                          </li>
                                           <li>
                                              <span class="item">
                                                <span class="item-left">
                                                    <img src="/Images/UserProfile/User.png" alt="" />
                                                    <span class="item-info">
                                                        <span>รอง กผญ</span>
                                                        <span class="text-yellow">รออนุมัติ</span>
                                                    </span>
                                                </span>
                                            </span>
                                          </li>
                                          
                                          <%--<li><a class="text-center" href="#">View Cart</a></li>--%>
                                      </ul>
                                </li>

                        </ul>
                       

                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">1. ข้อมูลผู้ขอเปลี่ยนแปลง</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="รหัสพนักงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="ชื่อ-นามสกุล"></asp:Label>
                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="ตำแหน่ง"></asp:Label>
                                    <asp:TextBox ID="TextBox3" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="รหัสหน่วยงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="ชื่อย่อหน่วยงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label13" runat="server" Text="กลุ่มธุรกิจ"></asp:Label>
                                    <asp:TextBox ID="TextBox6" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="โทรศัพท์"></asp:Label>
                                    <asp:TextBox ID="TextBox9" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="มือถือ"></asp:Label>
                                    <asp:TextBox ID="TextBox10" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">2. ข้อมูลรถยนต์ (เพิ่มแสดงรูปรถ)</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">ทะเบียนรถ</label>
                                    <asp:DropDownList ID="DropDownList5" runat="server" class="form-control">
                                        <asp:ListItem>-- เลือกข้อมูล --</asp:ListItem>
                                    </asp:DropDownList> 
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">จังหวัด</label>
                                    <asp:TextBox ID="TextBox108" runat="server" class="form-control"></asp:TextBox>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ประเภทรถ</label>
                                    <asp:TextBox ID="TextBox28" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>ยี่ห้อ</label>
                                    <asp:TextBox ID="TextBox7" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>รุ่นรถ</label>
                                    <asp:TextBox ID="TextBox8" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>รหัสผู้ครอบครอง</label>
                                    <asp:TextBox ID="TextBox14" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ชื่อผู้ครอบครอง</label>
                                    <asp:TextBox ID="TextBox29" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ตำแหน่ง</label>
                                    <asp:TextBox ID="TextBox30" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>                            
                        </div> 
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" Text="รหัสหน่วยงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox11" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label8" runat="server" Text="ชื่อย่อหน่วยงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox12" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label12" runat="server" Text="กลุ่มธุรกิจ"></asp:Label>
                                    <asp:TextBox ID="TextBox13" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">3. ข้อมูลที่เปลี่ยนแปลง</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                    <asp:RadioButton ID="RadioButton1" runat="server" Text="เปลี่ยนชื่อผู้ครอบครอง" />                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>เป็นรหัสพนักงาน</label>
                                    <asp:TextBox ID="TextBox15" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ชื่อ-นามสกุล</label>
                                    <asp:TextBox ID="TextBox16" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ตำแหน่ง</label>
                                    <asp:TextBox ID="TextBox19" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>                            
                        </div> 
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">รหัสหน่วยงาน</label>
                                    <asp:TextBox ID="TextBox20" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">แผนก</label>
                                    <asp:TextBox ID="TextBox21" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">ส่วน</label>
                                    <asp:TextBox ID="TextBox36" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">ฝ่าย/โครงการ/สำนัก</label>
                                    <asp:TextBox ID="TextBox37" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">กลุ่มธุรกิจ</label>
                                    <asp:TextBox ID="TextBox38" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                    <asp:RadioButton ID="RadioButton3" runat="server" Text="ขอส่งคืนยานพาหนะให้ กด.จบญ." />                                    
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">4. เหตุผลการเปลี่ยนแปลง</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">กรุณาระบุเหตุผล</label>
                                    <asp:DropDownList ID="DropDownList9" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                        <asp:ListItem> เปลี่ยนโครงสร้างหน่วยงาน </asp:ListItem>
                                        <asp:ListItem> ยุบหน่วยงาน </asp:ListItem>
                                        <asp:ListItem> อื่น ๆ (โปรดระบุ) </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="เหตุผลอื่น ๆ (โปรดระบุ)"></asp:Label>
                                    <b style="color:red">*</b>
                                    <textarea id="txtEditor" rows="5"  name="txtEditor" class="form-control"></textarea>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">5. สถานะ</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                       <div class="row">
                            <div class="col-md-3">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">สถานะ</label>
                                    <asp:TextBox ID="TextBox27" runat="server" class="form-control">รออนุมัติ</asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" Text="หมายเหตุ"></asp:Label>
                                    <b style="color:red">*</b>
                                    <textarea id="txtEditor2" rows="5"  name="txtEditor" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>         
                    </div>   
                    
                    <div class="box-footer">
                        <asp:Button ID="cmdAdd" runat="server" Text="บันทึก" SkinID="ButtonSuccess" />
                        <asp:Button ID="Button2" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  />
                    </div>
                </div>

            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>



