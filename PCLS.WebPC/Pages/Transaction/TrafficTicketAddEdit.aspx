﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/V1.0/Standard.Master" AutoEventWireup="true" CodeBehind="TrafficTicketAddEdit.aspx.cs" Inherits="PCLS.WebPC.Pages.Transaction.TrafficTicketAddEdit" Theme="Blue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="udpMain" runat="server">
    <ContentTemplate>
        <div class="content-wrapper" style="height:1500px; overflow:scroll">
            <section class="content-header">
                <h1><i class="fa fa-circle-o"></i><span class="caption-subject bold uppercase">&nbsp;ข้อมูลใบสั่งจราจร</span>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="../../Pages/Other/BlankPage.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li>การจัดการข้อมูล</li>
                    <li class="active">ข้อมูลใบสั่งจราจร</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลรถยนต์</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                    <div class="form-group">
                                    <label for="exampleInputEmail1">ทะเบียนรถ</label>
                                    <asp:TextBox runat="server" ID="txtLICENSE_CODE" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-2">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">จังหวัด</label>
                                    <asp:DropDownList ID="DropDownList4" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                    <label>ยี่ห้อ</label>
                                    <asp:DropDownList ID="DropDownList3" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>รุ่นรถ</label>
                                    <asp:DropDownList ID="DropDownList1" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ประเภทรถ</label>
                                    <asp:DropDownList ID="DropDownList2" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลผู้ครอบครองรถ</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    
                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="รหัสพนักงาน"></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Label ID="Label2" runat="server" Text="ชื่อ-นามสกุล"></asp:Label>
                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label3" runat="server" Text="ตำแหน่ง"></asp:Label>
                                    <asp:TextBox ID="TextBox3" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label4" runat="server" Text="ส่วน"></asp:Label>
                                    <asp:TextBox ID="TextBox4" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label5" runat="server" Text="ฝ่าย/โครงการ/สำนัก"></asp:Label>
                                    <asp:TextBox ID="TextBox5" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label6" runat="server" Text="ธุรกิจ"></asp:Label>
                                    <asp:TextBox ID="TextBox6" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                             <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label9" runat="server" Text="โทรศัพท์"></asp:Label>
                                    <asp:TextBox ID="TextBox9" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label10" runat="server" Text="มือถือ"></asp:Label>
                                    <asp:TextBox ID="TextBox10" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                              <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label11" runat="server" Text="อีเมล์"></asp:Label>
                                    <asp:TextBox ID="TextBox25" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลใบสั่ง</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">เลขที่ใบสั่ง</label>
                                    <asp:TextBox ID="TextBox20" runat="server" class="form-control"></asp:TextBox>                                      
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">ประเภทใบสั่ง</label>
                                    <asp:DropDownList ID="DropDownList6" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>                               
                                </div>
                            </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">วันที่กระทำความผิด</label>
                                    <asp:TextBox ID="TextBox11" runat="server" class="form-control" CssClass="datepicker"></asp:TextBox>                                   
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">เวลาที่กระทำความผิด</label>
                                    <asp:TextBox ID="TextBox12" runat="server" class="form-control"></asp:TextBox>                               
                                </div>
                            </div>
                        </div>
                        <div class="row">
                          
                        </div>   
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">ข้อหา</label>
                                    <asp:DropDownList ID="DropDownList7" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                    </asp:DropDownList>   
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">ข้อหาอื่น ๆ (โปรดระบุ)</label>
                                    <asp:TextBox ID="TextBox14" runat="server" class="form-control"></asp:TextBox>                             
                                </div>
                            </div>
                               <div class="col-md-5">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">สถานที่เกิดเหตุ</label>
                                    <asp:TextBox ID="TextBox13" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">จำนวนค่าปรับ (บาท)</label>
                                    <asp:TextBox ID="TextBox21" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">วันครบกำหนดชำระ</label>
                                    <asp:TextBox ID="TextBox22" runat="server" class="form-control" CssClass="datepicker"></asp:TextBox> 
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">หน่วยงานผู้ออกใบสั่ง</label>
                                    <asp:TextBox ID="TextBox23" runat="server" class="form-control"></asp:TextBox> 
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">หมายเลขโทรศัพท์</label>
                                    <asp:TextBox ID="TextBox24" runat="server" class="form-control"></asp:TextBox> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">ข้อมูลผู้ใช้รถ</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row">
                             <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label7" runat="server" Text="รหัสผู้ขอใช้"></asp:Label>
                                    <asp:TextBox ID="TextBox7" runat="server" class="form-control"></asp:TextBox>
                                </div>   
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label14" runat="server" Text="ชื่อผู้ขอใช้รถ"></asp:Label>
                                    <asp:TextBox ID="TextBox18" runat="server" class="form-control"></asp:TextBox>
                                </div>   
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label18" runat="server" Text="เลขที่ใบขอใช้รถ"></asp:Label>
                                    <asp:TextBox ID="TextBox17" runat="server" class="form-control"></asp:TextBox>
                                </div>   
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label12" runat="server" Text="วันที่ขอใช้รถ"></asp:Label>
                                    <asp:TextBox ID="TextBox16" runat="server" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label13" runat="server" Text="ถึงวันที่"></asp:Label>
                                    <asp:TextBox ID="TextBox15" runat="server" CssClass="datepicker"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                        </div>
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-Position">เอกสารแนบ</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <div class="box-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <asp:Label ID="Label15" runat="server" Text="เอกสารแนบ"></asp:Label>
                                    <asp:DropDownList ID="DropDownList5" runat="server" class="form-control">
                                        <asp:ListItem> --- เลือกข้อมูล --- </asp:ListItem>
                                        <asp:ListItem> ใบสั่ง </asp:ListItem>
                                        <asp:ListItem> อื่น ๆ </asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <asp:Label ID="Label16" runat="server" Text="อื่น ๆ (โปรดระบุ)"></asp:Label>
                                    <asp:TextBox runat="server" ID="TextBox19" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label ID="Label17" runat="server" Text="เลือกไฟล์"></asp:Label>
                                    <asp:FileUpload ID="fileUpload1" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                </div>
                            </div>
                        </div>
                       
                        <div class="box-footer">
                                <asp:Button ID="Button1" runat="server" Text="เพิ่ม" Class="btn btn-success" OnClick="cmdAdd_Click" />
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">รายการเอกสารแนบ</h3>
                                            <div class="pull-right box-tools">
                                            <%--<button type="button" class="btn btn-info btn-sm"  title="เพิ่มรายการการต่อประกันภัยภาคบังคับ (พ.ร.บ.)" data-toggle="modal" data-target="#modalInsurance1">
                                                <i class="fa fa-plus"></i>
                                            </button> --%>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <asp:GridView ID="dgvData" runat="server" class="table table-bordered" SkinID="GridNoPaging" OnRowDataBound="dgvData_RowDataBound" DataKeyNames="ID" AutoGenerateColumns="False">
                                            <HeaderStyle Wrap="false" />
                                            <RowStyle Wrap="false" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <HeaderStyle Width="50px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aView" target="_blank" runat="server">
                                                            <asp:Image ImageUrl="~/Images/Button/imgView.png" Width="24px" Height="24px" runat="server" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <HeaderStyle Width="50px" />
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <ItemTemplate>
                                                        <a style="cursor:pointer;" href="~/Pages/Master/CarDetail.aspx" id="aEdit" target="_blank" runat="server">
                                                            <asp:Image ImageUrl="~/Images/Button/imgDelete.png" Width="24px" Height="24px" runat="server" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ID" HeaderText="ลำดับ"></asp:BoundField>
                                                <asp:BoundField DataField="DOCUMENT" HeaderText="เอกสารแนบ"></asp:BoundField>
                                                <asp:BoundField DataField="FILENAME" HeaderText="ชื่อไฟล์"></asp:BoundField>
               
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="box-footer clearfix">
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            <li><a href="#">&laquo;</a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">&raquo;</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                </div>
              
                <div class="box box-primary" runat="server" id="divButton">
                    <div class="box-header with-border">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div id="divButtonSave" runat="server" style="text-align: right">
                                    <asp:Button ID="cmdAdd" runat="server" Text="บันทึก" SkinID="ButtonSuccess"  OnClick="cmdAdd_Click" />
                                    <asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" SkinID="ButtonDanger"  OnClick="cmdClear_Click"  />                                        
                                </div>
                                <div id="divButtonRequestSave" runat="server" visible="false"  style="text-align: right">
                                    <asp:Button ID="btnApprove" Text="อนุมัติ" runat="server" Class="btn btn-success" />
                                    <asp:Button ID="btnReject" runat="server" Text="ปฏิเสธ" OnClick="btnReject_Click" Class="btn btn-warning"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="ยกเลิก" Class="btn btn-danger" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                
            </section>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>



