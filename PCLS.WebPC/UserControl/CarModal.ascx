﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CarModal.ascx.cs" Inherits="PCLS.WebPC.UserControl.CarModal" %>

<!-- #region + Modal Information + -->
<div class="modal fade" style="z-index: 1060" id='<%= IDModel %>' role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <asp:Image ID="imgPopup" runat="server" ImageUrl="~/Images/imgTitleConfirm.png" Width="48" Height="48" />
                    <asp:Label ID="lblModalTitle" runat="server" CssClass="TextTitle" style="font-family:sans-serif">ข้อมูลการต่อภาษี</asp:Label>
                </h4>
            </div>
            <div class="modal-body">
                <span id="TextDetail" class="TextDetail" style="font-family:sans-serif"><%= TextDetail %></span>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>วันที่ต่อภาษี</label>
                                                        <asp:TextBox runat="server" ID="txtTaxDate" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>ค่าภาษี</label>
                                                        <asp:TextBox runat="server" ID="TextBox1" SkinID="null" CssClass="form-control" />
                                                    </div>
                                                </div>                                           
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>เงินเพิ่ม</label>
                                                        <asp:TextBox runat="server" ID="TextBox2" SkinID="null" CssClass="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>วันที่ตรวจภาพ</label>
                                                        <asp:TextBox runat="server" ID="TextBox3" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>สถานะการตรวจสภาพรถ</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList11" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>วันที่ตรวจภาพ NGV</label>
                                                        <asp:TextBox runat="server" ID="TextBox4" SkinID="null" CssClass="datepicker" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>สถานะการตรวจสภาพรถ NGV</label>
                                                        <asp:DropDownList runat="server" ID="DropDownList1" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
            </div>
            <div class="modal-footer">
                <asp:Button runat="server" ID="cmdModalSave" SkinID="null" class="btn btn-success" ClientIDMode="Static" Width="80px" CommandArgument='<%# CommandArgument %>' OnClientClick="$($(this).closest('.modal').get(0)).find('button').click()" Text="ตกลง" CssClass="btn btn-md bth-hover btn-success" />
                <asp:Button ID="cmdModalClose" runat="server" SkinID="null" class="btn btn-danger" data-dismiss="modal" Width="80px" aria-hidden="true" Text="ยกเลิก" UseSubmitBehavior="false" />
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hidUrl" runat="server" />
<asp:HiddenField ID="hidIDModel" runat="server" />
<!-- #endregion -->