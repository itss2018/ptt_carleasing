﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorModal.ascx.cs" Inherits="PCLS.WebPC.UserControl.ErrorModal" %>

<!-- #region + Modal Information + -->
<div class="modal fade" style="z-index: 1060" id='<%= IDModel %>' role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <asp:Image ID="imgPopup" runat="server" ImageUrl="~/Images/imgTitleError.png" Width="48" Height="48" />
                    <asp:Label ID="lblModalTitle" runat="server" CssClass="TextTitle" style="font-family:sans-serif">Transaction Error</asp:Label>
                </h4>
            </div>
            <div class="modal-body">
                <span id="TextDetail" class="TextDetail" style="font-family:sans-serif"><%= TextDetail %></span>
            </div>
            <div class="modal-footer">
                <asp:Button runat="server" ID="cmdModalSave" ClientIDMode="Static" Width="80px" CommandArgument='<%# CommandArgument %>' OnClientClick="$($(this).closest('.modal').get(0)).find('button').click()" Text="" CssClass="btn btn-md bth-hover btn-success" />
                <asp:Button ID="cmdModalClose" runat="server" SkinID="null" class="btn btn-danger" data-dismiss="modal" Width="80px" aria-hidden="true" Text="ปิด" UseSubmitBehavior="false" />
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hidUrl" runat="server" />
<asp:HiddenField ID="hidIDModel" runat="server" />
<!-- #endregion -->