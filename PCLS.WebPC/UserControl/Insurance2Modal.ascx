﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Insurance2Modal.ascx.cs" Inherits="PCLS.WebPC.UserControl.Insurance2Modal" %>

<!-- #region + Modal Information + -->
<div class="modal fade" style="z-index: 1060" id='<%= IDModel %>' role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <asp:Image ID="imgPopup" runat="server" ImageUrl="~/Images/imgTitleConfirm.png" Width="48" Height="48" />
                    <asp:Label ID="lblModalTitle" runat="server" CssClass="TextTitle" style="font-family:sans-serif">ข้อมูลประกันภัยภาคสมัครใจ</asp:Label>
                </h4>
            </div>
            <div class="modal-body">
                <span id="TextDetail" class="TextDetail" style="font-family:sans-serif"><%= TextDetail %></span>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>กรมธรรม์เลขที่</label>
                                                    <asp:TextBox runat="server" ID="TextBox31" CssClass="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>วันที่เริ่มต้น</label>
                                                    <asp:TextBox runat="server" ID="TextBox32" SkinID="null" CssClass="datepicker" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>วันที่สิ้นสุด</label>
                                                    <asp:TextBox runat="server" ID="TextBox33" SkinID="null" CssClass="datepicker" />
                                                </div>
                                            </div> 
                                        </div>
                  
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>บริษัทประกันภัย</label>
                                                    <asp:DropDownList runat="server" ID="DropDownList20" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>                                         
                                        </div>

                                        <div class="row">
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ทุนประกันภัย</label>
                                                    <asp:TextBox runat="server" ID="TextBox1" CssClass="form-control" />
                                                </div>
                                            </div>
                                          
                                        </div>

                                        <div class="row">
                                               <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label class="col-md-12">ประเภทไฟล์เอกสาร</label>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="DropDownList32"  runat="server" class="form-control" DataTextField="UPLOAD_NAME" DataValueField="UPLOAD_ID"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <asp:FileUpload ID="fileUpload8" CssClass="form-control"   type="file" runat="server" Width="100%" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <span class='glyphicon glyphicon-plus'></span>
                                                                    <asp:Button ID="Button11" runat="server" Text="เพิ่มไฟล์" CssClass="btn btn-success" UseSubmitBehavior="false" />
                                                                </div>
                                                            </div>
                                                     </div>
                                               </div>                                               
                                        </div>
            </div>
            <div class="modal-footer">
                <asp:Button runat="server" ID="cmdModalSave" SkinID="null" class="btn btn-success" ClientIDMode="Static" Width="80px" CommandArgument='<%# CommandArgument %>' OnClientClick="$($(this).closest('.modal').get(0)).find('button').click()" Text="ตกลง" CssClass="btn btn-md bth-hover btn-success" />
                <asp:Button ID="cmdModalClose" runat="server" SkinID="null" class="btn btn-danger" data-dismiss="modal" Width="80px" aria-hidden="true" Text="ยกเลิก" UseSubmitBehavior="false" />
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hidUrl" runat="server" />
<asp:HiddenField ID="hidIDModel" runat="server" />
<!-- #endregion -->