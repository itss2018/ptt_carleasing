﻿using System;
using System.Configuration;
using System.Data;
using System.Security.Authentication;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Newtonsoft.Json;
using PCLS.WebPC.Helper;
using PCLS.Bussiness.SystemData;
using PCLS.Bussiness.Authentication;


/// <summary>
/// Summary description for WebServiceTMS
/// </summary>
[WebService(Namespace = "https://ptttms.pttplc.com/TMSWebService/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class wsMobile : System.Web.Services.WebService
{
    //public TMSAuthentication AuthenticationInfo;

    public wsMobile()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region + Check Authen Web Service +
    //private bool AuthenticationWebService(TMSAuthentication authenticationInfo)
    //{
    //    if (authenticationInfo == null)
    //        throw new AuthenticationException("Please provide a Username and Password.");

    //    if (string.IsNullOrEmpty(authenticationInfo.Username) || string.IsNullOrEmpty(authenticationInfo.Password))
    //        throw new AuthenticationException("Please provide a Username and Password.");

    //    // Are the credentials valid?
    //    if (!authenticationInfo.IsUserValid())
    //        throw new AuthenticationException("Invalid Username or Password.");

    //    return true;
    //}
    #endregion

    int MaximumRow = 100;
    string MaximimRowMessage = "Result data over {0} rows";

    #region + Check Login +
    [SoapHeader("AuthenticationInfo")]
    [WebMethod(Description = "Check Login")]
    public string CheckLogin(string Domain, string UserName, string Password, string isUserLocal)
    {
        try
        {
            //if (AuthenticationWebService(AuthenticationInfo))
            //{
                try
                {
                    DataSet dsUser = new DataSet();
                    DataTable dtUser = new DataTable();
                    DataTable dtTemp = new DataTable();

                    if (string.Equals(isUserLocal, "0"))
                    {//Domain
                        if (DomainHelper.CheckDomain(Domain + UserName, Password))
                            dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + UserName + "'" + " AND IS_ACTIVE = 1");
                    }
                    else
                    {//Legacy
                        dtTemp = UserBLL.Instance.UserSelect2BLL(UserName, MD5Helper.GetMD5Hash(Password));

                        if (dtTemp.Rows.Count > 0)
                        {
                            if (string.Equals(dtTemp.Rows[0]["FLAG"].ToString(), "N"))
                                throw new Exception(dtTemp.Rows[0]["MESSAGE"].ToString());

                            dtUser = UserBLL.Instance.UserSelectBLL(" AND USERNAME = '" + UserName + "'" + " AND PASSWORD = '" + MD5Helper.GetMD5Hash(Password) + "'" + " AND IS_ACTIVE = 1");
                        }
                    }

                    if (dtUser.Rows.Count > 0)
                        LoginBLL.Instance.ClearLoginBLL(UserName);
                    else
                        throw new Exception("Not Authorize...");

                    dsUser.DataSetName = "dsUser";
                    dtUser.TableName = "dtUser";
                    dsUser.Tables.Add(dtUser.Copy());
                    return JsonConvert.SerializeObject(dsUser, Formatting.Indented);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            //}
            //return "Authentication error occured!!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }
    #endregion
}

//public class TMSAuthentication : SoapHeader
//{
//    private string user = ConfigurationManager.AppSettings["WebServiceUser"];
//    private string key = ConfigurationManager.AppSettings["WebServiceKey"];

//    public string Username;
//    public string Password;
//    public string AuthenticatedToken;

//    public bool IsUserValid()
//    {
//        if (Username == user && Password == key)
//        {
//            // Create and store the AuthenticatedToken before returning it
//            AuthenticatedToken = Guid.NewGuid().ToString();
//            HttpRuntime.Cache.Add(
//                AuthenticatedToken,
//                Username,
//                null,
//                System.Web.Caching.Cache.NoAbsoluteExpiration,
//                TimeSpan.FromMinutes(60),
//                System.Web.Caching.CacheItemPriority.NotRemovable,
//                null);

//            return true;
//        }
//        else
//        {
//            throw new AuthenticationException("Please provide a Username and Password.");
//        }
//    }
//}